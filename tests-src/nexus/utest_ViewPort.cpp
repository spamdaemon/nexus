#include <nexus/ViewPort.h>
#include <iostream>

using namespace ::std;
using namespace ::timber;
using namespace ::nexus;

struct VPort : public ViewPort
{
      VPort()throws() {}
      ~VPort()throws() {}

      /**
       * Override this method
       * @param newBounds
       * @param newView
       */
   public:
      void setViewPort(const ::indigo::view::ViewPort::Bounds& newBounds,
            const ::indigo::view::ViewPort::Window& newView)throws ( ::std::invalid_argument)
      {
         ViewPort::setViewPort(newBounds,newView);
   }
};

// this test replicates a problem cause by the fact that
// nexus uses screen coordinates for the pixels, but indigo
// was not tested with such a coordinate frame
void utest_MovePoint2Pixel_1()
{
   VPort vp;
   vp.setViewPort(::indigo::view::Bounds(-9783.44, -9783.44, 19567.9, 19567.9), ViewPort::Window(800, 800));
   ViewPort::Pixel ctrPix = vp.centerPixel();
   ViewPort::Point ctr = vp.centerPoint();
   ::std::cerr << "ViewPort : " << vp << ::std::endl;
   ::std::cerr << " CenterPoint : " << ctr << ::std::endl;
   ::std::cerr << " CenterPixel : " << ctrPix << ::std::endl;

   vp.movePointToPixel(ctr, ctrPix);
   ::std::cerr << "ViewPort : " << vp << ::std::endl;
   ::std::cerr << " CenterPoint : " << vp.centerPoint() << ::std::endl;
   ::std::cerr << " CenterPixel : " << vp.centerPixel() << ::std::endl;

   assert(vp.centerPoint().equals(ctr,1E-8));
}

void utest_MovePoint2Pixel_2()
{
   VPort vp;
   vp.setViewPort(::indigo::view::Bounds(-7.82715e+06,-7.82715e+06,1.56543e+07,1.56543e+07 ), ViewPort::Window(800, 800));
   ViewPort::Point ctr = vp.centerPoint();
   ::std::cerr << "ViewPort : " << vp << ::std::endl;
   ::std::cerr << " CenterPoint : " << ctr << ::std::endl;
   ::std::cerr << " CenterPixel : " << vp.centerPixel() << ::std::endl;

   vp.setViewPort(vp.bounds(), ViewPort::Window(800, 763));
   ::std::cerr << "ViewPort : " << vp << ::std::endl;
   ::std::cerr << " CenterPoint : " << vp.centerPoint() << ::std::endl;
   ::std::cerr << " CenterPixel : " << vp.centerPixel() << ::std::endl;

   vp.movePointToPixel(ctr, vp.centerPixel());
   ::std::cerr << "ViewPort : " << vp << ::std::endl;
   ::std::cerr << " CenterPoint : " << vp.centerPoint() << ::std::endl;
   ::std::cerr << " CenterPixel : " << vp.centerPixel() << ::std::endl;

   assert(vp.centerPoint().equals(ctr,1E-8));
}



