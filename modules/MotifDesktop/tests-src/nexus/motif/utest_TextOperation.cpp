#include <iostream>
#include <nexus/motif/TextOperation.h>

using namespace ::std;
using namespace ::nexus::motif;

static string apply(const TextOperation& x)
{
   string y = x.apply();
   ::std::cerr << "RES : " << y << ::std::endl;
   return y;
}

void test1()
{
   TextOperation x("0123456789");
   x.replace(4, 5, "45678");
   assert(apply(x) == string("0123456789"));
   x.replace(0, 2, "01");
   assert(apply(x) == string("0123456789"));
   x.replace(5, 5, "56789");
   assert(apply(x) == string("0123456789"));
   x.replace(4, 5, "abcd");
   assert(apply(x) == string("0123abcd9"));

   x = TextOperation("0123456789");
   x.replace(4, 3, "456");
   assert(apply(x) == string("0123456789"));
   x.replace(0, 10, "0123456789");

   assert(apply(x) == string("0123456789"));

   x = TextOperation("0123456789");
   x.replace(2, 4, "2345");
   assert(apply(x) == string("0123456789"));
   x.replace(4, 4, "4567");
   assert(apply(x) == string("0123456789"));
   x.replace(8, 2, "89");
   assert(apply(x) == string("0123456789"));

   x = TextOperation("0123456789");
   x.replace(5, 4, "5678");
   assert(apply(x) == string("0123456789"));
   x.replace(3, 4, "3456");
   assert(apply(x) == string("0123456789"));

   x = TextOperation("0123456789");
   x.replace(3, 4, "3456");
   assert(apply(x) == string("0123456789"));
   x.replace(5, 3, "567");
   assert(apply(x) == string("0123456789"));
}

int main()
{
   test1();
   return 0;
}
