#include <iostream>
#include <nexus/motif/Desktop.h>

using namespace ::std;
using namespace ::timber;
using namespace ::nexus::motif;

void test1()
{
}

int main(int argc, char** argv)
{

   Pointer< Desktop> desktop;
   int n = 1;
   const char* xargs[] = { "FOO", nullptr };

   try {
      desktop = Desktop::create(n, xargs);
   }
   catch (...) {
      cerr << "Failed to open deskop; cannot execute test" << endl;
      return 0;
   }

   try {
      test1();
   }
   catch (exception& e) {
      cerr << "Exception caught : " << e.what() << ::std::endl;
      return 1;
   }
   catch (...) {
      cerr << "Unknown exception caught" << ::std::endl;
      return 1;
   }
   return 0;
}
