#include <nexus/motif/PushButton.h>
#include <Xm/PushB.h>

#include <timber/logging.h>

using namespace ::timber::logging;

namespace nexus {
   namespace motif {
      PushButton::PushButton(Component& c) throws()
            : Component(c, "button", xmPushButtonWidgetClass), _graphic(RootGraphic::create(*this)), _observer(0)
      {
         ::std::string txt;
         setValue(XmNlabelString, txt);
         setValue(XmNrecomputeSize, False);
         setValue(XmNalignment, static_cast< Int>(XmALIGNMENT_CENTER));
         setValue(XmNborderWidth, 0);
         setValue(XmNmarginWidth, 0);
         setValue(XmNmarginHeight, 0);
         setValue(XmNmarginLeft, 0);
         setValue(XmNmarginRight, 0);
         setValue(XmNmarginTop, 0);
         setValue(XmNmarginBottom, 0);
         setValue(XmNdefaultButtonShadowThickness, 0);
         setValue(XmNhighlightOnEnter, False);
         setValue(XmNhighlightThickness, 0);

         ::XtAddCallback(widget(), XmNactivateCallback, pushButtonCB, this);
         ::XtAddCallback(widget(), XmNarmCallback, pushButtonCB, this);
         ::XtAddCallback(widget(), XmNdisarmCallback, pushButtonCB, this);
      }

      PushButton::~PushButton() throws()
      {
         ::XtRemoveCallback(widget(), XmNactivateCallback, pushButtonCB, this);
         ::XtRemoveCallback(widget(), XmNarmCallback, pushButtonCB, this);
         ::XtRemoveCallback(widget(), XmNdisarmCallback, pushButtonCB, this);
      }

      bool PushButton::hasForeground() const throws()
      {
         return true;
      }

      void PushButton::setFont(const ::timber::Pointer< ::indigo::Font>& f, bool inherited) throws()
      {
         Component::setFont(f, inherited);
         if (_renderer == nullptr && font() != nullptr) {
            setValue(XmNrenderTable, font()->font());
         }
      }

      void PushButton::setBorderWidth(Int w) throws()
      {
         setValue(XmNshadowThickness, w);
      }

      void PushButton::setForegroundColor(const Color& c, bool inherited) throws()
      {
         Component::setForegroundColor(c, inherited);
         if (_renderer != nullptr) {
            _renderer->setDefaultColors(backgroundColor().color(), foregroundColor().color());
            _graphic->scheduleRefresh();
         }
      }

      void PushButton::setBackgroundColor(const Color& c, bool inherited) throws()
      {
         Component::setBackgroundColor(c, inherited);
         if (_renderer != nullptr) {
            _renderer->setDefaultColors(backgroundColor().color(), foregroundColor().color());
            _graphic->scheduleRefresh();
         }
      }

      void PushButton::setText(const ::std::string& t) throws()
      {
         _graphic->setRootNode((::indigo::Node*) 0);
         _graphic->cancelRefreshRequest();

         if (_renderer != nullptr) {
            _renderer = nullptr;
            setValue(XmNlabelType, static_cast< Int>(XmSTRING));

            // set the font
            if (font() != nullptr) {
               setValue(XmNrenderTable, font()->font());
            }
         }
         setValue(XmNlabelString, t);
      }

      void PushButton::setIcon(const Icon& icon) throws()
      {
         if (_renderer == nullptr) {
            Colormap colors;
            ::XtVaGetValues(widget(), XmNcolormap, &colors, (XtPointer) 0);
            try {
               _renderer = ::indigo::x11::PixmapRenderer::create(screen(), icon.width(), icon.height(), 0, colors,
                     backgroundColor().color(), foregroundColor().color());
            }
            catch (...) {
            }
         }

         if (_renderer != nullptr && _renderer->resize(icon.width(), icon.height())) {
            _renderer->setGraphic(icon.graphic());
            setValue(XmNlabelType, static_cast< Int>(XmPIXMAP));
            _graphic->setRootNode(icon.graphic());
         }
         else {
            Log("nexus.motif.PushButton").debugging("Cannot create pixmap for icon; not rendering anything");
            _renderer = nullptr;
            setValue(XmNlabelType, static_cast< Int>(XmSTRING));
            setValue(XmNlabelString, blank());
         }
      }

      void PushButton::graphicExposed(bool) throws()
      {
         if (_renderer != nullptr) {
            _renderer->refresh();
            setValue(XmNlabelPixmap, _renderer->pixmap());
         }
      }

      void PushButton::pushButtonCB(Widget, XtPointer client_data, XtPointer data) throws()
      {
         PushButton* btn = static_cast< PushButton*>(client_data);
         if (btn->_observer != 0) {
            XmPushButtonCallbackStruct* cbs = static_cast< XmPushButtonCallbackStruct*>(data);

            switch (cbs->reason) {
               case XmCR_ACTIVATE:
                  btn->_observer->pushButtonActivated();
                  break;
               case XmCR_ARM:
                  btn->_observer->pushButtonPressed();
                  break;
               case XmCR_DISARM:
                  btn->_observer->pushButtonReleased();
                  break;
               default:
                  break;
            }
         }
      }

      void PushButton::setPushButtonObserver(::nexus::peer::PushButton::Observer* l) throws()
      {
         _observer = l;
      }

   }
}
