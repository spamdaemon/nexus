#include <nexus/motif/Desktop.h>
#include <nexus/motif/Canvas.h>
#include <nexus/motif/Container.h>
#include <nexus/motif/Label.h>
#include <nexus/motif/Menu.h>
#include <nexus/motif/MenuBar.h>
#include <nexus/motif/MenuItem.h>
#include <nexus/motif/PushButton.h>
#include <nexus/motif/TextArea.h>
#include <nexus/motif/TextField.h>
#include <nexus/motif/Window.h>
#include <nexus/motif/X11KeySym.h>

#include <nexus/InputModifiers.h>
#include <nexus/Time.h>
#include <nexus/Pixel.h>

#include <canopy/mt/Mutex.h>
#include <canopy/mt/MutexGuard.h>
#include <canopy/mt.h>

#include <timber/logging.h>
#include <timber/scheduler/ThreadPoolScheduler.h>

#include <cstring>
#include <cstdio>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <iostream>

using namespace ::canopy::mt;
using namespace ::timber::logging;

namespace nexus {
   namespace motif {
      namespace {

         static const char* DEFAULT_FIXED_FONT = "-*-courier-medium-r-*-*-*-140-*-*-*-*-*-*";
         static const char* DEFAULT_PROPORTIONAL_FONT = "-*-helvetica-medium-r-*-*-*-140-*-*-*-*-*-*";

         static Log log()
         {
            return Log("nexus.motif.Desktop");
         }

         /**
          * Ensure that a file descriptor is closed on exec.
          * @param fd a file descriptor
          * @return true if the operation was sucessful, false otherwise
          */
         static bool setCloseOnExec(int fd)
         throws()
         {
            int flags = ::fcntl(fd, F_GETFD);
            if (flags == -1) {
               return false;
            }
            flags |= FD_CLOEXEC;
            return ::fcntl(fd, F_SETFD, flags) == 0;
         }

         static void runMainLoop(::timber::Reference< Desktop> dt)
         {
            dt->processEvents();
         }

         struct DoNothing : public Runnable
         {
               DoNothing(::canopy::mt::Condition& cc)
                     : _condition(cc)
               {
               }

               ~DoNothing()throws()
               {
               }

            public:
               void run()throws()
               {
                  ::canopy::mt::MutexGuard< ::canopy::mt::Condition> G(_condition);
                  _condition.notifyAll();
               }

               ::canopy::mt::Condition& _condition;
         };

         static void notify(Int fd)
         throws ()
         {
            // notify the pipe
            static char buf[1] = { '#' };
            do {
               int count = ::write(fd, buf, sizeof(buf));
               if (count == 1) {
                  break;
               }
               else if (count == -1) {
                  if (errno != EAGAIN) {
                     log().severe("Could not notify pipe; application will be aborted and dump core");
                     ::std::abort();
                  }
               }
               else if (count == 0) {
                  log().warn("Nothing written to pipe; strange");
               }
            } while (true);
         }
      }

      Desktop::Desktop(Int& argc, const char** argv) throws (::std::exception)
            : _shutdown(false), _queue(::std::make_shared<::timber::mt::TransferQueue< ::timber::SharedRef< Runnable> >>()), _keyInterceptor(
                  nullptr), _scheduler(::timber::scheduler::ThreadPoolScheduler::create(1))
      {
         // open a pipe
         if (::pipe2(_fds, O_CLOEXEC) != 0) {
            throw ::std::runtime_error("Could not allocate required system resources");
         }

         _context = ::XtCreateApplicationContext();
         _display = ::XtOpenDisplay(_context, 0, 0, "Desktop", 0, 0, &argc, const_cast< char**>(argv));
         if (_display == 0) {
            ::close(_fds[0]);
            ::close(_fds[1]);
            ::XtDestroyApplicationContext(_context);
            throw ::std::runtime_error("Could not initialize the desktop");
         }

         _screen = DefaultScreenOfDisplay(_display);
         _xtInput = ::XtAppAddInput(_context, _fds[0], (XtPointer)(XtInputReadMask), executeRunnable, this);

         _fontWidget = ::XtAppCreateShell("x", "MyShell2", wmShellWidgetClass, display(), 0, 0);

         _mouseWheelEnabled = true;

      }

      Desktop::~Desktop() throws()
      {

         log().debugging("Destroying desktop");
         ::close(_fds[0]);
         ::close(_fds[1]);

         _scheduler->shutdown();

         if (_eventThread.get()) {
            ::canopy::mt::Thread::join(*_eventThread);
         }

         // destroy the font widget

         ::XtDestroyWidget(_fontWidget);
         ::XtRemoveInput(_xtInput);

         // this will probably never get called
         ::XtCloseDisplay(_display);

         ::XtDestroyApplicationContext(_context);
         log().debugging("Desktop destroyed");
      }

      ::timber::Reference< Desktop> Desktop::create(Int& argc, const char** argv) throws (::std::exception)
      {
         ::XtProcessLock();
         ::XtToolkitInitialize(); // is this threadsafe?
         bool initFailed = ::XtToolkitThreadInitialize() == FALSE;
         ::XtProcessUnlock();

         if (initFailed) {
            log().severe("X11 is not threadsafe");
            throw ::std::runtime_error("X11 is not threadsafe");
         }

         // check if we can open the X display
         const char* dpname = nullptr;
         for (Index i = 1; i < argc; ++i) {
            if (::std::strcmp("-display", argv[i]) == 0) {
               if (i + 1 == argc) {
                  throw ::std::runtime_error("Missing display name");
               }
               dpname = argv[i + 1];
               break;
            }
         }

         ::timber::Reference< Desktop> d(new Desktop(argc, argv));

         // start the mainloop thread at this time
         try {
            d->_eventThread.reset(
                  new ::canopy::mt::Thread(::canopy::createThread("UI-EventProcessor", runMainLoop, d)));
         }
         catch (...) {
            throw ::std::runtime_error("Could not start event thread");
         }

         // wait until the thread has actually started
         {
            ::canopy::mt::Condition condition;
            ::canopy::mt::MutexGuard< ::canopy::mt::Condition> G(condition);
            auto runner=::std::make_shared<DoNothing>(condition);
            if (d->enqueue(runner)) {
               condition.wait();
            }
         }

         return d;
      }

      bool Desktop::isEventThread() const throws()
      {
         return _eventThread.get() != nullptr && ::canopy::mt::Thread() == *_eventThread;
      }

      bool Desktop::enqueue(const ::timber::SharedRef< Runnable>& r) throws()
      {
         if (_shutdown) {
            log().debugging("Desktop::enqueue: already shutdown");
            return false;
         }
         log().debugging("Enqueueing runnable");
         _queue->enqueue(r);
         notify(_fds[1]);
         return true;
      }

      bool Desktop::enqueue(const ::timber::SharedRef< Runnable>& r, const Delay& delayMS,
            const Period& periodMS) throws()
      {
         if (delayMS == Delay::zero() && periodMS == Period::zero()) {
            return enqueue(r);
         }

         struct _ : public Runnable
         {
               _(Desktop& dt, const ::timber::SharedRef< Runnable>& xr) throws()
                     : _dt(dt), _delegate(xr)
               {
               }
               ~_() throws()
               {
               }
               void run()
               {
                  _dt.enqueue(_delegate);
               }
            private:
               Desktop& _dt;
               const ::timber::SharedRef< Runnable> _delegate;
         };
         auto rr = ::std::make_shared<_>(*this, r);
         return _scheduler->execute(rr, delayMS, periodMS);
      }

      void Desktop::shutdown() throws()
      {
         assert(isEventThread());

         // make sure the scheduler is shutdown first, because the tasks that are scheduled
         // are holding references to this object
         _scheduler->shutdown();

         if (!_shutdown) {
            ::XSync(display(), False);
            ::XtAppSetExitFlag(_context);
            _shutdown = true;
            log().debugging("Initiating desktop shutdown");
         }
      }

      void Desktop::waitShutdown() const throws()
      {
         assert(!isEventThread());
         try {
            if (_eventThread.get()) {
               ::canopy::mt::Thread::join(*_eventThread);
            }
         }
         catch (...) {
            log().severe("Error joining the event thread");
         }
      }

      Size Desktop::size() const throws()
      {
         return Size(WidthOfScreen(screen()), HeightOfScreen(screen()));
      }

      ::nexus::peer::Window* Desktop::createWindow() throws()
      {
         return new Window(*this);
      }

      ::nexus::peer::Container* Desktop::createContainer(::nexus::peer::Component& parent) throws()
      {
         return new Container(dynamic_cast< Component&>(parent));
      }

      ::nexus::peer::Canvas* Desktop::createCanvas2D(::nexus::peer::Component& parent) throws()
      {
         return new Canvas(dynamic_cast< Component&>(parent));
      }

      ::nexus::peer::Label* Desktop::createLabel(::nexus::peer::Component& parent) throws()
      {
         return new Label(dynamic_cast< Component&>(parent));
      }

      ::nexus::peer::Menu* Desktop::createMenu(::nexus::peer::Component& parent) throws()
      {
         Menu* pMenu = dynamic_cast< Menu*>(&parent);
         if (pMenu) {
            return new Menu(pMenu);
         }
         MenuBar* pMenuBar = dynamic_cast< MenuBar*>(&parent);
         if (pMenuBar) {
            return new Menu(*pMenuBar);
         }
         return nullptr;
      }

      ::nexus::peer::MenuBar* Desktop::createMenuBar(::nexus::peer::Window& parent) throws()
      {
         return new MenuBar(dynamic_cast< Window&>(parent));
      }

      ::nexus::peer::MenuItem* Desktop::createMenuItem(::nexus::peer::Menu& parent) throws()
      {
         return new MenuItem(dynamic_cast< Menu&>(parent));
      }

      ::nexus::peer::PushButton* Desktop::createPushButton(::nexus::peer::Component& parent) throws()
      {
         return new PushButton(dynamic_cast< Component&>(parent));
      }

      ::nexus::peer::TextField* Desktop::createTextField(::nexus::peer::Component& parent) throws()
      {
         return new TextField(dynamic_cast< Component&>(parent));
      }

      ::nexus::peer::TextArea* Desktop::createTextArea(::nexus::peer::Component& parent) throws()
      {
         return new TextArea(dynamic_cast< Component&>(parent));
      }

      ::timber::Reference< ::indigo::FontFactory> Desktop::fonts() const throws()
      {
         struct FFactory : public ::indigo::FontFactory
         {
               FFactory(Widget ftw) throws()
                     : _widget(ftw)
               {
               }
               ~FFactory() throws()
               {
               }
               ::timber::Pointer< ::indigo::Font> queryDefaultFont() const throws()
               {
                  return MotifFont::create(toRef< ::indigo::FontFactory>(), _widget, DEFAULT_FIXED_FONT);
               }
               ::timber::Pointer< ::indigo::Font> queryFont(const ::indigo::FontDescription& d) const throws()
               {
                  static bool emitMessage = true;
                  if (emitMessage) {
                     emitMessage = false;
                     Log("nexus.motif.Desktop").warn(
                           "queryFont not yet implemented; returning a default proportional font");
                  }
                  return MotifFont::create(toRef< ::indigo::FontFactory>(), _widget, DEFAULT_PROPORTIONAL_FONT);
               }
            private:
               Widget _widget;
         };

         if (_fonts == nullptr) {
            ::indigo::FontFactory* ff = new FFactory(_fontWidget);
            _fonts = ff;
         }
         return _fonts;
      }

      InputModifiers Desktop::queryInputModifiers() const throws()
      {
         return InputModifiers(
               InputModifiers::BUTTON1 | InputModifiers::BUTTON2 | InputModifiers::BUTTON3 | InputModifiers::CAPS_LOCK
                     | InputModifiers::CONTROL | InputModifiers::SHIFT | InputModifiers::META1);
      }

      void Desktop::setMouseWheelEnabled(bool enable) throws()
      {
         _mouseWheelEnabled = enable;
      }

      Pixel Desktop::pointerLocation() const throws()
      {
         ::Window child_return, root_return;
         Int root_x_return, root_y_return;
         Int win_x_return, win_y_return;
         UInt mask_return;

         if (::XQueryPointer(display(), RootWindowOfScreen(screen()), &root_return, &child_return, &root_x_return,
               &root_y_return, &win_x_return, &win_y_return, &mask_return)) {
            return Pixel(root_x_return, root_y_return);
         }
         log().severe("An error has occurred querying the pointer location");
         return Pixel(0, 0);
      }

      void Desktop::executeRunnable(void* desktop, int* fd, XtInputId* inputid) throws()
      {
         Desktop* dt = static_cast< Desktop*>(desktop);

         assert(*fd == dt->_fds[0]);
         assert(*inputid == dt->_xtInput);

         char buf[1];
         if (::read(*fd, buf, 1) == 0) {
            log().severe("Internal error : nothing read on pipe");
            ::std::abort();
         }
         if (dt->_queue->isEmpty()) {
            return;
         }

         try {
            auto runnable = dt->_queue->dequeue();
            runnable->run();
         }
         catch (const ::std::exception& e) {
            log().caught("Exception in Runnable", e);
         }
         catch (...) {
            log().debugging("Exception in Runnable");
         }
      }

      void Desktop::processEvents() throws()
      {
         log().debugging("Running UI mainloop");
         XtAppContext app = _context;
         // lock the context
         XtAppLock(app); // this can cause a deadlock
         {
            while (true) {
               XtInputMask mask = XtAppPending(app);
               if (mask == 0) {
                  if (::XtAppGetExitFlag(app) == TRUE) {
                     ::XSync(display(), False);
                     mask = ::XtAppPending(app);
                     if (mask == 0) {
                        break;
                     }
                  }
                  // nothing to do, so wait
                  mask = XtIMAll;
               }

               // wait or process a pending event
               XtAppProcessEvent(app, mask);
            }
            ::XSync(display(), True);
         }
         XtAppUnlock(app);
         log().debugging("Finished UI mainloop");
      }

      void Desktop::setKeyInterceptor(KeyInterceptor* interceptor) throws()
      {
         _keyInterceptor = interceptor;
      }

      bool Desktop::interceptKeyPressed(const ::timber::Reference< ::nexus::peer::KeySymbol>& ks, const Time& when,
            const InputModifiers& m) throws()
      {
         return _keyInterceptor && _keyInterceptor->keyPressed(ks, when, m);
      }

      bool Desktop::interceptKeyTyped(const ::timber::Reference< ::nexus::peer::KeySymbol>& ks, const Time& when,
            const InputModifiers& m) throws()
      {
         return _keyInterceptor && _keyInterceptor->keyTyped(ks, when, m);
      }

      bool Desktop::interceptKeyReleased(const ::timber::Reference< ::nexus::peer::KeySymbol>& ks, const Time& when,
            const InputModifiers& m) throws()
      {
         return _keyInterceptor && _keyInterceptor->keyReleased(ks, when, m);
      }

   }
}
