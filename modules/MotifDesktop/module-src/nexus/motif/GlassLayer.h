#ifndef _NEXUS_MOTIF_GLASSLAYER_H
#define _NEXUS_MOTIF_GLASSLAYER_H

#ifndef _NEXUS_H
#include <nexus/nexus.h>
#endif

#ifndef _Xm_h
#include <Xm/Xm.h>
#endif

#ifndef _XtintrinsicP_h
#include <X11/IntrinsicP.h>
#endif

namespace nexus {
  namespace motif {

    /**
     * This class installs an Input-Only window in front of a widget.
     * The GlassLayer will always be the same size as the widget itself.
     *
     * @author Raimund Merkert
     * @date 18 Feb 2003
     */
    class GlassLayer {
      GlassLayer&operator=(const GlassLayer&);
      GlassLayer(const GlassLayer&);

      /**
       * Create a new glasslayer as a child of the specified widget.
       * @param w a widget to which the glass layer is attached
       */
    public:
      GlassLayer (Widget w) throws();
	
      /** Destroy this glasslayer and release all its resources */
    public:
      ~GlassLayer () throws();

      /** Show the actual XWindow for the glasslayer. */
    private:
      void showGlassLayer() throws();

      /** Hide the glass layer (if there is one) */
    private:
      void hideGlassLayer() throws();

      /** Process a widget event. */
    private:
      static void processXEvent( Widget , XtPointer , XEvent* , Boolean* ) throws();

      /** The widget on which this layer is installed */
    private:
      Widget _widget;

      /** The glass layer window */
    private:
      ::Window _glass;
    };
  }
}

#endif
