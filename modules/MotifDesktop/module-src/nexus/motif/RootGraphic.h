#ifndef _NEXUS_MOTIF_ROOTGRAPHIC_H
#define _NEXUS_MOTIF_ROOTGRAPHIC_H

#ifndef _NEXUS_MOTIF_COMPONENT_H
#include <nexus/motif/Component.h>
#endif

#ifndef _NEXUS_MOTIF_DESKTOP_H
#include <nexus/motif/Desktop.h>
#endif

#ifndef _NEXUS_MOTIF_UTIL_H
#include <nexus/motif/util.h>
#endif

#ifndef _INDIGO_NODE_H
#include <indigo/Node.h>
#endif

#ifndef _INDIGO_SCENEGRAPH_H
#include <indigo/SceneGraph.h>
#endif

#ifndef _INDIGO_GFXOBSERVER_H
#include <indigo/GfxObserver.h>
#endif

#ifndef _TIMBER_WEAKLYCOUNTABLE_H
#include <timber/WeaklyCountable.h>
#endif

namespace nexus {
   namespace motif {

      /**
       * This class provides the capability to monitor a scenegraph
       * and send expose events to the component. This class is
       * abstract and clients are required to implement the method
       * <code>graphicExposed()</code>. Expose events are only
       * processed if the a refresh was scheduled. Means that components
       * may still need to process expose events themselves! If they
       * do so they should call <code>cancelRefreshRequest</code> to avoid
       * consecutive refreshes.
       * @date 07 Jun 2003
       */
      class RootGraphic : public ::indigo::SceneGraph, protected ::indigo::GfxObserver
      {
            RootGraphic(const RootGraphic&);
            RootGraphic&operator=(const RootGraphic&);

            /** The default repaint delay in millis */
         public:
            static const ::std::uint32_t DEFAULT_REPAINT_DELAY_MS = 25;

            /**
             * Create a new RootGraphic.
             * @param owner component
             * @pre REQUIRE_NON_ZERO(owner)
             */
         private:
            RootGraphic(Component& owner)throws();

            /** Destroy this scenegraph */
         public:
            ~RootGraphic()throws();

            /**
             * Set the repaint delay for this graphic. If a graphic in the scene graph is changed,
             * then a repaint is scheduled to run with a delay of at most this many milliseconds.
             * @param maxRepaintDelayMS the repaint delay in MS
             */
         public:
            inline void setRepaintDelay(const Desktop::Delay& maxRepaintDelayMS)throws() {_repaintDelayMS=maxRepaintDelayMS;}

            /**
             * Get the repaint delay.
             * @return the repaint delay in milliseconds
             */
         public:
            inline Desktop::Delay repaintDelay() const throws() {return _repaintDelayMS;}

            /**
             * Create a root graphic for the specified component.
             * @param owner a component that has a method called <code>graphicExposed(bool)</code>.
             */
         public:
            template <class T>
            inline static ::std::unique_ptr< RootGraphic> create(T& owner)throws()
            {
               struct Impl : public RootGraphic {
                  Impl (T& t) throws() : RootGraphic(t),_owner(t) {}
                  ~Impl () throws() {}
                  void graphicExposed (bool requested) throws()
                  {
                     RootGraphic::graphicExposed(requested);
                     _owner.graphicExposed(requested);
                  }

                  private:
                  T& _owner;
               };
               Impl* impl = new Impl(owner);
               return ::std::unique_ptr<RootGraphic>(impl);
            }

            /** Schedule a refresh. */
         public:
            void scheduleRefresh()throws();

            /**
             * Refresh immeditately.
             */
         public:
            inline void refresh()throws()
            {  graphicExposed(true);}

            /**
             * Create an observer for the the specified node.
             * @return an observer
             */
         protected:
            ::indigo::GfxObserver* createObserver(::indigo::Node&)throws()
            {  return this;}

            /**
             * Set the root node and schedule a refresh.
             * @param h a node
             */
         public:
            void setRootNode(::timber::Pointer< ::indigo::Node> h)throws();

            /**
             * The graphic has changed. Calls scheduleRefresh().
             */
         public:
            void gfxChanged(const ::indigo::GfxEvent&)throws();

            /**
             * The graphic has been exposed after a scheduled expose event. It is
             * is possible that an expose event was generated before a refresh
             * was scheduled. The flag gives information about this case.
             * Overriding methods must invoke this method to indicate that the
             * graphic has been refreshed.
             * @param requested true if the graphic was exposed due to a request
             */
         public:
            virtual void graphicExposed(bool requested)throws();

            /**
             * Cancel a previously scheduled refresh. Since
             * the refresh event has already been sent, it will
             * be delivered to the component. However, graphicExposed()
             * will not be invoked.
             */
         public:
            void cancelRefreshRequest()throws();

            /**
             * Enable or disable scheduling of refreshes.
             * @param enable if true, then enable automatic refreshes
             */
         public:
            void setAutoRefreshEnabled(bool enable)throws()
            {  _autoRefresh = enable;}

            /**
             * Test if auto refresh is enabled.
             * @return true if graphic is refreshed automatically
             */
         public:
            bool isAutoRefreshEnabled() const throws()
            {  return _autoRefresh;}

            /**
             * Test if a refresh is needed
             * @return true if a refresh is needed
             */
         public:
            inline bool needsRefresh() const throws()
            {  return _needRefresh;}

            /**
             * Process an exposure event.
             */
         private:
            static void processExposureEvent(Widget, XtPointer w, XEvent*, Boolean*)throws();

            /** The owner component */
         private:
            Component& _owner;

            /** True if refreshes should be scheduled */
         private:
            volatile bool _autoRefresh;

            /** True if a refresh has been scheduled */
         private:
            mutable bool _refreshScheduled;

            /** True if a refresh is needed */
         private:
            mutable bool _needRefresh;

            /** The repaint delay (millis) */
         private:
            Desktop::Delay _repaintDelayMS;

            /**
             * A reference to a weakly countable object; this pointer is used to indicate that this graphic is still alive.
             * This is necessary because repaints are scheduled asynchronously and this graphic might be destroyed between
             * before the scheduler has a chance to run the repaint request.
             */
         private:
            ::timber::Pointer< ::timber::WeaklyCountable> _livenessIndicator;
      };
   }
}
#endif
