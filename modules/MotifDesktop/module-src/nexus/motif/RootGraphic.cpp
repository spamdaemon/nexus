#include <nexus/motif/RootGraphic.h>
#include <nexus/motif/Desktop.h>
#include <timber/scheduler/Runnable.h>

namespace nexus {
   namespace motif {

      const ::std::uint32_t RootGraphic::DEFAULT_REPAINT_DELAY_MS;

      RootGraphic::RootGraphic(Component& owner)
      throws()
            : _owner(owner), _autoRefresh(true), _refreshScheduled(false), _needRefresh(false), _repaintDelayMS(
                  DEFAULT_REPAINT_DELAY_MS), _livenessIndicator(new ::timber::WeaklyCountable)
      {
      }

      RootGraphic::~RootGraphic()
      throws()
      {
         _livenessIndicator = nullptr;
         setRootNode(::timber::Pointer< ::indigo::Node>());
         ::XtRemoveEventHandler(_owner.widget(), ExposureMask, False, processExposureEvent, this);
      }

      void RootGraphic::setRootNode(::timber::Pointer< ::indigo::Node> h)
      throws()
      {
         if (rootNode() != h) {
            ::indigo::SceneGraph::setRootNode(h);
            _needRefresh = true;
            scheduleRefresh();
         }
      }

      void RootGraphic::scheduleRefresh()
      throws()
      {
         struct _ : public ::timber::scheduler::Runnable
         {
               _(RootGraphic& g) throws()
                     : _gfx(g)
               {
                  if (g._livenessIndicator) {
                     _livenessIndicator = g._livenessIndicator->weakPointer();
                  }
               }

               ~_() throws()
               {
               }

               void run()
               {
                  ::timber::Pointer< ::timber::WeaklyCountable> indicator = _livenessIndicator.get();
                  if (indicator) {
                     sendExposeEvent(_gfx._owner);
                     ::XtAddEventHandler(_gfx._owner.widget(), ExposureMask, False, processExposureEvent, &_gfx);
                  }
               }

            private:
               RootGraphic& _gfx;

               /** This weak pointer may get set to null if the root graphic is destroyed. */
            private:
               ::timber::WeakPointer< ::timber::WeaklyCountable> _livenessIndicator;
         };

         if (rootNode() != nullptr || _needRefresh) {
            if (!_refreshScheduled && _autoRefresh) {
               _refreshScheduled = true;
               auto r = ::std::make_shared<_>(*this);
               _owner.desktop().enqueue(r, repaintDelay(), Desktop::Period::zero());
            }
         }
      }

      void RootGraphic::gfxChanged(const ::indigo::GfxEvent&)
      throws()
      {
         _needRefresh = true;
         scheduleRefresh();
      }

      void RootGraphic::processExposureEvent(Widget, XtPointer w, XEvent* e, Boolean*)
      throws()
      {

         if (e->type == Expose && e->xexpose.count == 0) {
            RootGraphic* g = static_cast< RootGraphic*>(w);

            ::XtRemoveEventHandler(g->_owner.widget(), ExposureMask, False, processExposureEvent, g);
            if (g->_refreshScheduled) {
               if (g->_autoRefresh) {
                  g->graphicExposed(e->xany.send_event == True);
               }
               g->_refreshScheduled = false;
            }
         }
      }

      void RootGraphic::cancelRefreshRequest()
      throws()
      {
         ::XtRemoveEventHandler(_owner.widget(), ExposureMask, False, processExposureEvent, this);
         _refreshScheduled = false;
      }

      void RootGraphic::graphicExposed(bool)
      throws ()
      {
         _needRefresh = false;
      }

   }
}
