#ifndef _NEXUS_MOTIF_WINDOW_H
#define _NEXUS_MOTIF_WINDOW_H

#ifndef _NEXUS_PEER_WINDOW_H
#include <nexus/peer/Window.h>
#endif

#ifndef _NEXUS_MOTIF_COMPONENT_H
#include <nexus/motif/Component.h>
#endif

#ifndef _NEXUS_MOTIF_DESKTOP_H
#include <nexus/motif/Desktop.h>
#endif

namespace nexus {
  namespace motif {

    /**
     * This interface must be implemented by classes
     * that support composition.
     *
     * @author Raimund Merkert
     * @date 18 Feb 2003
     */
    class Window : public Component, public virtual ::nexus::peer::Window {
      Window(const Window&);
      Window&operator=(const Window&);

      /** The visibility state */
    private:
      enum VisibilityState { 
	OPENED,		//< the visibility state if the window has been opened
	CLOSED,		//< the visibility state if the window has been opened 
	TRANSITIONING	//< the visibility state if the window is being opened or closed
      };

      /** 
       * Constructor.
       * @param dt the desktop
       */
    public:
      Window (Desktop& dt) throws();

      /** Destroy this window and release all its resources */
    public:
      ~Window () throws();

      /**
       * @name Implementation of ::nexus::peer::Window
       * @{
       */

      /**
       * Set the bounds of this window. Also sets the 
       * bounds of the contained panel.
       * @param b bounds
       */
    public:
      void setBounds (const Bounds& b) throws();


      /**
       * Set the bounds for the specified child.
       * @param c a child
       * @param b the new bounds
       */
    public:
      void setChildBounds (::nexus::peer::Component& c, const Bounds& b) throws();

    public:
      void setVisible (bool visible) throws();
      void setTitle (const ::std::string& t) throws();
      void setMinimumSize (const Size& sz) throws();
      void setMaximumSize (const Size& sz) throws();
      void setWindowObserver (WindowObserver* l) throws();

      /*@}*/

      /**
       * @name Event listeners 
       * @{
       */

      /**
       * Fire a window event to the observer.
       */
    private:
      static void fireWindowEvent(Widget, XtPointer, XEvent*, Boolean*) throws();
	
     /**
       * Fire a window event to the observer.
       */
    private:
      static void fireContentEvent(Widget, XtPointer, XEvent*, Boolean*) throws();
	
      /**
       * Destroy the shell widget
       * All but the widget parameter are unused!
       */
    private:
      static void fireDestructor (Widget, XtPointer, XEvent*, Boolean*) throws();

      /** 
       * The callback invoked when the user attempts to close the window.
       */
    private:
      static void closeWindowCB (Widget,XtPointer,XtPointer) throws();

      /** 
       * The callback invoked when the user attempts to close the window.
       */
    private:
      static void windowDestroyedCB (Widget,XtPointer,XtPointer) throws();

      /*@}*/
      /** Get the shell widget */
    public:
      inline Widget window() const throws() { return XtParent(widget()); }

      /** Get the shell widget */
    public:
      inline Widget shell() const throws() { return XtParent(window()); }

      /** The window observer */
    private:
      WindowObserver*	_observer;

      /** The visibility of the window */
    private:
      VisibilityState _visibility;
	
      /** True if the window should visible or invisible */
    private:
      bool _visible;
    };
  }
}

#endif

