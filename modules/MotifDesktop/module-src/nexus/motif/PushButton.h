#ifndef _NEXUS_MOTIF_PUSHBUTTON_H
#define _NEXUS_MOTIF_PUSHBUTTON_H

#ifndef _NEXUS_ICON_H
#include <nexus/Icon.h>
#endif

#ifndef _NEXUS_PEER_PUSHBUTTON_H
#include <nexus/peer/PushButton.h>
#endif

#ifndef _NEXUS_MOTIF_COMPONENT_H
#include <nexus/motif/Component.h>
#endif

#ifndef _INDIGO_X11_PIXMAPRENDERER_H
#include <indigo/x11/PixmapRenderer.h>
#endif

#ifndef _NEXUS_MOTIF_ROOTGRAPHIC_H
#include <nexus/motif/RootGraphic.h>
#endif

namespace nexus {
  namespace motif {

    /**
     * This class is an implementation of a Button component
     * using the Motif toolkit.
     * @date 06 Jun 2003
     */
    class PushButton : public Component, public virtual ::nexus::peer::PushButton {
      PushButton(const PushButton&);
      PushButton&operator=(const PushButton&);

      /** 
       * Create a new button.
       * @param parent the parent component
       */
    public:
      PushButton (Component& parent) throws();

    public:	
      ~PushButton() throws();

    public:
      void setText (const ::std::string& t) throws();
      void setIcon (const Icon& i) throws();

      /**
       * Set the button observer.
       * @param l a listener or observer or 0 to clear it
       */
    public:
      void setPushButtonObserver (::nexus::peer::PushButton::Observer* l) throws();

      bool hasForeground () const throws();
      void setForegroundColor (const Color& c, bool inherited) throws();
      void setBackgroundColor (const Color& c, bool inherited) throws();
      void setBorderWidth (Int w) throws();

      /**
       * Set the font for this button.
       * This method has no effect on the display if the button uses an icon
       * instead of a text string.
       * @param f a font id
       * @param inherited not used
       */
    public:
      void setFont (const ::timber::Pointer< ::indigo::Font>& f, bool inherited) throws();

      /** 
       * The graphic has been exposed after a scheduled expose event.
       * @param requested true if this is due to a requested expose event
       */
    public:
      void graphicExposed (bool requested) throws();
	
      /**
       * The callback for arming, disarming, and activation
       */
    private:
      static void pushButtonCB(Widget, XtPointer, XtPointer) throws();

      /** A pixmap renderer */
    private:
      ::timber::Pointer< ::indigo::x11::PixmapRenderer> _renderer;
	
      /** A root graphic */
    private:
      ::std::unique_ptr<RootGraphic> _graphic;

      /** The pushbutton observer */
    private:
      ::nexus::peer::PushButton::Observer* _observer;

    };
  }
}

#endif
