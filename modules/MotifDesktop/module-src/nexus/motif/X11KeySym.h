#ifndef _NEXUS_MOTIF_X11KEYSYM_H
#define _NEXUS_MOTIF_X11KEYSYM_H

#ifndef _NEXUS_PEER_KEYSYMBOL_H
#include <nexus/peer/KeySymbol.h>
#endif

#ifndef X_H
#include <X11/X.h>
#endif

#ifndef _XLIB_H_
#include <X11/Xlib.h>
#endif

namespace nexus {
  namespace motif {
    /**
     * This interface must be implemented by classes represent key strokes.
     */
    class X11KeySym : public ::nexus::peer::KeySymbol {


      /** Default constructor. */
    private:
      inline X11KeySym  () throws() {}
	
      /** Destroy this symbol */
    public:
      ~X11KeySym() throws();

      /**
       * Create a new keysym object from a XKeyEvent.
       * @param e a key event
       * @return a X11KeySym reference
       */
    public:
      static ::timber::Reference< ::nexus::peer::KeySymbol> create(XKeyEvent& e) throws();

    public:
      KeySymbols::ID id() const throws();
      ::std::string symbols () const throws();
      Int asciiChar() const throws();
      ::std::string description () const throws();

      /**
       * Map a keysym to a KeySymbols id
       * @param sym a keysymbol
       * @return a KeySymbols::ID
       */
    private:
      static KeySymbols::ID mapKeySym(::KeySym sym) throws();

      /** The keysymbol id */
    private:
      KeySymbols::ID _id;

      /** The actual symbol */
    private:
      KeySym _keysym;

      /** The symbol string */
    private:
      ::std::string _string;

      /** The ASCII key character */
    private:
      Int _asciiChar;
    };
  }
}

#endif
