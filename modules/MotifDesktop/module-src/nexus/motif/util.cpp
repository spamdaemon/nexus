#include <nexus/motif/util.h>
#include <nexus/motif/Component.h>
#include <X11/X.h>
#include <X11/Xlib.h>

#include <iostream>

namespace nexus {
  namespace motif {

    Int removeExposeEvents (Display* dp, ::Window win) throws()
    {
      XEvent res;
      Int count = 0;
      // remove all expose events for this window
      while (::XCheckTypedWindowEvent(dp,win,Expose,&res)) {
	++count;
      }
      return count;
    }

    bool sendExposeEvent (Component& c) throws()
    {
      Size sz(c.size());
      XEvent event;
      event.type = Expose;
      event.xexpose.window = XtWindow(c.widget());
      event.xexpose.x = event.xexpose.y = 0;
      event.xexpose.width = sz.width();
      event.xexpose.height = sz.height();
      event.xexpose.count = 0;
      return ::XSendEvent (c.display(),event.xexpose.window,True,ExposureMask,&event)!=0;
    }

    static void destructorCB (Widget, XtPointer client_data, XtPointer ) throws()
    { 
      Widget w = reinterpret_cast<Widget>(client_data);
      if (XtIsRealized(w)) {
	::XtUnrealizeWidget(w);
      }
      ::XtDestroyWidget(w);
    }

    void addDestructor (Widget w, Widget t) throws()
    { ::XtAddCallback(w,XmNdestroyCallback,destructorCB,t); }

    void removeDestructor (Widget w, Widget t) throws()
    {  ::XtRemoveCallback(w,XmNdestroyCallback,destructorCB,t); }
      
  }
}

::std::ostream& operator<< (::std::ostream& out, const XEvent& event) throws()
{
  out << "XEvent { " << event.xany.window << " # " << event.xany.serial << " | ";
  switch (event.type) 
    {
    case KeyPress: {
      out << "KeyPress " << event.xkey.time;
      break;
    }
    case KeyRelease: {
      out << "KeyRelease " << event.xkey.time;
      break;
    }
    case ButtonPress: {
      out << "ButtonPress";
      break;
    }
    case ButtonRelease: {
      out << "ButtonRelease";
      break;
    }
    case MotionNotify: {
      out << "MotionNotify";
      break;
    }
    case EnterNotify: {
      out << "EnterNotify";
      break;
    }
    case LeaveNotify: {
      out << "LeaveNotify";
      break;
    }
    case FocusIn: {
      out << "FocusIn";
      break;
    }
    case FocusOut: {
      out << "FocusOut";
      break;
    }
    case KeymapNotify: {
      out << "KeymapNotify";
      break;
    }
    case Expose: {
      out << "Expose";
      break;
    }
    case GraphicsExpose: {
      out << "GraphicsExpose";
      break;
    }
    case NoExpose: {
      out << "NoExpose";
      break;
    }
    case VisibilityNotify: {
      out << "VisibilityNotify";
      break;
    }
    case CreateNotify: {
      out << "CreateNotify";
      break;
    }
    case DestroyNotify: {
      out << "DestroyNotify";
      break;
    }
    case UnmapNotify: {
      out << "UnmapNotify";
      break;
    }
    case MapNotify: {
      out << "MapNotify";
      break;
    }
    case MapRequest: {
      out << "MapRequest";
      break;
    }
    case ReparentNotify: {
      out << "ReparentNotify";
      break;
    }
    case ConfigureNotify: {
      out << "ConfigureNotify";
      break;
    }
    case ConfigureRequest: {
      out << "ConfigureRequest";
      break;
    }
    case GravityNotify: {
      out << "GravityNotify";
      break;
    }
    case ResizeRequest: {
      out << "ResizeRequest";
      break;
    }
    case CirculateNotify: {
      out << "CirculateNotify";
      break;
    }
    case CirculateRequest: {
      out << "CirculateRequest";
      break;
    }
    case PropertyNotify: {
      out << "PropertyNotify";
      break;
    }
    case SelectionClear: {
      out << "SelectionClear";
      break;
    }
    case SelectionRequest: {
      out << "SelectionRequest";
      break;
    }
    case SelectionNotify: {
      out << "SelectionNotify";
      break;
    }
    case ColormapNotify: {
      out << "ColormapNotify";
      break;
    }
    case ClientMessage: {
      out << "ClientMessage";
      break;
    }
    case MappingNotify: {
      out << "MappingNotify";
      break;
    }
    default: {
      out << "Unknown";
      break;
    }
    };

  return out << " }";
}

