#include <nexus/motif/TextComponent.h>
#include <nexus/motif/TextOperation.h>
#include <Xm/Text.h>

namespace nexus {
   namespace motif {

      TextComponent::TextComponent(Component& p, Widget w) throws()
            : Component(p, w), _model(0), _validate(true), _textOp(0)
      {
         setValue(XmNborderWidth, 0);
         setValue(XmNmarginWidth, 0);
         setValue(XmNmarginHeight, 0);
         setValue(XmNmarginLeft, 0);
         setValue(XmNmarginRight, 0);
         setValue(XmNmarginTop, 0);
         setValue(XmNmarginBottom, 0);
         setValue(XmNhighlightOnEnter, False);
         setValue(XmNhighlightThickness, 0);
      }

      TextComponent::~TextComponent() throws()
      {
      }

      bool TextComponent::hasForeground() const throws()
      {
         return true;
      }

      void TextComponent::setFont(const ::timber::Pointer< ::indigo::Font>& f, bool inherited) throws()
      {
         Component::setFont(f, inherited);
         if (font() != nullptr) {
            setValue(XmNrenderTable, font()->font());
         }
      }

      void TextComponent::setBorderWidth(Int w) throws()
      {
         setValue(XmNshadowThickness, w);
      }

      void TextComponent::validationCB(Widget, XtPointer client_data, XtPointer data) throws()
      {
         XmTextVerifyPtr cbs = static_cast< XmTextVerifyPtr>(data);
         TextComponent* tf = static_cast< TextComponent*>(client_data);

         if (cbs->reason == XmCR_MODIFYING_TEXT_VALUE && tf->textModel() != 0) {
            if (tf->_validate) {
               return;
            }

            TextOperation cr(tf->textModel()->text());
            tf->_textOp = &cr;
            tf->textModel()->replaceText(cbs->startPos, cbs->endPos - cbs->startPos,
                  ::std::string(cbs->text->ptr, 0, cbs->text->length));
            tf->_textOp = 0;

            cbs->doit = cr.count() > 0 || cr.text().length() > 0;
            if (cbs->doit) {
               // update the data from the change request
               cbs->startPos = cr.position();
               cbs->endPos = cr.position() + cr.count();
               cbs->text->length = cr.text().length();
               cbs->text->ptr = const_cast< char*>(cr.text().c_str());
            }
         }
      }

      void TextComponent::setTextModel(TextModel* model) throws()
      {
         if (_model != model) {
            if (_model == 0 && model != 0) {
               ::XtAddCallback(widget(), XmNmodifyVerifyCallback, validationCB, this);
               setValue(XmNeditable, true);
            }
            else if (_model != 0 && model == 0) {
               setValue(XmNeditable, false);
               ::XtRemoveCallback(widget(), XmNmodifyVerifyCallback, validationCB, this);
            }
            _model = model;
         }
      }

      void TextComponent::replaceText(size_type off, size_type count, const ::std::string& s) throws()
      {
         if (_textOp == 0) {
            setValidateEnabled(true);
            ::XmTextReplace(widget(), off, off + count, const_cast< char*>(s.c_str()));
            setValidateEnabled(false);
         }
         else {
            _textOp->replace(off, count, s);
         }
      }

   }
}
