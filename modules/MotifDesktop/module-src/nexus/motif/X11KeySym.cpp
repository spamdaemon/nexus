#include <nexus/motif/X11KeySym.h>
#include <X11/keysym.h>
#include <X11/Xutil.h>

using namespace ::timber;

namespace nexus {
  namespace motif {
    
    X11KeySym::~X11KeySym() throws()
    {}
    
    Reference< ::nexus::peer::KeySymbol> X11KeySym::create(XKeyEvent& e) throws()
    {
      Reference<X11KeySym> sym(new X11KeySym());
      // at this point we have a symbol structure owned only by this method
      char buf[128];
      XComposeStatus dummy[1];
      Int count= ::XLookupString(&e,buf,127,&sym->_keysym,dummy);
      sym->_string = ::std::string(buf,0,count);
      sym->_id = mapKeySym(sym->_keysym);
      sym->_asciiChar = -1;
      if (count==1 && static_cast<UByte>(buf[0])<128) {
	sym->_asciiChar = buf[0];
      }
      return sym;
    }

    KeySymbols::ID X11KeySym::id() const throws()
    { return _id; }
      
    ::std::string X11KeySym::symbols () const throws()
    { return _string; }

    Int X11KeySym::asciiChar() const throws()
    { return _asciiChar; }

    ::std::string X11KeySym::description () const throws()
    { return ::XKeysymToString(_keysym); }

    KeySymbols::ID X11KeySym::mapKeySym(::KeySym sym) throws()
    {
      switch (sym) {
      case XK_Home: return KeySymbols::KEY_HOME;
      case XK_Left: return KeySymbols::KEY_LEFT;
      case XK_Right: return KeySymbols::KEY_RIGHT;
      case XK_Up: return KeySymbols::KEY_UP;
      case XK_Down: return KeySymbols::KEY_DOWN;
      case XK_Page_Up:  return KeySymbols::KEY_PAGE_UP;
      case XK_Page_Down: return KeySymbols::KEY_PAGE_DOWN;
      case XK_End	: return KeySymbols::KEY_END;
      case XK_Begin	: return KeySymbols::KEY_BEGIN;
      case XK_Return : return KeySymbols::KEY_RETURN;
      case XK_BackSpace: return KeySymbols::KEY_BACKSPACE;
      case XK_Tab : return KeySymbols::KEY_TAB;
      case XK_Linefeed: return KeySymbols::KEY_LINEFEED;
      case XK_Clear: return KeySymbols::KEY_CLEAR;
      case XK_Pause: return KeySymbols::KEY_PAUSE;
      case XK_Scroll_Lock: return KeySymbols::KEY_SCROLL_LOCK;
      case XK_Sys_Req: return KeySymbols::KEY_SYS_REQ;
      case XK_Escape: return KeySymbols::KEY_ESCAPE;
      case XK_Delete: return KeySymbols::KEY_DELETE;
      case XK_Break: return KeySymbols::KEY_BREAK;
      case XK_Num_Lock: return KeySymbols::KEY_NUM_LOCK;
      case XK_a: return KeySymbols::KEY_a;
      case XK_b: return KeySymbols::KEY_b;
      case XK_c: return KeySymbols::KEY_c;
      case XK_d: return KeySymbols::KEY_d;
      case XK_e: return KeySymbols::KEY_e;
      case XK_f: return KeySymbols::KEY_f;
      case XK_g: return KeySymbols::KEY_g;
      case XK_h: return KeySymbols::KEY_h;
      case XK_i: return KeySymbols::KEY_i;
      case XK_j: return KeySymbols::KEY_j;
      case XK_k: return KeySymbols::KEY_k;
      case XK_l: return KeySymbols::KEY_l;
      case XK_m: return KeySymbols::KEY_m;
      case XK_n: return KeySymbols::KEY_n;
      case XK_o: return KeySymbols::KEY_o;
      case XK_p: return KeySymbols::KEY_p;
      case XK_q: return KeySymbols::KEY_q;
      case XK_r: return KeySymbols::KEY_r;
      case XK_s: return KeySymbols::KEY_s;
      case XK_t: return KeySymbols::KEY_t;
      case XK_u: return KeySymbols::KEY_u;
      case XK_v: return KeySymbols::KEY_v;
      case XK_w: return KeySymbols::KEY_w;
      case XK_x: return KeySymbols::KEY_x;
      case XK_y: return KeySymbols::KEY_y;
      case XK_z: return KeySymbols::KEY_z;
      case XK_A: return KeySymbols::KEY_A;
      case XK_B: return KeySymbols::KEY_B;
      case XK_C: return KeySymbols::KEY_C;
      case XK_D: return KeySymbols::KEY_D;
      case XK_E: return KeySymbols::KEY_E;
      case XK_F: return KeySymbols::KEY_F;
      case XK_G: return KeySymbols::KEY_G;
      case XK_H: return KeySymbols::KEY_H;
      case XK_I: return KeySymbols::KEY_I;
      case XK_J: return KeySymbols::KEY_J;
      case XK_K: return KeySymbols::KEY_K;
      case XK_L: return KeySymbols::KEY_L;
      case XK_M: return KeySymbols::KEY_M;
      case XK_N: return KeySymbols::KEY_N;
      case XK_O: return KeySymbols::KEY_O;
      case XK_P: return KeySymbols::KEY_P;
      case XK_Q: return KeySymbols::KEY_Q;
      case XK_R: return KeySymbols::KEY_R;
      case XK_S: return KeySymbols::KEY_S;
      case XK_T: return KeySymbols::KEY_T;
      case XK_U: return KeySymbols::KEY_U;
      case XK_V: return KeySymbols::KEY_V;
      case XK_W: return KeySymbols::KEY_W;
      case XK_X: return KeySymbols::KEY_X;
      case XK_Y: return KeySymbols::KEY_Y;
      case XK_Z: return KeySymbols::KEY_Z;
      case XK_0: return KeySymbols::KEY_0;
      case XK_1: return KeySymbols::KEY_1;
      case XK_2: return KeySymbols::KEY_2;
      case XK_3: return KeySymbols::KEY_3;
      case XK_4: return KeySymbols::KEY_4;
      case XK_5: return KeySymbols::KEY_5;
      case XK_6: return KeySymbols::KEY_6;
      case XK_7: return KeySymbols::KEY_7;
      case XK_8: return KeySymbols::KEY_8;
      case XK_9: return KeySymbols::KEY_9;
      case XK_Shift_L: return KeySymbols::KEY_SHIFT_LEFT;
      case XK_Shift_R: return KeySymbols::KEY_SHIFT_RIGHT;
      case XK_Control_L: return KeySymbols::KEY_CONTROL_LEFT;
      case XK_Control_R: return KeySymbols::KEY_CONTROL_RIGHT;
      case XK_Caps_Lock: return KeySymbols::KEY_CAPS_LOCK;
      case XK_Shift_Lock: return KeySymbols::KEY_SHIFT_LOCK;
      case XK_Meta_L: return KeySymbols::KEY_META_LEFT;
      case XK_Meta_R: return KeySymbols::KEY_META_RIGHT;
      case XK_Alt_L: return KeySymbols::KEY_ALT_LEFT;
      case XK_Alt_R: return KeySymbols::KEY_ALT_RIGHT;
      case XK_Super_L: return KeySymbols::KEY_SUPER_LEFT;
      case XK_Super_R: return KeySymbols::KEY_SUPER_RIGHT;
      case XK_Hyper_L: return KeySymbols::KEY_HYPER_LEFT;
      case XK_Hyper_R: return KeySymbols::KEY_HYPER_RIGHT;
      case XK_space: return KeySymbols::KEY_SPACE;
      case XK_exclam: return KeySymbols::KEY_EXCLAM;
      case XK_quotedbl: return KeySymbols::KEY_QUOTE_DOUBLE;
      case XK_numbersign: return KeySymbols::KEY_NUMBER_SIGN;
      case XK_dollar: return KeySymbols::KEY_DOLLAR;
      case XK_percent: return KeySymbols::KEY_PERCENT;
      case XK_ampersand: return KeySymbols::KEY_AMPERSAND;
      case XK_apostrophe: return KeySymbols::KEY_APOSTROPHE;
      case XK_parenleft: return KeySymbols::KEY_PAREN_LEFT;
      case XK_parenright: return KeySymbols::KEY_PAREN_RIGHT;
      case XK_asterisk: return KeySymbols::KEY_ASTERIX;
      case XK_plus: return KeySymbols::KEY_PLUS;
      case XK_minus: return KeySymbols::KEY_MINUS;
      case XK_comma: return KeySymbols::KEY_COMMA;
      case XK_period: return KeySymbols::KEY_PERIOD;
      case XK_colon: return KeySymbols::KEY_COLON;
      case XK_slash: return KeySymbols::KEY_SLASH;
      case XK_semicolon: return KeySymbols::KEY_SEMI_COLON;
      case XK_less: return KeySymbols::KEY_LESS;
      case XK_equal: return KeySymbols::KEY_EQUAL;
      case XK_greater: return KeySymbols::KEY_GREATER;
      case XK_question: return KeySymbols::KEY_QUESTION;
      case XK_at: return KeySymbols::KEY_AT;
      case XK_bracketleft: return KeySymbols::KEY_BRACKET_LEFT;
      case XK_bracketright: return KeySymbols::KEY_BRACKET_RIGHT;
      case XK_asciicircum: return KeySymbols::KEY_ASCII_CIRCUM;
      case XK_underscore: return KeySymbols::KEY_UNDERSCORE;
      case XK_backslash: return KeySymbols::KEY_BACKSLASH;
      case XK_grave: return KeySymbols::KEY_GRAVE;
      case XK_braceleft: return KeySymbols::KEY_BRACE_LEFT;
      case XK_braceright: return KeySymbols::KEY_BRACE_RIGHT;
      case XK_bar: return KeySymbols::KEY_BAR;
      case XK_asciitilde: return KeySymbols::KEY_ASCII_TILDE;


      case XK_nobreakspace : return KeySymbols::KEY_NO_BREAK_SPACE;
      case XK_exclamdown : return KeySymbols::KEY_EXCLAM_DOWN;
      case XK_cent : return KeySymbols::KEY_CENT;
      case XK_sterling : return KeySymbols::KEY_STERLING;
      case XK_currency : return KeySymbols::KEY_CURRENCY;
      case XK_yen : return KeySymbols::KEY_YEN;
      case XK_brokenbar : return KeySymbols::KEY_BROKEN_BAR;
      case XK_section : return KeySymbols::KEY_SECTION;
      case XK_diaeresis : return KeySymbols::KEY_DIAERESIS;
      case XK_copyright : return KeySymbols::KEY_COPYRIGHT;
      case XK_ordfeminine : return KeySymbols::KEY_ORDFEMININE;
      case XK_guillemotleft : return KeySymbols::KEY_GUILLEMOT_LEFT; 
      case XK_guillemotright : return KeySymbols::KEY_GUILLEMOT_RIGHT; 
      case XK_notsign : return KeySymbols::KEY_NOT_SIGN;
      case XK_hyphen : return KeySymbols::KEY_HYPHEN;
      case XK_registered : return KeySymbols::KEY_REGISTERED;
      case XK_macron : return KeySymbols::KEY_MACRON;
      case XK_degree : return KeySymbols::KEY_DEGREE;
      case XK_plusminus : return KeySymbols::KEY_PLUS_MINUS;
      case XK_onesuperior : return KeySymbols::KEY_ONE_SUPERIOR;
      case XK_twosuperior : return KeySymbols::KEY_TWO_SUPERIOR;
      case XK_threesuperior : return KeySymbols::KEY_THREE_SUPERIOR;
      case XK_acute : return KeySymbols::KEY_ACUTE;
      case XK_mu : return KeySymbols::KEY_MU;
      case XK_paragraph : return KeySymbols::KEY_PARAGRAPH;
      case XK_periodcentered : return KeySymbols::KEY_PERIOD_CENTERED;
      case XK_cedilla : return KeySymbols::KEY_CEDILLA;
      case XK_onequarter : return KeySymbols::KEY_ONE_QUARTER;
      case XK_onehalf : return KeySymbols::KEY_ONE_HALF;
      case XK_threequarters : return KeySymbols::KEY_THREE_QUARTERS;
      case XK_questiondown : return KeySymbols::KEY_QUESTION_DOWN;
      case XK_multiply : return KeySymbols::KEY_MULTIPLY;
      case XK_division : return KeySymbols::KEY_DIVISION;
      case XK_Agrave : return KeySymbols::KEY_A_GRAVE;
      case XK_Aacute : return KeySymbols::KEY_A_ACUTE;
      case XK_Acircumflex : return KeySymbols::KEY_A_CIRCUMFLEX;
      case XK_Atilde : return KeySymbols::KEY_A_TILDE;
      case XK_Adiaeresis : return KeySymbols::KEY_A_DIAERESIS;
      case XK_Aring : return KeySymbols::KEY_A_RING;
      case XK_AE : return KeySymbols::KEY_AE;
      case XK_agrave : return KeySymbols::KEY_a_GRAVE;
      case XK_aacute : return KeySymbols::KEY_a_ACUTE;
      case XK_acircumflex : return KeySymbols::KEY_a_CIRCUMFLEX;
      case XK_atilde : return KeySymbols::KEY_a_TILDE;
      case XK_adiaeresis : return KeySymbols::KEY_a_DIAERESIS;
      case XK_aring : return KeySymbols::KEY_a_RING;
      case XK_ae : return KeySymbols::KEY_ae;
      case XK_Ccedilla : return KeySymbols::KEY_C_CEDILLA;
      case XK_ccedilla : return KeySymbols::KEY_c_CEDILLA;
      case XK_Egrave : return KeySymbols::KEY_E_GRAVE;
      case XK_Eacute : return KeySymbols::KEY_E_ACUTE;
      case XK_Ecircumflex : return KeySymbols::KEY_E_CIRCUMFLEX;
      case XK_Ediaeresis : return KeySymbols::KEY_E_DIAERESIS;
      case XK_egrave : return KeySymbols::KEY_e_GRAVE;
      case XK_eacute : return KeySymbols::KEY_e_ACUTE;
      case XK_ecircumflex : return KeySymbols::KEY_e_CIRCUMFLEX;
      case XK_ediaeresis : return KeySymbols::KEY_e_DIAERESIS;
      case XK_Igrave : return KeySymbols::KEY_I_GRAVE;
      case XK_Iacute : return KeySymbols::KEY_I_ACUTE;
      case XK_Icircumflex : return KeySymbols::KEY_I_CIRCUMFLEX;
      case XK_Idiaeresis : return KeySymbols::KEY_I_DIAERESIS;
      case XK_igrave : return KeySymbols::KEY_I_GRAVE;
      case XK_iacute : return KeySymbols::KEY_i_ACUTE;
      case XK_icircumflex : return KeySymbols::KEY_i_CIRCUMFLEX;
      case XK_idiaeresis : return KeySymbols::KEY_i_DIAERESIS;
      case XK_ETH : return KeySymbols::KEY_ETH;
      case XK_eth : return KeySymbols::KEY_eth;
      case XK_Ntilde : return KeySymbols::KEY_N_TILDE;
      case XK_ntilde : return KeySymbols::KEY_n_TILDE;
      case XK_Oacute : return KeySymbols::KEY_O_ACUTE;
      case XK_Ograve : return KeySymbols::KEY_O_GRAVE;
      case XK_Ocircumflex : return KeySymbols::KEY_O_CIRCUMFLEX;
      case XK_Otilde : return KeySymbols::KEY_O_TILDE;
      case XK_Odiaeresis : return KeySymbols::KEY_O_DIAERESIS;
      case XK_Oslash : return KeySymbols::KEY_O_SLASH;
      case XK_oacute : return KeySymbols::KEY_o_ACUTE;
      case XK_ograve : return KeySymbols::KEY_o_GRAVE;
      case XK_ocircumflex : return KeySymbols::KEY_o_CIRCUMFLEX;
      case XK_otilde : return KeySymbols::KEY_o_TILDE;
      case XK_odiaeresis : return KeySymbols::KEY_o_DIAERESIS;
      case XK_oslash : return KeySymbols::KEY_o_SLASH;
      case XK_Ugrave : return KeySymbols::KEY_U_GRAVE;
      case XK_Uacute : return KeySymbols::KEY_U_ACUTE;
      case XK_Ucircumflex : return KeySymbols::KEY_U_CIRCUMFLEX;
      case XK_Udiaeresis : return KeySymbols::KEY_U_DIAERESIS;
      case XK_ugrave : return KeySymbols::KEY_u_GRAVE;
      case XK_uacute : return KeySymbols::KEY_u_ACUTE;
      case XK_ucircumflex : return KeySymbols::KEY_u_CIRCUMFLEX;
      case XK_udiaeresis : return KeySymbols::KEY_u_DIAERESIS;
      case XK_Yacute : return KeySymbols::KEY_Y_ACUTE;
      case XK_yacute : return KeySymbols::KEY_y_ACUTE;
      case XK_ydiaeresis : return KeySymbols::KEY_y_DIAERESIS;
      case XK_THORN : return KeySymbols::KEY_THORN;
      case XK_thorn : return KeySymbols::KEY_thorn;
      case XK_ssharp : return KeySymbols::KEY_s_SHARP;
      case XK_F1 : return KeySymbols::KEY_F1;
      case XK_F2 : return KeySymbols::KEY_F2;
      case XK_F3 : return KeySymbols::KEY_F3;
      case XK_F4 : return KeySymbols::KEY_F4;
      case XK_F5 : return KeySymbols::KEY_F5;
      case XK_F6 : return KeySymbols::KEY_F6;
      case XK_F7 : return KeySymbols::KEY_F7;
      case XK_F8 : return KeySymbols::KEY_F8;
      case XK_F9 : return KeySymbols::KEY_F9;
      case XK_F10 : return KeySymbols::KEY_F10;
      case XK_F11 : return KeySymbols::KEY_F11;
      case XK_F12 : return KeySymbols::KEY_F12;
      default:
	return KeySymbols::UNDEFINED;	
      }
    }
    
  }
}

