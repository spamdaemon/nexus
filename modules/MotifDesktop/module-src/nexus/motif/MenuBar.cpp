#include <nexus/motif/MenuBar.h>
#include <nexus/Bounds.h>

#include <Xm/RowColumn.h>
#include <Xm/CascadeB.h>

#include <iostream>

namespace nexus {
   namespace motif {

      MenuBar::MenuBar(Window& c) throws()
            : Component(c, XmCreateMenuBar(c.window(), cstr("menubar"), 0, 0)), _observer(0)
      {
         ::XtAddEventHandler(widget(), StructureNotifyMask, FALSE, fireStructureEvent, this);
      }

      MenuBar::~MenuBar() throws()
      {
         ::XtRemoveEventHandler(widget(), StructureNotifyMask, FALSE, fireStructureEvent, this);
      }

      void MenuBar::setMenuBarObserver(::nexus::peer::MenuBar::Observer* l) throws()
      {
         _observer = l;
      }

      void MenuBar::fireStructureEvent(Widget x, XtPointer mptr, XEvent* event, Boolean* propagate) throws()
      {
         *propagate = TRUE;
         MenuBar* m = static_cast< MenuBar*>(mptr);
         switch (event->type) {
            case ConfigureNotify: {
               XConfigureEvent& e = event->xconfigure;
               Bounds bnds(e.x, e.y, e.width, e.height);
               m->notifyBounds(bnds);
               break;
            }
            case MapNotify: {
               Bounds bounds = Component::getComponentBounds(m->widget());
               m->notifyBounds(bounds);
               break;
            }
            case UnmapNotify: {
               // ignore
               break;
            }
            default:
               break;
         }
      }

   }
}
