#ifndef _NEXUS_MOTIF_CONTAINER_H
#define _NEXUS_MOTIF_CONTAINER_H

#ifndef _NEXUS_MOTIF_CANVAS_H
#include <nexus/motif/Canvas.h>
#endif

#ifndef _NEXUS_PEER_CONTAINER_H
#include <nexus/peer/Container.h>
#endif

#ifndef _INDIGO_GROUP_H
#include <indigo/Group.h>
#endif

namespace nexus {
  namespace motif {
    /** Forward declaration */
    class GlassLayer;
    
    /**
     * This interface must be implemented by classes
     * that support composition.
     *
     * @author Raimund Merkert
     * @date 18 Feb 2003
     */
    class Container : public Canvas, public virtual ::nexus::peer::Container {
      Container(const Container&);
      Container&operator=(const Container&);

      /**
       * Create a new container as a child of the specified component.
       * @param parent a parent component
       */
    public:
      Container (Component& parent) throws();

      /** Destroy this container and release all its resources */
    public:
      ~Container () throws();

      /**
       * Set the bounds for the specified child.
       * @param c a child
       * @param b the new bounds
       */
    public:
      void setChildBounds (::nexus::peer::Component& c, const Bounds& b) throws();

    public:
      void setInterceptEventsEnabled (bool enable) throws();
      void setBackgroundGraphic (const ::timber::Pointer< ::indigo::Node> & gfx) throws();
      void setBorderGraphic (const ::timber::Pointer< ::indigo::Node> & gfx) throws();
      void setDoubleBufferEnabled (bool enabled) throws();

      /** Create the graphic if it doesn't exist yet. */
    private:
      void createGraphic () throws();
	
      /** Delete the graphic unless is still has two non-null children */
    private:
      void destroyGraphic () throws();

      /** A glass layer */
    private:
      GlassLayer* _glass;

      /** The graphic if there is one */
    private:
      ::timber::Pointer< ::indigo::Group> _gfx;
    };
  }
}

#endif
