#include <nexus/motif/Container.h>
#include <nexus/motif/GlassLayer.h>
#include <nexus/Bounds.h>

namespace nexus {
   namespace motif {

      Container::Container(Component& c) throws()
            : Canvas(c), _glass(0)
      {
         Canvas::setDoubleBufferEnabled(false);
         Canvas::setAutoRenderEnabled(true);
      }

      Container::~Container() throws()
      {
         delete _glass;
      }

      void Container::setDoubleBufferEnabled(bool enabled) throws()
      {
         Canvas::setDoubleBufferEnabled(enabled);
      }

      void Container::setChildBounds(::nexus::peer::Component& c, const Bounds& b) throws()
      {
         dynamic_cast< Component&>(c).setBounds(b);
      }

      void Container::setInterceptEventsEnabled(bool enable) throws()
      {
         if (enable && _glass == 0) {
            _glass = new GlassLayer(widget());
         }
         else if (!enable && _glass != 0) {
            delete _glass;
            _glass = 0;
         }
      }

      void Container::createGraphic() throws()
      {
         if (_gfx == nullptr) {
            _gfx = ::timber::Pointer< ::indigo::Group>(new ::indigo::Group(2, true));
            _gfx->set((::indigo::Node*) 0, 0);
            _gfx->set((::indigo::Node*) 0, 1);
            setGraphic(_gfx);
         }
      }

      void Container::destroyGraphic() throws()
      {
         if (_gfx != nullptr && _gfx->hasNode(0) && _gfx->hasNode(1)) {
            _gfx = nullptr;
            setGraphic(_gfx);
         }
      }

      void Container::setBackgroundGraphic(const ::timber::Pointer< ::indigo::Node> & gfx) throws()
      {
         if (gfx != nullptr) {
            createGraphic();
            _gfx->set(gfx, 0);
         }
         else if (_gfx != nullptr) {
            _gfx->set((::indigo::Node*) 0, 0);
            destroyGraphic();
         }
      }

      void Container::setBorderGraphic(const ::timber::Pointer< ::indigo::Node> & gfx) throws()
      {
         if (gfx != nullptr) {
            createGraphic();
            _gfx->set(gfx, 1);
         }
         else if (_gfx != nullptr) {
            _gfx->set((::indigo::Node*) 0, 1);
            destroyGraphic();
         }
      }

   }
}
