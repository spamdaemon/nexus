#ifndef _NEXUS_MOTIF_MENU_H
#define _NEXUS_MOTIF_MENU_H

#ifndef _NEXUS_PEER_MENU_H
#include <nexus/peer/Menu.h>
#endif

#ifndef _NEXUS_MOTIF_COMPONENT_H
#include <nexus/motif/Component.h>
#endif

#ifndef _INDIGO_X11_PIXMAPRENDERER_H
#include <indigo/x11/PixmapRenderer.h>
#endif

#ifndef _NEXUS_MOTIF_ROOTGRAPHIC_H
#include <nexus/motif/RootGraphic.h>
#endif

#ifndef _NEXUS_ICON_H
#include <nexus/Icon.h>
#endif

namespace nexus {
  namespace motif {
    class MenuBar;

    /**
     * The menu implements the nexus menu interface. Menus and menu items
     * can be added to this menu but must use the menuWidget() function instead
     * of widget() to obtain the parent widget for this class. When a menu item or
     * submenu is destroy, destroyChildWidget() must be called to clean up some 
     * visual artifacts.
     */
    class Menu : public Component, public virtual ::nexus::peer::Menu {
      Menu(const Menu&);
      Menu&operator=(const Menu&);

      /** 
       * Create a new menu as a submenu. This constructor 
       * takes a pointer rather than a reference to avoid confusion
       * with a copy constructor.
       * @param parent the parent menu.
       */
    public:
      Menu (Menu* parent) throws();

      /** 
       * Create a new menu as a top-level menu on the menubar.
       * @param parent the parent component
       */
    public:
      Menu (MenuBar& parent) throws();
      
    public:	
      ~Menu() throws();

    public:
      void setText (const ::std::string& t) throws();
      void setIcon (const Icon& i) throws();

        /**
       * Set the  observer.
       * @param l a listener or observer or 0 to clear it
       */
    public:
      void setMenuObserver (::nexus::peer::Menu::Observer* l) throws();

      bool hasForeground () const throws();
      void setForegroundColor (const Color& c, bool inherited) throws();
      void setBackgroundColor (const Color& c, bool inherited) throws();
      void setBorderWidth (Int w) throws();

      /**
       * @name Managing children
       * @{
       */

      /**
       * Get the widget to which menu items will be attached.
       * @return a widget that manages the menu item.
       */
    public:
      Widget getMenuWidget() throws();

      /**
       * Create a child widget that will be used to create a menu .
       * @return a menu item child widget
       */
    public:
      void destroyChildWidget() throws();

      /*@}*/

      /**
       * Set the font for this button.
       * This method has no effect on the display if the button uses an icon
       * instead of a text string.
       * @param f a font id
       * @param inherited not used
       */
    public:
      void setFont (const ::timber::Pointer< ::indigo::Font>& f, bool inherited) throws();

      /** 
       * The graphic has been exposed after a scheduled expose event.
       * @param requested true if this is due to a requested expose event
       */
    public:
      void graphicExposed (bool requested) throws();
	
      /**
       * The callback for arming, disarming, and activation
       */
    private:
      static void menuCB(Widget, XtPointer, XtPointer) throws();

      /**
       * This callback is invoked whenever the structure changes
       */
    private:
      static void fireStructureEvent(Widget, XtPointer, XEvent*, Boolean*) throws();

      /** A pixmap renderer */
    private:
      ::timber::Pointer< ::indigo::x11::PixmapRenderer> _renderer;
	
      /** A root graphic */
    private:
      ::std::unique_ptr<RootGraphic> _graphic;

      /** The menu observer */
    private:
      ::nexus::peer::Menu::Observer* _observer;

      /** The number of children (non-negative if a top-level menu) */
    private:
      Int _nChildren;

      /** The pull-down menu */
    private:
      Widget _pulldown;
    };
  }
}

#endif
