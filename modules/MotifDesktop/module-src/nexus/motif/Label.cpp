#include <nexus/motif/Label.h>
#include <nexus/HorizontalAlignment.h>
#include <Xm/Label.h>

namespace nexus {
   namespace motif {

      Label::Label(Component& c) throws()
            : Component(c, "label", xmLabelWidgetClass), _graphic(RootGraphic::create(*this))
      {
         ::std::string txt;
         setValue(XmNlabelString, txt);
         setValue(XmNrecomputeSize, False);
         setValue(XmNalignment, static_cast< Int>(XmALIGNMENT_BEGINNING));
         setValue(XmNmarginWidth, 0);
         setValue(XmNmarginHeight, 0);
         setValue(XmNhighlightOnEnter, False);
         setValue(XmNhighlightThickness, 0);
         setValue(XmNshadowThickness, 0);
      }

      Label::~Label() throws()
      {
      }

      bool Label::hasForeground() const throws()
      {
         return true;
      }

      void Label::setFont(const ::timber::Pointer< ::indigo::Font>& f, bool inherited) throws()
      {
         Component::setFont(f, inherited);
         if (_renderer == nullptr && font() != nullptr) {
            setValue(XmNrenderTable, font()->font());
         }
      }

      void Label::setForegroundColor(const Color& c, bool inherited) throws()
      {
         Component::setForegroundColor(c, inherited);
         if (_renderer != nullptr) {
            _renderer->setDefaultColors(backgroundColor().color(), foregroundColor().color());
            _graphic->scheduleRefresh();
         }
      }

      void Label::setBackgroundColor(const Color& c, bool inherited) throws()
      {
         Component::setBackgroundColor(c, inherited);
         if (_renderer != nullptr) {
            _renderer->setDefaultColors(backgroundColor().color(), foregroundColor().color());
            _graphic->scheduleRefresh();
         }
      }

      void Label::setText(const ::std::string& t) throws()
      {
         _graphic->setRootNode((::indigo::Node*) 0);
         _graphic->cancelRefreshRequest();

         if (_renderer != nullptr) {
            _renderer = nullptr;
            setValue(XmNlabelType, static_cast< Int>(XmSTRING));

            // set the font
            if (font() != nullptr) {
               setValue(XmNrenderTable, font()->font());
            }
         }
         setValue(XmNlabelString, t);
      }

      void Label::setIcon(const Icon& icon) throws()
      {
         _renderer = nullptr;
         Colormap colors;
         ::XtVaGetValues(widget(), XmNcolormap, &colors, (XtPointer) 0);
         _renderer = ::indigo::x11::PixmapRenderer::create(screen(), icon.width(), icon.height(), 0, colors,
               backgroundColor().color(), foregroundColor().color());
         _renderer->setGraphic(icon.graphic());
         setValue(XmNlabelType, static_cast< Int>(XmPIXMAP));

         _graphic->setRootNode(icon.graphic());
      }

      void Label::graphicExposed(bool) throws()
      {
         if (_renderer != nullptr) {
            _renderer->refresh();
            setValue(XmNlabelPixmap, _renderer->pixmap());
         }
      }

      void Label::setAlignment(const HorizontalAlignment& align) throws()
      {
         Int alignment;
         switch (align.value()) {
            case HorizontalAlignment::ALIGN_LEFT:
               alignment = XmALIGNMENT_BEGINNING;
               break;
            case HorizontalAlignment::ALIGN_RIGHT:
               alignment = XmALIGNMENT_END;
               break;
            case HorizontalAlignment::ALIGN_CENTER:
            default:
               alignment = XmALIGNMENT_CENTER;
               break;
         };
         setValue(XmNalignment, alignment);
      }

   }
}
