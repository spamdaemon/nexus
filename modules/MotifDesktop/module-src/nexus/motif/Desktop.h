#ifndef _NEXUS_MOTIF_DESKTOP_H
#define _NEXUS_MOTIF_DESKTOP_H

#ifndef _NEXUS_PEER_DESKTOP_H
#include <nexus/peer/Desktop.h>
#endif

#ifndef _CANOPY_MT_THREAD_H
#include <canopy/mt/Thread.h>
#endif

#ifndef _TIMBER_MT_TRANSFERQUEUE_H
#include <timber/mt/TransferQueue.h>
#endif

#ifndef _TIMBER_SCHEDULER_SCHEDULER_H
#include <timber/scheduler/Scheduler.h>
#endif

#ifndef _XtIntrinsic_h
#include <X11/Intrinsic.h>
#endif

namespace nexus {
   namespace motif {

      /**
       * This interface must be implemented
       * by all native desktops. The primary
       * purpose of the desktop is to create
       * components.
       */
      class Desktop : public ::nexus::peer::Desktop
      {
            Desktop&operator=(const Desktop&);
            Desktop(const Desktop&);

            /**
             * Create a new desktop.
             * @param argc argument count (will be modified after construction)
             * @param argv argument vector
             * @throw ::std::exception if the X11 display could not be opened
             */
         private:
            Desktop(Int& argc, const char** argv)
            throws ( ::std::exception);

            /**
             * Create a new motif desktop.
             * @param argc argument count (will be modified after construction)
             * @param argv argument vector
             * @return a new desktop
             * @throw ::std::exception if the X11 display could not be opened
             */
         public:
            static ::timber::Reference< Desktop> create(Int& argc, const char** argv)
            throws ( ::std::exception);

            /** Destroy this desktop and release all its resources */
         public:
            ~Desktop()throws();

            /**
             * @name Implementation of ::nexus::peer::Desktop
             * @{
             */

         public:

            void setKeyInterceptor(KeyInterceptor* l)throws();
            bool isEventThread() const throws();
            bool enqueue(
                  const ::timber::SharedRef< Runnable>& r) throws();
            bool enqueue(
                  const ::timber::SharedRef< Runnable>& r, const Delay&, const Period&) throws();
            void shutdown()throws();
            void waitShutdown() const throws();
            Size size() const throws();
            ::nexus::peer::Window* createWindow()throws();
            ::nexus::peer::Container* createContainer(::nexus::peer::Component& parent)throws();
            ::nexus::peer::Canvas* createCanvas2D(::nexus::peer::Component& parent)throws();
            ::nexus::peer::Label* createLabel(::nexus::peer::Component& parent)throws();
            ::nexus::peer::Menu* createMenu(::nexus::peer::Component& parent)throws();
            ::nexus::peer::MenuItem* createMenuItem(::nexus::peer::Menu& parent)throws();
            ::nexus::peer::MenuBar* createMenuBar(::nexus::peer::Window& parent)throws();
            ::nexus::peer::PushButton* createPushButton(::nexus::peer::Component& parent)throws();
            ::nexus::peer::TextField* createTextField(::nexus::peer::Component& parent)throws();
            ::nexus::peer::TextArea* createTextArea(::nexus::peer::Component& parent)throws();
            ::timber::Reference< ::indigo::FontFactory> fonts() const throws();
            InputModifiers queryInputModifiers() const throws();
            Pixel pointerLocation() const throws();

            /*@}*/

            /**
             * Intercept the specified key press.
             * @param key the pressed key
             * @param ks the keysymbol of the key that was pressed
             * @param when time the button was pressed
             * @param m the state of modifier keys and buttons at the time of the event
             * @pre REQUIRE_NON_ZERO(ks);
             * @return true if the key was intercepted, false otherwise.
             */
         public:
            bool interceptKeyPressed(const ::timber::Reference< ::nexus::peer::KeySymbol>& ks, const Time& when,
                  const InputModifiers& m)throws();

            /**
             * Intercept the specified key typed.
             * @param key the pressed key
             * @param ks the keysymbol of the key that was typed
             * @param when time the key  was typed
             * @param m the state of modifier keys and buttons at the time of the event
             * @pre REQUIRE_NON_ZERO(ks);
             * @return true if the key was intercepted, false otherwise.
             */
         public:
            bool interceptKeyTyped(const ::timber::Reference< ::nexus::peer::KeySymbol>& ks, const Time& when,
                  const InputModifiers& m)throws();

            /**
             * Intercept the specified key release.
             * @param key the pressed key
             * @param ks the keysymbol of the key that was released
             * @param when time the button was released
             * @param m the state of modifier keys and buttons at the time of the event
             * @pre REQUIRE_NON_ZERO(ks);
             * @return true if the key was intercepted, false otherwise.
             */
         public:
            bool interceptKeyReleased(const ::timber::Reference< ::nexus::peer::KeySymbol>& ks, const Time& when,
                  const InputModifiers& m)throws();

            /**
             * Enable the mouse wheel.
             * @param enable true if the mouse wheel should be enabled
             */
         public:
            void setMouseWheelEnabled(bool enable)throws();

            /**
             * Test if the mouse wheel is enabled.
             * @return true if the mouse wheel is enabled
             */
         public:
            inline bool isMouseWheelEnabled() const throws()
            {
               return _mouseWheelEnabled;
            }

            /**
             * @name X11 based methods
             * @{
             */

            /**
             * Get the display.
             * @return the display
             */
         public:
            inline Display* display() const throws()
            {
               return _display;
            }

            /**
             * Get the desktop screen.
             * @return the desktop's screen
             */
         public:
            inline Screen* screen() const throws()
            {
               return _screen;
            }

            /*@}*/

            /** Flush any changes to the server. */
         private:
            inline void flush() const throws()
            {
               ::XFlush(_display);
            }

            /**
             * A function that executes an item in the queue of the specified desktop
             * @param dt a desktop
             */
         private:
            static void executeRunnable(void*, int*, XtInputId*)throws();

            /**
             * The event processing loop
             */
         public:
            void processEvents()throws();

            /** The desktop's display */
         private:
            mutable ::Display* _display;

            /** The display screen */
         private:
            mutable ::Screen* _screen;

            /** An app context */
         private:
            mutable ::XtAppContext _context;

            /** An XtInput id */
         private:
            XtInputId _xtInput;

            /** A widget for font creation */
         private:
            mutable Widget _fontWidget;

            /** True if this desktop has been shutdown */
         private:
            bool _shutdown;

            /** True if the mouse wheel is enabled */
         private:
            bool _mouseWheelEnabled;

            /** An font factory */
         private:
            mutable ::timber::Pointer< ::indigo::FontFactory> _fonts;

            /// The application thread
         private:
            ::std::unique_ptr< ::canopy::mt::Thread> _eventThread;

            /// A runnable queue
         private:
            mutable ::timber::SharedRef< ::timber::mt::TransferQueue< ::timber::SharedRef< Runnable> > > _queue;

            /// File descriptors
         private:
            Int _fds[2];

            /// A key observer
         private:
            KeyInterceptor* _keyInterceptor;

            /** The threadpool scheduler */
         private:
            ::timber::SharedRef< ::timber::scheduler::Scheduler> _scheduler;
      };
   }
}

#endif
