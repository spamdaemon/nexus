#include <nexus/motif/Component.h>
#include <nexus/motif/Desktop.h>
#include <nexus/motif/util.h>
#include <nexus/motif/X11KeySym.h>
#include <nexus/Insets.h>
#include <nexus/Bounds.h>
#include <nexus/InputModifiers.h>
#include <nexus/Time.h>

#include <timber/logging.h>

using namespace ::timber::logging;

namespace nexus {
   namespace motif {
      namespace {
         template<class E>
         static Time eventTime(const E& evt)
         {
            return Time(evt.time * INT64_C(10000000));
         }

         static Log getLog()
         {
            return Log("nexus.motif.Component");
         }

         static Bool checkRepeatedEvent(Display *, XEvent *nextEvent, XPointer arg)
         {
            const XEvent* event = reinterpret_cast< XEvent*>(arg);

            return nextEvent->type == KeyPress && nextEvent->xany.serial == event->xany.serial;
         }

         /**
          * Create input modifers from the specified state information.
          * @param state a state
          * @return input modifiers
          */
         static InputModifiers convertInputModifiers(Int state)
         throws ()
         {
            static const Int STATE_COUNT = 9;

            static const Int x11States[STATE_COUNT] = {
                  Button1Mask, Button2Mask, Button3Mask, Button4Mask, Button5Mask, ShiftMask, LockMask, ControlMask,
                  Mod1Mask };
            static const Int masks[STATE_COUNT] = {
                  InputModifiers::BUTTON1, InputModifiers::BUTTON2, InputModifiers::BUTTON3, InputModifiers::BUTTON4,
                  InputModifiers::BUTTON5, InputModifiers::SHIFT, InputModifiers::CAPS_LOCK, InputModifiers::CONTROL,
                  InputModifiers::META1 };

            Int m = 0;
            for (Index i = 0; i < STATE_COUNT; ++i) {
               if (state & x11States[i]) {
                  m |= masks[i];
               }
            }
            return InputModifiers(m);
         }
      }

      Component::Component(Component& p, const char* name, WidgetClass wclass) throws()
            : _desktop(p.desktop()), _parent(&p), _widget(::XtCreateWidget(name, wclass, p.widget(), 0, 0))
      {
         initialize();
      }

      Component::Component(Component& p, Widget w) throws()
            : _desktop(p.desktop()), _parent(&p), _widget(w)
      {
         initialize();
      }

      Component::Component(Desktop& dt, Widget w) throws()
            : _desktop(dt), _parent(0), _widget(w)
      {
         initialize();
      }

      Component::~Component() throws()
      {
         if (XtIsRealized(widget())) {
            setExposureEventsEnabled(false);
         }
         _parent = 0;
         XtDestroyWidget(widget());
      }

      void Component::updateEventMask() throws()
      {
         // always! get keyboard events, because we have to forward them to the desktop first
         EventMask mask(KeyPressMask | KeyReleaseMask);

         if (_buttonObserver != 0) {
            mask |= ButtonPressMask | ButtonReleaseMask | EnterWindowMask | LeaveWindowMask | PointerMotionMask
                  | PointerMotionHintMask;
         }
         if (_wheelObserver != 0) {
            mask |= ButtonPressMask | ButtonReleaseMask;
         }
         if (_crossingObserver != 0) {
            mask |= EnterWindowMask | LeaveWindowMask;
         }
         if (_motionObserver != 0) {
            mask |= PointerMotionMask;
            mask &= ~PointerMotionHintMask;
         }
#if 0  // we  don't need this anymore, because key events will always be turned on 
         if (_keyObserver!=0) {
            mask |= KeyPressMask|KeyReleaseMask;
         }
#endif
         if (_focusObserver != 0) {
            mask |= FocusChangeMask;
         }

         const EventMask setMask((_eventMask | mask) & ~_eventMask);
         const EventMask clearMask(_eventMask & ~mask);

         assert((setMask & clearMask) == 0);

         if (setMask != 0) {
            ::XtAddEventHandler(widget(), mask, FALSE, processXEvent, this);
         }
         if (clearMask != 0) {
            ::XtRemoveEventHandler(widget(), clearMask, FALSE, processXEvent, this);
         }
         _eventMask = (_eventMask | setMask) & ~clearMask;
      }

      Size Component::size() const throws()
      {
         Dimension w, h;
         ::XtVaGetValues(widget(), XmNwidth, &w, XmNheight, &h, (XtPointer) 0);
         return Size(w, h);
      }

      void Component::setComponentObserver(ComponentObserver* l) throws()
      {
         _componentObserver = l;
      }

      void Component::setWheelObserver(WheelObserver* l) throws()
      {
         if (_wheelObserver != l) {
            _wheelObserver = l;
            updateEventMask();
         }
      }

      void Component::setButtonObserver(ButtonObserver* l) throws()
      {
         if (_buttonObserver != l) {
            _buttonObserver = l;
            updateEventMask();
         }
      }

      void Component::setMotionObserver(MotionObserver* l) throws()
      {
         if (_motionObserver != l) {
            _motionObserver = l;
            updateEventMask();
         }
      }

      void Component::setCrossingObserver(CrossingObserver* l) throws()
      {
         if (_crossingObserver != l) {
            _crossingObserver = l;
            updateEventMask();
         }
      }

      void Component::setKeyObserver(KeyObserver* l) throws()
      {
         if (_keyObserver != l) {
            _keyObserver = l;
            updateEventMask();
         }
      }

      void Component::setFocusObserver(FocusObserver* l) throws()
      {
         if (_focusObserver != l) {
            _focusObserver = l;
            updateEventMask();
         }
      }

      void Component::initialize() throws()
      {
         _bgColor = Color::BLACK;
         _bgPixel = convertColorToPixel(_bgColor);
         _fgColor = Color::WHITE;
         _fgPixel = convertColorToPixel(_fgColor);

         _inheritFont = true;
         _componentObserver = 0;
         _wheelObserver = 0;
         _buttonObserver = 0;
         _motionObserver = 0;
         _keyObserver = 0;
         _crossingObserver = 0;
         _focusObserver = 0;
         _inheritBGColor = true;
         _inheritFGColor = true;
         _eventMask = 0;
         setValue(XmNinitialResourcesPersistent, False);

         // update the initial event mask
         updateEventMask();
      }

      void Component::setComponentBounds(Widget wid, const Bounds& b) throws()
      {
         assert(wid != 0);
         ::Position x = b.x();
         ::Position y = b.y();
         ::Dimension w = b.width();
         ::Dimension h = b.height();
         ::Dimension border = 0;
         ::XtConfigureWidget(wid, x, y, w, h, border);
      }

      Bounds Component::getComponentBounds(Widget wd) throws()
      {
         Dimension w, h;
         ::Position x, y;
         ::XtVaGetValues(wd, XmNwidth, &w, XmNheight, &h, XmNx, &x, XmNy, &y, (XtPointer) 0);
         return Bounds(x, y, w, h);
      }

      void Component::setBounds(const Bounds& b) throws()
      {
         assert(b.width() > 0);
         assert(b.height() > 0);
         setComponentBounds(widget(), b);
      }

      void Component::setEnabled(bool enabled) throws()
      {
         XtSetSensitive(widget(), static_cast< Boolean>(enabled));
      }

      void Component::setVisible(bool visible) throws()
      {
         XtSetMappedWhenManaged(widget(), static_cast< Boolean>(visible));
         // now, manage the child
         if (!XtIsManaged(widget())) {
            XtManageChild(widget());
         }
      }

      void Component::refresh() throws()
      {
      }

      ::Pixel Component::convertColorToPixel(const Color& c) const throws()
      {
         XColor color;
         Colormap colors;

         c.rgb16a(color.red, color.green, color.blue);
         ::XtVaGetValues(widget(), XmNcolormap, &colors, (XtPointer) 0);

         if (::XAllocColor(display(), colors, &color)) {
            return color.pixel;
         }
         return 0;
      }

      ::Pixel Component::convertColorToPixel(double r, double g, double b) const throws()
      {
         Color c(r, g, b, 1.);
         return convertColorToPixel(c);
      }

      void Component::setForegroundColor(const Color& c, bool) throws()
      {
         assert(c != 0);
         _fgColor = c;
         _fgPixel = convertColorToPixel(c);

         if (hasForeground()) {
            XtVaSetValues(widget(), XmNforeground, _fgPixel, (XtPointer) 0);
         }
      }

      bool Component::hasForeground() const throws()
      {
         return false;
      }

      void Component::setFont(const ::timber::Pointer< ::indigo::Font>& f, bool) throws()
      {
         try {
            _font = f;
         }
         catch (...) {
            getLog().warn("Font is not a Motif font");
            _font = nullptr;
         }
      }

      void Component::setBackgroundColor(const Color& c, bool) throws()
      {
         assert(c != 0);
         _bgColor = c;
         _bgPixel = convertColorToPixel(c);
         XtVaSetValues(widget(), XmNbackground, _bgPixel, (XtPointer) 0);

         Colormap cmap;
         XtVaGetValues(widget(), XmNcolormap, &cmap, (XtPointer) 0);
         ::Pixel top, bottom, select, border;
         XmGetColors(XtScreen(widget()), cmap, _bgPixel, &border, &top, &bottom, &select);
         XtVaSetValues(widget(), XmNtopShadowColor, top, XmNbottomShadowColor, bottom, XmNarmColor, select,
               XmNborderColor, border, (XtPointer) 0);
      }

      void Component::setExposureEventsEnabled(bool enable) throws()
      {
         if (enable) {
            ::XtAddEventHandler(widget(), ExposureMask, False, processXEvent, this);
         }
         else {
            ::XtRemoveEventHandler(widget(), ExposureMask, False, processXEvent, this);
         }
      }

      void Component::processXEvent(Widget, XtPointer w, XEvent* event, Boolean* propagate) throws()
      {
         *propagate = TRUE;
         static_cast< Component*>(w)->processEvent(*event);
      }

      void Component::processEvent(XEvent& event) throws()
      {
         static volatile bool generateButtonClick = false;

         Log lg(getLog());
         if (lg.isLoggable(Level::DEBUGGING)) {
            LogEntry(lg).debugging() << "Process event " << event << doLog;
         }
         switch (event.type) {
            case KeyPress: {
               XKeyEvent& e = event.xkey;
               const ::timber::Reference< ::nexus::peer::KeySymbol> ks = X11KeySym::create(e);
               const Time tm = eventTime(e);
               const InputModifiers& im = convertInputModifiers(e.state);
               if (!_desktop.interceptKeyPressed(ks, tm, im)) {
                  if (_keyObserver != 0) {
                     _keyObserver->keyPressed(ks, tm, im);
                  }
               }
            }
               break;
            case KeyRelease: {
               XKeyEvent& e = event.xkey;
               const ::timber::Reference< ::nexus::peer::KeySymbol> ks = X11KeySym::create(e);
               const Time tm = eventTime(e);
               const InputModifiers& im = convertInputModifiers(e.state);

               // first task is to check if this KeyRelease event is followed by
               // a KeyPress event for the same key and the same time
               // if it is, then we generate only keyTyped event and not the keyReleased event
               {
                  if (!_desktop.interceptKeyTyped(ks, tm, im)) {
                     if (_keyObserver != 0) {
                        _keyObserver->keyTyped(ks, tm, im);
                     }
                  }
               }

               // it seems that X11 places a KeyPress event into the queue immediately
               // after the KeyRelease when it generates auto-repeat key events. The "fake"
               // events will have the same serial number - so we can use it to distinguish
               // between fake events and real events
               // if we find a fake event, we just pull it off the queue and ignore it
               XEvent nextEvent;
               if (XCheckIfEvent(desktop().display(), &nextEvent, checkRepeatedEvent,
                     reinterpret_cast< char*>(&event))) {
                  // not a true key-released event
               }
               else {
                  if (!_desktop.interceptKeyReleased(ks, tm, im)) {
                     if (_keyObserver != 0) {
                        _keyObserver->keyReleased(ks, tm, im);
                     }
                  }
               }
            }
               break;
            case ButtonPress: {
               XButtonEvent& e = event.xbutton;
               if (!desktop().isMouseWheelEnabled() || e.button < 4) {
                  if (_buttonObserver != 0) {
                     _buttonObserver->buttonPressed(e.x, e.y, e.button, eventTime(e), convertInputModifiers(e.state));
                     generateButtonClick = true;
                  }
               }
            }
               break;
            case ButtonRelease: {
               XButtonEvent& e = event.xbutton;
               if (desktop().isMouseWheelEnabled() && (e.button == 4 || e.button == 5)) {
                  if (_wheelObserver != 0) {
                     _wheelObserver->wheelMoved(e.button == 4, eventTime(e), convertInputModifiers(e.state));
                  }
               }
               else if (_buttonObserver != 0) {
                  _buttonObserver->buttonReleased(e.x, e.y, e.button, eventTime(e), convertInputModifiers(e.state));
                  if (generateButtonClick) {
                     _buttonObserver->buttonClicked(e.x, e.y, e.button, eventTime(e), convertInputModifiers(e.state));
                  }
               }
            }
               generateButtonClick = false;
               break;
            case MotionNotify:
               if (_motionObserver != 0) {
                  XMotionEvent& e = event.xmotion;
                  _motionObserver->moved(e.x, e.y, eventTime(e), convertInputModifiers(e.state));
               }
               generateButtonClick = false;
               break;
            case EnterNotify:
               if (_crossingObserver != 0) {
                  XCrossingEvent& e = event.xcrossing;
                  if (e.mode == NotifyNormal
                        && (e.detail == NotifyAncestor || e.detail == NotifyInferior || e.detail == NotifyNonlinear)) {
                     _crossingObserver->componentEntered(e.time, convertInputModifiers(e.state));
                  }
               }
               generateButtonClick = false;
               break;
            case LeaveNotify:
               if (_crossingObserver != 0) {
                  XCrossingEvent& e = event.xcrossing;
                  if (e.mode == NotifyNormal
                        && (e.detail == NotifyAncestor || e.detail == NotifyInferior || e.detail == NotifyNonlinear)) {
                     _crossingObserver->componentExited(e.time, convertInputModifiers(e.state));
                  }
               }
               generateButtonClick = false;
               break;
            case FocusIn:
               if (_focusObserver != 0) {
                  XFocusChangeEvent& e = event.xfocus;
                  _focusObserver->focusGained();
               }
               break;
            case FocusOut:
               if (_focusObserver != 0) {
                  XFocusChangeEvent& e = event.xfocus;
                  _focusObserver->focusLost();
               }
               break;
            case KeymapNotify: {
               break;
            }
            case Expose: {
               break;
            }
            case GraphicsExpose: {
               break;
            }
            case NoExpose: {
               break;
            }
            case VisibilityNotify: {
               break;
            }
            case CreateNotify: {
               break;
            }
            case DestroyNotify: {
               break;
            }
            case UnmapNotify: {
               break;
            }
            case MapNotify: {
               break;
            }
            case MapRequest: {
               break;
            }
            case ReparentNotify: {
               break;
            }
            case ConfigureNotify: {
               break;
            }
            case ConfigureRequest: {
               break;
            }
            case GravityNotify: {
               break;
            }
            case ResizeRequest: {
               break;
            }
            case CirculateNotify: {
               break;
            }
            case CirculateRequest: {
               break;
            }
            case PropertyNotify: {
               break;
            }
            case SelectionClear: {
               break;
            }
            case SelectionRequest: {
               break;
            }
            case SelectionNotify: {
               break;
            }
            case ColormapNotify: {
               break;
            }
            case ClientMessage: {
               break;
            }
            case MappingNotify: {
               break;
            }
            default: {
               break;
            }
         }
      }

      void Component::setValue(const ::String name, const ::std::string& value) throws()
      {
         XmString compound;
         char* str = const_cast< char*>(value.c_str());
         char* rtag = NULL;

         if (_font != nullptr) {
            // use the font tag
            rtag = const_cast< char*>(_font->tag());
            getLog().debugging(::std::string("Creating a string with font tag ") + rtag + ":" + value);
         }

         compound = XmStringGenerate(str, NULL, XmMULTIBYTE_TEXT, rtag);
         setValue(name, compound);
         XmStringFree(compound);
      }

      void Component::notifyInsets(const ::nexus::Insets& insets) throws()
      {
         if (_componentObserver != 0) {
            _componentObserver->notifyInsets(insets);
         }
      }

      void Component::notifyBounds(const ::nexus::Bounds& bnds) throws()
      {
         if (_componentObserver != 0) {
            _componentObserver->notifyBounds(bnds);
         }
      }

   }
}
