#ifndef _NEXUS_MOTIF_COMPONENT_H
#define _NEXUS_MOTIF_COMPONENT_H

#ifndef _NEXUS_PEER_COMPONENT_H
#include <nexus/peer/Component.h>
#endif

#ifndef _Xm_h
#include <Xm/Xm.h>
#endif

#ifndef _XtintrinsicP_h
#include <X11/IntrinsicP.h>
#endif

#ifndef _NEXUS_SIZE_H
#include <nexus/Size.h>
#endif

#ifndef _NEXUS_COLOR_H
#include <nexus/Color.h>
#endif

#ifndef _NEXUS_MOTIF_H
#include <nexus/motif/motif.h>
#endif

#ifndef _NEXUS_MOTIF_MOTIFFONT_H
#include <nexus/motif/MotifFont.h>
#endif

namespace nexus {
   class Bounds;
   class Pixel;
   class Insets;

   namespace motif {
      class Desktop;
      /**
       * This interface must be implemented by classes
       * that support composition.
       *
       * @author Raimund Merkert
       * @date 18 Feb 2003
       */
      class Component : public virtual ::nexus::peer::Component
      {
            Component&operator=(const Component&) = delete;
            Component(const Component&) = delete;

            /**
             * Create a component.
             * @param p the parent component
             * @param n the name for this widget
             * @param wclass the Xt widget class
             */
         protected:
            Component(Component& p, const char* n, WidgetClass wclass) throws();

            /**
             * Create a component using the specified motif widget
             * @param p the parent component
             * @param w a motif widget
             */
         protected:
            Component(Component& p, Widget w) throws();

            /**
             * Create a component using the specified motif widget. This
             * method actually creates a top-level component.
             * @param dt the desktop
             * @param w a motif widget
             */
         protected:
            Component(Desktop& dt, Widget w) throws();

            /**
             * Destroy this component and release all its resources.
             * Calls XtDestroyWidget(widget())
             */
         public:
            ~Component() throws();

            /**
             * Get the size of this component.
             * @return the size of this component
             */
         public:
            Size size() const throws();

            /**
             * Set the bounds for this component. The default
             * is to call <code>setComponentBounds(widget(),b)</code>.
             * @param b the new bounds.
             * @pre REQUIRE_CONDITION(b,b.width()>0 && b.height()>0);
             */
         public:
            virtual void setBounds(const Bounds& b) throws();

            /**
             * Get the desktop
             * @return the desktop
             */
         public:
            inline Desktop& desktop() const throws()
            {
               return _desktop;
            }

            /**
             * Notify the component observer of the current insets
             * @param insets the new insets
             */
         public:
            void notifyInsets(const Insets& insets) throws();

            /**
             * Notify the component observer of the current bounds
             * @param bnds the current component bounds
             */
         public:
            void notifyBounds(const Bounds& bnds) throws();

            /**
             * @name Implementation of ::nexus::peer::Component.
             * @{
             */

         public:
            void setComponentObserver(ComponentObserver* l) throws();
            void setWheelObserver(WheelObserver* l) throws();
            void setButtonObserver(ButtonObserver* l) throws();
            void setMotionObserver(MotionObserver* l) throws();
            void setKeyObserver(KeyObserver* l) throws();
            void setFocusObserver(FocusObserver* l) throws();
            void setCrossingObserver(CrossingObserver* l) throws();

            void setVisible(bool visible) throws();
            void setEnabled(bool enabled) throws();

            /**
             * Refresh this component. By default, the X toolkit takes
             * care of this, but some classes may need to override it.
             */
         public:
            void refresh() throws();

            /**
             * Enable ExposureMask for this component. If enabled, then
             * exposure events are generated.
             * @param enable if true, then enable exposure events
             */
         protected:
            void setExposureEventsEnabled(bool enable) throws();

            /**
             * Associate the specifed color with this component. If the
             * method hasForeground() returns true it sets the XmNforeground property.
             * @param c a foreground color
             * @param inherited not used
             * @pre REQUIRE_NON_ZERO(c);
             */
         public:
            void setForegroundColor(const Color& c, bool inherited) throws();

            /**
             * Set the background color.
             * @param c a color
             * @param inherited not used
             * @pre REQUIRE_NON_ZERO(c);
             */
         public:
            void setBackgroundColor(const Color& c, bool inherited) throws();

            /**
             * Set the font for this component. If the font is not a MotifFont,
             * then the result of this method is undefined.
             * @param f a font id or 0
             * @param inherited not used
             */
         public:
            void setFont(const ::timber::Pointer< ::indigo::Font>& f, bool inherited) throws();

            /**
             * Get the font for this component.
             * @return a font or 0 if the font is inherited
             */
            inline ::timber::Pointer< MotifFont> font() const throws()
            {
               return _font;
            }

            /*@}*/

            /**
             * Get the foreground pixel color for this component.
             * @return the foreground color
             */
         public:
            inline ::Pixel foregroundPixel() const throws()
            {
               return _fgPixel;
            }

            /**
             * Get the background pixel color for this component.
             * @return the background color
             */
         public:
            inline ::Pixel backgroundPixel() const throws()
            {
               return _bgPixel;
            }

            /**
             * Get the foreground pixel color for this component.
             * @return the foreground color
             */
         public:
            inline const Color& foregroundColor() const throws()
            {
               return _fgColor;
            }

            /**
             * Get the background pixel color for this component.
             * @return the background color
             */
         public:
            inline const Color& backgroundColor() const throws()
            {
               return _bgColor;
            }

            /**
             * Test if this component supports a foreground.
             * @return false by default
             */
         public:
            virtual bool hasForeground() const throws();

            /**
             * @name Methods that allow access to the widget
             * @{
             */

            /**
             * Get the parent component.
             * @return the parent
             */
         public:
            inline Component* parent() const throws()
            {
               return _parent;
            }

            /**
             * Get the motif widget for this component.
             * @return the motif widget for this component
             */
         public:
            inline Widget widget() const throws()
            {
               return _widget;
            }

            /**
             * Set an individual widget property.
             * @param w a widget
             * @param name property name
             * @param value the value
             */
         public:
            template<class T>
            static inline void setValue(Widget w, const ::String name, const T& value) throws()
            {
               ::Arg arg;
               XtSetArg(arg, name, value);
               ::XtSetValues(w, &arg, 1);
            }

            /**
             * Set an individual widget property.
             * @param name property name
             * @param value the value
             */
         public:
            template<class T>
            inline void setValue(const ::String name, const T& value) throws()
            {
               setValue(widget(), name, value);
            }

            /**
             * Set an individual widget property.
             * @param name property name
             * @param value the value
             */
         public:
            inline void setValue(const ::String name, Int value) throws()
            {
               setValue(widget(), name, value);
            }

            /**
             * Set a string property.
             * @param name property name
             * @param value the string value
             */
         public:
            void setValue(const ::String name, const ::std::string& value) throws();

            /**
             * Set the bounds for a widget.
             * @param w a widget
             * @param b the bounds for the widget
             */
         public:
            static void setComponentBounds(Widget w, const Bounds& b) throws();

            /**
             * Get the component bounds of a widget
             * @param w a widget
             * @return the bounds for the widget
             */
         public:
            static Bounds getComponentBounds(Widget w) throws();

            /*@}*/

            /**
             * Compute the pixel for the specified RGB value.
             * @param c a color
             * @return a pixel
             */
         protected:
            ::Pixel convertColorToPixel(const Color& c) const throws();

            /**
             * Compute the pixel for the specified RGB values.
             * @param r the red intensity (0 to 1)
             * @param g the green intensity (0 to 1)
             * @param b the blue intensity (0 to 1)
             * @return a pixel
             */
         protected:
            ::Pixel convertColorToPixel(double r, double g, double b) const throws();

            /**
             * Process the specified X event.
             * @param event an x event
             */
         protected:
            virtual void processEvent(XEvent& event) throws();

            /**
             * Get the display of this widget.
             * @return the display for this widget
             */
         public:
            inline Display* display() const throws()
            {
               return XtDisplay(widget());
            }

            /**
             * The screen on which this component lives.
             * @return the screen for thsi component
             */
         public:
            inline Screen* screen() const throws()
            {
               return XtScreen(widget());
            }

            /**
             * Initialize some default values
             */
         private:
            void initialize() throws();

            /** Process a widget event. */
         private:
            static void processXEvent(Widget shellWidget, XtPointer w, XEvent* event, Boolean* propagate) throws();

            /**
             * Set a new observer.
             * @param mask the event mask
             * @param obs the new observer
             * @param local the local to which the new observer will be written
             */
         protected:
            template<class T>
            void setObserver(EventMask mask, T* obs, T*& local) throws()
            {
               if (obs != local) {
                  if (local != 0) {
                     ::XtRemoveEventHandler(widget(), mask, FALSE, processXEvent, this);
                  }
                  local = obs;
                  if (local != 0) {
                     ::XtAddEventHandler(widget(), mask, FALSE, processXEvent, this);
                  }
               }
            }

            /**
             * Update the event mask and register observers as necessary.
             */
         private:
            void updateEventMask() throws();

            /** The desktop component */
         private:
            Desktop& _desktop;

            /** The parent component (may be null) */
         private:
            mutable Component* _parent;

            /** The motif widget for this component */
         private:
            mutable Widget _widget;

            /** True if this component inherits its font */
         private:
            bool _inheritFont;

            /** A font */
         private:
            ::timber::Pointer< MotifFont> _font;

            /** True if this component inherits its background color */
         private:
            bool _inheritBGColor;

            /** True if this component inherits its foreground color */
         private:
            bool _inheritFGColor;

            /** The foreground pixel color */
         private:
            ::Pixel _fgPixel;

            /** The background pixel color */
         private:
            ::Pixel _bgPixel;

            /** The foreground pixel color */
         private:
            Color _fgColor;

            /** The background pixel color */
         private:
            Color _bgColor;

            /**
             * @name The various observers for different events.
             * @{
             */
         private:
            ButtonObserver* _buttonObserver;
            MotionObserver* _motionObserver;
            KeyObserver* _keyObserver;
            FocusObserver* _focusObserver;
            CrossingObserver* _crossingObserver;
            WheelObserver* _wheelObserver;
            ComponentObserver* _componentObserver;

            /** The event mask in use to enable button clicks */
         private:
            ::EventMask _eventMask;
            /*@}*/

      };
   }
}

#endif

