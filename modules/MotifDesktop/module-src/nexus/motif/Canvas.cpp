#include <iostream>
#include <nexus/motif/Canvas.h>
#include <nexus/Bounds.h>
#include <indigo/render/NodeRenderer.h>
#include <indigo/render/NodePicker.h>
#include <indigo/x11/X11RenderContext.h>
#include <indigo/x11/PixmapRenderContext.h>
#include <indigo/x11/X11Renderer.h>
#include <indigo/x11/X11ContextFactory.h>
#include <indigo/cairo/RenderContext.h>

#include <cairo-xlib.h>
#include <Xm/DrawingA.h>

#include <timber/logging.h>

using namespace ::timber::logging;

namespace nexus {
   namespace motif {

      static bool const USE_X11Context = true;
      static bool const USE_CAIRO = true;

      namespace {
         static Log log()
         {
            return Log("nexus.motif.Canvas");
         }

      }

      Canvas::Canvas(Component& c)
      throws()
            : Component(c, "canvas2d", xmDrawingAreaWidgetClass), _doubleBufferEnabled(true)
      {
         _gfx = RootGraphic::create(*this);
         _context = XCreateGC(display(), RootWindowOfScreen(screen()), 0, 0);
         ::XtAddCallback(widget(), XmNexposeCallback, repaintCB, this);
         ::XtAddCallback(widget(), XmNresizeCallback, resizeCB, this);
         this->setValue(XmNresizePolicy, static_cast< Int>(XmRESIZE_NONE));
      }

      Canvas::~Canvas()
      throws()
      {
         ::XtRemoveCallback(widget(), XmNexposeCallback, repaintCB, this);
         ::XtRemoveCallback(widget(), XmNresizeCallback, resizeCB, this);
         XFreeGC(display(), gc());
         _gfx->setRootNode(::timber::Pointer< ::indigo::Node>());
      }

      bool Canvas::hasForeground() const throws()
      {
         return true;
      }

      void Canvas::render()
      throws()
      {
         if (_gfx->needsRefresh()) {
            _gfx->refresh();
         }
      }

      void Canvas::setAutoRenderEnabled(bool enable)
      throws()
      {
         _gfx->setAutoRefreshEnabled(enable);
         if (enable && _gfx->needsRefresh()) {
            _gfx->scheduleRefresh();
         }
      }

      void Canvas::repaintCB(Widget, XtPointer client_data, XtPointer data)
      throws()
      {
         XmDrawingAreaCallbackStruct* cbs = static_cast< XmDrawingAreaCallbackStruct*>(data);
         XExposeEvent & event = static_cast< XExposeEvent&>(cbs->event->xexpose);

         if (event.count == 0 && event.send_event == False) {
            Canvas* canvas = static_cast< Canvas*>(client_data);
            // remove all expose events currently in the queue for this window;
            // if we didn't do this, then we could potentially repaint too many
            // times, especially when resizing
            if (removeExposeEvents(canvas->display(), XtWindow(canvas->widget())) > 0) {
               canvas->_gfx->cancelRefreshRequest();
            }
            canvas->_gfx->scheduleRefresh();
         }
      }

      void Canvas::resizeCB(Widget, XtPointer client_data, XtPointer)
      throws()
      {
         static_cast< Canvas*>(client_data)->_gfx->scheduleRefresh();
      }

      void Canvas::setForegroundColor(const Color& c, bool inherited)
      throws()
      {
         Component::setForegroundColor(c, inherited);
         _gfx->scheduleRefresh();
      }

      void Canvas::setBackgroundColor(const Color& c, bool inherited)
      throws()
      {
         Component::setBackgroundColor(c, inherited);
         _gfx->scheduleRefresh();
      }

      void Canvas::setDoubleBufferEnabled(bool enabled)
      throws()
      {
         _doubleBufferEnabled = enabled;
         if (!enabled) {
            _renderer = nullptr;
         }
      }

      void Canvas::setGraphic(const ::timber::Pointer< ::indigo::Node>& gfx)
      throws()
      {
         _gfx->setRootNode(gfx);
      }

      void Canvas::graphicExposed(bool)
      throws()
      {
         if (XtIsRealized(widget())) {
            paint(XtWindow(widget()));
            // cancel the request, because it might not have been
            // triggered by a graphic update, but by a some outside
            // influence.
         }
         _gfx->cancelRefreshRequest();
      }

      ::std::unique_ptr< Canvas::PickInfo> Canvas::pickNode(const ::nexus::Pixel& px)
      throws()
      {
         ::std::unique_ptr< PickInfo> res;

         if (_gfx->rootNode() == nullptr) {
            return res;
         }
         if (px.x() < 0 || px.y() < 0) {
            return res;
         }

         const Size sz(size());

         // draw directly into the buffer
         Colormap colors;
         ::XtVaGetValues(widget(), XmNcolormap, &colors, (XtPointer) 0);

         Drawable drawable = XtWindow(widget());
         ::timber::Reference< ::indigo::x11::X11RenderContext> ctx(
               new ::indigo::x11::X11RenderContext(display(), gc(), drawable, colors));
         Bounds b(px.x(), px.y(), 1, 1);

         if (USE_X11Context) {
            LogEntry(log()).debugging() << "Picking at " << px << doLog;

            // use an off-screen renderer
            ::timber::Reference< ::indigo::x11::PixmapRenderContext> pixmap(
                  new ::indigo::x11::PixmapRenderContext(ctx, sz.width(), sz.height(), DefaultDepthOfScreen(screen())));
            ::timber::Reference< ::indigo::x11::X11ContextFactory> contextBuilder =
                  ::indigo::x11::X11ContextFactory::create(pixmap, foregroundColor().color());
            ::std::unique_ptr< ::indigo::render::PickContext> context = contextBuilder->createPickContext(b.x(), b.y(),
                  b.width(), b.height(), px.x(), px.y());
            ::std::unique_ptr< ::indigo::render::NodePicker> picker = ::indigo::render::NodePicker::create();
            auto pick = picker->pick(_gfx->rootNode(), *context);
            if (pick.node || pick.info) {
               LogEntry(log()).debugging() << "Picked something at " << px << doLog;
               res.reset(new PickInfo());
               res->node = pick.node;
               res->shape = pick.shape;
               res->info = pick.info;
            }
            else {
               LogEntry(log()).debugging() << "Nothing picked at " << px << doLog;
            }
         }
         else {
            ::timber::Reference< ::indigo::x11::X11Renderer> renderer = ::indigo::x11::X11Renderer::create(ctx,
                  foregroundColor().color());
            renderer->setDefaultFont(font());
            renderer->enablePickMode(px.x(), px.y());
            renderer->render(_gfx->rootNode(), b.x(), b.y(), 1, 1);
            ::std::unique_ptr < ::indigo::x11::X11Renderer::PickInfo > info = renderer->clearPickMode();
            if (info.get()) {
               res.reset(new PickInfo());
               res->node = info->picked;
               res->shape = info->detail;
               res->info = info->info;
            }
         }
         return res;
      }

      void Canvas::paint(Drawable drawable)
      throws ()
      {
         _gfx->cancelRefreshRequest();

         if (_gfx->rootNode() == nullptr) {
            // don't forget to clear the display if there is nothing
            // to be drawn
            ::XClearWindow(display(), XtWindow(widget()));
            return;
         }

         const Size sz(size());
         assert(sz.width() > 0 && sz.height() > 0);

         if (USE_CAIRO) {
            Bounds b(0, 0, sz.width(), sz.height());
            int w = b.width();
            int h = b.height();
            cairo_surface_t* surface = cairo_xlib_surface_create(display(), drawable,
                  DefaultVisual(display(), DefaultScreen(display())), w, h);
            cairo_t* context = cairo_create(surface);

            if (_doubleBufferEnabled) {
               cairo_push_group(context);
            }

            // clear (do this after the group has been rendered)
            auto bg = backgroundColor().color();

            cairo_set_source_rgb(context, bg.red(), bg.green(), bg.blue());
            cairo_paint(context);

            {
               ::std::unique_ptr< ::indigo::render::Context> ctx = ::indigo::cairo::RenderContext::create(context,
                     b.x(), b.y(), b.width(), b.height(), w, h);
               ::std::unique_ptr< ::indigo::render::NodeRenderer> renderer = ::indigo::render::NodeRenderer::create();
               renderer->render(_gfx->rootNode(), *ctx);
            }

            if (_doubleBufferEnabled) {
               cairo_pop_group_to_source(context);
               cairo_paint(context);
            }
            cairo_destroy(context);
            cairo_surface_destroy(surface);
            return;
         }

         if (_doubleBufferEnabled) {
            if (!USE_X11Context) {
               if (_renderer == nullptr) {
                  Colormap colors;
                  ::XtVaGetValues(widget(), XmNcolormap, &colors, (XtPointer) 0);
                  try {
                     _renderer = ::indigo::x11::PixmapRenderer::create(screen(), sz.width(), sz.height(), gc(), colors,
                           backgroundColor().color(), foregroundColor().color());
                  }
                  catch (...) {
                     log().warn("Could not allocate pixmap renderer; disabling double buffering");
                     _doubleBufferEnabled = false;
                  }
               }

               if (_renderer) {
                  if (_renderer->resize(sz.width(), sz.height())) {
                     _renderer->setDefaultFont(font());
                     _renderer->setGraphic(_gfx->rootNode());
                     _renderer->setDefaultColors(backgroundColor().color(), foregroundColor().color());
                     _renderer->refresh();
                     ::XCopyArea(display(), _renderer->pixmap(), drawable, gc(), 0, 0, _renderer->width(),
                           _renderer->height(), 0, 0);
                     return;
                  }
                  log().warn("Could not resize pixmap; disabling double buffering");
                  _doubleBufferEnabled = false;
                  _renderer = nullptr;
               }
            }
            else {
               Bounds b(0, 0, sz.width(), sz.height());
               Colormap colors;
               ::XtVaGetValues(widget(), XmNcolormap, &colors, (XtPointer) 0);

               ::timber::Reference< ::indigo::x11::X11RenderContext> ctx(
                     new ::indigo::x11::X11RenderContext(display(), gc(), drawable, colors));
               ::timber::Reference< ::indigo::x11::PixmapRenderContext> pixmap(
                     new ::indigo::x11::PixmapRenderContext(ctx, sz.width(), sz.height(),
                           DefaultDepthOfScreen(screen())));

               {
                  ::XSetBackground(pixmap->display(), pixmap->gc(), backgroundPixel());
                  ::XSetForeground(pixmap->display(), pixmap->gc(), backgroundPixel());
                  ::XFillRectangle(pixmap->display(), pixmap->drawable(), pixmap->gc(), 0, 0, pixmap->width(),
                        pixmap->height());
                  ::XSetForeground(pixmap->display(), pixmap->gc(), foregroundPixel());
               }

               ::timber::Reference< ::indigo::x11::X11ContextFactory> contextBuilder =
                     ::indigo::x11::X11ContextFactory::create(pixmap, foregroundColor().color());
               ::std::unique_ptr< ::indigo::render::NodeRenderer> renderer = ::indigo::render::NodeRenderer::create();
               ::std::unique_ptr< ::indigo::render::Context> context = contextBuilder->createContext(b.x(), b.y(),
                     b.width(), b.height());
               renderer->render(_gfx->rootNode(), *context);
               ::XCopyArea(display(), pixmap->pixmap(), drawable, gc(), 0, 0, pixmap->width(), pixmap->height(), 0, 0);
               return;
            }
         }

         // if double buffering is not enabled, then we need to clear the window
         ::XClearWindow(display(), XtWindow(widget()));
         // draw directly into the buffer
         Bounds b(0, 0, sz.width(), sz.height());
         ::XSetBackground(display(), gc(), backgroundPixel());
         ::XSetForeground(display(), gc(), foregroundPixel());
         Colormap colors;
         ::XtVaGetValues(widget(), XmNcolormap, &colors, (XtPointer) 0);
         ::timber::Reference< ::indigo::x11::X11RenderContext> ctx(
               new ::indigo::x11::X11RenderContext(display(), gc(), drawable, colors));

         if (!USE_X11Context) {
            ::timber::Reference< ::indigo::x11::X11Renderer> renderer = ::indigo::x11::X11Renderer::create(ctx,
                  foregroundColor().color());
            renderer->setDefaultFont(font());
            renderer->render(_gfx->rootNode(), b.x(), b.y(), b.width(), b.height());
         }
         else {
            ::timber::Reference< ::indigo::x11::X11ContextFactory> contextBuilder =
                  ::indigo::x11::X11ContextFactory::create(ctx, foregroundColor().color());
            ::std::unique_ptr< ::indigo::render::Context> context = contextBuilder->createContext(b.x(), b.y(),
                  b.width(), b.height());
            ::std::unique_ptr< ::indigo::render::NodeRenderer> renderer = ::indigo::render::NodeRenderer::create();
            renderer->render(_gfx->rootNode(), *context);
         }
      }
   }
}
