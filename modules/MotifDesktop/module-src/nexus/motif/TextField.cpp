#include <nexus/motif/TextField.h>
#include <Xm/Text.h>
#include <Xm/TextF.h>

namespace nexus {
   namespace motif {

      TextField::TextField(Component& c) throws()
            : TextComponent(c, XmCreateTextField(c.widget(), cstr("textfield"), 0, 0)), _observer(0)
      {
         setValue(XmNresizeWidth, False);
         ::XmTextSetString(widget(), blank());
      }

      TextField::~TextField() throws()
      {
      }

      void TextField::textFieldCB(Widget, XtPointer client_data, XtPointer data) throws()
      {
         TextField* tf = static_cast< TextField*>(client_data);
         if (tf->_observer != 0) {
            XmAnyCallbackStruct* cbs = static_cast< XmAnyCallbackStruct*>(data);

            switch (cbs->reason) {
               case XmCR_ACTIVATE:
                  tf->_observer->textFieldActivated();
                  break;
               default:
                  break;
            }
         }
      }

      void TextField::setTextFieldObserver(TextFieldObserver* l) throws()
      {
         if (_observer != l) {
            if (_observer == 0 && l != 0) {
               ::XtAddCallback(widget(), XmNactivateCallback, textFieldCB, this);
            }
            else if (_observer != 0 && l == 0) {
               ::XtRemoveCallback(widget(), XmNactivateCallback, textFieldCB, this);
            }
            _observer = l;
         }
      }

   }
}
