#include <nexus/motif/TextArea.h>
#include <Xm/Text.h>
#include <cctype>

namespace nexus {
   namespace motif {

      TextArea::TextArea(Component& c) throws()
            : TextComponent(c, ::XmCreateScrolledText(c.widget(), cstr("textarea"), 0, 0))
      {
         setValue(XmNresizeHeight, False);
         setValue(XmNresizeWidth, False);
         setValue(XmNeditMode, static_cast< Int>(XmMULTI_LINE_EDIT));
         setValue(XmNscrollVertical, True);
         ::XmTextSetString(widget(), blank());
      }

      TextArea::~TextArea() throws()
      {
      }

      void TextArea::setBounds(const Bounds& b) throws()
      {
         setComponentBounds(scroller(), b);
      }

   }
}
