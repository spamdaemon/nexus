#ifndef _NEXUS_MOTIF_MENUBAR_H
#define _NEXUS_MOTIF_MENUBAR_H

#ifndef _NEXUS_PEER_MENUBAR_H
#include <nexus/peer/MenuBar.h>
#endif

#ifndef _NEXUS_MOTIF_WINDOW_H
#include <nexus/motif/Window.h>
#endif

namespace nexus {
  namespace motif {

    /**
     * This class is an implementation of a Bar component
     * using the Motif toolkit.
     * @date 06 Jun 2003
     */
    class MenuBar : public Component, public virtual ::nexus::peer::MenuBar {
      MenuBar(const MenuBar&);
      MenuBar&operator=(const MenuBar&);

      /** 
       * Create a new bar.
       * @param parent the parent component
       */
    public:
      MenuBar (Window& parent) throws();

    public:	
      ~MenuBar() throws();

      /**
       * Set the bar observer.
       * @param l a listener or observer or 0 to clear it
       */
    public:
      void setMenuBarObserver (::nexus::peer::MenuBar::Observer* l) throws();
      
      /**
       * This callback is invoked whenever the structure changes
       */
    private:
      static void fireStructureEvent(Widget, XtPointer, XEvent*, Boolean*) throws();

      /** The menubar observer */
    private:
      ::nexus::peer::MenuBar::Observer* _observer;
    };
  }
}

#endif
