#ifndef _NEXUS_MOTIF_TEXTOPERATION_H
#define _NEXUS_MOTIF_TEXTOPERATION_H

#ifndef _NEXUS_H
#include <nexus/nexus.h>
#endif

namespace nexus {
  namespace motif {
    /**
     * This class encapsulates a string opertion in an object. 
     * <ul>
     * <li> replace
     * <li> insert
     * <li> delete
     * <li> do-nothing
     * </ul>
     */
    class TextOperation {

      /** An string index */
    public:
      typedef ::std::string::size_type size_type;
      
      /** Create a NOP operation */
    public:
      inline TextOperation () throws()
	: _pos(0),_count(0) {}
      
      /** 
       * Create a text operation that creates the specified string.
       * @param s a string or 0 for a NOP operation
       */
    public:
      inline TextOperation (const ::std::string& s) throws()
	: _source(s), _pos(0),_count(0) {}
      
      /**
       * Replace a portion of the text created by this text operation.
       * @param off an offset
       * @param len the number of characters to replace
       * @param s the text being replaced
       */
    public:
      void replace ( size_type off, size_type len, const ::std::string& s) throws();
      
      /**
       * Insert the specified string.
       * @param off an offset
       * @param s the text to be inserted
       */
    public:
      inline void insert (size_type off, const ::std::string& s) throws()
      { replace(off,0,s); }
      
      /**
       * Erase a substring.
       * @param off the first character to be deleted
       * @param len the number of characters to be deleted
       */
    public:
      inline void erase (size_type off, size_type len) throws()
      { replace(off,len,::std::string()); }

      /**
       * Normalize this operation. The effect is that of 
       */
    public:
      inline ::std::string normalize () throws()
      { 
	*this = TextOperation(apply()); 
	return _source;
      }
	  
      /**
       * Get the replacement text with respect to the original.
       * @return the text that replaces a section of the original text
       */
    public:
      inline const ::std::string& text() const throws() 
      { return _text; }
      
      /**
       * Get the replacement text with respect to the original.
       * @return the text that replaces a section of the original text
       */
    public:
      inline ::std::string& text() throws() 
      { return _text; }
	  
      /**
       * Get the start position. 
       * @return the index of the first character to be replaced
       */
    public:
      inline size_type position () const throws() { return _pos; }

      /**
       * Get the number of characters to be replaced.
       * @return the number of characters replaced
       */
    public:
      inline Int count () const throws() { return _count; }

      /**
       * Apply this text operation.
       * @return a string that results if this operation is applied to
       * the original string
       */
    public:
      ::std::string apply () const throws();

      /** The string to which to apply this operation */
    private:
      ::std::string _source;

      /** The character position at which to insert, delete, or replace */
    private:
      size_type _pos;

      /** The number of characters to replace or delete */
    private:
      size_type _count;

      /** The string that is inserted */
    private:
      ::std::string _text;
    };
  }
}

#endif
