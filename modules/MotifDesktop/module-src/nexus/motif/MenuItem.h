#ifndef _NEXUS_MOTIF_MENUITEM_H
#define _NEXUS_MOTIF_MENUITEM_H

#ifndef _NEXUS_PEER_MENUITEM_H
#include <nexus/peer/MenuItem.h>
#endif

#ifndef _NEXUS_MOTIF_COMPONENT_H
#include <nexus/motif/Component.h>
#endif

#ifndef _INDIGO_X11_PIXMAPRENDERER_H
#include <indigo/x11/PixmapRenderer.h>
#endif

#ifndef _NEXUS_MOTIF_ROOTGRAPHIC_H
#include <nexus/motif/RootGraphic.h>
#endif

#ifndef _NEXUS_ICON_H
#include <nexus/Icon.h>
#endif

namespace nexus {
  namespace motif {
    class Menu;

    /**
     * This class is an implementation of a  component
     * using the Motif toolkit.
     * @date 06 Jun 2003
     */
    class MenuItem : public Component, public virtual ::nexus::peer::MenuItem {
      MenuItem(const MenuItem&);
      MenuItem&operator=(const MenuItem&);

     /** 
       * Create a new menu as a top-level menu on the menubar.
       * @param parent the parent component
       */
    public:
      MenuItem (Menu& parent) throws();
      
    public:	
      ~MenuItem() throws();

    public:
      void setText (const ::std::string& t) throws();
      void setIcon (const Icon& i) throws();

        /**
       * Set the  observer.
       * @param l a listener or observer or 0 to clear it
       */
    public:
      void setMenuItemObserver (::nexus::peer::MenuItem::Observer* l) throws();

      bool hasForeground () const throws();
      void setForegroundColor (const Color& c, bool inherited) throws();
      void setBackgroundColor (const Color& c, bool inherited) throws();
      void setBorderWidth (Int w) throws();

      /**
       * Set the font for this button.
       * This method has no effect on the display if the button uses an icon
       * instead of a text string.
       * @param f a font id
       * @param inherited not used
       */
    public:
      void setFont (const ::timber::Pointer< ::indigo::Font>& f, bool inherited) throws();

      /** 
       * The graphic has been exposed after a scheduled expose event.
       * @param requested true if this is due to a requested expose event
       */
    public:
      void graphicExposed (bool requested) throws();
      
      /**
       * The callback for arming, disarming, and activation
       */
    private:
      static void menuItemCB(Widget, XtPointer, XtPointer) throws();

      /**
       * This callback is invoked whenever the structure changes
       */
    private:
      static void fireStructureEvent(Widget, XtPointer, XEvent*, Boolean*) throws();

      /** A pixmap renderer */
    private:
      ::timber::Pointer< ::indigo::x11::PixmapRenderer> _renderer;
	
      /** A root graphic */
    private:
      ::std::unique_ptr<RootGraphic> _graphic;

      /** The menu observer */
    private:
      ::nexus::peer::MenuItem::Observer* _observer;

      /** The parent menu */
    private:
      Menu& _menu;
    };
  }
}

#endif
