#ifndef _NEXUS_MOTIF_LABEL_H
#define _NEXUS_MOTIF_LABEL_H

#ifndef _NEXUS_ICON_H
#include <nexus/Icon.h>
#endif

#ifndef _NEXUS_PEER_LABEL_H
#include <nexus/peer/Label.h>
#endif

#ifndef _NEXUS_MOTIF_COMPONENT_H
#include <nexus/motif/Component.h>
#endif

#ifndef _INDIGO_X11_PIXMAPRENDERER_H
#include <indigo/x11/PixmapRenderer.h>
#endif

#ifndef _NEXUS_MOTIF_ROOTGRAPHIC_H
#include <nexus/motif/RootGraphic.h>
#endif

namespace nexus {
  namespace motif {

    /**
     * This class is an implementation of a Label component
     * using the Motif toolkit.
     * @date 06 Jun 2003
     */
    class Label : public Component, public virtual ::nexus::peer::Label {
      Label&operator=(const Label&);
      Label(const Label&);
	
      /** 
       * Create a new label.
       * @param parent the parent component
       */
    public:
      Label (Component& parent) throws();

    public:	
      ~Label() throws();

    public:
      void setAlignment (const HorizontalAlignment& align) throws();
      void setText (const ::std::string& t) throws();
      void setIcon (const Icon& i) throws();


      bool hasForeground () const throws();
      void setForegroundColor (const Color& c, bool inherited) throws();
      void setBackgroundColor (const Color& c, bool inherited) throws();

      /**
       * Set the font for this label.
       * This method has no effect on the display if the label uses an icon
       * instead of a text string.
       * @param f a font id
       * @param inherited not used
       */
    public:
      void setFont (const ::timber::Pointer< ::indigo::Font>& f, bool inherited) throws();

      /** A pixmap renderer */
    private:
      ::timber::Pointer< ::indigo::x11::PixmapRenderer> _renderer;

      /** 
       * The graphic has been exposed after a scheduled expose event.
       * @param requested true if this is due to a requested expose event
       */
    public:
      void graphicExposed (bool requested) throws();
	
      /** A root graphic */
    private:
      ::std::unique_ptr<RootGraphic> _graphic;
    };
  }
}

#endif
