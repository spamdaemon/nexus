#ifndef _NEXUS_MOTIF_H
#define _NEXUS_MOTIF_H

namespace nexus {
   /**
    * This namespace provides a Motif implementation
    * for all components in the native package.
    */
   namespace motif {

      /**
       * A C string.
       * @param str a const string
       * @return the string converted to a non-const pointer.
       */
      inline char* cstr(const char* str)
      {
         return const_cast< char*>(str);
      }

      /**
       * Get an empty const string as a non-const string. Several motif functions require passing
       * a string. Even though the string is constant, the in C++ we cannot use a string literal
       * because it is marked as const, which C doesn't understand.
       * @return an empty string
       */
      inline char* blank()
      {
         return cstr("");
      }

   }
}

#endif
