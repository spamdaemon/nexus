#include <nexus/motif/Menu.h>
#include <nexus/motif/MenuBar.h>
#include <nexus/Bounds.h>

#include <timber/logging.h>

#include <Xm/CascadeB.h>
#include <Xm/RowColumn.h>

using namespace ::timber::logging;

namespace nexus {
   namespace motif {

      Menu::Menu(Menu* p) throws()
            : Component(*p, XtVaCreateManagedWidget(blank(), xmCascadeButtonWidgetClass, p->getMenuWidget(), NULL)), _graphic(
                  RootGraphic::create(*this)), _observer(0), _nChildren(0), _pulldown(
                  XmCreatePulldownMenu(p->widget(), blank(), NULL, 0))
      {
         ::XtAddEventHandler(widget(), StructureNotifyMask, FALSE, fireStructureEvent, this);
      }

      Menu::Menu(MenuBar& p) throws()
            : Component(p, XtVaCreateManagedWidget(blank(), xmCascadeButtonWidgetClass, p.widget(), NULL)), _graphic(
                  RootGraphic::create(*this)), _observer(0), _nChildren(0), _pulldown(
                  XmCreatePulldownMenu(p.widget(),blank(), NULL, 0))
      {
         Log("nexus.motif.Menu").debugging("Creating top-level menu");
         ::XtAddEventHandler(widget(), StructureNotifyMask, FALSE, fireStructureEvent, this);
      }

      Menu::~Menu() throws()
      {
         ::XtRemoveEventHandler(widget(), StructureNotifyMask, FALSE, fireStructureEvent, this);
         XtDestroyWidget(_pulldown);
      }

      Widget Menu::getMenuWidget() throws()
      {
         if (++_nChildren == 1) {
            setValue(XmNsubMenuId, _pulldown);
         }
         return _pulldown;
      }

      void Menu::destroyChildWidget() throws()
      {
         if (_pulldown != 0) {
            if (--_nChildren == 0) {
               setValue(XmNsubMenuId, 0);
            }
         }
      }

      void Menu::setMenuObserver(::nexus::peer::Menu::Observer* l) throws()
      {
         _observer = l;
      }

      bool Menu::hasForeground() const throws()
      {
         return true;
      }

      void Menu::setFont(const ::timber::Pointer< ::indigo::Font>& f, bool inherited) throws()
      {
         Component::setFont(f, inherited);
         if (_renderer == nullptr && font() != nullptr) {
            setValue(XmNrenderTable, font()->font());
         }
      }

      void Menu::setForegroundColor(const Color& c, bool inherited) throws()
      {
         Component::setForegroundColor(c, inherited);
         if (_renderer != nullptr) {
            _renderer->setDefaultColors(backgroundColor().color(), foregroundColor().color());
            _graphic->scheduleRefresh();
         }
      }

      void Menu::setBackgroundColor(const Color& c, bool inherited) throws()
      {
         Component::setBackgroundColor(c, inherited);
         if (_renderer != nullptr) {
            _renderer->setDefaultColors(backgroundColor().color(), foregroundColor().color());
            _graphic->scheduleRefresh();
         }
      }

      void Menu::setText(const ::std::string& t) throws()
      {
         Log("nexus.motif.Menu").debugging("Setting menu " + t);
         _graphic->setRootNode((::indigo::Node*) 0);
         _graphic->cancelRefreshRequest();

         if (_renderer != nullptr) {
            _renderer = nullptr;
            setValue(XmNlabelType, static_cast< Int>(XmSTRING));

            // set the font
            if (font() != nullptr) {
               setValue(XmNrenderTable, font()->font());
            }
         }
         setValue(XmNlabelString, t);
      }

      void Menu::setIcon(const Icon& icon) throws()
      {
         if (_renderer == nullptr) {
            Colormap colors;
            ::XtVaGetValues(widget(), XmNcolormap, &colors, (XtPointer) 0);
            try {
               _renderer = ::indigo::x11::PixmapRenderer::create(screen(), icon.width(), icon.height(), 0, colors,
                     backgroundColor().color(), foregroundColor().color());
            }
            catch (...) {
            }
         }

         if (_renderer != nullptr && _renderer->resize(icon.width(), icon.height())) {
            _renderer->setGraphic(icon.graphic());
            setValue(XmNlabelType, static_cast< Int>(XmPIXMAP));
            _graphic->setRootNode(icon.graphic());
         }
         else {
            Log("nexus.motif.Menu").debugging("Cannot create pixmap for icon; not rendering anything");
            _renderer = nullptr;
            setValue(XmNlabelType, static_cast< Int>(XmSTRING));
            setValue(XmNlabelString, blank());
         }
      }

      void Menu::graphicExposed(bool) throws()
      {
         if (_renderer != nullptr) {
            _renderer->refresh();
            setValue(XmNlabelPixmap, _renderer->pixmap());
         }
      }

      void Menu::fireStructureEvent(Widget x, XtPointer mptr, XEvent* event, Boolean* propagate) throws()
      {
         *propagate = TRUE;
         Menu* m = static_cast< Menu*>(mptr);
         switch (event->type) {
            case ConfigureNotify: {
               XConfigureEvent& e = event->xconfigure;
               Bounds bnds(e.x, e.y, e.width, e.height);
               m->notifyBounds(bnds);
               break;
            }
            case MapNotify: {
               Bounds bounds = Component::getComponentBounds(m->widget());
               m->notifyBounds(bounds);
               break;
            }
            case UnmapNotify: {
               // ignore
               break;
            }
            default:
               break;
         }
      }

      void Menu::menuCB(Widget, XtPointer client_data, XtPointer data) throws()
      {
#if 0
         PushButton* btn = static_cast<PushButton*>(client_data);
         if (btn->_observer!=0) {
            XmPushButtonCallbackStruct* cbs = static_cast<XmPushButtonCallbackStruct*>(data);

            switch (cbs->reason) {
               case XmCR_ACTIVATE:
               btn->_observer->pushButtonActivated();
               break;
               case XmCR_ARM:
               btn->_observer->pushButtonPressed();
               break;
               case XmCR_DISARM:
               btn->_observer->pushButtonReleased();
               break;
               default:
               break;
            }
         }
#endif
      }
   }
}
