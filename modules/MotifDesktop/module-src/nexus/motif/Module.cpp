#include <nexus/motif/Desktop.h>
#include <nexus/DesktopModule.h>
#include <nexus/peer/Desktop.h>
#include <iostream>

namespace {
  static ::nexus::peer::Desktop* _desktop = 0;
  
  static int initPeer (int argc, const char** argv)
  {
    if (_desktop) {
      return 1;
    }
    try {
      ::timber::Reference< ::nexus::peer::Desktop> dt = ::nexus::motif::Desktop::create(argc,argv);
      dt->incrementCount(); // reference the desktop, so that we don't loose it
      _desktop = &*dt; // at this point, we've can keep savely obtain a pointer to the desktop
      return 0;
    }
    catch (const ::std::exception& e) {
      ::std::cerr << "Failed to initialize desktop: " << e.what() << ::std::endl;
      return -1;
    }
    catch (...) {
      return -1;
    }
  }
  
  static ::timber::Pointer< ::nexus::peer::Desktop> getPeer()
  { return _desktop; }
  
  static void shutdownPeer()
  {
    // do nothing
  }
}

extern "C" {
  ::nexus::DesktopModule peerModule = {
    initPeer,
    getPeer,
    shutdownPeer
  };
}

