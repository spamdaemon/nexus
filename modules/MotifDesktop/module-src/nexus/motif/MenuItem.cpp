#include <nexus/motif/MenuItem.h>
#include <nexus/motif/Menu.h>
#include <nexus/Bounds.h>

#include <timber/logging.h>

#include <Xm/PushB.h>

using namespace ::timber::logging;

namespace nexus {
   namespace motif {

      MenuItem::MenuItem(Menu& p) throws()
            : Component(p, XtVaCreateManagedWidget(blank(), xmPushButtonWidgetClass, p.getMenuWidget(), NULL)), _graphic(
                  RootGraphic::create(*this)), _observer(0), _menu(p)
      {
         Log("nexus.motif.MenuItem").debugging("Creating menu item");

         ::XtAddCallback(widget(), XmNactivateCallback, menuItemCB, this);
         ::XtAddCallback(widget(), XmNarmCallback, menuItemCB, this);
         ::XtAddCallback(widget(), XmNdisarmCallback, menuItemCB, this);

         ::XtAddEventHandler(widget(), StructureNotifyMask, FALSE, fireStructureEvent, this);
      }

      MenuItem::~MenuItem() throws()
      {
         ::XtRemoveEventHandler(widget(), StructureNotifyMask, FALSE, fireStructureEvent, this);

         ::XtRemoveCallback(widget(), XmNactivateCallback, menuItemCB, this);
         ::XtRemoveCallback(widget(), XmNarmCallback, menuItemCB, this);
         ::XtRemoveCallback(widget(), XmNdisarmCallback, menuItemCB, this);
         _menu.destroyChildWidget();
      }

      void MenuItem::setMenuItemObserver(::nexus::peer::MenuItem::Observer* l) throws()
      {
         _observer = l;
      }

      bool MenuItem::hasForeground() const throws()
      {
         return true;
      }

      void MenuItem::setFont(const ::timber::Pointer< ::indigo::Font>& f, bool inherited) throws()
      {
         Component::setFont(f, inherited);
         if (_renderer == nullptr && font() != nullptr) {
            setValue(XmNrenderTable, font()->font());
         }
      }

      void MenuItem::setForegroundColor(const Color& c, bool inherited) throws()
      {
         Component::setForegroundColor(c, inherited);
         if (_renderer != nullptr) {
            _renderer->setDefaultColors(backgroundColor().color(), foregroundColor().color());
            _graphic->scheduleRefresh();
         }
      }

      void MenuItem::setBackgroundColor(const Color& c, bool inherited) throws()
      {
         Component::setBackgroundColor(c, inherited);
         if (_renderer != nullptr) {
            _renderer->setDefaultColors(backgroundColor().color(), foregroundColor().color());
            _graphic->scheduleRefresh();
         }
      }

      void MenuItem::setText(const ::std::string& t) throws()
      {
         Log("nexus.motif.MenuItem").debugging("Setting menu item text " + t);
         _graphic->setRootNode((::indigo::Node*) 0);
         _graphic->cancelRefreshRequest();

         if (_renderer != nullptr) {
            _renderer = nullptr;
            setValue(XmNlabelType, static_cast< Int>(XmSTRING));

            // set the font
            if (font() != nullptr) {
               setValue(XmNrenderTable, font()->font());
            }
         }
         setValue(XmNlabelString, t);
      }

      void MenuItem::setIcon(const Icon& icon) throws()
      {
         if (_renderer == nullptr) {
            Colormap colors;
            ::XtVaGetValues(widget(), XmNcolormap, &colors, (XtPointer) 0);
            try {
               _renderer = ::indigo::x11::PixmapRenderer::create(screen(), icon.width(), icon.height(), 0, colors,
                     backgroundColor().color(), foregroundColor().color());
            }
            catch (...) {
            }
         }

         if (_renderer != nullptr && _renderer->resize(icon.width(), icon.height())) {
            _renderer->setGraphic(icon.graphic());
            setValue(XmNlabelType, static_cast< Int>(XmPIXMAP));
            _graphic->setRootNode(icon.graphic());
         }
         else {
            Log("nexus.motif.MenuItem").debugging("Cannot create pixmap for icon; not rendering anything");
            _renderer = nullptr;
            setValue(XmNlabelType, static_cast< Int>(XmSTRING));
            setValue(XmNlabelString, blank());
         }
      }

      void MenuItem::graphicExposed(bool) throws()
      {
         if (_renderer != nullptr) {
            _renderer->refresh();
            setValue(XmNlabelPixmap, _renderer->pixmap());
         }
      }

      void MenuItem::fireStructureEvent(Widget x, XtPointer mptr, XEvent* event, Boolean* propagate) throws()
      {
         *propagate = TRUE;
         Menu* m = static_cast< Menu*>(mptr);
         switch (event->type) {
            case ConfigureNotify: {
               XConfigureEvent& e = event->xconfigure;
               Bounds bnds(e.x, e.y, e.width, e.height);
               m->notifyBounds(bnds);
               break;
            }
            case MapNotify: {
               Bounds bounds = Component::getComponentBounds(m->widget());
               m->notifyBounds(bounds);
               break;
            }
            case UnmapNotify: {
               // ignore
               break;
            }
            default:
               break;
         }
      }

      void MenuItem::menuItemCB(Widget, XtPointer client_data, XtPointer data) throws()
      {
         MenuItem* btn = static_cast< MenuItem*>(client_data);
         if (btn->_observer != 0) {
            XmPushButtonCallbackStruct* cbs = static_cast< XmPushButtonCallbackStruct*>(data);

            switch (cbs->reason) {
               case XmCR_ACTIVATE:
                  btn->_observer->menuActivated();
                  break;
#if 0
                  case XmCR_ARM:
                  btn->_observer->pushButtonPressed();
                  break;
                  case XmCR_DISARM:
                  btn->_observer->pushButtonReleased();
                  break;
#endif
               default:
                  break;
            }
         }
      }
   }
}
