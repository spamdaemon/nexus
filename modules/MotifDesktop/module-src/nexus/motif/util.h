#ifndef _NEXUS_MOTIF_UTIL_H
#define _NEXUS_MOTIF_UTIL_H

#ifndef _Xm_h
#include <Xm/Xm.h>
#endif

#ifndef _NEXUS_H
#include <nexus/nexus.h>
#endif

#include <iosfwd>

namespace nexus {
   namespace motif {
      /** Forward declaration of component */
      class Component;

      /**
       * Send an expose event to the specified component.
       * @param c a component
       * @return true if an expose event was sent, false otherwise
       */
      bool sendExposeEvent (Component& c) throws();

      /**
       * Remove all expose events from the event queue for a given XWindow.
       * @param dp a display
       * @param win an X window
       * @return the number of events that have been removed from the queue
       */
      Int removeExposeEvents(Display* dp, ::Window win)
      throws();

      /**
       * Add a destruction callback to window to close another window.
       * @param w the widget to which a callback should be attached
       * @param t the widget that should be destroyed when the callback is triggered
       */
      void addDestructor(Widget w, Widget t)
      throws();

      /**
       * Remove a destruction callback previously set with addDestructor.
       * @param w the widget from which a callback should be removed
       * @param t the widget that would have been destroyed if the callback had been triggered.
       */
      void removeDestructor(Widget w, Widget t)
   throws();
}
}

/**
 * Print an xevent to an output stream.
 * @param out an output stream
 * @param event an xevent
 * @return out
 */
::std::ostream& operator<<(::std::ostream& out, const XEvent& event)
throws();

#endif
