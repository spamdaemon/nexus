#include <iostream>
#include <canopy/hash.h>
#include <nexus/motif/MotifFont.h>

#include <timber/logging.h>

using namespace ::timber::logging;

namespace nexus {
   namespace motif {

      const char MotifFont::TAG_NAME[] = { "OPAQUE_TAG" };

      ::timber::Pointer< MotifFont> MotifFont::create(const ::timber::Reference< ::indigo::FontFactory> ff, Widget w,
            const char* spec) throws()
      {
         Display* dpy = XtDisplay(w);
         ::XmFontListEntry ft = ::XmFontListEntryLoad(dpy, const_cast< char*>(spec), XmFONT_IS_FONT,
               const_cast< char*>(TAG_NAME));

         LogEntry logger("nexus.motif.MotifFont");
         if (ft == nullptr) {
            logger.warn() << "No font found for spec " << spec << doLog;
            return ::timber::Pointer< MotifFont>();
         }

         XmFontType ftType;
         XFontStruct* ftPointer = static_cast< ::XFontStruct*>(XmFontListEntryGetFont(ft, &ftType));
         Arg args;
         XtSetArg(args, XmNfont, ftPointer);
         ::XmRendition rend = ::XmRenditionCreate(w, const_cast< char*>(TAG_NAME), &args, 1);

         ::timber::Pointer< MotifFont> font(new MotifFont(ff));
         font->_display = dpy;
         font->_fontID = ftPointer->fid;
         font->_font = ::XmRenderTableAddRenditions(0, &ft, 1, XmMERGE_REPLACE);
         font->_xlfd = ::std::string(spec);
         XmRenditionFree(rend);
         XmFontListEntryFree(&ft);

         // load the xft font
         font->_xftFont = nullptr;
         for (int i = 0; font->_xftFont == nullptr && i < ScreenCount(dpy); ++i) {
            if (ScreenOfDisplay(dpy, i) == XtScreen(w)) {
               logger.tracing() << "Created Xft font with Xfld spec " << spec << doLog;
               font->_xftFont = XftFontOpenXlfd(dpy, i, spec);
            }
         }
         if (font->_xftFont == nullptr) {
            logger.warn() << "Failed to create a font from description " << spec << doLog;
            font = ::timber::Pointer< MotifFont>();
         }
         else if (logger.isLoggable(Level::DEBUGGING)) {
            logger.debugging() << "Created a motif font from spec " << spec << doLog;
         }
         return font;
      }

      MotifFont::MotifFont(const ::timber::Reference< ::indigo::FontFactory>& ff) throws()
            : _display(nullptr), _xftFont(nullptr), _factory(ff)
      {
      }

      MotifFont::~MotifFont() throws()
      {
         if (_xftFont != nullptr) {
            XftFontClose(_display, _xftFont);
         }
         XmRenderTableFree(_font);
      }

      ::indigo::FontDescription MotifFont::description() const throws()
      {
         return _description;
      }

      size_t MotifFont::hashValue() const throws()
      {
         return ::canopy::hashString(_xlfd.c_str(), _xlfd.length());
      }

      bool MotifFont::equals(const ::indigo::Font& f) const throws()
      {
         const MotifFont* ft = dynamic_cast< const MotifFont*>(&f);
         if (ft == 0) {
            return false;
         }
         return _xlfd == ft->_xlfd;
      }
      ::indigo::Rectangle MotifFont::stringBounds(const ::std::string& s) const throws()
      {
         Dimension w, h;
         char* str = const_cast< char*>(s.c_str());
         char* rtag = const_cast< char*>(TAG_NAME);
         XmString xstr = XmStringGenerate(str, NULL, XmMULTIBYTE_TEXT, rtag);
         XmStringExtent(_font, xstr, &w, &h);
         XmStringFree(xstr);
         return ::indigo::Rectangle(w, h);
      }

      ::Font MotifFont::x11Font() const throws()
      {
         return _fontID;
      }

      ::XftFont* MotifFont::xftFont() const throws()
      {
         return _xftFont;
      }

   }
}
