#include <nexus/motif/GlassLayer.h>
#include <cassert>

namespace nexus {
  namespace motif {

    GlassLayer::GlassLayer(Widget w) throws() 
    : _widget(w),_glass(0)
    {
      ::XtAddEventHandler(_widget,StructureNotifyMask,FALSE,processXEvent,this);
      ::XtAddEventHandler(_widget,SubstructureNotifyMask,FALSE,processXEvent,this);
      showGlassLayer();	
    }

    GlassLayer::~GlassLayer() throws() 
    {
      ::XtRemoveEventHandler(_widget,StructureNotifyMask,FALSE,processXEvent,this);
      ::XtRemoveEventHandler(_widget,SubstructureNotifyMask,FALSE,processXEvent,this);
      hideGlassLayer();
    }

    void GlassLayer::processXEvent(Widget, XtPointer w, XEvent* event, Boolean* propagate) throws()
    { 
      *propagate = TRUE;
      GlassLayer* c = static_cast<GlassLayer*>(w);
      Display* dp = XtDisplay(c->_widget);

      switch (event->type) {
      case CreateNotify: {
	if (c->_glass!=0) {
	  ::XRaiseWindow(dp,c->_glass);
	}
	break;
      }
      case DestroyNotify: {
	const XDestroyWindowEvent& e = event->xdestroywindow;
	if (e.window==c->_glass) {
	  c->_glass = 0;
	  // no need to hide it since it's already destroyed
	}
	break;
      }
      case UnmapNotify: {
	const XUnmapEvent& e = event->xunmap;
	if (c->_glass!=0 && e.window==XtWindow(c->_widget)) {
	  c->hideGlassLayer();
	}
	break;
      }
      case MapNotify: {
	const XMapEvent& e = event->xmap;
	if (c->_glass==0 && e.window==XtWindow(c->_widget)) {
	  c->showGlassLayer();
	}
	if (c->_glass!=0) {
	  ::XRaiseWindow(dp,c->_glass);
	}
	break;
      }
      case ConfigureNotify: {
	const XConfigureEvent& e = event->xconfigure;
	if (c->_glass!=0 && e.window==XtWindow(c->_widget)) {
	  ::XResizeWindow(dp,c->_glass,e.width,e.height);
	}
	break;
      }
      }
    }
      
    void GlassLayer::hideGlassLayer() throws()
    {
      if (_glass!=0) {
	if (XtIsRealized(_widget)) {
	  // can only destroy the glass if it's not
	  // already been destroyed
	  ::XDestroyWindow(XtDisplay(_widget),_glass);
	}
	_glass = 0;
      }
    }
      
    void GlassLayer::showGlassLayer() throws()
    {
      if (XtIsRealized(_widget)) {
	assert(_glass==0);
	Dimension w,h;
	::XtVaGetValues(_widget,XmNwidth,&w,XmNheight,&h,(XtPointer)0);
	_glass = ::XCreateWindow(XtDisplay(_widget),XtWindow(_widget),0,0,w,h,0,
				 CopyFromParent,InputOnly,CopyFromParent,0,0);
	::XMapWindow(XtDisplay(_widget),_glass);
      }
    }
      
  }
}
