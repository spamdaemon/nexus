#ifndef _NEXUS_MOTIF_TEXTAREA_H
#define _NEXUS_MOTIF_TEXTAREA_H

#ifndef _NEXUS_PEER_TEXTAREA_H
#include <nexus/peer/TextArea.h>
#endif

#ifndef _NEXUS_MOTIF_TEXTCOMPONENT_H
#include <nexus/motif/TextComponent.h>
#endif

namespace nexus {
  namespace motif {

    /**
     * This class is an implementation of a TextArea component
     * using the Motif toolkit.
     * @date 06 Jun 2003
     */
    class TextArea : public TextComponent, public virtual ::nexus::peer::TextArea {
      /** No default methods allowed */
      TextArea&operator=(const TextArea&);
      TextArea(const TextArea&);

      /** 
       * Create a new TextArea.
       * @param parent the parent component
       */
    public:
      TextArea (Component& parent) throws();

    public:	
      ~TextArea() throws();

      /**
       * Get the scroll widget.
       * @return a scrollable window
       */
    public:
      inline Widget scroller() const throws()
      { return XtParent(widget()); }

    public:
      void setBounds (const Bounds& b) throws();
    };
  }
}

#endif
