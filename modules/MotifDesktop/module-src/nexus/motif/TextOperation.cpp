#include <nexus/motif/TextOperation.h>

namespace nexus {
  namespace motif {
    void TextOperation::replace (size_type off, size_type len, const ::std::string& s) throws()
    {
      assert((off+len) <= (_source.length()-_count+_text.length()));

      if (_count == 0 && _text.length()==0) {
	_pos = off;
	_count = len;
	_text = s;
	return;
      }
      

      // the indexes in the replacement that correspond to xs0 and xe0
      const size_type xs0 = _pos;
      const size_type xe0 = xs0+_text.length();

      // the indices of the new replacement with respect to the replaced string
      const size_type ys0 = off;
      const size_type ye0 = ys0+len;

      // the indices of the current operation with respect to the original string
      const size_type xs1 = _pos;
      const size_type xe1 = xs1+_count;

      // the indices of the new replacement with respect to the original string
      const size_type ys1 = (ys0 < xe0) ? ys0 : (xe1 + ys0-xe0);
      const size_type ye1 = (ye0 < xe0) ? ye0 : (xe1 + ye0-xe0);

      if (ys0 >= xs0 && ye0 <= xe0) {
	_text.replace(ys0-xs0,len,s);
	return;
      }
      if (ys0 <= xs0 && ye0 >= xe0) {
	_text = s;
	_pos = ys0;
	_count += xs0-ys0 + ye0-xe0;
	return;
      }	

      if (ye0 <= xs0) {
	::std::string tmp(s);
	tmp += _source.substr(ye1,xs1-ye1);
	tmp += _text;
	_text.swap(tmp);
	_count += xs1-ye1 + len;
	_pos = ys1;
	return;
      }
      
      if (ys0 >= xe0) {
	::std::string tmp(_text);
	tmp += _source.substr(xe1,ys1-xe1);
	tmp += s;
	_text.swap(tmp);
	_count += len + ys1-xe1;
	return;
      }
      if (ys0 < xs0) {
	assert(ye0 >= xs0 && ye0 <= xe0);
	::std::string tmp(s);
	tmp += _text.substr(ye0-xs0,xe0-ye0);
	tmp.swap(_text);
	_count += xs0-ys0;
	_pos = ys1;
	return;
      }
      if (ys0 >= xs0) {
	assert (ye0 > xe0);
	::std::string tmp(_text,0,ys0-xs0);
	tmp += s;
	_text.swap(tmp);
	_count += ye0-xe0;
	return;
      }
      assert(false);
    }

    ::std::string TextOperation::apply () const throws()
    {
      ::std::string res(_source);
      res.replace(_pos,_count,_text);
      return res;
    }
      
  }
}
