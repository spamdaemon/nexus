#ifndef _NEXUS_MOTIF_CANVAS_H
#define _NEXUS_MOTIF_CANVAS_H

#ifndef _NEXUS_MOTIF_COMPONENT_H
#include <nexus/motif/Component.h>
#endif

#ifndef _NEXUS_MOTIF_ROOTGRAPHIC_H
#include <nexus/motif/RootGraphic.h>
#endif

#ifndef _NEXUS_PEER_CANVAS_H
#include <nexus/peer/Canvas.h>
#endif

#ifndef _INDIGO_X11_PIXMAPRENDERER_H
#include <indigo/x11/PixmapRenderer.h>
#endif 

namespace nexus {
  namespace motif {
    
    /**
     * This is an implementation of a 2d canvas. The canvas
     * uses a XmDrawingArea widget, which is actually
     * a container. This means that Canvas object may act
     * as containers.
     *
     * @author Raimund Merkert
     * @date 18 Feb 2003
     */
    class Canvas : public Component, public virtual ::nexus::peer::Canvas {
      Canvas&operator=(const Canvas&) = delete;
      Canvas(const Canvas&) = delete;

      /** 
       * Create a new canvas.
       * @param parent the parent component
       */
    public:
      Canvas (Component& parent) throws();

      /** Destroy this Canvas */
    public:
      ~Canvas () throws();
	
      /**
       * @name Overridden methods
       * @{
       */
    public:
      bool hasForeground () const throws();
      void setForegroundColor (const Color& c, bool inherited) throws();
      void setBackgroundColor (const Color& c, bool inherited) throws();
      void setGraphic (const ::timber::Pointer< ::indigo::Node>& gfx) throws();
      void render() throws();
      void setAutoRenderEnabled (bool enabled) throws();
      void setDoubleBufferEnabled (bool enabled) throws();
      ::std::unique_ptr<PickInfo> pickNode(const ::nexus::Pixel& px) throws();
      /*@}*/

      /** 
       * Paint the canvas into the specified drawable. 
       * @param drawable a drawable
       * @param info pickInfo
       */
    private:
      void paint (Drawable drawable) throws();

      /**
       * Initialize the specified graphics context.
       * @return a graphic context
       */
    protected:
      inline GC& gc() const throws()
      { return _context; }
	
      /** The repaint callback */
    private:
      static void repaintCB (Widget, XtPointer, XtPointer) throws();

      /** The resize callback */
    private:
      static void resizeCB (Widget, XtPointer, XtPointer) throws();
	
      /** The graphics context for this canvas */
    private:
      mutable GC _context;

      /** 
       * The graphic has been exposed after a scheduled expose event.
       * @param requested true if this is due to a requested expose event
       */
    public:
      void graphicExposed (bool requested) throws();

      /** True if canvas should be double-buffered */
    private:
      bool _doubleBufferEnabled;

      /** A root graphic */
    private:
      ::std::unique_ptr<RootGraphic> _gfx;

      /** A pixmap renderer */
    private:
      ::timber::Pointer< ::indigo::x11::PixmapRenderer> _renderer;
    };
  }
}

#endif
