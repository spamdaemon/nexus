#include <nexus/motif/Window.h>
#include <nexus/motif/util.h>
#include <nexus/Bounds.h>
#include <nexus/Insets.h>
#include <timber/logging.h>

#include <Xm/Protocols.h>
#include <Xm/MainW.h>

using namespace ::timber::logging;

namespace nexus {
   namespace motif {

      Window::Window(Desktop& dt)
      throws()
            : Component(dt,
                  ::XtCreateManagedWidget("container", compositeWidgetClass,
                        ::XtCreateManagedWidget("win", xmMainWindowWidgetClass,
                              ::XtAppCreateShell(blank(), blank(), topLevelShellWidgetClass, dt.display(), 0, 0), 0, 0),
                        0, 0)), _observer(0), _visibility(CLOSED), _visible(false)
      {
         ::XtAddEventHandler(widget(), StructureNotifyMask, FALSE, fireContentEvent, this);
         ::XtAddEventHandler(shell(), StructureNotifyMask, FALSE, fireWindowEvent, this);
         ::XtAddCallback(shell(), XmNdestroyCallback, windowDestroyedCB, this);
         // handle the user's close request for this window
         setValue(XmNdeleteResponse, static_cast< Int>(XmDO_NOTHING));
         Atom WM_DELETE_WINDOW = XInternAtom(display(), "WM_DELETE_WINDOW", False);
         ::XmAddWMProtocolCallback(shell(), WM_DELETE_WINDOW, closeWindowCB, this);
         addDestructor(widget(), window());
         addDestructor(window(), shell());
      }

      Window::~Window()
      throws()
      {

         Atom WM_DELETE_WINDOW = XInternAtom(display(), "WM_DELETE_WINDOW", True);
         assert(WM_DELETE_WINDOW != None);

         ::XtRemoveCallback(shell(), XmNdestroyCallback, windowDestroyedCB, this);
         ::XmRemoveWMProtocolCallback(shell(), WM_DELETE_WINDOW, closeWindowCB, this);
         ::XtRemoveEventHandler(shell(), StructureNotifyMask, FALSE, fireWindowEvent, this);
         ::XtRemoveEventHandler(widget(), StructureNotifyMask, FALSE, fireContentEvent, this);

         if (XtIsRealized(window())) {
            setExposureEventsEnabled(false);
         }
         XtDestroyWidget(window());

         // destroy the window
         if (_visibility == TRANSITIONING) {
            Log("nexus.motif.Window").debugging("Window visibility  : transitioning");
            // remove the destructor for the shell, because the structure notify
            // will do it for us
            removeDestructor(window(), shell());
            removeDestructor(widget(), window());
            ::XtAddEventHandler(shell(), StructureNotifyMask, FALSE, fireDestructor, 0);
         }
         else {
            LogEntry("nexus.motif.Window").debugging() << "Window visibility  : " << _visibility << doLog;
         }
      }

      void Window::setChildBounds(::nexus::peer::Component& c, const Bounds& b)
      throws()
      {
         dynamic_cast< Component&>(c).setBounds(b);
      }

      void Window::setTitle(const ::std::string& t)
      throws()
      {
         ::XtVaSetValues(shell(), XmNtitle, t.c_str(), (XtPointer) 0);
      }

      void Window::setBounds(const Bounds& b)
      throws()
      {
         Component::setBounds(Bounds(0, 0, b.width(), b.height()));
         setComponentBounds(shell(), b);
      }

      void Window::setVisible(bool visible)
      throws()
      {
         if (visible && _visibility == CLOSED) {
            Component::setVisible(true);
            ::XtRealizeWidget(shell());
            _visibility = TRANSITIONING;
            XSync(desktop().display(), False);
         }
         else if (!visible && _visibility == OPENED) {
            ::XtUnrealizeWidget(shell());
            Component::setVisible(false);
            _visibility = TRANSITIONING;
            XSync(desktop().display(), False);
         }
         _visible = visible;
      }

      void Window::setMinimumSize(const Size& sz)
      throws()
      {
         Int w = sz.width();
         Int h = sz.height();
         ::XtVaSetValues(shell(), XmNminHeight, h, XmNminWidth, w, (XtPointer) 0);
      }

      void Window::setMaximumSize(const Size& sz)
      throws()
      {
         Int w = sz.width();
         Int h = sz.height();
         ::XtVaSetValues(shell(), XmNmaxHeight, h, XmNmaxWidth, w, (XtPointer) 0);
      }

      void Window::closeWindowCB(Widget, XtPointer client_data, XtPointer)
      throws()
      {
         Window* win = static_cast< Window*>(client_data);
         if (win->_observer != 0) {
            win->_observer->windowClosedByUser();
         }
      }

      void Window::windowDestroyedCB(Widget, XtPointer client_data, XtPointer)
      throws()
      {
         Window* win = static_cast< Window*>(client_data);
         if (win->_observer != 0) {
            win->_observer->windowDestroyed();
         }
      }

      void Window::fireDestructor(Widget w, XtPointer, XEvent*, Boolean*)
      throws()
      {
         Log("nexus.motif.Window").debugging("Window::fireDestructor");
         if (XtIsRealized(w)) {
            ::XtUnrealizeWidget(w);
         }
         ::XtDestroyWidget(w);
      }

      void Window::fireWindowEvent(Widget xx, XtPointer w, XEvent* event, Boolean* propagate)
      throws()
      {
         *propagate = TRUE;

         Window* win = static_cast< Window*>(w);
         if (win->_observer == 0) {
            return;
         }
         switch (event->type) {
            case ConfigureNotify: {
               XConfigureEvent& e = event->xconfigure;
               Bounds bounds(e.x, e.y, e.width, e.height);
               win->_observer->windowBoundsChanged(bounds);
               return;
            }
            case MapNotify: {
               win->_visibility = OPENED;
               win->setVisible(win->_visible); // this might cause the window to be closed
               Bounds bounds = Component::getComponentBounds(win->window());
               win->_observer->windowVisibilityChanged(true);
               win->_observer->windowBoundsChanged(bounds);
               return;
            }
            case UnmapNotify: {
               win->_visibility = CLOSED;
               win->setVisible(win->_visible); // this might cause the window to be reopened
               win->_observer->windowVisibilityChanged(false);
               return;
            }
            default:
               break;
         }
      }

      void Window::fireContentEvent(Widget x, XtPointer w, XEvent* event, Boolean* propagate)
      throws()
      {
         *propagate = TRUE;
         Window* win = static_cast< Window*>(w);
         switch (event->type) {
            case ConfigureNotify: {
               XConfigureEvent& e = event->xconfigure;
               Bounds bounds = Component::getComponentBounds(win->window());
               Bounds tmp(e.x, e.y, e.width, e.height);
               Int left = tmp.x();
               Int top = tmp.y();
               Int right = bounds.width() - left - tmp.width();
               Int bottom = bounds.height() - top - tmp.height();
               if (right >= 0 && bottom >= 0) {
                  const Insets insets(left, right, top, bottom);
                  LogEntry("nexus.motif.Window").debugging() << "Insets changed : " << insets << doLog;
                  win->notifyInsets(insets);
               }
               break;
            }
            case MapNotify: {
               // ignore for now
               Bounds tmp = Component::getComponentBounds(win->widget());
               Bounds bounds = Component::getComponentBounds(win->window());
               Int left = tmp.x();
               Int top = tmp.y();
               Int right = bounds.width() - left - tmp.width();
               Int bottom = bounds.height() - top - tmp.height();
               if (right >= 0 && bottom >= 0) {
                  const Insets insets(left, right, top, bottom);
                  LogEntry("nexus.motif.Window").debugging() << "Insets changed : " << insets << doLog;
                  win->notifyInsets(insets);
               }
               break;
               break;
            }
            case UnmapNotify: {
               //	    win->notifyInsets(Insets());
               break;
            }
            default:
               break;
         }
      }

      void Window::setWindowObserver(WindowObserver* l)
      throws ()
      {
         _observer = l;
      }
   }
}
