#ifndef _NEXUS_MOTIF_TEXTFIELD_H
#define _NEXUS_MOTIF_TEXTFIELD_H

#ifndef _NEXUS_PEER_TEXTFIELD_H
#include <nexus/peer/TextField.h>
#endif

#ifndef _NEXUS_MOTIF_TEXTCOMPONENT_H
#include <nexus/motif/TextComponent.h>
#endif

namespace nexus {
  namespace motif {

    /**
     * This class is an implementation of a TextField component
     * using the Motif toolkit.
     * @date 06 Jun 2003
     */
    class TextField : public TextComponent, public virtual ::nexus::peer::TextField {
      TextField&operator=(const TextField&);
      TextField(const TextField&);

      /** 
       * Create a new TextField.
       * @param parent the parent component
       */
    public:
      TextField (Component& parent) throws();

    public:	
      ~TextField() throws();

    public:
      void setTextFieldObserver (TextFieldObserver* l) throws();

      /** The callback function */
    private:
      static void textFieldCB(Widget, XtPointer, XtPointer) throws();

      /** The textfield observer */
    private:
      TextFieldObserver* _observer;
    };
  }
}

#endif
