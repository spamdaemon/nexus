#ifndef _NEXUS_MOTIF_MOTIFFONT_H
#define _NEXUS_MOTIF_MOTIFFONT_H

#ifndef _INDIGO_X11_XRENDERFONT_H
#include <indigo/x11/XRenderFont.h>
#endif

#ifndef _INDIGO_FONTFACTORY_H
#include <indigo/FontFactory.h>
#endif

#ifndef _NEXUS_H
#include <nexus/nexus.h>
#endif

#ifndef _Xm_h
#include <Xm/Xm.h>
#endif

#include <X11/Xft/Xft.h>

namespace nexus {
   namespace motif {

      /**
       * This font represents a motif font.
       */
      class MotifFont : public ::indigo::x11::XRenderFont
      {
            MotifFont&operator=(const MotifFont&) = delete;
            MotifFont(const MotifFont&) = delete;
            MotifFont() = delete;

            /** The font's tag name */
         public:
            static const char TAG_NAME[];

            /** Default constructor. */
         private:
            MotifFont(const ::timber::Reference<::indigo::FontFactory>& factory)throws();

            /** The destructor */
         public:
            ~MotifFont()throws();

            /**
             * Create a new motif font.
             * @param w a widget
             * @param spec a null-terminated specification string
             * @return a font or 0 if none could be allocated
             */
         public:
            static ::timber::Pointer< MotifFont> create(const ::timber::Reference<::indigo::FontFactory> ff, Widget w, const char* spec)throws();

         public:
            ::indigo::Rectangle stringBounds(const ::std::string& s) const throws();

            ::indigo::FontDescription description() const throws();
            ::Font x11Font() const throws();
            ::XftFont* xftFont() const throws();

         public:
            size_t hashValue() const throws();

            /**
             * Compare this id with the specified id. If the
             * id is not of type MotifFont, then false is returned.
             * @param f a font id.
             * @return true if f is a MotifFont and is the same font as this font.
             */
         public:
            bool equals(const ::indigo::Font& f) const throws();

            /**
             * Get the font tag.
             * @return the font's tag
             */
         public:
            inline const char* tag() const throws()
            {  return TAG_NAME;}

            /**
             * Get the render table associated with this font.
             * @return a render table
             */
         public:
            inline XmRenderTable font() const throws()
            {  return _font;}

            /** The display */
         private:
            Display* _display;

            /** A render table */
         private:
            XmRenderTable _font;

            /** The XLFD font name */
         private:
            ::std::string _xlfd;

            /** The font description */
         private:
            ::indigo::FontDescription _description;

            /** The font id */
         private:
            ::Font _fontID;

            /** The xft font */
         private:
            ::XftFont* _xftFont;

            /** The font-factory that created this font */
         private:
            ::timber::Reference<::indigo::FontFactory> _factory;
      };

   }
}

#endif
