#ifndef _NEXUS_MOTIF_TEXTCOMPONENT_H
#define _NEXUS_MOTIF_TEXTCOMPONENT_H

#ifndef _NEXUS_PEER_TEXTCOMPONENT_H
#include <nexus/peer/TextComponent.h>
#endif

#ifndef _NEXUS_MOTIF_COMPONENT_H
#include <nexus/motif/Component.h>
#endif

#include <Xm/Text.h>

namespace nexus {
  namespace motif {
    /** Forward declartion */
    class TextOperation;

    /**
     * This class is an implementation of a TextComponent component
     * using the Motif toolkit.
     * @date 01 Aug 2003
     */
    class TextComponent : public Component, public virtual ::nexus::peer::TextComponent {
      /** No default methods allowed */
      TextComponent&operator=(const TextComponent&);
      TextComponent(const TextComponent&);

      /** 
       * Create a new TextComponent.
       * @param p the parent component
       * @param w a widget
       */
    protected:
      TextComponent (Component& p, Widget w) throws();

    protected:	
      ~TextComponent() throws();
	
    public:
      void setTextModel (TextModel* m) throws();
      bool hasForeground () const throws();
      void setBorderWidth (Int w) throws();
      void replaceText (size_type off, size_type count, const ::std::string& s) throws();


      /** The validation callback */
    private:
      static void validationCB(Widget, XtPointer, XtPointer) throws();
	
      /**
       * Get the text model.
       * @return the text model or 0 if editing is disabled
       */
    protected:
      inline TextModel* textModel() const throws()
      { return _model; }

      /**
       * Set the font for this TextComponent.
       * This method has no effect on the display if the TextComponent uses an icon
       * instead of a text string.
       * @param f a font id
       * @param inherited not used
       */
    public:
      void setFont (const ::timber::Pointer< ::indigo::Font>& f, bool inherited) throws();

      /**
       * Enable or disable validation.
       * @param enable true if validation should be enabled
       */
    protected:
      inline void setValidateEnabled(bool enable) throws()
      { _validate = enable; }
	
      /** The text model. */
    private:
      TextModel* _model;

      /** Do not validate if true */
    private:
      bool _validate;

      /** A change request */
    private:
      TextOperation* _textOp;
    };
  }
}

#endif
