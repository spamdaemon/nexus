#include <canopy/time/Time.h>
#include <canopy/mt/Thread.h>
#include <canopy/mt/Mutex.h>
#include <timber/Application.h>
#include <timber/media/ImageFactory.h>
#include <timber/media/ImageBuffer.h>
#include <timber/media/image/ScaleOp.h>
#include <timber/ios/URIInputStream.h>

#include <indigo/Color.h>
#include <indigo/SolidFillNode.h>
#include <indigo/StrokeNode.h>
#include <indigo/Polygon.h>
#include <indigo/Polygons.h>
#include <indigo/Group.h>
#include <indigo/Raster.h>
#include <indigo/Transform2D.h>
#include <indigo/AffineTransform2D.h>

#include <nexus/event/WindowEvent.h>
#include <nexus/event/ButtonEvent.h>
#include <nexus/event/CrossingEvent.h>

#include <nexus/Desktop.h>
#include <nexus/Window.h>
#include <nexus/LayoutContainer.h>
#include <nexus/layout/FixedLayout.h>
#include <nexus/layout/FlowLayout.h>
#include <nexus/Label.h>
#include <nexus/PushButton.h>
#include <nexus/Canvas.h>

#include <cstring>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <cstdlib>

using namespace std;
using namespace canopy;
using namespace canopy::mt;
using namespace canopy::time;
using namespace timber;
using namespace timber::ios;
using namespace timber::media;
using namespace timber::media::image;
using namespace indigo;
using namespace nexus;
using namespace nexus::layout;
using namespace nexus::event;

using ::indigo::Raster;

// set to false to prevent automatic scaling to
static bool doResize = true;

struct RunnableFunction : public Runnable
{
      RunnableFunction(void (*f)())
            : _f(f)
      {
      }
      ~RunnableFunction()throws()
      {
      }

      void run()
      {
         (*_f)();
      }

   private:
      void (*_f)();
};

class MyCanvas : public Canvas
{

   public:
      MyCanvas()throws()
      {
         setButtonEventEnabled(true);
         setCrossingEventEnabled(true);

         Reference < ::indigo::Raster > image = new ::indigo::Raster();
         Reference< Group> group = new Group();
         Reference< Polygons> polygons = new Polygons();
         Reference< Transform2D> transform = new Transform2D();
         group->add(transform);
         group->add(image);
         group->add(new ::indigo::SolidFillNode());
         group->add(new ::indigo::StrokeNode(::indigo::Color::GREEN));
         group->add(polygons);

         setGraphic(group);
      }
      ~MyCanvas()throws()
      {
      }

      void setImage(::std::shared_ptr < ImageBuffer> img)
      {
         _image = img;
         Reference< Group> group(graphic());
         Reference < ::indigo::Raster > image = group->node(1);
         auto buf = _image;
         if (buf) {
            if (doResize) {
               image->setPoints( { 0, 0 }, { (double) width(), 0 }, { 0, -(double) height() });
            }
            image->setImage(buf);
         }
      }

      void doLayout() throws(LayoutException)
      {
         //TODO: this is an expensive operation; better to enqueue a painter to repaint in about 100ms or to do it
         // in a separate thread??
         setImage(_image);
      }

      void processCrossingEvent(const Reference< CrossingEvent>& e)throws()
      {
         if (e->id() == CrossingEvent::COMPONENT_ENTERED) {
            setForegroundColor(::nexus::Color(0., 1., 1.));
         }
         else {
            setForegroundColor(::nexus::Color(1., 1., 1.));
         }
      }

   private:
      ::std::shared_ptr< ImageBuffer> _image;
};

class MyWindow : public Window
{

   public:
      MyWindow()throws()
      {
         setCloseAction(EXIT);
         Reference< LayoutContainer< FlowLayout> > ct = new LayoutContainer< FlowLayout>(Flow::HORIZONTAL);
         ct->setName("CONTENT");
         setContent(ct);
         content()->setBackgroundColor(::nexus::Color(0.0, 0.33, 0.0));
         content()->setForegroundColor(::nexus::Color(0.66, 0.66, 0.66));

         struct WListener : public WindowEventListener
         {
               ~WListener() throws()
               {
               }
               void processWindowEvent(const Reference< WindowEvent>& e) throws()
               {
                  switch (e->id()) {
                     case WindowEvent::WINDOW_OPENED: {
                        ::std::cerr << "Window opened" << ::std::endl;
                        break;
                     }
                     case WindowEvent::WINDOW_CLOSED: {
                        ::std::cerr << "Window closed" << ::std::endl;
                        break;
                     }
                  }
               }
         };
         WindowEventListener* l = new WListener();
         addWindowEventListener(l);

         Reference< Canvas> canvas = new MyCanvas();
         setContent(canvas);
      }

      ~MyWindow()throws()
      {
      }

      inline ::timber::Pointer< Component> content() const throws()
      {
         return Window::content();
      }

   protected:
      void processWindowCloseRequest()throws()
      {
         ::std::cerr << "Requested to close this window" << ::std::endl;
      }

   protected:
      SizeHints sizeHints() const throws()
      {
         SizeHints hints(Window::sizeHints());
         return SizeHints(hints.minimumSize(), Size::MAX, hints.preferredSize());
      }

   public:
      //  static  Pointer<MyWindow> win;
      static MyWindow* win;
};

struct UpdateImage : public Runnable
{
      UpdateImage(Mutex& m, Reference< Canvas> c, ::std::shared_ptr<ImageBuffer> img)
            : _mutex(m), _canvas(c), _image(img)
      {
      }
      ~UpdateImage()throws()
      {
      }

      void run()
      {
         auto buf = _image;
         if (buf) {
            Reference< Group> group(_canvas->graphic());
            Reference< ::indigo::Polygons> polys = group->node(4);
            polys->clear();
            _canvas->setImage(buf);
            _canvas->render();
         }
      }

   private:
      Mutex& _mutex;
      Reference< MyCanvas> _canvas;
      ::std::shared_ptr<ImageBuffer> _image;
};

//Pointer<MyWindow> MyWindow::win;
MyWindow* MyWindow::win(0);

static void initialize()
{
   ::std::cerr << "Initializing" << ::std::endl;
   assert(Desktop::isEventThread());

   ::std::cerr << "Getting desktop size" << ::std::endl;
   Size size = Desktop::size();
   ::std::cerr << "Desktop size " << size << ::std::endl;
   MyWindow::win = new MyWindow();

   ::std::cerr << "Setting title" << ::std::endl;
   MyWindow::win->setTitle("Hello, World!");
   Bounds bounds(100, 100, size.width() / 2, size.height() / 2);
   ::std::cerr << "Setting visibility" << ::std::endl;

   ::std::cerr << "Setting bounds " << bounds << ::std::endl;
   MyWindow::win->requestBounds(bounds);
   MyWindow::win->setTitle("Video Player");
   MyWindow::win->requestSize(Size(240, 120));
   MyWindow::win->setBackgroundColor(::nexus::Color(1., 1., 1.));
   MyWindow::win->setVisible(true);
}

int main(int argc, const char** argv)
{
   Application::instance().saveConfiguration();
   Desktop::configure(argc, argv);
   Mutex mutex;
   const char* imageURL = 0;

   if (argc != 2) {
      cout << flush;
      cerr << flush;
      cerr << "Usage: imageviewer <url>";
      exit(1);
      return 1;
   }

   imageURL = argv[1];

   // load the file
   ::std::shared_ptr< ImageBuffer> image;
   try {
      URIInputStream stream(imageURL);
      image = ::std::dynamic_pointer_cast<ImageBuffer>( ImageFactory::getBuiltinFactory()->read(stream,argv[1]));
      if (!image) {
        ::std::cerr << "failed to load image" << ::std::endl;
        exit(1);
      }
   }
   catch (const exception& e) {
      cerr << "Unexpected exception caught";
      exit(1);
   }
   Desktop::enqueue(::std::make_shared<RunnableFunction>(initialize), true);
   MyWindow::win->requestSize(Size(image->width(), image->height()));
   Desktop::enqueue(::std::make_shared<UpdateImage>(mutex, MyWindow::win->content(), image), false);
   Desktop::waitShutdown();
   return 0;
}
