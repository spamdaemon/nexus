#include <canopy/video/VideoSource.h>
#include <canopy/video/FrameGrabber.h>
#include <canopy/time/Time.h>
#include <canopy/mt/Thread.h>
#include <canopy/mt/Mutex.h>
#include <timber/Application.h>
#include <timber/AtomicPointerExchange.h>
#include <timber/media/ImageBuffer.h>
#include <timber/media/format/Jpeg.h>
#include <timber/media/format/Png.h>
#include <timber/media/image/ScaleOp.h>
#include <timber/media/ImageFactory.h>

#include <indigo/Color.h>
#include <indigo/SolidFillNode.h>
#include <indigo/StrokeNode.h>
#include <indigo/Polygon.h>
#include <indigo/Polygons.h>
#include <indigo/Group.h>
#include <indigo/Raster.h>
#include <indigo/Transform2D.h>
#include <indigo/AffineTransform2D.h>

#include <nexus/event/WindowEvent.h>
#include <nexus/event/ButtonEvent.h>
#include <nexus/event/CrossingEvent.h>

#include <nexus/Desktop.h>
#include <nexus/Window.h>
#include <nexus/LayoutContainer.h>
#include <nexus/layout/FixedLayout.h>
#include <nexus/layout/FlowLayout.h>
#include <nexus/Label.h>
#include <nexus/PushButton.h>
#include <nexus/Canvas.h>

#include <cstring>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <cstdlib>

using namespace std;
using namespace canopy;
using namespace canopy::video;
using namespace canopy::mt;
using namespace canopy::time;
using namespace timber;
using namespace timber::media;
using namespace timber::media::format;
using namespace timber::media::image;
using namespace indigo;
using namespace nexus;
using namespace nexus::layout;
using namespace nexus::event;

using ::indigo::Raster;

static bool doResize = true;

struct RunnableFunction : public Runnable
{
      RunnableFunction(void (*f)())
            : _f(f)
      {
      }
      ~RunnableFunction()throws()
      {
      }

      void run()
      {
         (*_f)();
      }

   private:
      void (*_f)();
};

class MyCanvas : public Canvas
{

   public:
      MyCanvas()throws()
      {
         setButtonEventEnabled(true);
         setCrossingEventEnabled(true);

         Reference< ::indigo::Raster> image = new ::indigo::Raster();
         Reference< Group> group = new Group();
         Reference< Polygons> polygons = new Polygons();
         Reference< Transform2D> transform = new Transform2D();
         group->add(transform);
         group->add(image);
         group->add(new ::indigo::SolidFillNode());
         group->add(new ::indigo::StrokeNode(::indigo::Color::GREEN));
         group->add(polygons);

         setGraphic(group);
      }
      ~MyCanvas()throws()
      {
      }

      void setImage(const ::std::shared_ptr< ImageBuffer> img)
      {
         _image = img;
         Reference< Group> group(graphic());
         Reference < ::indigo::Raster > image = group->node(1);
         shared_ptr< ImageBuffer> buf = _image;
         if (buf) {
            if (doResize) {
               image->setPoints( { 0, 0 }, { (double) width(), 0 }, { 0, -(double) height() });
            }
            image->setImage(buf);
         }
      }

      void doLayout()throws(LayoutException)
      {
         setImage(_image);
   }

   void processCrossingEvent(const Reference< CrossingEvent>& e)
   throws()
   {
      if (e->id() == CrossingEvent::COMPONENT_ENTERED) {
         setForegroundColor(::nexus::Color(0., 1., 1.));
      }
      else {
         setForegroundColor(::nexus::Color(1., 1., 1.));
      }
   }

   private:
   shared_ptr< ImageBuffer> _image;
};

class MyWindow : public Window
{

   public:
      MyWindow()throws()
      {
         setCloseAction(EXIT);
         Reference< LayoutContainer< FlowLayout> > ct = new LayoutContainer< FlowLayout>(Flow::HORIZONTAL);
         ct->setName("CONTENT");
         setContent(ct);
         content()->setBackgroundColor(::nexus::Color(0.0, 0.33, 0.0));
         content()->setForegroundColor(::nexus::Color(0.66, 0.66, 0.66));

         struct WListener : public WindowEventListener
         {
            ~WListener() throws()
            {
            }
            void processWindowEvent(const Reference< WindowEvent>& e) throws()
            {
               switch (e->id()) {
                  case WindowEvent::WINDOW_OPENED: {
                     ::std::cerr << "Window opened" << ::std::endl;
                     break;
                  }
                  case WindowEvent::WINDOW_CLOSED: {
                     ::std::cerr << "Window closed" << ::std::endl;
                     break;
                  }
               }
            }
         };
         WindowEventListener* l = new WListener();
         addWindowEventListener(l);

         Reference< Canvas> canvas = new MyCanvas();
         setContent(canvas);
      }

      ~MyWindow()throws()
      {
      }

      inline ::timber::Pointer< Component> content() const throws()
      {
         return Window::content();
      }

   protected:
      void processWindowCloseRequest()throws()
      {
         ::std::cerr << "Requested to close this window" << ::std::endl;
      }

   protected:
      SizeHints sizeHints() const throws()
      {
         SizeHints hints(Window::sizeHints());
         return SizeHints(hints.minimumSize(), Size::MAX, hints.preferredSize());
      }

   public:
      //  static  Pointer<MyWindow> win;
      static MyWindow* win;
};

struct UpdateImage : public Runnable
{
      UpdateImage(Reference< Canvas> c, AtomicPointerExchange< ImageBuffer>& img)
            : _canvas(c), _image(img)
      {
      }
      ~UpdateImage()throws()
      {
      }

      void run()
      {
         auto buf = _image.take();
         if (buf) {
            Reference< Group> group(_canvas->graphic());
            Reference< ::indigo::Polygons> polys = group->node(4);
            polys->clear();
            _canvas->setImage(buf);
            _canvas->render();
         }
      }

   private:
      Reference< MyCanvas> _canvas;
      AtomicPointerExchange< ImageBuffer>& _image;
};

//Pointer<MyWindow> MyWindow::win;
MyWindow* MyWindow::win(0);

static void initialize()
{
   ::std::cerr << "Initializing" << ::std::endl;
   assert(Desktop::isEventThread());

   ::std::cerr << "Getting desktop size" << ::std::endl;
   Size size = Desktop::size();
   ::std::cerr << "Desktop size " << size << ::std::endl;
   MyWindow::win = new MyWindow();

   ::std::cerr << "Setting title" << ::std::endl;
   MyWindow::win->setTitle("Hello, World!");
   Bounds bounds(100, 100, size.width() / 2, size.height() / 2);
   ::std::cerr << "Setting visibility" << ::std::endl;

   ::std::cerr << "Setting bounds " << bounds << ::std::endl;
   MyWindow::win->requestBounds(bounds);
   MyWindow::win->setTitle("Video Player");
   MyWindow::win->requestSize(Size(240, 120));
   MyWindow::win->setBackgroundColor(::nexus::Color(1., 1., 1.));
   MyWindow::win->setVisible(true);
}

int main(int argc, const char** argv)
{
   Application::instance().saveConfiguration();
   Desktop::configure(argc, argv);
   const char* source = nullptr;
   double sz = 0;
   unsigned int delay = 0;
   bool toCout = false;
   bool jpeg = false;
   bool png = false;

   for (int arg = 1; arg < argc; ++arg) {
      if (string(argv[arg]) == "-file") {
         if (++arg == argc) {
            cerr << "Missing file name" << endl;
            exit(1);
         }
         source = argv[arg];
      }
      else if (string(argv[arg]) == "-delay") {
         if (++arg == argc) {
            cerr << "Missing delay" << endl;
            exit(1);
         }
         if (::std::sscanf(argv[arg], "%u", &delay) != 1) {
            cerr << "Could not parse " << argv[arg] << " as a int" << endl;
            exit(1);
         }
      }
      else if (string(argv[arg]) == "-sz") {
         if (++arg == argc) {
            cerr << "Missing size" << endl;
            exit(1);
         }
         if (::std::sscanf(argv[arg], "%lf", &sz) != 1) {
            cerr << "Could not parse " << argv[arg] << " as a double" << endl;
            exit(1);
         }
      }
      else if (string(argv[arg]) == "-stdout") {
         toCout = true;
      }
      else if (string(argv[arg]) == "-jpeg") {
         jpeg = true;
      }
      else if (string(argv[arg]) == "-png") {
         png = true;
      }
   }

   auto f = ::timber::media::ImageFactory::getBuiltinFactory();

   Desktop::enqueue(::std::make_shared<RunnableFunction>(initialize), true);
   unique_ptr< ifstream> fin;
   do {
      if (source != nullptr) {
         fin.reset(new ifstream(source, ::std::ios::binary));
      }
      istream& xin = fin.get() ? *fin : ::std::cin;

      size_t frame = 0;
      ::std::shared_ptr< ImageBuffer> buf;
      AtomicPointerExchange< ImageBuffer> image;
      Thread::suspend(500000000);
      while (true) {
         try {
            if (png) {
               buf = Png::read(xin, f);
            }
            else if (jpeg) {
               buf = Jpeg::read(xin, f);
            }
            else {
               buf = ImageBuffer::readSunRasterImage(xin);
            }
         }
         catch (...) {
            break;
         }
         if (buf == nullptr) {
            buf = ::std::dynamic_pointer_cast< ImageBuffer>(f->read(xin));
            if (buf == nullptr) {
               break;
            }
         }
         if (toCout) {
            buf->writeSunRasterImage(::std::cout);
            ::std::cout << flush;
         }
         if (sz > 0) {
            unique_ptr< ScaleOp> scaler = ScaleOp::createFastScale(size_t(sz * buf->width()),
                  size_t(sz * buf->height()));
            buf = scaler->apply(buf);
         }

         if ((++frame % 200) == 0) {
            ::std::cerr << "Frame " << frame << ::std::endl;
         }
         // note: this function need not be executed from within the event thread
         if (doResize) {
            //	buf = buf->resize(MyWindow::win->width(),MyWindow::win->height());
         }
         else {
            MyWindow::win->requestSize(Size(buf->width(), buf->height()));
         }
         image = buf;
         if (!Desktop::enqueue(::std::make_shared<UpdateImage>(MyWindow::win->content(), image), false)) {
            source = nullptr;
            break;
         }
         if (source) {
            Thread::suspend(100 * 1000000);
         }
         else if (delay > 0) {
            Thread::suspend(delay * 1000000);
         }
      }
   } while (source != nullptr);

   ::std::cerr << "Done reading" << ::std::endl;

   //  Desktop::shutdown();
   Desktop::waitShutdown();
   return 0;
}
