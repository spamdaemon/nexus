#include <canopy/time/Time.h>
#include <timber/Application.h>

#include <nexus/Desktop.h>

#include <iostream>

using namespace canopy;
using namespace canopy::time;
using namespace timber;
using namespace indigo;
using namespace nexus;

struct RunnableFunction : public Runnable
{
      RunnableFunction(void (*f)())
            : _f(f)
      {
      }
      ~RunnableFunction()throws()
      {
      }

      void run()
      {
         (*_f)();
      }

   private:
      void (*_f)();
};

static void initialize()
{
   ::std::cerr << "Initializing" << ::std::endl;
   assert(Desktop::isEventThread());

   ::std::cerr << "Getting desktop size" << ::std::endl;
   Size size = Desktop::size();

   ::std::cout << "Desktop size : " << size.width() << ' ' << size.height() << ::std::endl;
}

int main(int argc, const char** argv)
{
   Application::instance().saveConfiguration();
   try {
      Desktop::configure(argc, argv);
      Desktop::enqueue(::std::make_shared<RunnableFunction>(initialize), true);
      Desktop::enqueue(::std::make_shared<RunnableFunction>(Desktop::shutdown));
      Desktop::waitShutdown();
      return 0;
   }
   catch (const ::std::exception& e) {
      ::std::cerr << e.what() << ::std::endl;
      return 1;
   }
}
