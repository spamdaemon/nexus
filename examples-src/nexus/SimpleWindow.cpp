#include <iostream>
#include <nexus/Desktop.h>
#include <nexus/Window.h>
#include <nexus/LayoutContainer.h>
#include <nexus/layout/FixedLayout.h>
#include <nexus/layout/FlowLayout.h>
#include <nexus/Label.h>
#include <nexus/Menu.h>
#include <nexus/MenuBar.h>
#include <nexus/MenuItem.h>
#include <nexus/PushButton.h>
#include <nexus/Canvas.h>
#include <nexus/event/ActionEvent.h>
#include <nexus/event/KeyEvent.h>
#include <nexus/event/KeyEventInterceptor.h>
#include <nexus/event/WindowEvent.h>
#include <nexus/event/ButtonEvent.h>
#include <nexus/event/CrossingEvent.h>

#include <indigo/Group.h>
#include <indigo/Circles.h>
#include <indigo/Ellipses.h>
#include <indigo/Polygons.h>
#include <indigo/StrokeNode.h>
#include <indigo/SolidFillNode.h>
#include <indigo/Transform2D.h>

#include <canopy/mt/Thread.h>
#include <timber/Application.h>

#ifdef USE_TIMER
#include <gps/thread/timer/Timer.h>
#include <gps/thread/timer/Task.h>
#include <gps/thread/timer/PeriodicSchedule.h>
#endif

#include <cstdlib>
#include <cmath>

using namespace nexus;
using namespace indigo;
using namespace nexus;
using namespace nexus::event;
using namespace nexus::layout;
using namespace timber;
using namespace timber::logging;

static Reference<Component> createPrimitive ();


struct RunnableFunction : public Runnable {
  RunnableFunction(void (*f)()) : _f(f) {}
  ~RunnableFunction() throws() {}

  void run()
  { (*_f)(); }

private:
  void (*_f)();
};

struct ActionListener : public ActionEventListener {

  ~ActionListener() throws() {}
  
  void processActionEvent (const ::timber::Reference<ActionEvent>& e) throws()
  {
    Pointer<MenuItem> item = e->source().tryDynamicCast<MenuItem>();
    if (item) {
      ::std::cerr << "Menu item " << item->text() << ": " << item->actionID() << " pressed " 
		  << ::std::endl;
      if (item->actionID()=="QUIT") {
	Desktop::shutdown();
      }
    }
  }

};

static  ::timber::Pointer<Node> createTriangle (double scale)
{
  Polygon p(Point(0,0),Point(scale,0),Point(scale/2,scale));
  return new Polygons(p);
}

static  ::timber::Pointer<Transform2D> getScaleTransform()
{
  static  ::timber::Pointer<Transform2D> t(new Transform2D());
  return t;
}

static  ::timber::Pointer<Transform2D> getRotateTransform()
{
  static  ::timber::Pointer<Transform2D> t(new Transform2D());
  return t;
}

static  ::timber::Pointer<Transform2D> getAirplaneRotator()
{
  static  ::timber::Pointer<Transform2D> t(new Transform2D());
  return t;
}

static Icon createIcon()
{
    Point pts[30];
    int i=0;
    pts[i++] = Point(0.523,0);
    pts[i++] = Point(0.512,0.035); 
    pts[i++] =Point(0.479,0.058); 
    pts[i++] =Point(0.440,0.067); 
    pts[i++] =Point(0.100,0.067); 
    pts[i++] =Point(-0.137,0.450); 
    pts[i++] =Point(-0.218,0.450); 
    pts[i++] =Point(-0.132,0.170); 
    pts[i++] =Point(-0.132,0.067); 
    pts[i++] =Point(-0.315,0.067); 
    pts[i++] =Point(-0.423,0.202); 
    pts[i++] =Point(-0.477,0.202); 
    pts[i++] =Point(-0.477,0.073); 
    pts[i++] =Point(-0.423,0.024); 
    pts[i++] =Point(-0.423,-0.024); 
    pts[i++] =Point(-0.477,-0.073); 
    pts[i++] =Point(-0.477,-0.202); 
    pts[i++] =Point(-0.423,-0.202); 
    pts[i++] =Point(-0.315,-0.067); 
    pts[i++] =Point(-0.132,-0.067); 
    pts[i++] =Point(-0.132,-0.170); 
    pts[i++] =Point(-0.218,-0.450); 
    pts[i++] =Point(-0.137,-0.450); 
    pts[i++] =Point(0.100,-0.067); 
    pts[i++] =Point(0.440,-0.067); 
    pts[i++] =Point(0.479,-0.058); 
    pts[i++] =Point(0.512,-0.035); 
    
    Polygon p(i,pts);
    
     ::timber::Pointer<Group> grp(new Group());
    AffineTransform2D t(0,25,25,25);
    grp->add(new Transform2D(t));
    grp->add(getAirplaneRotator());
    grp->add(new  ::indigo::SolidFillNode(::indigo::Color::GREY));
    grp->add(new Polygons(p));

    return Icon(grp, Size(50,50));
}

static  ::timber::Pointer<Node> defaultRectangle ()
{
  static  ::timber::Pointer<Node> _data;

  if (_data==nullptr) {
    Point pts[4];
    pts[0] = Point(.5,0);
    pts[1] = Point(1,0.5);
    pts[2] = Point(0.5,1);
    pts[3] = Point(0,.5);
    Polygon p(4,pts);
    
     ::timber::Pointer<Group> grp(new Group());
    grp->add(getRotateTransform());
    grp->add(getScaleTransform());
    grp->add(new SolidFillNode(::indigo::Color::BLACK));
    grp->add(new StrokeNode(::indigo::Color::BLACK));
    grp->add(new Polygons(p));
    _data = grp;
  }
  return _data;
}

static  ::timber::Pointer<Node> defaultEllipse ()
{
  static  ::timber::Pointer<Node> _data;

  if (_data==nullptr) {
    Point pts[4];
    pts[0] = Point(.5,0);
    pts[1] = Point(1,0.5);
    pts[2] = Point(0.5,1);
    pts[3] = Point(0,.5);
    Polygon p(4,pts);
    
     ::timber::Pointer<Group> grp(new Group());
    grp->add(getRotateTransform());
    grp->add(getScaleTransform());
    grp->add(Reference<Group>(defaultRectangle())->node(2));
    grp->add(new StrokeNode(::indigo::Color::YELLOW,2));
   grp->add(new Ellipses(Ellipse::createCovarianceEllipse(Point(0.5,0.5),.25,.0625,.0625)));//Point(0.5,0.5),.5,.25));
    _data = grp;
  }
  return _data;
}

static  ::timber::Pointer<Node> createEllipse (double scale)
{
   ::timber::Pointer<Group> g(new Group());

  AffineTransform2D t;
  t.setToScale(scale);
  g->add(new Transform2D(t));
  g->add(defaultEllipse());
  return g;
}

static  ::timber::Pointer<Node> createRectangle (double scale)
{
   ::timber::Pointer<Group> g(new Group());

  AffineTransform2D t;
  t.setToScale(scale);
  g->add(new Transform2D(t));
  g->add(defaultRectangle());
  return g;
}

class MyCanvas : public Canvas {
  int on;
   ::timber::Pointer<Group> graphic;

public:
  MyCanvas () throws()
    : on(0),graphic(new Group())
  {
    setButtonEventEnabled(true);
    setCrossingEventEnabled(true);
    setGraphic(graphic);
  }
  ~MyCanvas() throws() {} 

  void doLayout() throws(LayoutException)
  {
    switch(on) {
    case 1:
      setForegroundColor(::nexus::Color(0.,1.,0.,0.5));
      graphic->set(createRectangle(width()),0);
      break;
    case 2:
      setForegroundColor(::nexus::Color(0.,1.,0.,0.7));
      graphic->set(createEllipse(width()),0);
      break;
    default:
      setForegroundColor(::nexus::Color(0.,1.,0.,1.));
      graphic->set(createTriangle(width()),0);
      break;
    }
  }

  void toggleCanvas () throws()
  {
    on = (on +1) % 3;
    doLayout();

    createPrimitive();
  }
  
  void processCrossingEvent (const Reference<CrossingEvent>& e) throws()
  {
    if (e->id()==CrossingEvent::COMPONENT_ENTERED) {
      setForegroundColor(::nexus::Color(0.,1.,1.));
    }
    else {
      setForegroundColor(::nexus::Color(1.,1.,1.));
    }
  }
};


class MyWindow : public Window {
  
public:
  MyWindow () throws()
  { 
    setCloseAction(EXIT);
    Reference< LayoutContainer<FlowLayout> > ct = new LayoutContainer<FlowLayout>(Flow::HORIZONTAL);
    ct->setName("CONTENT");
    setContent(ct); 
    content()->setBackgroundColor(::nexus::Color(0.0,0.33,0.0));
    content()->setForegroundColor(::nexus::Color(0.66,0.66,0.66));
    
    struct WListener : public WindowEventListener {
      ~WListener() throws() {}
      void processWindowEvent (const Reference<WindowEvent>& e) throws()
      {
	switch (e->id()) {
	case WindowEvent::WINDOW_OPENED: {
	  ::std::cerr << "Window opened" << ::std::endl; 
	  break;
	}
	case WindowEvent::WINDOW_CLOSED: {
	  ::std::cerr << "Window closed" << ::std::endl; 
	  break;
	}
	}
      }
    };

    struct KListener : public KeyEventListener {
      KListener(MyWindow* xwin)  :_window(xwin) {
      }
      ~KListener() throws()  {}
      void processKeyEvent(const Reference<KeyEvent>& e) throws()
      {
	if (e->id()==KeyEvent::KEY_PRESSED) {
	  if (e->key().id()==KeySymbols::KEY_F2) {
	    _window->toggleMenuBar();
	  }
	}
      }
    private:
      MyWindow* _window;
    };
    KeyEventListener* k = new KListener(this);
    //ct->setInterceptEventsEnabled(true);
    content()->addKeyEventListener(k);
    
    WindowEventListener* l = new WListener();
    addWindowEventListener(l);
  }
  
  ~MyWindow () throws()
  {
  }

  void toggleMenuBar()
  {
    Pointer<MenuBar> bar = menuBar();
    if (bar) {
      bar = Pointer<MenuBar>();
    }
    else {
      bar= new MenuBar();
      Reference<Menu> mn = bar->addMenu("File");
      mn->setBackgroundColor(::nexus::Color::GREEN);
      Reference<MenuItem> mi = mn->addMenuItem("Open");
      mi->setActionID("OPEN");
      mi->addActionEventListener(new ActionListener());
      
      mi = mn->addMenuItem("Quit");
      mi->setActionID("QUIT");
      mi->addActionEventListener(new ActionListener());
      
      mn = bar->addMenu("Edit");
      mn = bar->addMenu("Preferences");
      mn = bar->addMenu("Help");
      mn->setBackgroundColor(::nexus::Color::YELLOW);
      mn = mn->addMenu("About");
      mn->setBackgroundColor(::nexus::Color::GREEN);
      mn->addMenu("Version");
    }
    setMenuBar(bar);
  }
  

  inline ::timber::Pointer<Component> content () const throws()
  { return Window::content(); }

protected:
  void processWindowCloseRequest () throws()
  {
    ::std::cerr << "Requested to close this window" << ::std::endl;
  }

protected:
  SizeHints sizeHints () const throws()
  {
    SizeHints hints(Window::sizeHints());
    return SizeHints(hints.minimumSize(),Size::MAX,hints.preferredSize());
  }

public:
  //  static  Pointer<MyWindow> win;
  static MyWindow* win;
};

struct KInterceptor : public KeyEventInterceptor {
  KInterceptor(MyWindow* xwin)  :_window(xwin) {
  }
  ~KInterceptor() throws()  {}
  bool interceptKeyEvent(const KeyEvent::EventID& id, const KeySymbol& key, const Time& , const InputModifiers&) throws()
  {
    if (id==KeyEvent::KEY_PRESSED) {
      if (key.id()==KeySymbols::KEY_F1) {
	_window->toggleMenuBar();
	return true;
      }
    }
    return false;
  }
private:
  MyWindow* _window;
};


//Pointer<MyWindow> MyWindow::win;
MyWindow* MyWindow::win(0);

static Reference<Component> createPrimitive ()
{
  class EventListener : public ButtonEventListener
  {
  public:
    EventListener()  {}
    ~EventListener() throws() {}
  public:
    void processButtonEvent (const Reference<ButtonEvent>& e) throws()
    {
      if (e->id()==ButtonEvent::BUTTON_PRESSED) {
	Reference<MyCanvas> cv(e->source());
	cv->toggleCanvas(); 
      }
    }
  };

  Reference< LayoutContainer<FixedLayout> > c = new LayoutContainer<FixedLayout>();
  c->setName("PRIMITIVE");
  Reference< LayoutContainer<FixedLayout> > d = new LayoutContainer<FixedLayout>();
  d->setName("Y");
  Reference<Canvas> e = new MyCanvas();
  ButtonEventListener* xl = new EventListener();
  e->addButtonEventListener(xl);
  e->setName("Y");
  
  e->setBackgroundColor(::nexus::Color(1.,0.,0.));
  e->setForegroundColor(::nexus::Color(0.,0.,1.));
  d->setBackgroundColor(::nexus::Color(1.,1.,0.));
  c->setBackgroundColor(::nexus::Color(0.,1.,0.));
  d->addComponent(e,Bounds(10,10,40,40));
  c->addComponent(d,Bounds(0,0,60,60));
  return c;
}

static Reference< LayoutContainer<FlowLayout> > createFlowContent()
{
  Reference< LayoutContainer<FlowLayout> > outer = new LayoutContainer<FlowLayout>(Flow::VERTICAL);
  outer->setName("OUTER");
  outer->addComponent(createPrimitive(),0);
  outer->addComponent(createPrimitive(),1);
  outer->addComponent(createPrimitive(),2);
  outer->setBackgroundColor(::nexus::Color(0.,0.,0.));
  outer->setForegroundColor(::nexus::Color(1.,1.,1.));
  return outer;
}
static void rotate()
{
  static Double phi = 0;
  static Double scale = 1;
  if (Desktop::isEventThread()) {
    phi += 2.0*M_PI/30.0;
    if (phi>2.0*M_PI) {
      phi -= 2.0*M_PI;
    }
    Double cosine = ::std::cos(phi);
    scale = .80* cosine;
    if (scale<0) {
      scale = -scale;
      cosine = -cosine;
    }
    scale += .20;
     ::timber::Pointer<Group> grp(defaultRectangle());
    AffineTransform2D at;
    at.setToRotate(phi,.5,.5);
    getRotateTransform()->setTransform(at);
    at.setToRotate(-phi);
    getAirplaneRotator()->setTransform(at);
    at.setToScale(scale,.5,.5);
    getScaleTransform()->setTransform(at);
    ::timber::Pointer< ::indigo::SolidFillNode> m(grp->node(2));
    m->setColor(::indigo::Color(1,0,1,cosine));
  }
  else {
    Desktop::enqueue(::std::make_shared<RunnableFunction>(rotate),true);
  }
}


static void changeBackground()
{
  static int count = 0;
  if (MyWindow::win==0) {
    ::std::exit(0);
  }

  if (Desktop::isEventThread()) {
    Reference< LayoutContainer<FlowLayout> > c = MyWindow::win->content();
    if (count%2 == 0) {
      Reference<PushButton>pb(new PushButton(createIcon()));
      
      Reference<Label>lb(new Label("THE TEXT IN THE MIDDLE"));
      pb->setResizeCapability(ResizeCapability::BOTH);
      lb->setResizeCapability(ResizeCapability::BOTH);
      c->addComponent(createFlowContent(),c->componentCount());
      c->addComponent(lb,c->componentCount());
      c->addComponent(createFlowContent(),c->componentCount());
      c->addComponent(pb,c->componentCount());
      c->addComponent(createFlowContent(),c->componentCount());
    }
    else while (c->componentCount()>5) {
      c->removeComponent(0);
    }
  }
  else {
    Desktop::enqueue(::std::make_shared<RunnableFunction>(changeBackground),true);
    ++count;
  }
}



static void initialize()
{
  ::std::cerr << "Initializing" << ::std::endl;
  assert(Desktop::isEventThread());
  
  ::std::cerr << "Getting desktop size" << ::std::endl;
  Size size = Desktop::size();
  ::std::cerr << "Desktop size " << size << ::std::endl;
  //  MyWindow::win = Pointer<MyWindow>(new MyWindow());
  MyWindow::win = new MyWindow();

  ::std::cerr << "Setting title" << ::std::endl;
  MyWindow::win->setTitle("Hello, World!");
  MyWindow::win->setMenuBar(new MenuBar());

  Bounds bounds(100,100, size.width()/2, size.height()/2);
  ::std::cerr << "Setting visibility" << ::std::endl;

  ::std::cerr << "Setting bounds " << bounds << ::std::endl;
  MyWindow::win->requestBounds(bounds);
  MyWindow::win->setTitle("My Little Window Example");
  MyWindow::win->requestSize(Size(700,500));
  MyWindow::win->setBackgroundColor(::nexus::Color(1.,1.,1.));
  MyWindow::win->setVisible(true);

#ifdef USE_TIMER
  // start a timer
  {
    Timer t;
    Runnable r(new RunnableFunction(changeBackground));
    ::gps::thread::timer::Task task = t.createTask("no name", r);
    task.schedule(PeriodicSchedule(Duration::milliseconds(10),1));
    ::gps::thread::timer::Task task2 = t.createTask("rotate", new RunnableFunction(rotator));
    task2.schedule(PeriodicSchedule(Duration::milliseconds(100)));
  }
#endif

  Desktop::addKeyEventInterceptor(new KInterceptor(MyWindow::win));
}


int main(Int argc,  const char** argv)
{
  const char** cargv = const_cast<const char**>(argv);
  Application::instance().saveConfiguration();
  
  Log("nexus.Component").setLevel(Level::INFO);
  Log("nexus.Composite").setLevel(Level::INFO);
  Log("nexus.Window").setLevel(Level::INFO);
  Log("nexus.motif.MotifFont").setLevel(Level::WARN);
  Log("nexus.motif.Menu").setLevel(Level::WARN);

  
  try {
    Component::setDebugEnabled(true);
    Desktop::configure(argc,argv);
    Desktop::enqueue(::std::make_shared<RunnableFunction>(initialize),true);
    Desktop::enqueue(::std::make_shared<RunnableFunction>(changeBackground),true);

    while(true) {
      if (Desktop::enqueue(::std::make_shared<RunnableFunction>(rotate),true)) {
	canopy::mt::Thread::suspend(100000000);
      }
      else {
	break;
      }
    }


    Desktop::waitShutdown();
#ifdef USE_TIMER
    Timer::shutdownTimers();
#endif
  }
  catch (const ::std::exception& e) {
    ::std::cerr << "EXCEPTION " << e.what() << ::std::endl;
    // that's ok, because it most likely happened because
    // we're not allowed to open the display
    return 0;
  }
  catch (...) {
    return 1;
  }
  return 0;
  
}
