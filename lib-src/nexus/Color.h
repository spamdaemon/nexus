#ifndef _NEXUS_COLOR_H
#define _NEXUS_COLOR_H

#ifndef _NEXUS_H
#include <nexus/nexus.h>
#endif

#ifndef _INDIGO_COLOR_H
#include <indigo/Color.h>
#endif

#include <iosfwd>

namespace nexus {
   /**
    * This class is used to define a color
    * in the RGBA color space. Each color
    * is specified as a combination of
    * three colors red,green, and blue.
    * A fourth value, the opacity value
    * is used to represent the color's opacity.
    */
   class Color
   {
         /** The shared color implementation */
      private:
         class SharedImpl : public ::indigo::Color
         {
               SharedImpl(const SharedImpl&);

               /**
                * Create a new shared implementation with a reference count
                * of 1.
                * @param r the red color component [0..1]
                * @param g the green color component [0..1]
                * @param b the blue color component [0..1]
                * @param a the opacity color component [0..1]
                */
            public:
               inline SharedImpl(double r, double g, double b, double a)throws()
               : ::indigo::Color(r,g,b,a) {}

               /** Delete this color */
            public:
               inline ~SharedImpl()throws() {}

               /** Reference this color */
            public:
               inline void incrementCount() const throws()
               {  _count.ref();}

               /** Reduce the reference count */
            public:
               inline void decrementCount() const throws()
               {
                  if (_count.unref()==0) {
                     delete this;
                  }
               }

               /** A reference count */
            private:
               mutable ::timber::ReferenceCount _count;
         };

         /**
          * @name Predefined colors
          * @{
          */
      public:
         static const Color RED;
         static const Color GREEN;
         static const Color BLUE;
         static const Color BLACK;
         static const Color WHITE;
         static const Color YELLOW;
         static const Color CYAN;
         static const Color MAGENTA;
         static const Color LIGHT_GREY;
         static const Color GREY;
         static const Color DARK_GREY;
         /*@}*/

         /**
          * Default constructor.
          * FIXME: this is a dangerous constructor, because it leaves the pointer unitialized
          */
      public:
         Color()throws();

         /**
          * Create a new color from a graphics color.
          * @param c a color
          */
      public:
         Color(const ::indigo::Color& c)throws();

         /**
          * Copy constructor.
          * @param c a color
          */
      public:
         Color(const Color& c)throws();

         /**
          * Create a new color from a graphics color. The opacity of
          * the original color is multiplied by a.
          * @param c a color
          * @param a a multiplicative oopacity value [0..1]
          */
      public:
         Color(const Color& c, double a)throws();

         /**
          * Create a new color with 8-bit RGBI values.
          * @param r the red color component [0..255]
          * @param g the green color component [0..255]
          * @param b the blue color component [0..255]
          * @param i the color opacity [0..255]
          */
      public:
         Color(UByte r, UByte g, UByte b, UByte i)throws();

         /**
          * Create a new color with 8-bit RGB values.
          * @param r the red color component [0..255]
          * @param g the green color component [0..255]
          * @param b the blue color component [0..255]
          */
      public:
         Color(UByte r, UByte g, UByte b)throws();

         /**
          * Create a new color with 16-bit RGBI values.
          * @param r the red color component [0..65535]
          * @param g the green color component [0..26553555]
          * @param b the blue color component [0..65535]
          * @param i the color opacity [0..65535]
          */
      public:
         Color(UShort r, UShort g, UShort b, UShort i)throws();

         /**
          * Create a new color with 16-bit RGB values.
          * @param r the red color component [0..65535]
          * @param g the green color component [0..26553555]
          * @param b the blue color component [0..65535]
          */
      public:
         Color(UShort r, UShort g, UShort b)throws();

         /**
          * Create a new color.
          * @param r the red color component [0..1]
          * @param g the green color component [0..1]
          * @param b the blue color component [0..1]
          * @param i the color opacity [0..1]
          */
      public:
         Color(double r, double g, double b, double i)throws();

         /**
          * Create a new color.
          * @param r the red color component [0..1]
          * @param g the green color component [0..1]
          * @param b the blue color component [0..1]
          */
      public:
         Color(double r, double g, double b)throws();

         /** Destroy this color. */
      public:
         ~Color()throws();

         /**
          * Copy operator.
          * @param c a color
          * @return this color
          */
      public:
         Color& operator=(const Color& c)throws();

         /**
          * Get the red color component.
          * @return the red color component
          */
      public:
         inline double red() const throws() {return _rgbi->red();}

         /**
          * Get the green color component.
          * @return the green color component
          */
      public:
         inline double green() const throws() {return _rgbi->green();}

         /**
          * Get the blue color component.
          * @return the blue color component
          */
      public:
         inline double blue() const throws() {return _rgbi->blue();}

         /**
          * Get the opacity of this color.
          * @return the opacity of this color
          */
      public:
         inline double opacity() const throws() {return _rgbi->opacity();}

         /**
          * Map the color to 8 bit rgba values
          * @param r an output parameter for the red component
          * @param g an output parameter for the green component
          * @param b an output parameter for the blue component
          * @param a an output parameter for opacity (0 translucent, 0xff opaque)
          */
      public:
         inline void rgba8(UByte& r, UByte& g, UByte& b, UByte& a) const throws()
         {  _rgbi->rgba8(r,g,b,a);}

         /**
          * Map the color to 16 bit rgba values
          * @param r an output parameter for the red component
          * @param g an output parameter for the green component
          * @param b an output parameter for the blue component
          * @param a an output parameter for opacity (0 translucent, 0xffff opaque)
          */
      public:
         inline void rgba16(UShort& r, UShort& g, UShort& b, UShort& a) const throws()
         {  _rgbi->rgba16(r,g,b,a);}

         /**
          * Map the color to 32 bit rgba values
          * @param r an output parameter for the red component
          * @param g an output parameter for the green component
          * @param b an output parameter for the blue component
          * @param a an output parameter for opacity (0 translucent, 0xffffffff opaque)
          */
      public:
         inline void rgba32(UInt& r, UInt& g, UInt& b, UInt& a) const throws()
         {  _rgbi->rgba32(r,g,b,a);}

         /**
          * Map the color to 8 bit rgb values
          * @param r an output parameter for the red component
          * @param g an output parameter for the green component
          * @param b an output parameter for the blue component
          */
      public:
         inline void rgb8a(UByte& r, UByte& g, UByte& b) const throws()
         {  _rgbi->rgb8a(r,g,b);}

         /**
          * Map the color to 16 bit rgb values
          * @param r an output parameter for the red component
          * @param g an output parameter for the green component
          * @param b an output parameter for the blue component
          */
      public:
         inline void rgb16a(UShort& r, UShort& g, UShort& b) const throws()
         {  _rgbi->rgb16a(r,g,b);}

         /**
          * Map the color to 32 bit rgb values
          * @param r an output parameter for the red component
          * @param g an output parameter for the green component
          * @param b an output parameter for the blue component
          */
      public:
         inline void rgb32a(UInt& r, UInt& g, UInt& b) const throws()
         {  _rgbi->rgb32a(r,g,b);}

         /**
          * This method may be used to test if the color is 0.
          * @return true if this color reference is 0
          */
      public:
         inline bool operator==(::std::nullptr_t  x) const throws()
         {  return _rgbi==x;}

         /**
          * This method may be used to test if the color is not 0.
          * @return true if this color reference is not 0
          */
      public:
         inline bool operator!=(::std::nullptr_t x) const throws()
         {  return _rgbi!=x;}

         /**
          * Check the two color references are the same.
          * @param f a color
          * @return true if this and f reference the same color instance
          */
      public:
         inline bool operator==(const Color& f) const throws()
         {  return _rgbi==f._rgbi;}

         /**
          * Check the two color references are the same.
          * @param f a color
          * @return true if this and f do not reference the same color instance
          */
      public:
         inline bool operator!=(const Color& f) const throws()
         {  return _rgbi!=f._rgbi;}

         /**
          * A hashcode for this color.
          * @return a hashcode for this font
          */
      public:
         inline size_t hashValue() const throws()
         {  return _rgbi==nullptr ? 0 : _rgbi->hashValue();}

         /**
          * Compare for the equality of two colors by value; the comparison
          * is done by RGB values, which are adjuste for the opacity.
          * @param c a color
          * @return true if c and this color are actually the same
          */
      public:
         bool equals(const Color& c) const throws();

         /**
          * Get the graphic color.
          * @pre REQUIRE_NON_ZERO(*this)
          * @return a gfx::Color object
          */
      public:
         inline ::indigo::Color color() const throws()
         {  return *_rgbi;}

         /** The r,g,b,a values */
      private:
         ::timber::Pointer< SharedImpl> _rgbi;
   };
}

::std::ostream& operator<<(::std::ostream& out, const ::nexus::Color& c)
throws();

#endif
