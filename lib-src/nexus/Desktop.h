#ifndef _NEXUS_DESKTOP_H
#define _NEXUS_DESKTOP_H

#ifndef _NEXUS_H
#include <nexus/nexus.h>
#endif

#ifndef _NEXUS_RUNNABLE_H
#include <nexus/Runnable.h>
#endif

#ifndef _NEXUS_INPUTMODIFIERS_H
#include <nexus/InputModifiers.h>
#endif

#ifndef _NEXUS_COLOR_H
#include <nexus/Color.h>
#endif

#ifndef _NEXUS_SIZE_H
#include <nexus/Size.h>
#endif

#ifndef _NEXUS_PIXEL_H
#include <nexus/Pixel.h>
#endif

#ifndef _INDIGO_FONTFACTORY_H
#include <indigo/FontFactory.h>
#endif

#include <vector>
#include <cstdint>
#include <chrono>
#include <functional>

namespace nexus {
   namespace peer {
      /**
       * @name Forward declarations of native components and desktop
       * @{
       */
      class Desktop;
      class Canvas;
      class Component;
      class Container;
      class Label;
      class Menu;
      class MenuBar;
      class MenuItem;
      class PushButton;
      class TextArea;
      class TextField;
      class Window;

   /*@}*/
   }

   namespace event {
      class KeyEventInterceptor;
   }

   /** The component class */
   class Component;

   /** The desktop component class */
   class DesktopComponent;

   /**
    * This class defines the implementation for the desktop.
    * The desktop is a singleton object whose primary purpose
    * is to create native component objects. The desktop also
    * provides information about the native graphics subsystem.
    * <p>
    * In order to start the desktop, the variable NEXUS_DESKTOP_MODULE_PATH
    * must be defined as a macro and point to the location of the desktop
    * module to be used.
    */
   class Desktop
   {
         Desktop&operator=(const Desktop&);
         Desktop(const Desktop&);
         ~Desktop();
         Desktop();

         /** A delay */
      public:
         typedef ::std::chrono::milliseconds Delay;

         /** A scheduling period */
      public:
         typedef ::std::chrono::milliseconds Period;

         /** A no-delay constant */
      public:
         static const Delay NO_DELAY;

         /** A no-period constant */
      public:
         static const Period NO_PERIOD;

         /**
          * The name of the application property indicating which peer module to load for the desktop.
          * The name of this property is <em>nexus.desktop.provider</em> and is the full path of a dynamic
          * object.
          * @see DesktopModule
          */
      public:
         static const char* DESKTOP_PROVIDER;

         /** The instantiator function */
      private:
         class Instantiator;

         /** The desktop component is a friend of the desktop */
      public:
         friend class DesktopComponent;

         /** The desktop peer */
      public:
         typedef ::nexus::peer::Desktop DesktopPeer;

         /** A component peer */
      public:
         typedef ::nexus::peer::Component ComponentPeer;

         /** The window peer */
      public:
         typedef ::nexus::peer::Window WindowPeer;

         /** The container peer */
      public:
         typedef ::nexus::peer::Container* ContainerPeer;

         /** The canvas peer */
      public:
         typedef ::nexus::peer::Canvas* CanvasPeer;

         /** The button peer */
      public:
         typedef ::nexus::peer::PushButton* PushButtonPeer;

         /** The textfield peer */
      public:
         typedef ::nexus::peer::TextField* TextFieldPeer;

         /** The textarea peer */
      public:
         typedef ::nexus::peer::TextArea* TextAreaPeer;

         /** The label peer */
      public:
         typedef ::nexus::peer::Label* LabelPeer;

         /** The menu item peer */
      public:
         typedef ::nexus::peer::MenuItem* MenuItemPeer;

         /** The menu peer */
      public:
         typedef ::nexus::peer::Menu* MenuPeer;

         /** The menu bar peer */
      public:
         typedef ::nexus::peer::MenuBar* MenuBarPeer;

         /**
          * Configure the desktop.
          * @param argc the number of arguments (may be modified)
          * @param argv the arguments
          * @throw std::exception if the desktop could not be configured
          */
      public:
         static void configure(Int& argc, const char** argv) throws (::std::exception);

          /**
          * Configure the desktop, execute an initalization function and
          * wait for for shutdown. If the initialization function return
          * a non-zero value, then the desktop is shutdown.
          * @param argc the number of arguments (may be modified)
          * @param argv the arguments
          * @param init the initialization function
          * @return the result from running the initialization function
          */
      public:
         static void configure(Int& argc, const char** argv,
               const ::std::function< void()>& init) throws (::std::exception);

         /**
          * Configure the desktop, execute an initalization function and
          * wait for for shutdown. If the initialization function return
          * a non-zero value, then the desktop is shutdown.
          * @param argc the number of arguments (may be modified)
          * @param argv the arguments
          * @param init the initialization function
          * @return the result from running the initialization function
          */
      public:
         static void configure(Int& argc, const char** argv,
               const ::std::function< void(size_t, const char**)>& init) throws (::std::exception);

         /**
          * Shutdown the desktop.
          */
      public:
         static void shutdown() throws();

         /**
          * Suspend the calling thread until the desktop has been shutdown.
          */
      public:
         static void waitShutdown() throws();

         /**
          * Test if the calling thread is the event thread.
          * @return true if the calling thread is the event thread
          */
      public:
         static bool isEventThread() throws();

         /**
          * Run the specified runnable in the event thread. If the current
          * thread is already the event thread, then proc is simply run,
          * otherwise it is enqueued for later execution.
          * @param proc a runnable to run
          * @return true if proc was either run or enqueued, false otherwise
          */
      public:
         static bool run(const ::timber::SharedRef< Runnable>& proc) throws();

         /**
          * Enqueue the specified runnable for execution in the event thread.
          * @param r a runnable
          * @param delayMS the time in milliseconds by which to delay the task or NO_DELAY
          * @return true if the runnable was scheduled, false otherwise
          */
      public:
         static bool enqueue(const ::timber::SharedRef< Runnable>& r, const Delay& delayMS) throws();

         /**
          * Enqueue the specified runnable for execution in the event thread. The task is executed periodically
          * until it is cancelled.
          * @param r a runnable
          * @param delayMS the time in milliseconds by which to delay the task or NO_DELAY
          * @param periodMS the scheduling period or NO_PERIOD if not a repeated task
          * @return true if the runnable was scheduled, false otherwise
          */
      public:
         static bool enqueue(const ::timber::SharedRef< Runnable>& r, const Delay& delayMS,
               const Period& periodMS) throws();

         /**
          * Enqueue the specified runnable for execution in the event thread.
          * If waitForIt is set to true, the calling thread will wait
          * until the runnable has been executed. It is an error to call this
          * method from within the event thread if waitForIt is true.
          * @param r a runnable
          * @param waitForIt if true, then r will be executed before the run exits
          * @return true if the runnable was scheduled, false otherwise
          */
      public:
         static bool enqueue(const ::timber::SharedRef< Runnable>& r, bool waitForIt) throws();

         /**
          * Enqueue the specified runnable for later execution.
          * @param r a runnable
          * @return true if the runnable was scheduled, false otherwise
          */
      public:
         static inline bool enqueue(const ::timber::SharedRef< Runnable>& r) throws()
         {
            return enqueue(r, false);
         }

         /**
          * Enqueue the specified functional for later execution. Since this method
          * waits for the function to complete, it should be only during initialization.
          * @param a function a lambda function
          * @return true if the function was scheduled, false otherwise
          */
      public:
         static bool enqueueAndWait(const ::std::function< void()>& r) throws();

         /**
          * Get the bounds for this desktop.
          * @return the desktop bounds
          */
      public:
         static Size size() throws();

         /**
          * @name Creating and destroying peers.
          * @{
          */

         /**
          * Destroy the specified peer.
          * @param p a component peer
          */
      public:
         static void destroyComponentPeer(ComponentPeer* p) throws();

         /**
          * Create a new window.
          * @return a window or 0 if it could not be created
          */
      public:
         static WindowPeer* createWindow() throws();

         /*
          * Create a new container
          * @param p the parent component peer
          * @return a new container or 0 if it could not be created
          */
      public:
         static ContainerPeer createContainer(ComponentPeer& p) throws();

         /**
          * Create a new canvas for 2d graphics.
          * @param p the parent for the new canvas
          * @return a new canvas for 2d graphics or 0 if it cannot be created
          */
      public:
         static CanvasPeer createCanvas2D(ComponentPeer& p) throws();

         /**
          * Create a new label.
          * @param p the parent for the new label
          * @return a new label or 0 if it cannot be created
          */
      public:
         static LabelPeer createLabel(ComponentPeer& p) throws();

         /**
          * Create a new push button.
          * @param p the parent for the new push button
          * @return a new button or 0 if it cannot be created
          */
      public:
         static PushButtonPeer createPushButton(ComponentPeer& p) throws();

         /**
          * Create a new one line text field
          * @param p the parent for the new text field
          * @return a new text field or 0 if it could not be created
          */
      public:
         static TextFieldPeer createTextField(ComponentPeer& p) throws();

         /**
          * Create a new multiline text area
          * @param p the parent for the new text area
          * @return a new text area or 0 if it could not be created
          */
      public:
         static TextAreaPeer createTextArea(ComponentPeer& p) throws();

         /**
          * Create a new menu bar.
          * @param win a window peer
          * @return a new menu bar
          */
      public:
         static MenuBarPeer createMenuBar(WindowPeer& win) throws();

         /**
          * Create a new menu.
          * @param  p the parent menu
          * @return a new menu
          */
      public:
         static MenuPeer createMenu(ComponentPeer& p) throws();

         /**
          * Create a new menu item.
          * @param  p the parent menu
          * @return a new menu
          */
      public:
         static MenuItemPeer createMenuItem(::nexus::peer::Menu& p) throws();

         /*@}*/

         /**
          * Set the default background color.
          * @param c the default background color or 0 for WHITE
          */
      public:
         static void setDefaultBackgroundColor(const Color& c) throws();

         /**
          * Get the default background color. The returned
          * color is never 0.
          * @return the default background color
          */
      public:
         static Color defaultBackgroundColor() throws();

         /**
          * Set the default foreground color.
          * @param c the default foreground color or 0 for BLACK
          */
      public:
         static void setDefaultForegroundColor(const Color& c) throws();

         /**
          * Get the default foreground color. The returned
          * color is never 0.
          * @return the default foreground color
          */
      public:
         static Color defaultForegroundColor() throws();

         /**
          * Get the current pointer position.
          * @return the position of the pointer
          */
      public:
         static Pixel pointerLocation() throws();

         /**
          * @name Font manipulation.
          * @{
          */

         /**
          * Get the font factory.
          * @return the font factory
          */
      public:
         static ::timber::Reference< ::indigo::FontFactory> fonts() throws();

         /**
          * Set the default font.
          * @param f a the default font
          */
      public:
         static void setDefaultFont(const ::timber::Pointer< ::indigo::Font>& f) throws();

         /**
          * Get the default font.
          * @return the default font.
          */
      public:
         static ::timber::Pointer< ::indigo::Font> defaultFont() throws();

         /**
          * Get a font that matches the specified
          * description.
          * @param d a font description
          * @return the font that best matches the description
          */
      public:
         static ::timber::Pointer< ::indigo::Font> getFont(const ::indigo::FontDescription& d) throws();

         /**
          * Get the input modifiers supported by this desktop.
          * @return the input modifiers supported by this desktop.
          */
      public:
         static InputModifiers getInputModifiers() throws();

         /*@}*/

         /**
          * Test if the specified component is actually displayed on the screen
          * @param c a component
          */
      public:
         static bool isComponentShowing(::timber::Pointer< Component> c) throws();

         /**
          * Compute appropriate shadow and highlight colors given a background color.
          * @param bg a background color
          * @param shadow the shadow color
          * @param highlight the highlight color.
          */
      public:
         static void computeShadowHighlightColors(const Color& bg, ::indigo::Color& shadow,
               ::indigo::Color& highlight) throws();

         /**
          * Add a desktop component for which a peer has been created.
          * The reference count for the component will be increased by 1.
          * @param c a component
          * @pre REQUIRE_TRUE(c->hasPeer())
          */
      private:
         static void addComponent(DesktopComponent* c) throws();

         /**
          * Remove a component that no longer has a peer. The
          * reference count will be decreased by 1, possibly destroying
          * the component.
          * @param c a component
          * @pre REQUIRE_FALSE(c->hasPeer())
          */
      private:
         static void removeComponent(DesktopComponent* c) throws();

         /**
          * Remove all components
          */
      private:
         static void removeComponents() throws();

         /**
          * Add an interceptor for a key event.
          * @param interceptor a key event interceptor
          */
      public:
         static void addKeyEventInterceptor(
               const ::timber::Reference< ::nexus::event::KeyEventInterceptor>& interceptor) throws();

         /**
          * Remove an interceptor for a key event.
          * @param interceptor a key event interceptor
          */
      public:
         static void removeKeyEventInterceptor(
               const ::timber::Pointer< ::nexus::event::KeyEventInterceptor>& interceptor) throws();

   };
}

#endif
