#ifndef _NEXUS_FLOW_H
#define _NEXUS_FLOW_H

#ifndef _NEXUS_H
#include <nexus/nexus.h>
#endif

namespace nexus {

  /**
   * This class defines a horizontal flow.
   */
  class Flow {
    /** The flow values */
  public:
    enum FlowValue { 
      FLOW_LEFT_TO_RIGHT,   //< something flowing left to right
      FLOW_TOP_TO_BOTTOM    //< something flowing top to bottom
    };
      
    /** A horizontal flow from left to right */
  public:
    static const Flow HORIZONTAL;

    /** A vertical flow from top to bottom */
  public:
    static const Flow VERTICAL;

    /** Flow from left to right */
  public:
    static const Flow LEFT_TO_RIGHT;

    /** Flow from top to bottom */
  public:
    static const Flow TOP_TO_BOTTOM;

    /**
     * Create a new flow
     * @param a an flow
     */
  public:
    inline Flow (FlowValue a) throws()
      : _flow(a) {}

    /** Create horizontal flow. */
  public:
    inline Flow () throws()
      : _flow(FLOW_LEFT_TO_RIGHT) {}
      
    /**
     * Compute a hashvalue.
     * @return a hashvalue
     */
  public:
    inline size_t hashValue () const throws()
    { return _flow; }
      
    /**
     * Compare two flows.
     * @param a an flow
     * @return true if the two flows are the same
     */
  public:
    inline bool operator==(const Flow& a) const throws()
    { return _flow == a._flow; }

    /**
     * Compare two flows.
     * @param a an flow
     * @return true if the two flows are the same
     */
  public:
    inline bool equals(const Flow& a) const throws()
    { return *this == a; }
      
    /**
     * Compare two flows.
     * @param a an flow
     * @return true if the two flows are different
     */
  public:
    inline bool operator!=(const Flow& a) const throws()
    { return _flow != a._flow; }

    /**
     * Get the flow value. This method is primarily
     * used in switch statements.
     * @return the flow value
     */
  public:
    inline FlowValue value () const throws() { return _flow; }

    /**
     * Test if this is a vertical or a horizontal flow 
     * @return true if this flow is a vertical flow
     */
  public:
    inline bool isVertical () const throws()
    { return _flow==FLOW_TOP_TO_BOTTOM; }

    /**
     * Test if this is a vertical or a horizontal flow 
     * @return true if this flow is a horiontal flow
     */
  public:
    inline bool isHorizontal () const throws()
    { return _flow==FLOW_LEFT_TO_RIGHT; }

    /** A flow */
  private:
    FlowValue _flow;
  };
}


#endif
