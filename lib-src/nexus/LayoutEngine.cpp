#include <nexus/LayoutEngine.h>

namespace nexus {
  
    
  LayoutEngine::Observer::~Observer() throws()
  {}

  LayoutEngine::~LayoutEngine() throws()
  {}

  void LayoutEngine::addObserver (const ::timber::Reference<Observer>& obs) throws()
  {
    _observers.addListener(obs);
  }
      
  void LayoutEngine::removeObserver (const ::timber::Pointer<Observer>& obs) throws()
  { _observers.removeListener(obs); }
      
  void LayoutEngine::layoutChanged () throws()
  {
    if (_observers.listenerCount()>0) {
      _observers.notify(&Observer::layoutChanged,*this);
    }
  }

}

