#include <nexus/PushButton.h>
#include <nexus/event/ActionEvent.h>

namespace nexus {

   PushButton::PushButton() throws (::std::runtime_error)
         : _borderWidth(DEFAULT_BORDER_WIDTH), _observer(0)
   {
      updatePreferredSize();
   }

   PushButton::PushButton(const ::std::string& t) throws (::std::runtime_error)
         : _text(t), _borderWidth(DEFAULT_BORDER_WIDTH), _observer(0)
   {
      updatePreferredSize();
   }

   PushButton::PushButton(const Icon& i) throws (::std::runtime_error)
         : _icon(i), _borderWidth(DEFAULT_BORDER_WIDTH), _observer(0)
   {
      assert(i.graphic());
      updatePreferredSize();
   }

   PushButton::~PushButton() throws()
   {
      delete _observer;
   }

   void PushButton::setBorderWidth(Int w) throws()
   {
      assert(w >= 0);
      if (_borderWidth != w) {
         _borderWidth = w;
         if (hasPeer()) {
            button()->setBorderWidth(w);
         }
         if (updatePreferredSize()) {
            revalidate();
         }
      }
   }

   bool PushButton::updatePreferredSize() const throws()
   {
      Size sz;
      if (_icon.graphic() != nullptr) {
         sz = _icon.size();
      }
      else if (!_text.empty()) {
         // search for a font
         ::timber::Pointer< ::indigo::Font> f = font();
         if (f) {
            sz = f->stringBounds(_text);
         }
      }

      sz = sz.adjust(2 * borderWidth(), 2 * borderWidth());
      if (sz.width() == 0 || sz.height() == 0) {
         sz = Size(::std::max(sz.width(), 1), ::std::max(sz.height(), 1));
      }

      if (sz != _preferredSize) {
         _preferredSize = sz;
         return true;
      }
      return false;
   }

   void PushButton::setText(const ::std::string& t) throws()
   {
      if (t != _text) {
         _text = t;
         _icon = Icon();
         if (hasPeer()) {
            button()->setText(_text);
         }
         if (updatePreferredSize()) {
            revalidate();
         }
      }
   }

   void PushButton::setIcon(const Icon& i) throws()
   {
      if (_icon.size() != i.size() || i.graphic() != _icon.graphic()) {
         _text.clear();
         _icon = i;
         if (hasPeer()) {
            button()->setIcon(_icon);
         }
         if (updatePreferredSize()) {
            revalidate();
         }
      }
   }

   SizeHints PushButton::getSizeHints() const throws()
   {
      Int w = Size::MAX.width();
      Int h = Size::MAX.height();

      switch (_resizeCapability.value()) {
         case ResizeCapability::RESIZE_NONE:
            w = _preferredSize.width();
            h = _preferredSize.height();
            break;
         case ResizeCapability::RESIZE_VERTICAL:
            w = _preferredSize.width();
            break;
         case ResizeCapability::RESIZE_HORIZONTAL:
            h = _preferredSize.height();
            break;
         case ResizeCapability::RESIZE_BOTH:
         default:
            break;
      };

      return SizeHints(_preferredSize, Size(w, h), _preferredSize);
   }

   void PushButton::fontChanged() throws()
   {
      updatePreferredSize();
      revalidate();
   }

   void PushButton::initializePeer() throws()
   {
      struct Observer : public Peer::Observer
      {
            inline Observer(PushButton& b) throws()
                  : _owner(b)
            {
            }
            ~Observer() throws()
            {
            }
            void pushButtonActivated() throws()
            {
               _owner.processActionEvent(new ActionEvent(&_owner, _owner.actionID()));
            }

            void pushButtonPressed() throws()
            {
            }

            void pushButtonReleased() throws()
            {
            }

         private:
            PushButton& _owner;
      };

      Component::initializePeer();
      if (_icon.graphic() != nullptr) {
         button()->setIcon(_icon);
      }
      else if (!_text.empty()) {
         button()->setText(_text);
      }
      button()->setBorderWidth(_borderWidth);
      if (_observer == 0) {
         _observer = new Observer(*this);
      }

      button()->setPushButtonObserver(_observer);
   }

   PushButton::PeerComponent* PushButton::createPeerComponent(PeerComponent* p) throws()
   {
      assert(p != 0);
      return Desktop::createPushButton(*p);
   }

   void PushButton::setResizeCapability(const ResizeCapability& rc) throws()
   {
      if (_resizeCapability != rc) {
         _resizeCapability = rc;
         if (rc != ResizeCapability::BOTH) {
            revalidate();
         }
      }
   }

   void PushButton::setActionID(const ::std::string& s) throws()
   {
      _actionID = s;
   }

   void PushButton::addActionEventListener(const ::timber::Reference< ::nexus::event::ActionEventListener>& l) throws()
   {
      _actionListeners.addListener(l);
   }

   void PushButton::removeActionEventListener(const ::timber::Pointer< ::nexus::event::ActionEventListener>& l) throws()
   {
      _actionListeners.removeListener(l);
   }

   void PushButton::processActionEvent(const ::timber::Reference< ActionEvent>& e) throws()
   {
      _actionListeners.notify(&::nexus::event::ActionEventListener::processActionEvent, e);
   }

}

