#ifndef _NEXUS_FONT_H
#define _NEXUS_FONT_H

#ifndef _INDIGO_FONT_H
#include <indigo/Font.h>
#endif

namespace nexus {
  
  using ::indigo::Font;
}

#endif
