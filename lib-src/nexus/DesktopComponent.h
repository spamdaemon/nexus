#ifndef _NEXUS_DESKTOPCOMPONENT_H
#define _NEXUS_DESKTOPCOMPONENT_H

#ifndef _NEXUS_COMPONENT_H
#include <nexus/Component.h>
#endif

#include <stdexcept>

namespace nexus {
  /**
   * This abstract components defines a desktop component.
   * Desktop components cannot have parents.
   */
  class DesktopComponent : public Component {

    /** The desktop is a friend, so that it may invoke destroyPeer() */
    friend class Desktop;
	
    /**
     * Default constructor 
     * @throw std::exception if the desktop has not been configured
     */
  public:
    DesktopComponent () throws (::std::exception);
	    
    /** Destroy this desktop component. */
  public:
    ~DesktopComponent () throws();

    /**
     * Return true if this component is actually visible
     * on the desktop, false otherwise.
     * @return true if this component is visible
     */
  public:
    inline bool isShowing () const throws()
    { return _showing; }

    /**
     * Create the peer. If the peer is actually created,
     * then this component is registered with the Desktop.
     */
  protected:
    void initializePeer() throws();

    /**
     * Destroy the peer. This deregisteres the component from
     * the desktop and set isShowing() to false.
     */
  protected:
    void destroyPeer() throws();

    /**
     * Set the font. If the font is cleared, then
     * set the default font.
     * @param f a font or 0 to use the default font
     */
  public:
    void setFont (const ::timber::Pointer< ::indigo::Font>& f) throws();

    /**
     * Set the foreground color. The default color is 
     * specified by the Desktop.
     * @param fg the foreground color or 0 to use a default color
     */
  public:
    void setForegroundColor (const Color& fg) throws();

    /**
     * Set the background color. The default color is 
     * specified by the Desktop.
     * @param bg the background color or 0 to use a default color
     */
  public:
    void setBackgroundColor (const Color& bg) throws();

    /**
     * Set the showing flag. 
     * @param showing true if this component is showing, false otherwise.
     */
  protected:
    void setShowing (bool showing) throws();

    /** True if this component is currently showing, false otherwise */
  private:
    bool _showing;
  };
}
#endif
