#include <nexus/peer/Desktop.h>			
#include <nexus/peer/Component.h>

namespace nexus {
   namespace peer {

      Desktop::KeyInterceptor::~KeyInterceptor() throws()
      {
      }

      Desktop::~Desktop() throws()
      {
      }

      void Desktop::destroyComponentPeer(Component* cp) throws()
      {
         delete cp;
      }

      void Desktop::computeShadowHighlightColors(const Color& bg, ::indigo::Color& shadow,
            ::indigo::Color& highlight) throws()
      {
         if (bg == 0) {
            highlight = ::indigo::Color::LIGHT_GREY;
            shadow = ::indigo::Color::DARK_GREY;
         }
         else {
            ::indigo::Color c(bg.color());
            c.normalize();
            double mx = ::std::max(::std::max(c.red(), c.green()), c.blue());

            double shadowMult, hiliteMult;
            if (mx > .85) {
               shadowMult = .48;
               hiliteMult = .80;
            }
            else {
               shadowMult = 0.75;
               hiliteMult = 1.25;
            }

            shadow = c.getScaledColor(1, 1, 1, shadowMult);
            highlight = c.getScaledColor(1, 1, 1, hiliteMult);

            shadow.normalize();
            if (hiliteMult != 1.0) {
               highlight.normalize();
            }

         }
      }

   }
}
