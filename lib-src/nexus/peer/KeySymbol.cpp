#include <nexus/peer/KeySymbol.h>

namespace nexus {
  namespace peer {
    
    Int KeySymbol::asciiChar() const throws()
    {
      const ::std::string s(symbols());
      if (s.length()==1 && static_cast<UByte>(s[0])<128) {
	return static_cast<unsigned char>(s[0]);
      }
      return -1;
    }
  }
}
