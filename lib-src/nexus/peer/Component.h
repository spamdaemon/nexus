#ifndef _NEXUS_PEER_COMPONENT_H
#define _NEXUS_PEER_COMPONENT_H

#ifndef _NEXUS_H
#include <nexus/nexus.h>
#endif

#ifndef _NEXUS_FONT_H
#include <nexus/Font.h>
#endif

#ifndef _NEXUS_TIME_H
#include <nexus/Time.h>
#endif

namespace nexus {
   /**
    * @name Forward declarations
    * @{
    */
   class Bounds;
   class Color;
   class InputModifiers;
   class Insets;
   /*@}*/

   namespace peer {

      /** Forward declaration of the desktop class */
      class Desktop;

      /** A key symbol */
      class KeySymbol;

      /**
       * This interface must be implemented
       * by all native components.
       *
       */
      class Component
      {
            Component(const Component&) = delete;
            Component&operator=(const Component&) = delete;

            /** The desktop is a friend of this class so that it can delete an instance of it */
         public:
            friend class Desktop;

            /** A component observer */
         public:
            class ComponentObserver
            {

                  /** Destructor */
               public:
                  virtual ~ComponentObserver() throws() = 0;

                  /**
                   * Notify a component that insets have changed.
                   * @param insets the bounds of the component
                   */
               public:
                  virtual void notifyInsets(const ::nexus::Insets& insets) throws() = 0;

                  /**
                   * Notify that this component's bounds have changed. Not all components
                   * will generate this event. This is primarily for those components
                   * whose location is managed by the underlying toolkit.
                   * @param bounds the bounds of the component
                   */
               public:
                  virtual void notifyBounds(const ::nexus::Bounds& bounds) throws() = 0;
            };

            /** A button observer. */
         public:
            class ButtonObserver
            {
                  /** Destroy this observer */
               public:
                  virtual ~ButtonObserver() throws() = 0;

                  /**
                   * A (mouse) button button was pressed.
                   * @param x the current x position
                   * @param y the current y position
                   * @param b the button that was pressed
                   * @param when time the button was pressed
                   * @param m the state of modifier keys and buttons at the time of the event
                   */
               public:
                  virtual void buttonPressed(Int x, Int y, Int b, const Time& when,
                        const InputModifiers& m) throws() = 0;

                  /**
                   * A (mouse) button was released.
                   * @param x the current x position
                   * @param y the current y position
                   * @param b the button that was released
                   * @param when time the button was released
                   * @param m the state of modifier keys and buttons at the time of the event
                   */
               public:
                  virtual void buttonReleased(Int x, Int y, Int b, const Time& when,
                        const InputModifiers& m) throws() = 0;

                  /**
                   * A (mouse) button was pressed and released without an intervening pointer motion.
                   * @param x the current x position
                   * @param y the current y position
                   * @param b the button that was released
                   * @param when time the button was released
                   * @param m the state of modifier keys and buttons at the time of the event
                   */
               public:
                  virtual void buttonClicked(Int x, Int y, Int b, const Time& when,
                        const InputModifiers& m) throws() = 0;
            };

            /** A motion observer. */
         public:
            class MotionObserver
            {
                  /** Destroy this observer */
               public:
                  virtual ~MotionObserver() throws() = 0;

                  /**
                   * Process a mouse event.
                   * @param x the current x position
                   * @param y the current y position
                   * @param when time the button was moved
                   * @param m the state of modifier keys and buttons at the time of the event
                   */
               public:
                  virtual void moved(Int x, Int y, const Time& when, const InputModifiers& m) throws() = 0;
            };

            /** A (mouse)wheel observer */
         public:
            class WheelObserver
            {
                  /** Destroy this observer */
               public:
                  virtual ~WheelObserver() throws() = 0;

                  /**
                   * Process a mouse event.
                   * @param up if true then wheel was moved up, otherwise it was moved down
                   * @param when time the button was moved
                   * @param m the state of modifier keys and buttons at the time of the event
                   */
               public:
                  virtual void wheelMoved(bool up, const Time& when, const InputModifiers& m) throws() = 0;
            };

            /** A key observer. */
         public:
            class KeyObserver
            {
                  /** Destroy this observer */
               public:
                  virtual ~KeyObserver() throws() = 0;

                  /**
                   * A keyboard key was pressed.
                   * @param ks the keysymbol of the key that was pressed
                   * @param when time the button was pressed
                   * @param m the state of modifier keys and buttons at the time of the event
                   * @pre REQUIRE_NON_ZERO(ks);
                   */
               public:
                  virtual void keyPressed(const ::timber::Reference< KeySymbol>& ks, const Time& when,
                        const InputModifiers& m) throws() = 0;

                  /**
                   * A keyboard key was typed.
                   * @param ks the keysymbol of the key that was typed
                   * @param when time the button was typed
                   * @param m the state of modifier keys and buttons at the time of the event
                   * @pre REQUIRE_NON_ZERO(ks);
                   */
               public:
                  virtual void keyTyped(const ::timber::Reference< KeySymbol>& ks, const Time& when,
                        const InputModifiers& m) throws() = 0;

                  /**
                   * A keyboard key was released.
                   * @param ks the keysymbol of the key that was released
                   * @param when time the button was released
                   * @param m the state of modifier keys and buttons at the time of the event
                   * @pre REQUIRE_NON_ZERO(ks);
                   */
               public:
                  virtual void keyReleased(const ::timber::Reference< KeySymbol>& ks, const Time& when,
                        const InputModifiers& m) throws() = 0;
            };

            /** A focus observer. */
         public:
            class FocusObserver
            {
                  /** Destroy this observer */
               public:
                  virtual ~FocusObserver() throws() = 0;

                  /**
                   * This component has gained the focus.
                   */
               public:
                  virtual void focusGained() throws() = 0;

                  /**
                   * This component has lost the focus.
                   */
               public:
                  virtual void focusLost() throws() = 0;
            };

            /** A crossing observer. */
         public:
            class CrossingObserver
            {
                  /** Destroy this observer */
               public:
                  virtual ~CrossingObserver() throws() = 0;

                  /**
                   * This pointer has left this component.
                   * @param when the time the pointer left this component
                   * @param m the state of modifier keys and buttons at the time of the event
                   */
               public:
                  virtual void componentExited(const Time& when, const InputModifiers& m) throws() = 0;

                  /**
                   * This pointer has entered this component.
                   * @param when the time the component has entered
                   * @param m the state of modifier keys and buttons at the time of the event
                   */
               public:
                  virtual void componentEntered(const Time& when, const InputModifiers& m) throws() = 0;
            };

            /** Default constructor */
         protected:
            inline Component() throws()
            {
            }

            /** Destroy this component and release all its resources */
         protected:
            virtual ~Component() throws() = 0;

            /**
             * @name Setting observers
             * @{
             */

            /**
             * Set the component observer.
             * @param l the component observer
             */
         public:
            virtual void setComponentObserver(ComponentObserver* l) throws() = 0;

            /**
             * Set or remove a button observer. If there is already
             * an observer, then it is replaced with the new one.
             * Deletion of the observer is the responsibility of the caller.
             * @param l a window observer or 0 to remove the current one
             */
         public:
            virtual void setButtonObserver(ButtonObserver* l) throws() = 0;

            /**
             * Set or remove a wheel observer. If there is already
             * an observer, then it is replaced with the new one.
             * Deletion of the observer is the responsibility of the caller.
             * @param l a window observer or 0 to remove the current one
             */
         public:
            virtual void setWheelObserver(WheelObserver* l) throws() = 0;

            /**
             * Set or remove a motion observer. If there is already
             * an observer, then it is replaced with the new one.
             * Deletion of the observer is the responsibility of the caller.
             * @param l a window observer or 0 to remove the current one
             */
         public:
            virtual void setMotionObserver(MotionObserver* l) throws() = 0;

            /**
             * Set or remove a keyboard observer. If there is already
             * an observer, then it is replaced with the new one.
             * Deletion of the observer is the responsibility of the caller.
             * @param l a window observer or 0 to remove the current one
             */
         public:
            virtual void setKeyObserver(KeyObserver* l) throws() = 0;

            /**
             * Set or remove a focus observer. If there is already
             * an observer, then it is replaced with the new one.
             * Deletion of the observer is the responsibility of the caller.
             * @param l a window observer or 0 to remove the current one
             */
         public:
            virtual void setFocusObserver(FocusObserver* l) throws() = 0;

            /**
             * Set or remove a crossing observer. If there is already
             * an observer, then it is replaced with the new one.
             * Deletion of the observer is the responsibility of the caller.
             * @param l a window observer or 0 to remove the current one
             */
         public:
            virtual void setCrossingObserver(CrossingObserver* l) throws() = 0;

            /*@}*/

            /**
             * Show or hide this component. The component will only
             * be visible if its container is also visible.
             * @param visible true to show this component false otherwise.
             */
         public:
            virtual void setVisible(bool visible) throws() = 0;

            /**
             * Enable or disable this component.
             * @param enabled true if this component is enabled, false to disable it
             */
         public:
            virtual void setEnabled(bool enabled) throws() = 0;

            /**
             * Set the foreground color. The <code>inherited</code> parameter
             * is only a hint and need not be used.
             * @param c the foreground color
             * @param inherited true if the color is actually inherited
             * @pre REQUIRE_NON_ZERO(c)
             */
         public:
            virtual void setForegroundColor(const Color& c, bool inherited) throws() = 0;

            /**
             * Set the background color. The <code>inherited</code> parameter
             * is only a hint and need not be used.
             * @param c the background color or 0 to inherit the parent's color
             * @param inherited true if the color is actually inherited
             * @pre REQUIRE_NON_ZERO(c)
             */
         public:
            virtual void setBackgroundColor(const Color& c, bool inherited) throws() = 0;

            /**
             * Set the font. The <code>inherited</code> parameter
             * is only a hint and need not be used.
             * @param f a font or 0 to clear the current font
             * @param inherited true if the color is actually inherited
             */
         public:
            virtual void setFont(const ::timber::Pointer< ::indigo::Font>& f, bool inherited) throws() = 0;

            /**
             * Update the visual for this component.
             */
         public:
            virtual void refresh() throws() = 0;
      };
   }
}

#endif
