#ifndef _NEXUS_PEER_MENU_H
#define _NEXUS_PEER_MENU_H

#ifndef _NEXUS_PEER_COMPONENT_H
#include <nexus/peer/Component.h>
#endif

namespace nexus {
  /**
   * @name Forward declartions
   * @{
   */
  class Icon;
  /*@}*/
  namespace peer {

    /**
     * This interface defines a native menu. A menu
     * is a passive component which can be display 
     * either as text or as a graphic icon, but not both.
     *
     * @date 05 Jun 2003
     */
    class Menu : public virtual Component {
      Menu&operator=(const Menu&);
      Menu(const Menu&);

      /** A push button observer */
    public:
      class Observer {
	/** Destroy this observer */
      public:
	virtual ~Observer () throws() = 0;

      };

      /** Default constructor */
    protected:
      inline Menu () throws()
      {}
	
      /** Destroy this menu and release all its resources */
    protected:
      virtual ~Menu () throws() = 0;

      /**
       * Set or remove the menu  observer
       * Deletion of the observer is the responsibility of the caller.
       * @param l a menu observer or 0 to remove the current observer
       */
    public:
      virtual void setMenuObserver (Observer* l) throws() = 0;
	
      /** 
       * Set the menus's text.
       * @param t the text
       */
    public:
      virtual void setText (const ::std::string& t) throws() = 0;

      /** 
       * Set the menus's icon. 
       * @param i an icon
       */
    public:
      virtual void setIcon (const Icon& i) throws() = 0;
    };
  }
}

#endif
