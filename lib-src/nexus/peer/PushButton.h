#ifndef _NEXUS_PEER_PUSHBUTTON_H
#define _NEXUS_PEER_PUSHBUTTON_H

#ifndef _NEXUS_PEER_COMPONENT_H
#include <nexus/peer/Component.h>
#endif

namespace nexus {
  /**
   * @name Forward declartions
   * @{
   */
  class Icon;
  class Size;
  /*@}*/

  namespace peer {

    /**
     * This interface defines a native button. A button
     * is a passive component which can be display 
     * either as text or as a graphic icon, but not both.
     *
     */
    class PushButton : public virtual Component {
      PushButton&operator=(const PushButton&);
      PushButton(const PushButton&);

      /** A push button observer */
    public:
      class Observer {
	/** Destroy this observer */
      public:
	virtual ~Observer () throws() = 0;

	/**
	 * The button was pressed, but not yet released.
	 */
      public:
	virtual void pushButtonPressed () throws() = 0;

	/**
	 * The button was released. This does not imply
	 * that the action was invoked.
	 */
      public:
	virtual void pushButtonReleased () throws() = 0;

	/**
	 * The button was activated. This method is invoked
	 * when the pointer was pressed and released over this button.
	 */
      public:
	virtual void pushButtonActivated () throws() = 0;

      };

      /** Default constructor */
    protected:
      inline PushButton () throws()
      {}
	
      /** Destroy this button and release all its resources */
    protected:
      virtual ~PushButton () throws() = 0;

      /**
       * Set or remove the push button observer.
       * Deletion of the observer is the responsibility of the caller.
       * @param l a push button observer or 0 to remove the current observer
       */
    public:
      virtual void setPushButtonObserver (Observer* l) throws() = 0;
	
      /** 
       * Set the button's text.
       * @param t the text
       */
    public:
      virtual void setText (const ::std::string& t) throws() = 0;

      /** 
       * Set the button's icon. 
       * @param i an icon
       */
    public:
      virtual void setIcon (const Icon& i) throws() = 0;

      /**
       * Set the border width.
       * @param w the width or thickness of the border.
       */
    public:
      virtual void setBorderWidth (Int w) throws() = 0;
    };
  }
}

#endif
