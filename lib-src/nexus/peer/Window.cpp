#include <nexus/peer/Window.h>

namespace nexus {
  namespace peer {
    
    Window::~Window() throws() {}
    
    Window::WindowObserver::~WindowObserver() throws() {}
    
  }
}
