#ifndef _NEXUS_PEER_WINDOW_H
#define _NEXUS_PEER_WINDOW_H

#ifndef _NEXUS_PEER_COMPONENT_H
#include <nexus/peer/Component.h>
#endif

namespace nexus {
  /** Forward declare size */
  class Size;

  /** Forward declare bounds */
  class Bounds;

  namespace peer {
    /**
     * This interface must be implemented by classes
     * that support composition.
     *
     */
    class Window : public virtual Component {
      Window(const Window&);
      Window&operator=(const Window&);

      /**
       * A window observer interface.
       */
    public:
      class WindowObserver {
	/** Destroy this observer */
      public:
	virtual ~WindowObserver() throws() = 0;

	/**
	 * The window bounds have changed.
	 *
	 * @param b the new window bounds.
	 */
      public:
	virtual void windowBoundsChanged (const Bounds& b) throws() = 0;

	/** 
	 * The window's visibility has changed.
	 * @param visible true if the window has become visible, false if it has
	 * become hidden
	 */
      public:
	virtual void windowVisibilityChanged (bool visible) throws() = 0;

	/** 
	 * The user has requested that the window be closed. When processing
	 * this method, the window must either be closed or deleted.
	 */
      public:
	virtual void windowClosedByUser () throws() = 0;

	/**
	 * This window has been destroyed.
	 */
      public:
	virtual void windowDestroyed () throws() = 0;
      };

      /** Default constructor */
    protected:
      inline Window () throws() {}

      /** Destroy this window and release all its resources */
    protected:
      virtual ~Window () throws() = 0;
	
      /**
       * Set the window title.
       * @param t the new window title
       */
    public:
      virtual void setTitle (const ::std::string& t) throws() = 0;

      /**
       * Set the minimum size for this window. If both 
       * the maximum and minimum sizes are the same, then
       * the window will not be resizable.
       * @param sz the minimum window size
       */
    public:
      virtual void setMinimumSize (const Size& sz) throws() = 0;

      /**
       * Set the maximum size for this window. If both 
       * the maximum and minimum sizes are the same, then
       * the window will not be resizable.
       * @param sz the maximum window size.
       */
    public:
      virtual void setMaximumSize (const Size& sz) throws() = 0;

      /**
       * Set or remove a window window observer. If there is already
       * a window observer, then it is replaced with the new one.
       * Deletion of the observer is the responsibility of the caller.
       * @param l a window observer or 0 to remove the current one
       */
    public:
      virtual void setWindowObserver (WindowObserver* l) throws() = 0;

      /**
       * Set the bounds for this window.
       * @param b the bounds for this window
       */
    public:
      virtual void setBounds (const Bounds& b) throws() = 0;

      /**
       * Set the component bounds.
       * @param c a child of this container
       * @param b the new bounds
       * @pre REQUIRE_CONDITION(b,b.width()>0 && b.height()>0)
       */
    public:
      virtual void setChildBounds (Component& c, const Bounds& b) throws() = 0;

    };
  }
}

#endif

