#ifndef _NEXUS_PEER_CONTAINER_H
#define _NEXUS_PEER_CONTAINER_H

#ifndef _NEXUS_PEER_COMPONENT_H
#include <nexus/peer/Component.h>
#endif

#ifndef _INDIGO_NODE_H
#include <indigo/Node.h>
#endif

namespace nexus {
  
  /** Forward declare Bounds */
  class Bounds;

  namespace peer {

    /**
     * This interface must be implemented by classes
     * that support composition.
     *
     */
    class Container : public virtual Component {
      Container(const Container&);
      Container&operator=(const Container&);

      /** Default constructor */
    protected:
      inline Container () throws() {}

      /** Destroy this container and release all its resources */
    protected:
      virtual ~Container () throws() = 0;

      /**
       * Set the component bounds.
       * @param c a child of this container
       * @param b the new bounds
       * @pre REQUIRE_CONDITION(b,b.width()>0 && b.height()>0)
       */
    public:
      virtual void setChildBounds (Component& c, const Bounds& b) throws() = 0;

      /**
       * Enable interception of events of children. All events that would
       * normally be sent to children of this component are sent to this
       * component instead.
       * @param enable if true, then enable event interception
       */
    public:
      virtual void setInterceptEventsEnabled (bool enable) throws() = 0;

      /**
       * Enable or disable double buffering. This is merely a hint
       * and need not be respected; if resource allocation should 
       * fail for double buffering, then rendering should be done
       * with singe-buffering.<br>
       * This method is optional with the default implementation 
       * doing nothing.
       * @param enabled true if double buffering should be enabled.
       */
    public:
      virtual void setDoubleBufferEnabled (bool enabled) throws();

      /**
       * Set the background graphic node to be displayed in this container.
       * Any changes to graphic node or its children must cause this container to repaint itself.
       * The graphic must appear below any children of this container.
       * @param gfx a node or 0 to remove the current node
       */
    public:
      virtual void setBackgroundGraphic (const ::timber::Pointer< ::indigo::Node> & gfx) throws() = 0;
	
      /**
       * Set the border graphic. The border graphic is to be drawn on top of the background graphic.
       * @param gfx a border graphic or 0 to clear it
       */
    public:
      virtual void setBorderGraphic (const ::timber::Pointer< ::indigo::Node> & gfx) throws() = 0;

      /**
       * Add another component to this container
       * @param c a component
       * @param i a an index
       */
    public:
      //	virtual void addComponent (Component* c, Index i) throws() = 0;

      /**
       * Remove the component at the specified index.
       * @param i a component index
       */
    public:
      //	virtual void removeComponent (Index i) throws() = 0;
    };
  }
}

#endif
