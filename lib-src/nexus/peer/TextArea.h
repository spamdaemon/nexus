#ifndef _NEXUS_PEER_TEXTAREA_H
#define _NEXUS_PEER_TEXTAREA_H

#ifndef _NEXUS_PEER_TEXTCOMPONENT_H
#include <nexus/peer/TextComponent.h>
#endif

namespace nexus {
  namespace peer {
    /**
     * This interface defines a native text component
     * capable of display and editing multiline text.
     *
     * @date 05 Jun 2003
     */
    class TextArea : public virtual TextComponent {
      /** No copying allowed */
      TextArea&operator=(const TextArea&);
      TextArea(const TextArea&);
	
      /** Default constructor */
    protected:
      inline TextArea () throws()
      {}
	
      /** Destroy this button and release all its resources */
    protected:
      virtual ~TextArea () throws() = 0;
    };
  }
}

#endif
