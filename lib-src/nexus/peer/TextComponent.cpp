#include <nexus/peer/TextComponent.h>

namespace nexus {
  namespace peer {
    
    TextComponent::~TextComponent() throws() {}
    
    TextComponent::TextModel::~TextModel() throws() {}
  }
}
