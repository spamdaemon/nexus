#ifndef _NEXUS_PEER_TEXTCOMPONENT_H
#define _NEXUS_PEER_TEXTCOMPONENT_H

#ifndef _NEXUS_PEER_COMPONENT_H
#include <nexus/peer/Component.h>
#endif

namespace nexus {
  namespace peer {

    /**
     * This interface defines a native text component. This
     * text component is expected to only allow one line text 
     * entry.
     *
     */
    class TextComponent : public virtual Component {
      TextComponent(const TextComponent&);
      TextComponent&operator=(const TextComponent&);
	
	/** The size type */
      public:
 	typedef ::std::string::size_type size_type;

     /** A TextComponent model */
    public:
      class TextModel {

	/** The size type */
      public:
	typedef ::std::string::size_type size_type;

	/** Destroy this model */
      public:
	virtual ~TextModel () throws() = 0;

	/**
	 * Get the text stored in this model.
	 * @return the stored text
	 */
      public:
	virtual const ::std::string& text() const throws() = 0;

	/**
	 * The user has entered a replacement text.
	 * @param off the index of the first character to be replaced
	 * @param count the number of characters to be replaced
	 * @param s a replacement string or 0 if characters are to be deleted
	 */
      public:
	virtual void replaceText (size_type off, size_type count, const ::std::string& s) throws() = 0;
      };
	
      /** Default constructor */
    protected:
      inline TextComponent () throws()
      {}
	
      /** Destroy this button and release all its resources */
    protected:
      virtual ~TextComponent () throws() = 0;

      /**
       * Set or remove the model. If the model is set to 0,
       * then this component will not be editable.
       * @param m an model or 0 to remove the current model and disable the field
       */
    public:
      virtual void setTextModel (TextModel* m) throws() = 0;

      /**
       * Replace the text in this component.
       * @param off the index of the first character to be replaced
       * @param count the number of characters to be replaced
       * @param s a replacement string or 0 if characters are to be deleted
       */
    public:
      virtual void replaceText (size_type off, size_type count, const ::std::string& s) throws() = 0;

      /**
       * Set the border width.
       * @param w the width or thickness of the border.
       */
    public:
      virtual void setBorderWidth (Int w) throws() = 0;
    };
  }
}

#endif
