#include <nexus/peer/Component.h>

namespace nexus {
  namespace peer {
    
    Component::~Component() throws() {}
    
    Component::ComponentObserver::~ComponentObserver() throws() {}
    Component::WheelObserver::~WheelObserver() throws() {}
    Component::KeyObserver::~KeyObserver() throws() {}
    Component::MotionObserver::~MotionObserver() throws() {}
    Component::ButtonObserver::~ButtonObserver() throws() {}
    Component::FocusObserver::~FocusObserver() throws() {}
    Component::CrossingObserver::~CrossingObserver() throws() {}
  }
}
