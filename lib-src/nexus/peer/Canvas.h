#ifndef _NEXUS_PEER_CANVAS_H
#define _NEXUS_PEER_CANVAS_H

#ifndef _NEXUS_PEER_COMPONENT_H
#include <nexus/peer/Component.h>
#endif

#ifndef _INDIGO_NODE_H
#include <indigo/Node.h>
#endif

#ifndef _INDIGO_PICKINFO_H
#include <indigo/PickInfo.h>
#endif

namespace nexus {
   class Pixel;
   namespace peer {

      /**
       * This interface is used to draw 2d graphics.
       *
       */
      class Canvas : public virtual Component
      {
            Canvas(const Canvas&) = delete;
            Canvas&operator=(const Canvas&) = delete;

         public:
            struct PickInfo
            {

                  /** The node that was picked */
                  ::timber::Pointer< ::indigo::PickInfo> info;

                  /** The node that was picked (null if none was picked) */
                  ::timber::Pointer< ::indigo::Node> node;

                  /** The index of the shape that was picked (undefined if node is null) */
                  size_t shape;
            };

            /** Default constructor */
         protected:
            inline Canvas() throws()
            {
            }

            /** Destroy this Canvas */
         public:
            virtual ~Canvas() throws() = 0;

            /**
             * Set the graphic node to be displayed in this canvas.
             * Any changes to this node or its children
             * must cause this canvas to repaint itself.
             * @param gfx a node or 0 to remove the current node
             */
         public:
            virtual void setGraphic(const ::timber::Pointer< ::indigo::Node> & gfx) throws() = 0;

            /**
             * Enable automatic rendering. Changes to the graphic must be tracked,
             * and periodically repainted if enabled.
             * @param enabled if true then changes to the graphic will be rendered shortly afterwards
             */
         public:
            virtual void setAutoRenderEnabled(bool enabled) throws() = 0;

            /**
             * Enable or disable double buffering. This is merely a hint
             * and need not be respected; if resource allocation should
             * fail for double buffering, then rendering should be done
             * with singe-buffering.<br>
             * This method is optional with the default implementation
             * doing nothing.
             * @param enabled true if double buffering should be enabled.
             */
         public:
            virtual void setDoubleBufferEnabled(bool enabled) throws();

            /**
             * Render the graphic now.
             */
         public:
            virtual void render() throws() = 0;

            /**
             * Determine information about a pixel.
             * @param p the pixel
             * @return a information about the picked node
             */
         public:
            virtual ::std::unique_ptr< PickInfo> pickNode(const ::nexus::Pixel& p) throws() = 0;

      };
   }
}

#endif
