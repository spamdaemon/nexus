#ifndef _NEXUS_PEER_TEXTFIELD_H
#define _NEXUS_PEER_TEXTFIELD_H

#ifndef _NEXUS_PEER_TEXTCOMPONENT_H
#include <nexus/peer/TextComponent.h>
#endif

namespace nexus {
  namespace peer {
    /**
     * This interface defines a native text component. This
     * text component is expected to only allow one line text 
     * entry.
     *
     * @date 05 Jun 2003
     */
    class TextField : public virtual TextComponent {
      TextField&operator=(const TextField&);
      TextField(const TextField&);

      /** A TextField observer */
    public:
      class TextFieldObserver {
	/** Destroy this observer */
      public:
	virtual ~TextFieldObserver () throws() = 0;

	/** The textfield was activated. */
      public:
	virtual void textFieldActivated () throws() = 0;
      };

      /** Default constructor */
    protected:
      inline TextField () throws()
      {}
	
      /** Destroy this button and release all its resources */
    protected:
      virtual ~TextField () throws() = 0;

      /**
       * Set or remove the TextField observer.
       * Deletion of the observer is the responsibility of the caller.
       * @param l a TextField observer or 0 to remove the current observer
       */
    public:
      virtual void setTextFieldObserver (TextFieldObserver* l) throws() = 0;
    };
  }
}

#endif
