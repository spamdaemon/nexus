#ifndef _NEXUS_PEER_MENUBAR_H
#define _NEXUS_PEER_MENUBAR_H

#ifndef _NEXUS_PEER_COMPONENT_H
#include <nexus/peer/Component.h>
#endif

namespace nexus {
  namespace peer {

    /**
     * This interface defines a native menuBar. A menuBar
     * is a passive component which can be display 
     * either as text or as a graphic icon, but not both.
     *
     * @date 05 Jun 2003
     */
    class MenuBar : public virtual Component {
      MenuBar&operator=(const MenuBar&);
      MenuBar(const MenuBar&);

      /** A push button observer */
    public:
      class Observer {
	/** Destroy this observer */
      public:
	virtual ~Observer () throws() = 0;

      };

      /** Default constructor */
    protected:
      inline MenuBar () throws()
      {}
	
      /** Destroy this menuBar and release all its resources */
    protected:
      virtual ~MenuBar () throws() = 0;

      /**
       * Set or remove the menu bar observer
       * Deletion of the observer is the responsibility of the caller.
       * @param l a menubar observer or 0 to remove the current observer
       */
    public:
      virtual void setMenuBarObserver (Observer* l) throws() = 0;
	
    };
  }
}

#endif
