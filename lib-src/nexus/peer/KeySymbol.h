#ifndef _NEXUS_PEER_KEYSYMBOL_H
#define _NEXUS_PEER_KEYSYMBOL_H

#ifndef _NEXUS_H
#include <nexus/nexus.h>
#endif

#ifndef _NEXUS_KEYSYMBOLS_H
#include <nexus/KeySymbols.h>
#endif

#include <string>

namespace nexus {
  namespace peer {
    /**
     * This interface must be implemented by classes represent key symbols.
     */
    class KeySymbol : public ::timber::Counted {
      KeySymbol(const KeySymbol&);
      KeySymbol&operator=(const KeySymbol&);

      /** Default constructor. */
    protected:
      inline KeySymbol  () throws()
      {}
	
      /** Destructor */
    protected:
      ~KeySymbol() throws() {}

      /**
       * Get the Keysymbol id.
       * @return the key symbol id.
       */
    public:
      virtual KeySymbols::ID id() const throws() = 0;

      /**
       * Get the character symbols representing this key.
       * @return the string representation for this keysymbol
       * or 0 if it has no string representation.
       */
    public:
      virtual ::std::string symbols () const throws() = 0;

      /**
       * Get the ascii character for this symbol. The
       * default implementation uses <code>toString()</code> to
       * find the character.
       * @return the ascii key character or -1 if <code>toString().length()!=1</code>.
       */
    public:
      virtual Int asciiChar() const throws();
	
      /**
       * Describe this key symbol using a text string. The
       * text string for a given symbol should always be the
       * same. This method will also return text for keys
       * that don't have symbols bound to them.
       * @return a descriptive text for this symbol
       */
    public:
      virtual ::std::string description () const throws() = 0;
    };
  }
}

#endif
