#ifndef _NEXUS_PEER_LABEL_H
#define _NEXUS_PEER_LABEL_H

#ifndef _NEXUS_PEER_COMPONENT_H
#include <nexus/peer/Component.h>
#endif

namespace nexus {
    /**
     * @name Forward declartions
     * @{
     */
    class Icon;
    class HorizontalAlignment;
    /*@}*/

    namespace peer {

      /**
       * This interface defines a native label. A label
       * is a passive component which can be display 
       * either as text or as a graphic icon, but not both.
       *
       * @date 05 Jun 2003
       */
      class Label : public virtual Component {
	Label&operator=(const Label&);
	Label(const Label&);

	/** Default constructor */
      protected:
	inline Label () throws()
	  {}
	
	/** Destroy this label and release all its resources */
      protected:
	virtual ~Label () throws() = 0;

	/**
	 * Set the alignment for the text or the icon.
	 * @param align the horizontal alignment
	 */
      public:
	virtual void setAlignment (const HorizontalAlignment& align) throws() = 0;

	/** 
	 * Set the label's text.
	 * @param t the text
	 */
      public:
	virtual void setText (const ::std::string& t) throws() = 0;
	
	/** 
	 * Set the label's icon. 
	 * @param i an icon
	 */
      public:
	virtual void setIcon (const Icon& i) throws() = 0;

      };
  }
}

#endif
