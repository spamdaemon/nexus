#ifndef _NEXUS_PEER_DESKTOP_H
#define _NEXUS_PEER_DESKTOP_H

#ifndef _NEXUS_H
#include <nexus/nexus.h>
#endif

#ifndef _NEXUS_SIZE_H
#include <nexus/Size.h>
#endif

#ifndef _NEXUS_PIXEL_H
#include <nexus/Pixel.h>
#endif

#ifndef _NEXUS_COLOR_H
#include <nexus/Color.h>
#endif

#ifndef _NEXUS_RUNNABLE_H
#include <nexus/Runnable.h>
#endif

#ifndef _NEXUS_INPUTMODIFIERS_H
#include <nexus/InputModifiers.h>
#endif

#ifndef _NEXUS_TIME_H
#include <nexus/Time.h>
#endif

#ifndef _INDIGO_FONTFACTORY_H
#include <indigo/FontFactory.h>
#endif

#include <chrono>

namespace nexus {
   namespace peer {

      /** A key symbol */
      class KeySymbol;

      /**
       * @name Forward declarations of native components
       * @{
       */
      class Canvas;
      class Component;
      class Container;
      class Label;
      class Menu;
      class MenuBar;
      class MenuItem;
      class PushButton;
      class TextArea;
      class TextField;
      class Window;

      /*@}*/
      /**
       * This interface must be implemented
       * by all native desktops. The primary
       * purpose of the desktop is to create
       * components.
       *
       * @author Raimund Merkert
       * @date 18 Feb 2003
       */
      class Desktop : public ::timber::Counted
      {
            Desktop&operator=(const Desktop&);
            Desktop(const Desktop&);

            /** A delay */
         public:
            typedef ::std::chrono::milliseconds Delay;

            /** A scheduling period */
         public:
            typedef ::std::chrono::milliseconds Period;

            /**
             * The key interceptor is used to intercept key-events at the desktop level before they
             * are passed to their components
             */
         public:
            class KeyInterceptor
            {
                  /** Destroy this observer */
               public:
                  virtual ~KeyInterceptor() throws() = 0;

                  /**
                   * A keyboard key was pressed.
                   * @param ks the keysymbol of the key that was pressed
                   * @param when time the button was pressed
                   * @param m the state of modifier keys and buttons at the time of the event
                   * @pre REQUIRE_NON_ZERO(ks);
                   * @return true if the key was intercepted, false otherwise.
                   */
               public:
                  virtual bool keyPressed(const ::timber::Reference< KeySymbol>& ks, const Time& when,
                        const InputModifiers& m) throws() = 0;

                  /**
                   * A keyboard key was typed.
                   * @param ks the keysymbol of the key that was typed
                   * @param when time the button was typed
                   * @param m the state of modifier keys and buttons at the time of the event
                   * @pre REQUIRE_NON_ZERO(ks);
                   * @return true if the key was intercepted, false otherwise.
                   */
               public:
                  virtual bool keyTyped(const ::timber::Reference< KeySymbol>& ks, const Time& when,
                        const InputModifiers& m) throws() = 0;

                  /**
                   * A keyboard key was released.
                   * @param ks the keysymbol of the key that was released
                   * @param when time the button was released
                   * @param m the state of modifier keys and buttons at the time of the event
                   * @pre REQUIRE_NON_ZERO(ks);
                   * @return true if the key was intercepted, false otherwise.
                   */
               public:
                  virtual bool keyReleased(const ::timber::Reference< KeySymbol>& ks, const Time& when,
                        const InputModifiers& m) throws() = 0;
            };

            /** Default constructor */
         protected:
            inline Desktop() throws()
            {
            }

            /** Destroy this desktop and release all its resources */
         public:
            virtual ~Desktop() throws() = 0;

            /**
             * Test if the calling thread is the event thread.
             * @return true if the calling thread is the event thread
             */
         public:
            virtual bool isEventThread() const throws() = 0;

            /**
             * Enqueue the specified runnable for execution within the UI event loop.
             * @param r a runnable
             * @param delayMS the delay with which to run the object in millis
             * @param periodMS the scheduling period in millis
             * @pre REQUIRE_NON_ZERO(r)
             * @return true if the runnable was enqueued, false otherwise
             */
         public:
            virtual bool enqueue(const ::timber::SharedRef< Runnable>& r, const Delay& delayMS,
                  const Period& periodMS) throws() = 0;

            /**
             * Shutdown this desktop.
             * @pre REQUIRE_TRUE(isEventThread())
             */
         public:
            virtual void shutdown() throws() = 0;

            /**
             * Suspend the calling thread until the desktop has been shutdown.
             */
         public:
            virtual void waitShutdown() const throws() = 0;

            /**
             * Get the desktop bounds.
             * @return the bounds for the desktop
             */
         public:
            virtual Size size() const throws() = 0;

            /**
             * Destroy the specified component peer. The default
             * is to simply do <code>deleteComponentPeer(c)</code>
             * @param c a component
             */
         public:
            virtual void destroyComponentPeer(Component* c) throws();

            /**
             * Create a new window.
             * @return a window or 0 if it could not be created
             */
         public:
            virtual Window* createWindow() throws() = 0;

            /**
             * Create a new container
             * @param parent the containing component
             * @return a new container or 0 if it could not be created
             */
         public:
            virtual Container* createContainer(Component& parent) throws() = 0;

            /**
             * Create a canvas for 2d graphics.
             * @param parent the parent component
             * @return a canvas for 2 graphics
             */
         public:
            virtual Canvas* createCanvas2D(Component& parent) throws() = 0;

            /**
             * Create a label.
             * @param parent the parent component
             * @return a label
             */
         public:
            virtual Label* createLabel(Component& parent) throws() = 0;

            /**
             * Create a menu as a child of another component. The parent
             * component must either be a menu or a menubar. Any other
             * parent may not be supported.j
             * @param parent the parent component
             * @return a menu
             */
         public:
            virtual Menu* createMenu(Component& parent) throws() = 0;

            /**
             * Create a menu item as a child of another menu.
             * @param parent the parent menu
             * @return a menu item
             */
         public:
            virtual MenuItem* createMenuItem(Menu& parent) throws() = 0;

            /**
             * Create a menu bar.
             * @param parent the window for the menubar
             * @return a menubar
             */
         public:
            virtual MenuBar* createMenuBar(Window& parent) throws() = 0;

            /**
             * Create a button.
             * @param parent the parent component
             * @return a button
             */
         public:
            virtual PushButton* createPushButton(Component& parent) throws() = 0;

            /**
             * Create a one line text field.
             * @param parent the parent component
             * @return a one line text field
             */
         public:
            virtual TextField* createTextField(Component& parent) throws() = 0;

            /**
             * Create a multiline text area.
             * @param parent the parent component
             * @return a multiline text area
             */
         public:
            virtual TextArea* createTextArea(Component& parent) throws() = 0;

            /**
             * Get a font factory for this desktop.
             * @return a font factory
             */
         public:
            virtual ::timber::Reference< ::indigo::FontFactory> fonts() const throws() = 0;

            /**
             * Query the input modifier mask that this desktop actually supports.
             * @return the input modifier mask actually supported by this desktop.
             */
         public:
            virtual InputModifiers queryInputModifiers() const throws() = 0;

            /**
             * Compute appropriate shadow and highlight colors given a background color.
             * @param bg a background color or 0 to use a default color
             * @param shadow the shadow color
             * @param highlight the highlight color.
             */
         public:
            virtual void computeShadowHighlightColors(const Color& bg, ::indigo::Color& shadow,
                  ::indigo::Color& highlight) throws();

            /**
             * Get the current pointer position.
             * @return the position of the pointer
             */
         public:
            virtual Pixel pointerLocation() const throws() = 0;

            /**
             * Set a global key interceptor.
             * @param l an interceptor or 0 to remove the current interceptor
             */
         public:
            virtual void setKeyInterceptor(KeyInterceptor* l) throws() = 0;
      };
   }
}

#endif
