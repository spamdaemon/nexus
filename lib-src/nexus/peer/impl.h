#ifndef _NEXUS_PEER_H
#define _NEXUS_PEER_H

namespace nexus {
  

    /**
     * This namespace defines the interfaces that need to be implemented
     * when using a new windowing system. By implementing the various
     * interfaces it is possible to run an application with different kinds
     * of windowing systems such as Motif, Windows, Qt, GTK. A user using the
     * KDE deskop on Linux will experience the application as true KDE application,
     * but a GNOME user will experiences the same appliaction as GNOME application.<br>
     * When creating a new native implementation, the first interface to be implemented
     * should be the Desktop interface. It is the means by which other native components
     * are created and destroyed.
     */
    namespace peer {

    }
  }
}


#endif
