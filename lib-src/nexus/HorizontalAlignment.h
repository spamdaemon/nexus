#ifndef _NEXUS_HORIZONTALALIGNMENT_H
#define _NEXUS_HORIZONTALALIGNMENT_H

#ifndef _NEXUS_H
#include <nexus/nexus.h>
#endif

namespace nexus {
  /**
   * This class defines a horizontal alignment.
   */
  class HorizontalAlignment {
    /** The alignment values */
  public:
    enum Alignment { 
      ALIGN_LEFT,   //< align on the left
      ALIGN_CENTER, //< align in the center
      ALIGN_RIGHT   //< align on the right
    };
      
    /** Align on the right */
  public:
    static const HorizontalAlignment LEFT;

    /** Align in the center */
  public:
    static const HorizontalAlignment CENTER;

    /** Align on the right */
  public:
    static const HorizontalAlignment RIGHT;

    /**
     * Create a new horizontal alignment
     * @param a an alignment
     */
  public:
    inline HorizontalAlignment (Alignment a) throws()
      : _align(a) {}

    /**
     * Create a center alignment
     */
  public:
    inline HorizontalAlignment () throws()
      : _align(ALIGN_CENTER) {}

    /**
     * Compute a hashvalue.
     * @return a hashvalue
     */
  public:
    inline size_t hashValue () const throws()
    { return _align; }

    /**
     * Compare two alignments.
     * @param a an alignment
     * @return true if the two alignments are the same
     */
  public:
    inline bool operator==(const HorizontalAlignment& a) const throws()
    { return _align == a._align; }

    /**
     * Compare two alignments.
     * @param a an alignment
     * @return true if the two alignments are the same
     */
  public:
    inline bool equals(const HorizontalAlignment& a) const throws()
    { return *this == a; }

    /**
     * Compare two alignments.
     * @param a an alignment
     * @return true if the two alignments are different
     */
  public:
    inline bool operator!=(const HorizontalAlignment& a) const throws()
    { return _align != a._align; }

    /**
     * Get the alignment value. This method is primarily
     * used in switch statements.
     * @return the alignment value
     */
  public:
    inline Alignment value () const throws() { return _align; }

    /** An alignment */
  private:
    Alignment _align;
  };
}


#endif
