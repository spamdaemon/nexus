#include <nexus/TextComponent.h>
#include <nexus/text/TextModelEvent.h>
#include <nexus/text/SimpleTextModel.h>

namespace nexus {
  TextComponent::TextComponent () throws (::std::exception)
  : _editable(true),
    _borderWidth(DEFAULT_BORDER_WIDTH),
    _observer(0),
    _model(new ::nexus::text::SimpleTextModel())
  {}
      
  TextComponent::TextComponent (const TextModel& m) throws (::std::exception)
  : _editable(true),
    _borderWidth(DEFAULT_BORDER_WIDTH),
    _observer(0),_model(m)
  {}
  
  TextComponent::~TextComponent () throws()
  {
    _model->removeTextModelEventListener(_modelListener);
    delete _observer;
  }
      
  void TextComponent::setEditable (bool editingEnabled) throws()
  {
    if (_editable!=editingEnabled) {
      _editable = editingEnabled;
      if (hasPeer()) {
	textComponent()->setTextModel(_editable ? _observer : 0);
      }
    }
  }

  void TextComponent::setBorderWidth (Int w) throws()
  {
    assert(w>=0);
    if (_borderWidth!=w) {
      _borderWidth=w;
      if (hasPeer()) {
	textComponent()->setBorderWidth(w);
      }
      updateSizeHints();
    }
  }
      
  void TextComponent::setTextModel (const TextModel& m) throws()
  {
    if (_model!=m) {
      if (hasPeer()) {
	_model->removeTextModelEventListener(_modelListener);
	textComponent()->replaceText(0,textLength(),m->text());
	m->addTextModelEventListener(_modelListener);
      }
      _model = m;
    }
  }

  void TextComponent::fontChanged () throws()
  {	updateSizeHints(); }
      
  void TextComponent::updateSizeHints() throws()
  {
    const SizeHints h(computeSizeHints());
    const Int bw = 2*borderWidth();
    _sizeHints = SizeHints(h.minimumSize().adjust(bw,bw),
			   h.maximumSize().adjust(bw,bw),
			   h.preferredSize().adjust(bw,bw));
	
	
    // is there a difference in size
    if (_sizeHints.minimumWidth() > width() ||
	_sizeHints.minimumHeight() > height() ||
	_sizeHints.maximumWidth() < width() ||
	_sizeHints.maximumHeight() < height()) {
      revalidate();
    }
  }

  SizeHints TextComponent::getSizeHints () const throws()
  { return _sizeHints; }

  void TextComponent::destroyPeer() throws()
  {
    _model->removeTextModelEventListener(_modelListener);
    Component::destroyPeer();
  }

  void TextComponent::initializePeer () throws()
  {
    struct Observer : public Peer::TextModel {
      inline Observer (TextComponent& b) throws() : _owner(b) {}
      ~Observer () throws() {}
      void replaceText (size_type off, size_type count, const ::std::string& rText) throws()
      {
	try {
	  _owner.textModel()->replaceText(off,count,rText); 
	}
	catch (...) {
	}
      }
      const ::std::string& text() const throws() { return _owner.textModel()->text(); }
	  
    private:
      TextComponent& _owner;
    };
    
    struct Listener : public ::nexus::text::TextModelEventListener {
      ~Listener() throws() {}
      inline Listener (TextComponent& b) throws() : _owner(b) {}
      void textModelChanged (const ::nexus::text::TextModelEvent& e) throws()
      { _owner.textComponent()->replaceText(e.position(),e.count(),e.replacement()); }
    private:
      TextComponent& _owner;
    };

    Component::initializePeer();
    if (_observer==0) {
      _observer = new Observer(*this);
    }
    if (_modelListener==nullptr) {
      ::nexus::text::TextModelEventListener* tmp = new Listener(*this);
      _modelListener = tmp;
    }
    _model->addTextModelEventListener(_modelListener);

    textComponent()->replaceText(0,0,_model->text());
    textComponent()->setTextModel(_editable ? _observer : 0);
    textComponent()->setBorderWidth(_borderWidth);
  }
}

