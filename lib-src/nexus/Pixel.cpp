#include <nexus/Pixel.h>
#include <iostream>

::std::ostream& operator<< (::std::ostream& out, const ::nexus::Pixel& b) throws()
{ 
  out << '(' << b.x() << ';' << b.y() << ')';
  return out;
}

