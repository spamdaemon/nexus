#ifndef _NEXUS_PUSHBUTTON_H
#define _NEXUS_PUSHBUTTON_H

#ifndef _NEXUS_COMPONENT_H
#include <nexus/Component.h>
#endif

#ifndef _NEXUS_EVENT_ACTIONEVENTLISTENER_H
#include <nexus/event/ActionEventListener.h>
#endif

#ifndef _NEXUS_PEER_PUSHBUTTON_H
#include <nexus/peer/PushButton.h>
#endif

#ifndef _NEXUS_ICON_H
#include <nexus/Icon.h>
#endif

#ifndef _NEXUS_RESIZECAPABILITY_H
#include <nexus/ResizeCapability.h>
#endif

namespace nexus {
  
    
      
  /**
   * This class implements a passive component which is 
   * a button. A button may be represented either graphically
   * through an icon, or textually by a string.
   * @date 06 Jun 2003
   */
  class PushButton : public Component {
    /** No copying allowed */
    PushButton(const PushButton&);
    PushButton&operator=(const PushButton&);

    /** An action event */
  public:
    typedef ::nexus::event::ActionEvent ActionEvent;

    /** The default border width */
  public:
    static const Int DEFAULT_BORDER_WIDTH = 2;

    /** The implementation class is a container */
  public:
    typedef ::nexus::peer::PushButton Peer;

    /**
     * The default constructor 
     * @throws ConfigurationException if the desktop was not initialized
     */
  public:
    PushButton () throws (::std::runtime_error);

    /**
     * Create a new button with a specified text
     * @param txt a text string or 0
     */
  public:
    PushButton (const ::std::string& txt) throws (::std::runtime_error);

    /**
     * Create a new button with a specified icon
     * @param i an icon
     * @pre REQUIRE_CONDITION(i,i.graphic()!=0)
     * @pre REQUIRE_CONDITION(i.size(),i.height()>0 && i.width()>0)
     */
  public:
    PushButton (const Icon& i) throws (::std::runtime_error);

    /** Destroy this button */
  public:
    ~PushButton () throws();

    /**
     * Get the text for this button.
     * @return the text for this button or 0 if no text is used.
     */
  public:
    inline const ::std::string& text() const throws() 
    { return _text; }

    /**
     * Set the text for this button. Sets the icon to 0.
     * @param t the text for this button or 0
     */
  public:
    virtual void setText (const ::std::string& t) throws();
	
    /**
     * Get the icon for this button.
     * @return the icon for this button or an icon with no graphic.
     */
  public:
    inline Icon icon() const throws() 
    { return _icon; }
	
    /**
     * Set the icon for this button. Sets the text
     * to 0.
     * @param i the icon for this button
     */
  public:
    virtual void setIcon (const Icon& i) throws();


    /**
     * Set the border width.
     * @param w the border width
     * @pre REQUIRE_GREATER_OR_EQUAL(w,0)
     */
  public:
    virtual void setBorderWidth (Int w) throws();

    /**
     * Get the border width.
     * @return the border width 
     */
  public:
    inline Int borderWidth () const throws()
    { return _borderWidth; }

    /**
     * Set horizontal resizability.
     * @param rc if true, the button can be horizontally resized
     */
  public:
    virtual void setResizeCapability (const ResizeCapability& rc) throws();

    /**
     * Get the resize capability.
     * @return the buttons capability to resize
     */
  public:
    inline ResizeCapability resizeCapability () const throws()
    { return _resizeCapability; }
	
  protected:
    SizeHints getSizeHints () const throws();

    /**
     * @name Action events
     * @{
     */

    /**
     * Set the action identifier. The action identifier
     * is a string identifying the action independent of the 
     * source component.
     */
  public:
    void setActionID(const ::std::string& s) throws();
    
    /**
     * Get the action identifier.
     * @return the action id.
     */
  public:
    inline const ::std::string& actionID() const throws()
    { return _actionID; }

    /**
     * Add an action listener.
     * @param l an action listener
     */
  public:
    void addActionEventListener (const ::timber::Reference< ::nexus::event::ActionEventListener>& l) throws();
	
    /**
     * Remove an action listener.
     * @param l an action listener
     */
  public:
    void removeActionEventListener (const ::timber::Pointer< ::nexus::event::ActionEventListener>& l) throws();

    /**
     * Process an action event.
     * @param e an action event
     */
  protected:
    virtual  void processActionEvent (const ::timber::Reference<ActionEvent>& e) throws();
	
    /*@}*/

  protected:
    void initializePeer() throws();
    PeerComponent* createPeerComponent (PeerComponent* p) throws();
    void fontChanged () throws();

    /** 
     * Calculate the preferred size 
     * @return true if the preferred size has changed, false otherwise
     */
  private:
    bool updatePreferredSize() const throws();

    /**
     * Get the window peer.
     * @return the widow peer
     */
  private:
    inline Peer* button() const throws()
    { return impl<Peer>(); }

    /** The text for this button */
  private:
    ::std::string _text;

    /** The icon for this button */
  private:
    Icon _icon;
	
    /** The resize capability */
  private:
    ResizeCapability _resizeCapability;

    /** The cached preferred size */
  private:
    mutable Size _preferredSize;

    /** The border width */
  private:
    Int _borderWidth;

    /** The action listeners */
  private:
    EventListeners< ::nexus::event::ActionEventListener> _actionListeners;

    /** The observer */
  private:
    Peer::Observer* _observer;

    /** The action id */
  private:
    ::std::string _actionID;
  };
}
#endif
