#ifndef _NEXUS_KEYSYMBOL_H
#define _NEXUS_KEYSYMBOL_H

#ifndef _NEXUS_H
#include <nexus/nexus.h>
#endif

#ifndef _NEXUS_PEER_KEYSYMBOL_H
#include <nexus/peer/KeySymbol.h>
#endif

#include <iosfwd>

namespace nexus {

  /**
   * This class provides a mapping of keys to symbols that represent it visually.
   * A key may be bound to an arbitrary set of symbols which may be obtained through 
   * the symbols() method call. Some keys do not have associated symbols. If the
   * key can be represented by a single ASCII character, then asciiChar() can be used
   * to obtain that character quickly.
   * Each key is furthermore capable to providing a string that describes the key. For
   * example, the string <code>Return</code> might describe the <code>enter</code> key.
   *
   * @todo Allow mapping of keys to arbitrary symbols
   */
  class KeySymbol {

    /**
     * Default constructor.
     * The toString() method will return 0 for this kind of string.
     */
  public:
    inline KeySymbol  () throws()
    {}

    /**
     * Create a new keysymbol.
     * @param ks a native keysymbol
     */
  public:
    inline KeySymbol (const ::timber::Reference< ::nexus::peer::KeySymbol>& ks) throws()
      : _symbol(ks) {}

	
    /**
     * Get the unique id for this symbol.
     * @return a unique id for this symbol.
     */
  public:
    inline KeySymbols::ID id() const throws()
    { return _symbol->id(); }
    /**
     * Compare two key symbols.
     * @param c a key symbol 
     * @return true if this and f are the same key symbol
     */
  public:
    inline bool equals (const KeySymbol& c) const throws()
    { return id()==c.id(); }
      
    /**
     * Compare two key symbols.
     * @param f a key symbol 
     * @return true if this and f are the same key symbol
     */
  public:
    inline bool operator==(const KeySymbol& f) const throws()
    { return equals(f); }
      
    /**
     * Compare two key symbols.
     * @param f a key symbol 
     * @return true if this and f are different key symbols.
     */
  public:
    inline bool operator!=(const KeySymbol& f) const throws()
    { return !equals(f); }

    /**
     * A hashcode for this color.
     * @return a hashcode for this font
     */
  public:
    inline size_t hashValue () const throws()
    { return _symbol==nullptr ? -1 : id(); }
      
    /**
     * Get the string representation for this key symbol.
     * @return the string representation for this keysymbol
     * or 0 if it has no string representation
     */
  public:
    inline ::std::string symbols () const throws()
    { return _symbol->symbols(); }

    /**
     * Get the ascii character for this symbol. The
     * default implementation uses <code>toString()</code> to
     * find the character.
     * @return the ascii key character or -1 if <code>toString().length()!=1</code>.
     */
  public:
    inline Int asciiChar() const throws()
    { return _symbol->asciiChar(); }
      
    /**
     * Describe this key symbol using a text string. The
     * text string for a given symbol should always be the
     * same. This method will also return text for keys
     * that don't have symbols bound to them.
     * @return a descriptive text for this symbol
     */
  public:
    inline ::std::string description () const throws()
    { return _symbol->description(); }
	
    /** An integer representing representing this key symbol */
  private:
    ::timber::Pointer< ::nexus::peer::KeySymbol> _symbol;
  };
}

::std::ostream& operator<< (::std::ostream& out, const ::nexus::KeySymbol& e);

#endif
