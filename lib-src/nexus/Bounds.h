#ifndef _NEXUS_BOUNDS_H
#define _NEXUS_BOUNDS_H

#ifndef _NEXUS_SIZE_H
#include <nexus/Size.h>
#endif

#ifndef _NEXUS_PIXEL_H
#include <nexus/Pixel.h>
#endif

#include <iosfwd>

namespace nexus {
  /**
   * This class is used to define the size and location
   * of a component or desktop.
   *
   * @author Raimund Merkert
   * @date 18 Feb 2003
   */
  class Bounds {
    /**
     * Default constructor. Creates bounds 0,0,0,0
     */
  public:
    inline Bounds () throws() {}
      
    /**
     * Create a new bounds object.
     * @param px the x position
     * @param py the y position
     * @param w the width
     * @param h the height
     */
  public:
    inline Bounds (Int px, Int py, Int w, Int h) throws()
      : _pt(px,py), _size(w,h) {}
      
    /**
     * Create a new bounds object.
     * @param topleft the top-left corner
     * @param sz the size of the bounds
     */
  public:
    inline Bounds (const Pixel& topleft, const Size& sz) throws()
      : _pt(topleft),_size(sz) {}

    /**
     * Create a new bounds object. This is equivalent
     * to <code>Bounds(Pixel(),sz)</code>.
     * @param sz the size of the bounds
     */
  public:
    inline Bounds (const Size& sz) throws()
      : _size(sz) {}

    /**
     * Get the x location.
     * @return the x coordinate of the position
     */
  public:
    inline Int x() const throws() { return _pt.x(); }

    /**
     * Get the y location.
     * @return the y coordinate of the position
     */
  public:
    inline Int y() const throws() { return _pt.y(); }
      
    /**
     * Get the width 
     * @return the width
     */
  public:
    inline Int width() const throws() { return _size.width(); }

    /**
     * Get the height.
     * @return the height
     */
  public:
    inline Int height () const throws() { return _size.height(); }

    /**
     * Get the corner pixel.
     * @return a corner pixel
     */
  public:
    inline Pixel pixel () const throws()
    { return _pt; }

    /** 
     * Get width and height.
     * @return the width and height size
     */
  public:
    inline Size size () const throws() 
    { return _size; }
      
    /**
     * Test if the point is within these bounds.
     * @param px the x-coordinate of a point
     * @param py the y-coordinate of a point
     * @return true if the point is inside these bounds
     */
  public:
    bool contains (Int px, Int py) const throws();

    /**
     * Test if the specified pixel is contained within these bounds.
     * @param p a pixel
     * @return true if the pixel is contained within these bounds
     */
  public:
    inline bool contains (const Pixel& p) const throws()
    { return contains(p.x(),p.y()); }
      
    /**
     * Move these bounds by the specified amount.
     * @param dx the amount in x to move 
     * @param dy the amount in y to move
     * @return the moved bounds
     */
  public:
    inline Bounds moveBounds(Int dx, Int dy) const throws()
    { return Bounds(x()+dx,y()+dy,width(),height()); }

    /**
     * Merge two bounds.
     * @param b a bounds object
     * @return a new bounds object
     */
  public:
    Bounds mergeBounds (const Bounds& b) const throws();

    /**
     * Intersect two bounds.
     * @param b a bounds object
     * @return the bounds that represent the intersection
     */
  public:
    Bounds intersectBounds (const Bounds& b) const throws();

    /**
     * Compute the bounds for centering the specified size.
     * @param sz a size
     * @return the bounds of the specified size, such that the bouds are centered 
     * inside these bounds
     */
  public:
    Bounds centerBounds (const Size& sz) const throws();

    /**
     * Enlarge these bounds by the specified amount. 
     * The bounds are widended by w/2 and h/2 around
     * the center of the bounds. The following invariant
     * holds: b.widenBounds(x,y).centerBounds(b.size())==b
     * @param w the extra width
     * @param h the extra height
     * @post b.widenBounds(x,y).centerBounds(b.size())==b
     * @return the widened bounds
     */
  public:
    Bounds widenBounds (Int w, Int h) const throws();

    /**
     * Translate these bounds.
     * @param dx the translate distance along x
     * @param dy the translate distance along y
     * @return a new bounds 
     */
  public:
    inline Bounds translateBounds (Int dx, Int dy) const throws()
    { return Bounds(x()+dx,y()+dy,width(),height()); }

    /**
     * Compute a hashvalue.
     * @return a hashvalue
     */
  public:
    inline size_t hashValue () const throws()
    { return _pt.hashValue() ^ _size.hashValue(); }

    /**
     * Compare two bounds.
     * @param b a bounds object
     * @return true if this and b are equal
     */
  public:
    inline bool operator==(const Bounds& b) const throws()
    { return _size==b.size() && _pt==b._pt; }
      
    /**
     * Compare two bounds.
     * @param b a bounds object
     * @return true if this and b are equal
     */
  public:
    inline bool equals(const Bounds& b) const throws()
    { return *this==b; }
      
    /**
     * Compare two bounds.
     * @param b a bounds object
     * @return true if this and b are equal
     */
  public:
    inline bool operator!=(const Bounds& b) const throws()
    { return _size != b._size || _pt!=b._pt; }

    /** The pixel */
  private:
    Pixel _pt;

    /** The size */
  private:
    Size	_size;
  };
}

::std::ostream& operator<< (::std::ostream& out, const ::nexus::Bounds& b) throws();

#endif
