#ifndef _NEXUS_PROXYCOMPONENT_H
#define _NEXUS_PROXYCOMPONENT_H

#ifndef _NEXUS_COMPONENT_H
#include <nexus/Component.h>
#endif

#include <iosfwd>

namespace nexus {
  
  namespace peer {
    /** Forward declaration */
    class Container;
  }

    

  /**
   * This class allows a component to be wrapped inside
   * a Proxy. This allows for writing components in terms
   * of other components, without having to expose details
   * of the proxied component. This class is akin to the Composite
   * component class, but the proxied component cannot be changed.
   *
   * @author Raimund Merkert
   * @date 30 Jul 2003
   * @todo CrossingEvents and FocusEvents need to be handled correctly
   * if child is strictly inside the ProxyComponent
   */
  class ProxyComponent : public Component {
    ProxyComponent(const ProxyComponent&);
    ProxyComponent&operator=(const ProxyComponent&);

    /** The implementation class is a container */
  public:
    typedef ::nexus::peer::Container Peer;
	
    /** An event listener */
  private:
    class EventListener;

    /** 
     * Create a new proxy for the specified component.
     * @param c the proxied component
     * @pre REQUIRE_NON_ZERO(c)
     * @throw ConfigurationException if the desktop has not been configured
     */
  public:
    ProxyComponent (const ::timber::Reference<Component>& c) throws (::std::runtime_error);
	
    /** Destroy this container.*/
  public:
    ~ProxyComponent () throws();
	
    /**
     * Get the child component.
     * @return a child
     */
  protected:
    template <class C>
      inline C& child() const throws()
      { return dynamic_cast<C&>(_child); }
    

    PeerComponent* createPeerComponent (PeerComponent* p) throws();
    void createChildPeers() throws();

  public:
    ::std::ostream& print (::std::ostream& out, bool deep, Int indent) const throws();

    /**
     * @name Geometry Management
     * @{
     */
	
    /**
     * Make the child the same size. Sets the child to 
     * the size of this component. Override as necessary.
     */
  protected:
    void doLayout() throws(LayoutException);

    /**
     * Returns the size hints of the child 
     * @return child().sizeHints()
     */
  protected:
    SizeHints getSizeHints() const throws();
	
    /*@}*/

    /**
     * @name Event handling control
     * @{
     */
	
  public:
    void addComponentEventListener (const ::timber::Reference< ::nexus::event::ComponentEventListener>& l) throws();
    void removeComponentEventListener (const ::timber::Pointer< ::nexus::event::ComponentEventListener>& l) throws();
     void addMotionEventListener (const ::timber::Reference< ::nexus::event::MotionEventListener>& l) throws();
     void removeMotionEventListener (const ::timber::Pointer< ::nexus::event::MotionEventListener>& l) throws();
     void addWheelEventListener (const ::timber::Reference< ::nexus::event::WheelEventListener>& l) throws();
     void removeWheelEventListener (const ::timber::Pointer< ::nexus::event::WheelEventListener>& l) throws();
     void addButtonEventListener (const ::timber::Reference< ::nexus::event::ButtonEventListener>& l) throws();
     void removeButtonEventListener (const ::timber::Pointer< ::nexus::event::ButtonEventListener>& l) throws();
     void addFocusEventListener (const ::timber::Reference< ::nexus::event::FocusEventListener>& l) throws();
     void removeFocusEventListener (const ::timber::Pointer< ::nexus::event::FocusEventListener>& l) throws();
     void addKeyEventListener (const ::timber::Reference< ::nexus::event::KeyEventListener>& l) throws();
     void removeKeyEventListener (const ::timber::Pointer< ::nexus::event::KeyEventListener>& l) throws();
     void addCrossingEventListener (const ::timber::Reference< ::nexus::event::CrossingEventListener>& l) throws();
     void removeCrossingEventListener (const ::timber::Pointer< ::nexus::event::CrossingEventListener>& l) throws();
	
    /*@}*/
	
	
    /** The proxied component */
  private:
    Component& _child;

    /** An event listener */
  private:
    ::timber::Reference< EventListener> _proxyListener;

    /** We're hijacking the event listeners */
  private:
    EventListeners< ::nexus::event::ComponentEventListener> _componentListeners;
    EventListeners< ::nexus::event::WheelEventListener> _wheelListeners;
    EventListeners< ::nexus::event::MotionEventListener> _motionListeners;
    EventListeners< ::nexus::event::ButtonEventListener> _buttonListeners;
    EventListeners< ::nexus::event::KeyEventListener> _keyListeners;
    EventListeners< ::nexus::event::FocusEventListener> _focusListeners;
    EventListeners< ::nexus::event::CrossingEventListener> _crossingListeners;
  };
}

#endif
