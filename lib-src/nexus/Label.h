#ifndef _NEXUS_LABEL_H
#define _NEXUS_LABEL_H

#ifndef _NEXUS_COMPONENT_H
#include <nexus/Component.h>
#endif

#ifndef _NEXUS_PEER_LABEL_H
#include <nexus/peer/Label.h>
#endif

#ifndef _NEXUS_ICON_H
#include <nexus/Icon.h>
#endif

#ifndef _NEXUS_HORIZONTALALIGNMENT_H
#include <nexus/HorizontalAlignment.h>
#endif

#ifndef _NEXUS_RESIZECAPABILITY_H
#include <nexus/ResizeCapability.h>
#endif

namespace nexus {
  /**
   * This class implements a passive component which is 
   * a label. A label may be represented either graphically
   * through an icon, or textually by a string.
   * @date 06 Jun 2003
   */
  class Label : public Component {
    /** No copying allowed */
    Label(const Label&);
    Label&operator=(const Label&);

    /** The implementation class is a container */
  public:
    typedef ::nexus::peer::Label Peer;

    /**
     * The default constructor 
     * @throws ConfigurationException if the desktop was not initialized
     */
  public:
    Label () throws (::std::runtime_error);

    /**
     * Create a new label with a specified text
     * @param txt a text string or 0
     */
  public:
    Label (const ::std::string& txt) throws (::std::runtime_error);

    /**
     * Create a new label with a specified icon
     * @param i an icon
     * @pre REQUIRE_CONDITION(i,i.graphic()!=0)
     * @pre REQUIRE_CONDITION(i.size(),i.height()>0 && i.width()>0)
     */
  public:
    Label (const Icon& i) throws (::std::runtime_error);

    /** Destroy this label */
  public:
    ~Label () throws();

    /**
     * Get the text for this label.
     * @return the text for this label or 0 if no text is used.
     */
  public:
    inline const ::std::string& text() const throws() 
    { return _text; }

    /**
     * Set the text for this label. Sets the icon to 0.
     * @param t the text for this label or 0
     */
  public:
    virtual void setText (const ::std::string& t) throws();
	
    /**
     * Get the icon for this label.
     * @return the icon for this label or an icon with no graphic.
     */
  public:
    inline Icon icon() const throws() 
    { return _icon; }
	
    /**
     * Set the icon for this label. Sets the text
     * to 0.
     * @param i the icon for this label
     */
  public:
    virtual void setIcon (const Icon& i) throws();

    /**
     * Set the alignment for the text or the icon.
     * @param align the horizontal alignment
     */
  public:
    virtual void setAlignment (const HorizontalAlignment& align) throws();

    /**
     * Get the alignment.
     * @return the alignment
     */
  public:
    inline HorizontalAlignment alignment () const throws()
    { return _align; }
	
    /**
     * Set horizontal resizability.
     * @param rc if true, the label can be horizontally resized
     */
  public:
    virtual void setResizeCapability (const ResizeCapability& rc) throws();

    /**
     * Get the resize capability.
     * @return the labels capability to resize
     */
  public:
    inline ResizeCapability resizeCapability () const throws()
    { return _resizeCapability; }
	
  protected:
    SizeHints getSizeHints () const throws();
	
  protected:
    void initializePeer() throws();
    PeerComponent* createPeerComponent (PeerComponent* p) throws();
    void fontChanged () throws();

    /** 
     * Calculate the preferred size 
     * @return true if the preferred size has changed, false otherwise
     */
  private:
    bool updatePreferredSize() const throws();

    /**
     * Get the window peer.
     * @return the widow peer
     */
  private:
    inline Peer* label() const throws()
    { return impl<Peer>(); }

    /** The text for this label */
  private:
    ::std::string _text;

    /** The icon for this label */
  private:
    Icon _icon;
	
    /** The alignment */
  private:
    HorizontalAlignment _align;
	
    /** The resize capability */
  private:
    ResizeCapability _resizeCapability;

    /** The cached preferred size */
  private:
    mutable Size _preferredSize;
  };
}
#endif
