#ifndef _NEXUS_PIXEL_H
#define _NEXUS_PIXEL_H

#ifndef _NEXUS_H
#include <nexus/nexus.h>
#endif

#ifndef _CANOPY_HASH_H
#include <canopy/hash.h>
#endif

#include <iosfwd>

namespace nexus {

  /**
   * This class is used to represent points whose coordinates
   * are integer values. The term Pixel was chosen, because
   * instances of this class are often associated with screen
   * pixels.<p>
   * The coordinates of a pixel may be negative, possibly indicating
   * pixels that off the visible portion of a screen.
   */
  class Pixel {
    /** Create a default pixel */
  public:
    inline Pixel () throws()
      : _x(0),_y(0) {}

    /** 
     * Create a new pixel.  
     * @param fx x-coordinate of the pixel
     * @param fy y-coordinate of the pixel
     */
  public:
    inline Pixel (Int fx, Int fy) throws()
      : _x(fx), _y(fy) 
    {}
      
    /**
     * Get the x coordinate
     * @return the x-coordinate
     */
  public:
    inline Int x() const throws() { return _x; }

    /**
     * Get the y coordinate
     * @return the y-coordinate
     */
  public:
    inline Int y() const throws() { return _y; }

    /** Compare two pixels. */
  public:
    inline bool equals (const Pixel& pt) const throws()
    { return _x==pt._x && _y==pt._y; }

    /**
     * Compare two pixels.
     */
  public:
    inline bool operator== (const Pixel& pt) const throws()
    { return equals(pt); }

    /**
     * Compare two pixels.
     */
  public:
    inline bool operator!= (const Pixel& pt) const throws()
    { return !equals(pt); }

    /**
     * Compute a hashcode for this pixel.
     * @return a hashcode for this pixel
     */
  public:
    inline size_t hashValue () const throws()
    { return ::canopy::hashValue(_x) ^ ::canopy::hashValue(_y); }
      
    /** The pixel coordinates */
  private:
    Int _x,_y;
  };
    
}

/**
 * Print the pixel to the specified output stream.
 * @param out an output stream
 * @param b a pixel
 * @return out
 */
::std::ostream& operator<< (::std::ostream& out, const ::nexus::Pixel& b) throws();

#endif
