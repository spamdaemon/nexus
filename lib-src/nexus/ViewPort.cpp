#include <nexus/ViewPort.h>
#include <indigo/view/IPixel.h>

namespace nexus {

   namespace {
      static const bool ENABLE_PIXEL_ROUNDING = false;
   }

   ViewPort::ViewPort()
   throws()
   {}

   ViewPort::~ViewPort()
   throws() {}

   ::indigo::AffineTransform2D ViewPort::getModelViewTransform() const
   throws()
   {

      ::indigo::AffineTransform2D t = ::indigo::view::ViewPort::getModelViewTransform();

      // first, translate so that (0,h) is at (0,0)
      t.translate(0,- viewWindow().height());

      // now, all the negative y's need to be turned into positive ones
      t.scale(1,-1);

      return t;
   }

   ::indigo::AffineTransform2D ViewPort::getViewModelTransform() const
   throws()
   {

      ::indigo::AffineTransform2D r(0,1,-1,0,viewWindow().height());
      ::indigo::AffineTransform2D l = ::indigo::view::ViewPort::getModelViewTransform();

      return ::indigo::AffineTransform2D(l,r);
   }

   void ViewPort::pixel2point(double px, double py, double& outx, double& outy) const
   throws()
   {
      py = viewWindow().height() - py;
      if (ENABLE_PIXEL_ROUNDING) {
         px-=0.5;
         py-=0.5;
      }
      ::indigo::view::ViewPort::pixel2point(px,py,outx,outy);
   }

   void ViewPort::point2pixel(double px, double py, double& outx, double& outy) const
   throws()
   {
      ::indigo::view::ViewPort::point2pixel(px,py,outx,outy);
      if (ENABLE_PIXEL_ROUNDING) {
         outx = ::std::round(outx);
         outy = ::std::round(outy);
      }
      outy = viewWindow().height() - outy;
   }

   ViewPort::Pixel ViewPort::centerPixel() const
   throws()
   {
      ViewPort::Pixel c= ::indigo::view::ViewPort::centerPixel();
      if (ENABLE_PIXEL_ROUNDING) {
         c = ViewPort::Pixel(::std::round(c.x()),::std::round(c.y()));
      }
      return c;
   }

   bool ViewPort::checkViewPort(const Bounds& newBounds, const ViewPort::Window& newView) const
throws()
{
   if (newView.width() != ::std::round(newView.width()) || newView.height() != ::std::round(newView.height())) {
      return false;
   }

   return ::indigo::view::ViewPort::checkViewPort(newBounds,newView);
}

}
