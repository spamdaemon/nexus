#ifndef _NEXUS_EVENT_WINDOWEVENT_H
#define _NEXUS_EVENT_WINDOWEVENT_H

#ifndef _NEXUS_WINDOW_H
#include <nexus/Window.h>
#endif

#ifndef _NEXUS_EVENT_UIEVENT_H
#include <nexus/event/UIEvent.h>
#endif

namespace nexus {
  namespace event {

    /**
     * This class represents window events that are caused
     * by external hardware, such as a mouse, keyboard or
     * are triggered by such hardware.
     */
    class WindowEvent : public UIEvent {
      WindowEvent&operator=(const WindowEvent&);
      WindowEvent(const WindowEvent&);

      /** The types of window events */
    public:
      enum EventID { 
	WINDOW_OPENED, //< The window has been opened
	WINDOW_CLOSED  //< The window has been closed
      };
	  
      /**
       * Create a new window event.
       * @param c a component
       * @pre REQUIRE_NON_ZERO(c)
       * @param eid the event id
       */
    public:
      WindowEvent (const ::timber::Reference<Window>& c, EventID eid) throws();
	
      /** Destroy this event. */
    public:
      ~WindowEvent () throws();

      /**
       * Get the window that caused this event.
       * @return the source cast to a window
       */
    public:
      inline ::timber::Reference<Window> window() const throws()
      { return source(); }
      
      /** 
       * Get the event id
       * @return the event id
       */
    public:
      inline EventID id() const throws()
      { return _id; }

      /** The event id */
    private:
      EventID _id;
    };
  }
}

#endif
