#include <nexus/event/PointerEvent.h>

namespace nexus {
  namespace event {
    PointerEvent::PointerEvent (const ::timber::Reference<Component>& c, InputEventClass ieClass, Int px, Int py, const Time& w, const InputModifiers& m) throws()
    : InputEvent(c,ieClass), _pixel(px,py),_when(w),_modifiers(m)
    {}

    PointerEvent::~PointerEvent() throws()
    {  }
  }
}
