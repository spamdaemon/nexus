#ifndef _NEXUS_EVENT_KEYEVENTLISTENER_H
#define _NEXUS_EVENT_KEYEVENTLISTENER_H

#ifndef _NEXUS_H
#include <nexus/nexus.h>
#endif

namespace nexus {
  namespace event {

    /** Forward declaration of events */
    class KeyEvent;
	
    /**
     * This interface must be implemented by wishing to listen
     * to key events.
     */
    class KeyEventListener : public virtual ::timber::Counted {
      /** No copying allowed */
      KeyEventListener&operator=(const KeyEventListener&);
      KeyEventListener(const KeyEventListener&);

      /** Default constructor */
    public:
      inline KeyEventListener() throws()
      {}

      /** Destroy this listener */
    public:
      ~KeyEventListener() throws()
	{}
	  
      /**
       * Process a key event.
       * @param e a key event
       */
    public:
      virtual void processKeyEvent (const ::timber::Reference<KeyEvent>& e) throws() = 0;
    };
  }
}
#endif
