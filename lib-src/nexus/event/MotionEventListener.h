#ifndef _NEXUS_EVENT_MOTIONEVENTLISTENER_H
#define _NEXUS_EVENT_MOTIONEVENTLISTENER_H

#ifndef _NEXUS_H
#include <nexus/nexus.h>
#endif

namespace nexus {
  namespace event {

    /** Forward declaration of events */
    class MotionEvent;

    /**
     * This interface must be implemented to process
     * motion events from a pointer device.
     */
    class MotionEventListener : public virtual ::timber::Counted {
      /** No copying allowed */
      MotionEventListener&operator=(const MotionEventListener&);
      MotionEventListener(const MotionEventListener&);

      /** Default constructor */
    public:
      inline MotionEventListener() throws()
      {}

      /** Destroy this listener */
    public:
      ~MotionEventListener() throws()
	{}
	  
      /**
       * Process a pointer motion event. Default is to do nothing.
       * @param e a pointer motion event
       */
    public:
      virtual void processMotionEvent (const ::timber::Reference<MotionEvent>& e) throws() = 0;
    };
  }
}
#endif
