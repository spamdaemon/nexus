#ifndef _NEXUS_EVENT_CROSSINGEVENTLISTENER_H
#define _NEXUS_EVENT_CROSSINGEVENTLISTENER_H

#ifndef _NEXUS_H
#include <nexus/nexus.h>
#endif

namespace nexus {
  namespace event {

    /** Forward declaration of events */
    class CrossingEvent;
	
    /**
     * This interface must be implemented by wishing to listen
     * to crossing events.
     */
    class CrossingEventListener : public virtual ::timber::Counted {
      /** No copying allowed */
      CrossingEventListener&operator=(const CrossingEventListener&);
      CrossingEventListener(const CrossingEventListener&);

      /** Default constructor */
    public:
      inline CrossingEventListener() throws()
      {}

      /** Destroy this listener */
    public:
      ~CrossingEventListener() throws()
	{}
	  
      /**
       * Process a component crossing event.
       * @param e a mouse crossing event
       */
    public:
      virtual void processCrossingEvent (const ::timber::Reference<CrossingEvent>& e) throws() = 0;
    };
  }
}
#endif
