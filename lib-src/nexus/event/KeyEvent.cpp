#include <nexus/event/KeyEvent.h>
#include <ostream>

namespace nexus {
  namespace event {
    
    KeyEvent::KeyEvent (const ::timber::Reference<Component>& c, const EventID& t, const Time& w, const InputModifiers& m, const KeySymbol& k) throws()
    : InputEvent(c,KEY_EVENT),_id(t),_time(w),_modifiers(m),_keySymbol(k)
    {}
    KeyEvent::~KeyEvent() throws()
    {  }
  }
}


::std::ostream& operator<< (::std::ostream& out, const ::nexus::event::KeyEvent& s)
{
   out << "KeyEvent { id: ";
   switch(s.id()) {
      case ::nexus::event::KeyEvent::KEY_PRESSED: out << "KEY_PRESSED"; break;
      case ::nexus::event::KeyEvent::KEY_RELEASED: out << "KEY_RELEASED"; break;
      case ::nexus::event::KeyEvent::KEY_TYPED: out << "KEY_TYPED"; break;
   }
   out << ", key: " <<  s.key() << " }";
   return out;
}
