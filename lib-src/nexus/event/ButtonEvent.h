#ifndef _NEXUS_EVENT_BUTTONEVENT_H
#define _NEXUS_EVENT_BUTTONEVENT_H

#ifndef _NEXUS_EVENT_POINTEREVENT_H
#include <nexus/event/PointerEvent.h>
#endif

namespace nexus {
  namespace event {
	
    /**
     * This event is generated whenever a physical button is pressed 
     * or released. A button may be a mouse or trackball button, 
     * but could be any other kind of device.
     */
    class ButtonEvent : public PointerEvent {
      /** No default constructor */
      ButtonEvent&operator=(const ButtonEvent&);
      ButtonEvent(const ButtonEvent&);

      /** The types of button events */
    public:
      enum EventID { 
	BUTTON_PRESSED,  //< a button was pressed
	BUTTON_RELEASED,  //< a button was released
	BUTTON_CLICKED   //< a button was pressed and then released
      };

      /** Destroy this button event */
    public:
      ~ButtonEvent () throws();

      /**
       * Create a new motion event.
       * @param c the source component
       * @param t the event id
       * @param px the x coordinate of a reported position
       * @param py the y coordinate of a reported position
       * @param bn a button number
       * @param w when this event was generated
       * @param m a set of input modifiers active at the time of the event
       */
    public:
      ButtonEvent (const ::timber::Reference<Component>& c, const EventID& t, Int px, Int py, Int bn, const Time& w, const InputModifiers& m) throws();

      /**
       * Get the event id.
       * @return the kind of key event
       */
    public:
      inline EventID id () const throws() { return _id; }
 
      /**
       * Get the which button was pressed or released.
       * @return the button that generated this event
       */
    public:
      inline Int button () const throws() { return _button; }

      /** The event id */
    private:
      EventID _id;

      /** The button that was released */
    private:
      Int _button;
    };
  }
}

#endif
