#include <nexus/event/FocusEvent.h>

namespace nexus {
  namespace event {
    FocusEvent::FocusEvent (const ::timber::Reference<Component>& c, const EventID& t) throws()
    : InputEvent(c,FOCUS_EVENT),_id(t)
    {}
    
    FocusEvent::~FocusEvent() throws()
    { }
  }
}
