#ifndef _NEXUS_EVENT_INPUTEVENT_H
#define _NEXUS_EVENT_INPUTEVENT_H

#ifndef _NEXUS_EVENT_UIEVENT_H
#include <nexus/event/UIEvent.h>
#endif


namespace nexus {
  namespace event {

    /**
     * This class represents input events that are caused
     * by external hardware, such as a mouse, keyboard or
     * are triggered by such hardware.
     */
    class InputEvent : public UIEvent {
      InputEvent(const InputEvent&);
      InputEvent&operator=(const InputEvent&);
      
      /** The event mask */
    public:
      enum InputEventClass {
	MOTION_EVENT = 1, //< a motion event
	BUTTON_EVENT = 2, //< a button event
	WHEEL_EVENT  = 4, //< a wheel event
	KEY_EVENT    = 8, //< a key event
	FOCUS_EVENT  = 16, //< a focus event
	CROSSING_EVENT=32 //< a crossing event
      };
	  
      /**
       * Create a new component event.
       * @param c a component
       * @pre REQUIRE_NON_ZERO(c)
       * @param ieClass the input event class 
       */
    protected:
      InputEvent (const ::timber::Reference<Component>& c, InputEventClass ieClass) throws();
	
      /** Destroy this event. */
    public:
      ~InputEvent () throws();

      /** 
       * Get the event mask
       * @return the event mask that corresponds to this event
       */
    public:
      inline InputEventClass eventClass () const throws() { return _class; }

      /** The input event class */
    private:
      InputEventClass _class;
    };
  }
}

#endif
