#ifndef _NEXUS_EVENT_BUTTONEVENTLISTENER_H
#define _NEXUS_EVENT_BUTTONEVENTLISTENER_H

#ifndef _NEXUS_H
#include <nexus/nexus.h>
#endif

namespace nexus {
  namespace event {

    /** Forward declaration of events */
    class ButtonEvent;
	
    /**
     * This interface must be implemented by wishing to listen
     * to button events.
     */
    class ButtonEventListener : public virtual ::timber::Counted {
      /** No copying allowed */
      ButtonEventListener&operator=(const ButtonEventListener&);
      ButtonEventListener(const ButtonEventListener&);

      /** Default constructor */
    public:
      inline ButtonEventListener() throws()
      {}
	  
      /** Destroy this listener */
    public:
      ~ButtonEventListener() throws()
	{}
	  
      /**
       * Process a button pressed event.
       * @param e a button event
       */
    public:
      virtual void processButtonEvent (const ::timber::Reference<ButtonEvent>& e) throws() = 0;
    };
  }
}
#endif
