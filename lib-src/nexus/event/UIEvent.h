#ifndef _NEXUS_EVENT_UIEVENT_H
#define _NEXUS_EVENT_UIEVENT_H

#ifndef _NEXUS_H
#include <nexus/nexus.h>
#endif

#ifndef _NEXUS_COMPONENT_H
#include <nexus/Component.h>
#endif

namespace nexus {
  namespace event {

    /**
     * Events generated by a UI component are subclasses of UIEvent.
     */
    class UIEvent : public ::timber::Counted {
      UIEvent&operator=(const UIEvent&);

      /**
       * Create a new UI event.
       * @param c a component
       * @pre REQUIRE_NON_ZERO(c)
       */
    protected:
      UIEvent (const ::timber::Reference<Component>& c) throws();
	
      /**
       * Create a opy of the specified component event.
       * @param e a component event
       */
    protected:
      UIEvent (const UIEvent& e) throws();
      
      /** Destroy this event. */
    public:
      virtual ~UIEvent () throws();

      /**
       * Get the source component.
       * @return the source component
       */
    public:
      ::timber::Reference<Component> source () const throws();

      /** The source component */
    private:
      ::timber::Reference<Component> _source;
    };
  }
}

#endif
