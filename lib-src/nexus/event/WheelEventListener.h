#ifndef _NEXUS_EVENT_WHEELEVENTLISTENER_H
#define _NEXUS_EVENT_WHEELEVENTLISTENER_H

#ifndef _NEXUS_H
#include <nexus/nexus.h>
#endif

namespace nexus {
  namespace event {

    /** Forward declaration of events */
    class WheelEvent;

    /**
     * This interface must be implemented to process
     * wheel events.
     */
    class WheelEventListener : public virtual ::timber::Counted {
      /** No copying allowed */
      WheelEventListener&operator=(const WheelEventListener&);
      WheelEventListener(const WheelEventListener&);
      
      /** Default constructor */
    public:
      inline WheelEventListener() throws()
      {}

      /** Destroy this listener */
    public:
      ~WheelEventListener() throws()
	{}
	  
      /**
       * Process a wheel event. Default is to do nothing.
       * @param e a wheel event
       */
    public:
      virtual void processWheelEvent (const ::timber::Reference<WheelEvent>& e) throws() = 0;
    };
  }
}
#endif
