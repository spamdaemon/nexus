#ifndef _NEXUS_EVENT_POINTEREVENT_H
#define _NEXUS_EVENT_POINTEREVENT_H

#ifndef _NEXUS_EVENT_INPUTEVENT_H
#include <nexus/event/InputEvent.h>
#endif

#ifndef _NEXUS_TIME_H
#include <nexus/Time.h>
#endif

#ifndef _NEXUS_PIXEL_H
#include <nexus/Pixel.h>
#endif

#ifndef _NEXUS_INPUTMODIFIERS_H
#include <nexus/InputModifiers.h>
#endif

namespace nexus {
  
    
  namespace event {

    /**
     * This class represents a pointer event. A pointer events
     * has always an XY position.
     */
    class PointerEvent : public InputEvent {
      /** No default constructor */
      PointerEvent&operator=(const PointerEvent&);
      PointerEvent(const PointerEvent&);

      /** Destroy this pointer event */
    public:
      ~PointerEvent () throws();

      /**
       * Create a new pointer event.
       * @param c the source component
       * @param ieClass the input event class
       * @param px the x coordinate of a reported position
       * @param py the y coordinate of a reported position
       * @param w when this event was generated
       * @param m a set of input modifiers active at the time of the event
       */
    protected:
      PointerEvent (const ::timber::Reference<Component>& c, InputEventClass ieClass, Int px, Int py, const Time& w, const InputModifiers& m) throws();
	  
      /**
       * The Pixel where the event occured.
       * @return the pixel where the pointer was reported
       */
    public:
      inline Pixel pixel () const throws()
      { return _pixel; }

      /**
       * Get the x coordinate of the reported position.
       * @return the x coordinate of the reported position
       */
    public:
      inline Int x() const throws() { return _pixel.x(); }

      /**
       * Get the y coordinate of the reported position.
       * @return the y coordinate of the reported position
       */
    public:
      inline Int y() const throws() { return _pixel.y(); }

      /**
       * Get the time at which this event occurred.
       * @return the time at which this event was generated
       */
    public:
      inline const Time& when () const throws() { return _when; }

      /**
       * Get the input modifiers for this key event.
       * @return the state of keys and buttons at the time of this event.
       */
    public:
      inline InputModifiers modifiers() const throws() { return _modifiers; }

      /** The position */
    private:
      Pixel _pixel;

      /** The time at which this event occurred */
    private:
      Time _when;

      /** The input modifiers */
    private:
      InputModifiers _modifiers;

    };
  }
}

#endif
