#include <nexus/event/WheelEvent.h>

namespace nexus {
  namespace event {
    WheelEvent::WheelEvent (const ::timber::Reference<Component>& c, const EventID& t, const Time& w, const InputModifiers& m) throws()
    : InputEvent(c,WHEEL_EVENT),_id(t),_time(w),_modifiers(m)
    {}
    
    WheelEvent::~WheelEvent() throws()
    { }
  }
}
