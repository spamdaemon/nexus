#include <nexus/event/MotionEvent.h>

namespace nexus {
  namespace event {
    MotionEvent::MotionEvent (const ::timber::Reference<Component>& c, Int px, Int py, const Time& w, const InputModifiers& m) throws()
    : PointerEvent(c,MOTION_EVENT,px,py,w,m)
    {}
    
    MotionEvent::~MotionEvent() throws()
    {  }
  }
}
