#ifndef _NEXUS_EVENT_FOCUSEVENTLISTENER_H
#define _NEXUS_EVENT_FOCUSEVENTLISTENER_H

#ifndef _NEXUS_H
#include <nexus/nexus.h>
#endif

namespace nexus {
  namespace event {

    /** Forward declaration of events */
    class FocusEvent;
	
    /**
     * This interface must be implemented by wishing to listen
     * to focus events.
     */
    class FocusEventListener : public virtual ::timber::Counted {
      /** No copying allowed */
      FocusEventListener&operator=(const FocusEventListener&);
      FocusEventListener(const FocusEventListener&);

      /** Default constructor */
    public:
      inline FocusEventListener() throws()
      {}

      /** Destroy this listener */
    public:
      ~FocusEventListener() throws()
	{}
	  
      /**
       * Process a focus event.
       * @param e a focus event
       */
    public:
      virtual void processFocusEvent (const ::timber::Reference<FocusEvent>& e) throws() = 0;
    };
  }
}

#endif
