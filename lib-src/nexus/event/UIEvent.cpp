#include <nexus/event/UIEvent.h>

namespace nexus {
  namespace event {
    UIEvent::UIEvent (const ::timber::Reference<Component>& c) throws()
    : _source(c) {  }

    UIEvent::UIEvent (const UIEvent& e) throws()
    : ::timber::Counted(),_source(e._source) 
    {}

    UIEvent::~UIEvent() throws()
    {  }

    ::timber::Reference<Component> UIEvent::source () const throws()
    { return _source; }
  }
}
