#ifndef _NEXUS_EVENT_WINDOWEVENTLISTENER_H
#define _NEXUS_EVENT_WINDOWEVENTLISTENER_H

#ifndef _NEXUS_H
#include <nexus/nexus.h>
#endif

namespace nexus {
  namespace event {

    /** Forward declaration of events */
    class WindowEvent;
	
    /**
     * This interface must be implemented to process
     * window events.
     */
    class WindowEventListener : public virtual ::timber::Counted {
      /** No copying allowed */
      WindowEventListener&operator=(const WindowEventListener&);
      WindowEventListener(const WindowEventListener&);

      /** Default constructor */
    public:
      inline WindowEventListener() throws()
      {}

      /** Destroy this listener */
    public:
      ~WindowEventListener() throws()
	{}

      /** 
       * Process the specified window event.
       * @param e a window event
       */
    public:
      virtual void processWindowEvent (const ::timber::Reference<WindowEvent>& e) throws() = 0;
    };
  }
}
#endif
