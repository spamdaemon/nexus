#ifndef _NEXUS_EVENT_KEYEVENT_H
#define _NEXUS_EVENT_KEYEVENT_H

#ifndef _NEXUS_EVENT_INPUTEVENT_H
#include <nexus/event/InputEvent.h>
#endif

#ifndef _NEXUS_INPUTMODIFIERS_H
#include <nexus/InputModifiers.h>
#endif

#ifndef _NEXUS_KEYSYMBOL_H
#include <nexus/KeySymbol.h>
#endif

#ifndef _NEXUS_TIME_H
#include <nexus/Time.h>
#endif

#include <iosfwd>

namespace nexus {
  namespace event {
    /**
     * This event is generated whenever a key was pressed
     * or released.
     */
    class KeyEvent : public InputEvent {
      KeyEvent&operator=(const KeyEvent&);
      KeyEvent(const KeyEvent&);

      /** The types of key events. */
    public:
      enum EventID { 
	/** The key press event is generated upon the down-press of a key */
	KEY_PRESSED,  
	/** 
	 * The key typed event is generated whenever a key is released. This also
	 * generated when the key is pressed for a long time to simulate repeated
	 * key strokes.
	 */
	KEY_TYPED,  
	/** The key release event is generated upon the release of a key */
	KEY_RELEASED
      };

      /** Destroy this key event */
    public:
      ~KeyEvent () throws();

      /**
       * Create a new key event.
       * @param c the source component
       * @param t the event id
       * @param w when this event occurred
       * @param m a set of input modifiers active at the time of the event
       * @param k the actual key symbol
       */
    public:
      KeyEvent (const ::timber::Reference<Component>& c, const EventID& t, const Time& w, const InputModifiers& m, const KeySymbol& k) throws();

      /**
       * Get the event id.
       * @return the kind of key event
       */
    public:
      inline EventID id () const throws() { return _id; }
 
      /**
       * Get the time at which this event occurred.
       * @return the time at which this event was generated
       */
    public:
      inline const Time& when () const throws() { return _time; }

      /**
       * Get the input modifiers for this key event.
       * @return the state of keys and buttons at the time of this event.
       */
    public:
      inline InputModifiers modifiers() const throws() { return _modifiers; }

      /**
       * Get the key that generated this event.
       * @return the key symbol
       */
    public:
      inline KeySymbol key() const throws() { return _keySymbol; }

      /** The event id */
    private:
      EventID _id;

      /** The time at which this event occurred */
    private:
      Time _time;

      /** The input modifiers */
    private:
      InputModifiers _modifiers;

      /** The key symbol */
    private:
      KeySymbol _keySymbol;
    };
  }
}

::std::ostream& operator<< (::std::ostream& out, const ::nexus::event::KeyEvent& e);

#endif
