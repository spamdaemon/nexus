#ifndef _NEXUS_EVENT_VIEWER2DEVENTLISTENER_H
#define _NEXUS_EVENT_VIEWER2DEVENTLISTENER_H

#ifndef _NEXUS_H
#include <nexus/nexus.h>
#endif

namespace nexus {
  namespace event {

    /** Forward declaration of events */
    class Viewer2DEvent;
	
    /**
     * This interface must be implemented to process
     * viewer2D events.
     */
    class Viewer2DEventListener : public virtual ::timber::Counted {
      /** No copying allowed */
      Viewer2DEventListener&operator=(const Viewer2DEventListener&);
      Viewer2DEventListener(const Viewer2DEventListener&);

      /** Default constructor */
    public:
      inline Viewer2DEventListener() throws()
      {}

      /** Destroy this listener */
    public:
      ~Viewer2DEventListener() throws()
	{}

      /** 
       * Process the specified viewer2D event.
       * @param e a viewer2D event
       */
    public:
      virtual void processViewer2DEvent (const ::timber::Reference<Viewer2DEvent>& e) throws() = 0;
    };
  }
}
#endif
