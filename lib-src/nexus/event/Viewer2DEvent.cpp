#include <nexus/event/Viewer2DEvent.h>

namespace nexus {
  namespace event {
    
    Viewer2DEvent::Viewer2DEvent (const ::timber::Reference<Viewer2D>& c, EventID eid) throws()
    : UIEvent(c),_id(eid) {}
    
    Viewer2DEvent::~Viewer2DEvent () throws()
    {}
  }
}
