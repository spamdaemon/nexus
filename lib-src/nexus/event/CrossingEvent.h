#ifndef _NEXUS_EVENT_CROSSINGEVENT_H
#define _NEXUS_EVENT_CROSSINGEVENT_H

#ifndef _NEXUS_EVENT_INPUTEVENT_H
#include <nexus/event/InputEvent.h>
#endif

#ifndef _NEXUS_TIME_H
#include <nexus/Time.h>
#endif

#ifndef _NEXUS_INPUTMODIFIERS_H
#include <nexus/InputModifiers.h>
#endif

namespace nexus {
  namespace event {

    /**
     * This event is generated whenever the pointer
     * leaves one window and enters another. The component
     * is receives a COMPONENT_EXITED event, while the one
     * that is entered receives a COMPONENT_ENTERED event.
     * When using a pointer device, such as a mouse, then the
     * two components involved are usually parent and child,
     * but other input devices such as a touchscreen may
     * generate the event between two unrelated components.
     */
    class CrossingEvent : public InputEvent {
      CrossingEvent&operator=(const CrossingEvent&);
      CrossingEvent(const CrossingEvent&);

      /** The types of crossing events */
    public:
      enum EventID { COMPONENT_ENTERED, COMPONENT_EXITED };

      /** Destroy this crossing event */
    public:
      ~CrossingEvent () throws();

      /**
       * Create a new crossing event.
       * @param c the source component
       * @param t the event id
       * @param w when this event was generated
       * @param m a set of input modifiers active at the time of the event
       */
    public:
      CrossingEvent (const ::timber::Reference<Component>& c, const EventID& t, const Time& w, const InputModifiers& m) throws();

      /**
       * Get the event id.
       * @return the kind of key event
       */
    public:
      inline EventID id () const throws() { return _id; }

      /**
       * Get the time at which this event occurred.
       * @return the time at which this event was generated
       */
    public:
      inline const Time& when () const throws() { return _time; }

      /**
       * Get the input modifiers for this key event.
       * @return the state of keys and buttons at the time of this event.
       */
    public:
      inline InputModifiers modifiers() const throws() { return _modifiers; }

      /** The event id */
    private:
      EventID _id;

      /** The time at which this event occurred */
    private:
      Time _time;

      /** The input modifiers */
    private:
      InputModifiers _modifiers;

    };
  }
}

#endif
