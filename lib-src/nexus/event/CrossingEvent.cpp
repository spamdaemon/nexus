#include <nexus/event/CrossingEvent.h>

namespace nexus {
  namespace event {
    CrossingEvent::CrossingEvent (const ::timber::Reference<Component>& c, const EventID& t, const Time& w, const InputModifiers& m) throws()
    : InputEvent(c,CROSSING_EVENT),_id(t),_time(w),_modifiers(m)
    {}

    CrossingEvent::~CrossingEvent() throws()
    {  }
  }
}
