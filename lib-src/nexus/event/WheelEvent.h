#ifndef _NEXUS_EVENT_WHEELEVENT_H
#define _NEXUS_EVENT_WHEELEVENT_H

#ifndef _NEXUS_EVENT_INPUTEVENT_H
#include <nexus/event/InputEvent.h>
#endif

#ifndef _NEXUS_INPUTMODIFIERS_H
#include <nexus/InputModifiers.h>
#endif

#ifndef _NEXUS_TIME_H
#include <nexus/Time.h>
#endif

namespace nexus {
  namespace event {
    /**
     * This event is generated whenever a wheel was pressed
     * or released.
     */
    class WheelEvent : public InputEvent {
      WheelEvent&operator=(const WheelEvent&);
      WheelEvent(const WheelEvent&);

      /** The types of wheel events */
    public:
      enum EventID { WHEEL_UP, WHEEL_DOWN };

      /** Destroy this wheel event */
    public:
      ~WheelEvent () throws();

      /**
       * Create a new wheel event.
       * @param c the source component
       * @param t the event id
       * @param w when this event occurred
       * @param m a set of input modifiers active at the time of the event
       */
    public:
      WheelEvent (const ::timber::Reference<Component>& c, const EventID& t, const Time& w, const InputModifiers& m) throws();
      
      /**
       * Get the event id.
       * @return the kind of wheel event
       */
    public:
      inline EventID id () const throws() { return _id; }
 
      /**
       * Test if the wheel was rolled up.
       * @return true if the wheel was rolled up
       */
    public:
      inline bool wheelUp() const throws() { return _id==WHEEL_UP; }
 
      /**
       * Test if the wheel was rolled down.
       * @return true if the wheel was rolled down
       */
    public:
      inline bool wheelDown() const throws() { return _id==WHEEL_DOWN; }

      /**
       * Get the time at which this event occurred.
       * @return the time at which this event was generated
       */
    public:
      inline const Time& when () const throws() { return _time; }

      /**
       * Get the input modifiers for this wheel event.
       * @return the state of wheels and buttons at the time of this event.
       */
    public:
      inline InputModifiers modifiers() const throws() { return _modifiers; }

      /** The event id */
    private:
      EventID _id;

      /** The time at which this event occurred */
    private:
      Time _time;

      /** The input modifiers */
    private:
      InputModifiers _modifiers;
    };
  }
}

#endif
