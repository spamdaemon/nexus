#include <nexus/event/WindowEvent.h>

namespace nexus {
  namespace event {

    WindowEvent::WindowEvent (const ::timber::Reference<Window>& c, EventID eid) throws()
    : UIEvent(c),_id(eid) {}
    
    WindowEvent::~WindowEvent () throws()
    {}
  }
}
