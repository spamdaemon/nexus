#ifndef _NEXUS_EVENT_KEYEVENTINTERCEPTOR_H
#define _NEXUS_EVENT_KEYEVENTINTERCEPTOR_H

#ifndef _NEXUS_H
#include <nexus/nexus.h>
#endif

#ifndef _NEXUS_EVENT_KEYEVENT_H
#include <nexus/event/KeyEvent.h>
#endif

namespace nexus {
  namespace event {
    /**
     * This interface must be implemented by wishing to listen
     * to key events.
     */
    class KeyEventInterceptor : public virtual ::timber::Counted {
      /** No copying allowed */
      KeyEventInterceptor&operator=(const KeyEventInterceptor&);
      KeyEventInterceptor(const KeyEventInterceptor&);

      /** Default constructor */
    public:
      inline KeyEventInterceptor() throws()
      {}

      /** Destroy this interceptor */
    public:
      ~KeyEventInterceptor() throws()
	{}
	  
      /**
       * Process a key event. If this interceptor consumes the event,
       * then no further interceptor and event listener will be notified.
       * @param id the key event id
       * @param ks the key a key 
       * @param when the time of the key event
       * @param im the input modifiers
       * @return true if this interceptor has consumed the event.
       */
    public:
      virtual bool interceptKeyEvent (const KeyEvent::EventID& id, const KeySymbol& ks, const Time& when, const InputModifiers& im) throws() = 0;
    };
  }
}
#endif
