#include <nexus/event/ActionEvent.h>

namespace nexus {
   namespace event {
      ActionEvent::ActionEvent(const ::timber::Reference< Component>& c) throws()
            : ActionEvent(c, ::std::string())
      {
      }

      ActionEvent::ActionEvent(const ::timber::Reference< Component>& c, const ::std::string& aid) throws()
            : UIEvent(c), _id(aid)
      {
      }

      ActionEvent::~ActionEvent() throws()
      {
      }
   }
}
