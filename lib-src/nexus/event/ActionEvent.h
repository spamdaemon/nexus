#ifndef _NEXUS_EVENT_ACTIONEVENT_H
#define _NEXUS_EVENT_ACTIONEVENT_H

#ifndef _NEXUS_EVENT_UIEVENT_H
#include <nexus/event/UIEvent.h>
#endif

namespace nexus {
  namespace event {

    /**
     * This class represents action events that are
     * caused by the user pressing some key or button.
     */
    class ActionEvent : public UIEvent {
      ActionEvent&operator=(const ActionEvent&);
      ActionEvent(const ActionEvent&);

      /**
       * Create a new component event.
       * @param c a component
       * @pre REQUIRE_NON_ZERO(c)
       */
    public:
      ActionEvent (const ::timber::Reference<Component>& c) throws();

      /**
       * Create a new component event.
       * @param c a component
       * @pre REQUIRE_NON_ZERO(c)
       * @param aid the action id string
       */
    public:
      ActionEvent (const ::timber::Reference<Component>& c, const ::std::string& aid) throws();

      /** Destroy this event */
    public:
      ~ActionEvent () throws();

      /**
       * Get the action id for this action.
       * @return the id for this action or 0 if not specified
       */
    public:
      inline const ::std::string& actionID () const throws() 
      { return _id; }

      /** The action id */
    private:
      const ::std::string& _id;
    };
  }
}
#endif
