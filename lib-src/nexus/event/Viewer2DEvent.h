#ifndef _NEXUS_EVENT_VIEWER2DEVENT_H
#define _NEXUS_EVENT_VIEWER2DEVENT_H

#ifndef _NEXUS_VIEWER2D_H
#include <nexus/Viewer2D.h>
#endif

#ifndef _NEXUS_EVENT_UIEVENT_H
#include <nexus/event/UIEvent.h>
#endif

namespace nexus {
  namespace event {

    /**
     * This class represents viewer2D events that are caused
     * by external hardware, such as a mouse, keyboard or
     * are triggered by such hardware.
     */
    class Viewer2DEvent : public UIEvent {
      Viewer2DEvent&operator=(const Viewer2DEvent&);
      Viewer2DEvent(const Viewer2DEvent&);
      
      /** The types of viewer2D events */
    public:
      enum EventID { 
	VIEW_CHANGED //< The model-view transform has changed.
      };
	  
      /**
       * Create a new viewer2D event.
       * @param c a component
       * @pre REQUIRE_NON_ZERO(c)
       * @param eid the event id
       */
    public:
      Viewer2DEvent (const ::timber::Reference<Viewer2D>& c, EventID eid) throws();
	
      /** Destroy this event. */
    public:
      ~Viewer2DEvent () throws();

      /**
       * Get the viewer2D that caused this event.
       * @return the source cast to a viewer2D
       */
    public:
      inline ::timber::Reference<Viewer2D> viewer2D() const throws()
      { return source(); }
	  
      /** 
       * Get the event id
       * @return the event id
       */
    public:
      inline EventID id() const throws()
      { return _id; }
	  
      /** The event id */
    private:
      EventID _id;
    };
  }
}
#endif
