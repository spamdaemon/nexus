#ifndef _NEXUS_EVENT_MOTIONEVENT_H
#define _NEXUS_EVENT_MOTIONEVENT_H

#ifndef _NEXUS_EVENT_POINTEREVENT_H
#include <nexus/event/PointerEvent.h>
#endif

namespace nexus {
  namespace event {

    /**
     * This class represents a pointer event. Pointer events
     * may be generated by a mouse or trackball, in which case
     * many such events are generated. Other devices such as a
     * touchscreen monitor may not generate many events at all.
     */
    class MotionEvent : public PointerEvent {
      MotionEvent&operator=(const MotionEvent&);
      MotionEvent(const MotionEvent&);

      /** Destroy this motion event */
    public:
      ~MotionEvent () throws();

      /**
       * Create a new motion event.
       * @param c the source component
       * @param px the x coordinate of a reported position
       * @param py the y coordinate of a reported position
       * @param w when this event was generated
       * @param m a set of input modifiers active at the time of the event
       */
    public:
      MotionEvent (const ::timber::Reference<Component>& c, Int px, Int py, const Time& w, const InputModifiers& m) throws();
    };
  }
}

#endif
