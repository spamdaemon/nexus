#include <nexus/event/ComponentEvent.h>

namespace nexus {
  namespace event {
    ComponentEvent::ComponentEvent (const ::timber::Reference<Component>& c, EventID eid) throws()
    : UIEvent(c),_id(eid) {}
    
    ComponentEvent::~ComponentEvent() throws()
    {  }
  }
}
