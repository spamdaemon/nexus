#include <nexus/event/ButtonEvent.h>

namespace nexus {
  namespace event {
    ButtonEvent::ButtonEvent (const ::timber::Reference<Component>& c, const EventID& t, Int px, Int py, Int bn, const Time& w, const InputModifiers& m) throws()
    : PointerEvent(c,BUTTON_EVENT,px,py,w,m),_id(t), _button(bn)
    {}

    ButtonEvent::~ButtonEvent() throws()
    { }

  }
}
