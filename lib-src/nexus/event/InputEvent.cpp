#include <nexus/event/InputEvent.h>
#include <cassert>

namespace nexus {
  namespace event {
    InputEvent::InputEvent (const ::timber::Reference<Component>& c, InputEventClass ieClass) throws()
    : UIEvent(c),_class(ieClass) 
    {
      assert((ieClass & (ieClass-1))==0);
    }

    InputEvent::~InputEvent() throws()
    { }
  }
}
