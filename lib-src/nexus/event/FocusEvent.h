#ifndef _NEXUS_EVENT_FOCUSEVENT_H
#define _NEXUS_EVENT_FOCUSEVENT_H

#ifndef _NEXUS_EVENT_INPUTEVENT_H
#include <nexus/event/InputEvent.h>
#endif

namespace nexus {
  namespace event { 

    /**
     * This event is generated whenever the component
     * that has the keyboard focus is changed. Once event
     * is generated for the component that lost the focus
     * and another once is generated for the component
     * that has gained the focus.
     */
    class FocusEvent : public InputEvent {
      FocusEvent&operator=(const FocusEvent&);
      FocusEvent(const FocusEvent&);

      /** The types of focus events */
    public:
      enum EventID { FOCUS_GAINED, FOCUS_LOST };

      /** Destroy this focus event */
    public:
      ~FocusEvent () throws();

      /**
       * Create a new focus event.
       * @param c the source component
       * @param t the event id
       */
    public:
      FocusEvent (const ::timber::Reference<Component>& c, const EventID& t) throws();

      /**
       * Get the event id.
       * @return the kind of focus event
       */
    public:
      inline EventID id () const throws() { return _id; }

      /** The event id */
    private:
      EventID _id;
    };
  }
}

#endif
