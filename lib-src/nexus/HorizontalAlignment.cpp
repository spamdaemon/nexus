#include <nexus/HorizontalAlignment.h>

namespace nexus {
  const HorizontalAlignment HorizontalAlignment::LEFT(HorizontalAlignment::ALIGN_LEFT);
  const HorizontalAlignment HorizontalAlignment::CENTER(HorizontalAlignment::ALIGN_CENTER);
  const HorizontalAlignment HorizontalAlignment::RIGHT(HorizontalAlignment::ALIGN_RIGHT);
}
