#ifndef _NEXUS_EVENTLISTENERS_H
#define _NEXUS_EVENTLISTENERS_H

#ifndef _TIMBER_EVENT_DISPATCHER_H
#include <timber/event/Dispatcher.h>
#endif

namespace nexus {
  /**
   * This class extends Listeners by providing a public dispatch
   * method. This class is meant to be used by classes that
   * require event dispatching.
   *
   */
  template <class LISTENER> class EventListeners {
    
    /** The Listener implementation, which must be reference countable */
  public:
    typedef LISTENER Listener;

    /** A functor to call an simple function */
  private:
    struct DispatchFunction {
      inline DispatchFunction (void (Listener::*method)()) 
      : _method(method) {}
      inline void operator() (::timber::Reference<Listener >& listener)
      { ((*listener).*_method)(); }
      
    private:
      void (Listener::*_method)();
    };
      
    /** A functor to call an event callback */
  private:
    template <class DATA> struct DispatchEvent {
      inline DispatchEvent (void (Listener::*method)(DATA&), DATA& data) 
      : _method(method), _data(data) {}
      inline void operator() (::timber::Reference<Listener >& listener)
      { ((*listener).*_method)(_data); }
      
    private:
      void (Listener::*_method)(DATA&);
      DATA& _data;
    };
      

    /**
     * Create a new event listeners.
     */
  public:
    inline EventListeners () throws(){}
    
    /**
     * Destroy this event listeners. Any listeners
     * that have been registered with autodelete flag
     * set will be deleted.
     */
  public:
    ~EventListeners () throws() {}

    /**
     * Test if listeners are currently being dispatched.
     * @return true if listeners are currently being dispatched.
     */
  public:
    inline bool isDispatching() const throws()
    { return _listeners.isDispatching(); }
    
    /**
     * Get the number of listeners.
     * @return the number of listeners
     */
  public:
    inline Int listenerCount() const throws()
    { return _listeners.size(); }
    
    /**
     * Add the specified listener.
     * @param l a listener
     * @pre REQUIRE_NON_ZERO(l)
     */
  public:
    inline void addListener(const ::timber::Reference<Listener>& l) throws()
    { _listeners.add(l); }
    
    /**
     * Check if the specified listener exists.
     */
  public:
    bool contains (const ::timber::Pointer<Listener>& l) const throws()
    { return _listeners.contains(l); }
    
    /**
     * Remove all listeners.
     */
  public:
    inline void clear () throws()
    { _listeners.clear(); }
    
    /**
     * Remove the specified listener. It is not
     * an error if the listener does not exist.
     * @param l a listener
     */
  public:
    inline void removeListener(const ::timber::Pointer<Listener>& l) throws()
    { _listeners.remove(l); }
    
    /**
     * The notify all listeners by invoking the specifeid method.
     * This is a convenience method.
     * @param method a function to be invoked on each listener.
     */
  public:
    inline void notify (void (Listener::*method)()) 
    {
	DispatchFunction f(method);
	_listeners.dispatch(f);	    
    }

    /**
     * Notify all listeners and pass a data object to each.
     * @param method a method with the signature <code>void m(const Data& m)</code>
     * @param data the data to be passed to the method
     */
  public:
    template <class DATA>
      inline void notify (void (Listener::*method)(DATA&), DATA& data)
      {
	DispatchEvent<DATA> f(method,data);
	_listeners.dispatch(f);	    
      }

  	/** The listeners */
      private:
    ::timber::event::Dispatcher< ::timber::Reference<Listener > > _listeners;
  
  };
}

#endif
