#include <nexus/TextArea.h>
#include <cctype>

namespace nexus {
  const char TextArea::DEFAULT_SIZE_STRING[] = { "Hello, World!" };

  TextArea::TextArea () throws (::std::exception)
  : _minRowCount(DEFAULT_MIN_ROW_COUNT),
    _maxRowCount(-1),
    _preferredRowCount(DEFAULT_MIN_ROW_COUNT),
    _minSizeString(DEFAULT_SIZE_STRING),
    _preferredSizeString(DEFAULT_SIZE_STRING)
  {	}
      
  TextArea::TextArea (const TextModel& m) throws (::std::exception)
  : TextComponent(m),
    _minRowCount(DEFAULT_MIN_ROW_COUNT),
    _maxRowCount(-1),
    _preferredRowCount(DEFAULT_MIN_ROW_COUNT),
    _minSizeString(DEFAULT_SIZE_STRING),
    _preferredSizeString(DEFAULT_SIZE_STRING)
  {	}
      
  TextArea::TextArea (const ::std::string& t) throws (::std::exception)
  : _minRowCount(DEFAULT_MIN_ROW_COUNT),
    _maxRowCount(-1),
    _preferredRowCount(DEFAULT_MIN_ROW_COUNT),
    _minSizeString(DEFAULT_SIZE_STRING),
    _preferredSizeString(DEFAULT_SIZE_STRING)
  {	 setText(t); }
      
  TextArea::~TextArea () throws()
  {}
      
  bool TextArea::isMultiLineCapable () const throws()
  { return true; }


  void TextArea::initializePeer () throws()
  {
    TextComponent::initializePeer();
  }

  TextArea::PeerComponent* TextArea::createPeerComponent (PeerComponent* p) throws()
  {
    assert(p!=0);
    return Desktop::createTextArea(*p); 
  }

  void TextArea::deriveSizeBounds (const ::std::string& ms, const ::std::string& MS, Int m, Int M) throws()
  {
    _minRowCount = m <=0 ? DEFAULT_MIN_ROW_COUNT : m;
    _maxRowCount = M <=0 ? -1 : M;

    if (_maxRowCount<=0) {
      _maxRowCount = -1;
    }
	
    _maxSizeString = MS;
    if (ms.empty()) {
      _minSizeString = DEFAULT_SIZE_STRING;
    }
    else {
      _minSizeString = ms;
    }
    updateSizeHints();
  }

  void TextArea::derivePreferredSize (const ::std::string& s, Int nr) throws()
  {
    _preferredRowCount = nr <= 0 ? DEFAULT_MIN_ROW_COUNT : nr;
    if (s.empty()) {
      _preferredSizeString = DEFAULT_SIZE_STRING;
    }
    else {
      _preferredSizeString = s;
    }
    updateSizeHints();
  }

  SizeHints TextArea::computeSizeHints () const throws()
  {
    assert(_minRowCount>0);
    assert(_preferredRowCount>0);
	
    const bool haveUpperBounds = !_maxSizeString.empty();

    Size preferredSz = calculateSize(_preferredSizeString,_preferredRowCount);
    Size sz1 = calculateSize(_minSizeString,_minRowCount);
    Size sz2;
    if (haveUpperBounds) {
      if (_maxRowCount>0) {
	sz2 = calculateSize(_maxSizeString,_maxRowCount);
      }
      else {
	sz2 = calculateSize(_maxSizeString,1);
	sz2 = Size(sz2.width(),Size::MAX.height());
      }
    }
    else {
      if (_maxRowCount>0) {
	sz2 = calculateSize(_minSizeString,_maxRowCount);
	sz2 = Size(Size::MAX.width(),sz2.height());
      }
      else {
	sz2 = Size::MAX;
      }
    }
	
    Size minSz = sz1.min(sz2).min(preferredSz);
    Size maxSz = sz1.max(sz2).max(preferredSz);
	
    return SizeHints(minSz,maxSz,preferredSz); 
  }
      
  Size TextArea::calculateSize (const ::std::string& s, Int nrows) const throws()
  {
    Size sz;
    // search for a font
    ::timber::Pointer< ::indigo::Font> f = font();
    if (f) { 
      sz = f->stringBounds(s);
    }
	
    if (sz.width()==0 || sz.height()==0) {
      sz = Size(::std::max(sz.width(),1),::std::max(sz.height(),1));
    }
    return Size(sz.width(),sz.height()*nrows);
  }
}

  
