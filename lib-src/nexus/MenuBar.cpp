#include <nexus/MenuBar.h>
#include <nexus/Menu.h>
#include <nexus/peer/Window.h>

namespace nexus {
  
    
  MenuBar::MenuBar () throws (::std::runtime_error)
  : _observer(0)
  { }
  

  MenuBar::~MenuBar () throws()
  {
    for (size_t i=0;i<_menus.size();++i) {
      detachChild(_menus[i]);
    }
    delete _observer;
  }
      
  void MenuBar::initializePeer() throws()
  {
    struct Observer : public Peer::Observer {
      inline Observer (MenuBar& b) throws() : _owner(b) {}
      ~Observer () throws() {}

    private:
      MenuBar& _owner;
    };

    Component::initializePeer();
    if (_observer==0) {
      _observer = new Observer(*this);
    }

    menuBar()->setMenuBarObserver(_observer);
  }

  MenuBar::PeerComponent* MenuBar::createPeerComponent (PeerComponent* p) throws()
  {
    typedef ::nexus::peer::Window WindowPeer;
    
    assert(p!=0);
    WindowPeer* win = dynamic_cast<WindowPeer*>(p);
    if (win==0) {
      return 0;
    }
    return Desktop::createMenuBar(*win); 
  }

  SizeHints MenuBar::getSizeHints() const throws()
  {
    return SizeHints();
  }

  
  void MenuBar::addMenu(::timber::Reference<Menu> menu) throws()
  {
    Component* m = attachChild(menu);
    if (m) {
      _menus.push_back(m);
    }
  }
    
  ::timber::Reference<Menu> MenuBar::addMenu(const ::std::string& nm) throws()
  {
    ::timber::Reference<Menu> menu = new Menu(nm);
    addMenu(menu);
    return menu;
  }
  
  ::timber::Reference<Menu> MenuBar::addMenu(const char* nm) throws()
  { return addMenu( ::std::string(nm)); }
  
  void MenuBar::removeMenu (::timber::Reference<Component> menu) throws()
  {
    for (size_t i=0;i<_menus.size();++i) {
      ::timber::Reference<Component> m = _menus[i];
      if (m==menu) {
	detachChild(_menus[i]);
	_menus.erase(_menus.begin()+i);
	break;
      }
    }
  }
  
  
}

