#include <nexus/Container.h>

namespace nexus {

   Container::Container(::std::unique_ptr< LayoutEngine> engine) throws (::std::exception) :
      Composite(::std::move(engine))
   {
   }

   Container::Container() throws(::std::exception)
   {
   }

   Container::~Container() throws()
   {
   }

}
