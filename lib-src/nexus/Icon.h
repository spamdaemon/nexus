#ifndef _NEXUS_ICON_H
#define _NEXUS_ICON_H

#ifndef _INDIGO_NODE_H
#include <indigo/Node.h>
#endif

#ifndef _NEXUS_SIZE_H
#include <nexus/Size.h>
#endif


namespace nexus {
  /**
   * This class represents graphics which are are bounded in size.
   * An icon is like a small canvas.
   */
  class Icon {
    /** Typedef for a graphic handle */
  public:
    typedef ::timber::Pointer< ::indigo::Node> GraphicHandle;
    
    /** Create a default icon with default size and no graphic */
  public:
    inline Icon () throws() {}
    
    /**
     * Copy constructor.
     * @param icon an icon
     */
  public:
    inline Icon (const Icon& icon) throws()
      : _icon(icon._icon),_size(icon._size) {}
    
    /**
     * Create an icon with the specified graphic and size.
     * @param icon the icon graphic
     * @param sz the icon size
     * @pre REQUIRE_NON_ZERO(icon)
     * @pre REQUIRE_CONDITION(sz.width()>0 && sz.height()>0)
     */
  public:
    inline Icon (const GraphicHandle& icon, const Size& sz) throws()
      : _icon(icon), _size(sz)
    {
      assert(sz.width()>0);
      assert(sz.height()>0);
    }
    
    /** Destructor. */
  public:
    inline ~Icon() throws() {}
    
    /** 
     * The width of this icon 
     * @return the width of this icon
     */
  public:
    inline Int width() const throws() 
    { return _size.width(); }
    
    /** 
     * The height of this icon 
     * @return the wheightidth of this icon
     */
  public:
    inline Int height() const throws() 
    { return _size.height(); }
    
    /**
     * Get the icon size
     * @return the icon's size
     */
  public:
    inline Size size() const throws()
    { return _size; }
    
    /**
     * Get the icon graphic.
     * @return a graphic handle
     */
  public:
    inline GraphicHandle graphic () const throws()
    { return _icon; }

    /**
     * Copy operator. Both icons will share the same graphic.
     * @param icon an icon
     * @return this icon
     */
  public:
    inline Icon& operator=(const Icon& icon) throws()
      {
	_icon = icon._icon;
	_size = icon._size;
	return *this;
      }
      
    /** The icon graphic */
  private:
    GraphicHandle _icon;

    /** The icon size */
  private:
    Size _size;
  };
}

#endif
