#ifndef _NEXUS_MENUITEM_H
#define _NEXUS_MENUITEM_H

#ifndef _NEXUS_EVENT_ACTIONEVENTLISTENER_H
#include <nexus/event/ActionEventListener.h>
#endif

#ifndef _NEXUS_COMPONENT_H
#include <nexus/Component.h>
#endif

#ifndef _NEXUS_PEER_MENUITEM_H
#include <nexus/peer/MenuItem.h>
#endif

#ifndef _NEXUS_ICON_H
#include <nexus/Icon.h>
#endif

namespace nexus {

  /**
   * The Menu is attached to a window and serves as the hook for adding menu items
   * to it. Typically, a menu is instantiated by a Window.
   */
  class MenuItem : public Component {
    MenuItem(const MenuItem&);
    MenuItem&operator=(const MenuItem&);
	
    /** An action event */
  public:
    typedef ::nexus::event::ActionEvent ActionEvent;

    /** The implementation class */
  public:
    typedef ::nexus::peer::MenuItem Peer;
    
    /** 
     * Default constructor.
     * @throw ConfigurationException if the desktop has not been configured
     */
  public:
    MenuItem () throws (::std::runtime_error);
    
    /** 
     * Constructor with a name.
     * @param nm the name of the menu
     * @throw ConfigurationException if the desktop has not been configured
     */
  public:
    MenuItem (const ::std::string& nm) throws (::std::runtime_error);
    
    /** 
     * Constructor with an icon.
     * @param i an icon
     * @throw ConfigurationException if the desktop has not been configured
     */
  public:
    MenuItem (const Icon& i) throws (::std::runtime_error);
    
    /** Destroy this menu and release all its resources */
  public:
    ~MenuItem () throws();
    
    /** @name Action events
     * @{
     */

   /**
     * Set the action identifier. The action identifier
     * is a string identifying the action independent of the 
     * source component.
     */
  public:
    void setActionID(const ::std::string& s) throws();
	
    /**
     * Get the action identifier.
     * @return the action id.
     */
  public:
    inline const ::std::string& actionID() const throws()
    { return _actionID; }

    /**
     * Add an action listener.
     * @param l an action listener
     */
  public:
    void addActionEventListener (const ::timber::Reference< ::nexus::event::ActionEventListener>& l) throws();
	
    /**
     * Remove an action listener.
     * @param l an action listener
     */
  public:
    void removeActionEventListener (const ::timber::Pointer< ::nexus::event::ActionEventListener>& l) throws();

    /**
     * Process an action event.
     * @param e an action event
     */
  protected:
    virtual  void processActionEvent (const ::timber::Reference<ActionEvent>& e) throws();
	
    /*@}*/

    /**
     * Get the text for this button.
     * @return the text for this button or 0 if no text is used.
     */
  public:
    inline const ::std::string& text() const throws() 
    { return _text; }

    /**
     * Set the text for this button. Sets the icon to 0.
     * @param t the text for this button or 0
     */
  public:
    virtual void setText (const ::std::string& t) throws();
	
    /**
     * Get the icon for this button.
     * @return the icon for this button or an icon with no graphic.
     */
  public:
    inline Icon icon() const throws() 
    { return _icon; }
	
    /**
     * Set the icon for this button. Sets the text
     * to 0.
     * @param i the icon for this button
     */
  public:
    virtual void setIcon (const Icon& i) throws();

    
    /**
     * Get the menu peer.
     * @return the widow peer
     */
  private:
    inline Peer* menuItem() const throws()
    { return impl<Peer>(); }
    
  protected:
    void initializePeer() throws();
    PeerComponent* createPeerComponent (PeerComponent* p) throws();
    SizeHints getSizeHints() const throws();

    /** The observer */
  private:
    Peer::Observer* _observer;

    /** The text for this button */
  private:
    ::std::string _text;

    /** The icon for this button */
  private:
    Icon _icon;

     /** The action listeners */
  private:
    EventListeners< ::nexus::event::ActionEventListener> _actionListeners;


    /** The action id */
  private:
    ::std::string _actionID;
 };
}

#endif

