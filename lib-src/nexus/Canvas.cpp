#include <nexus/Canvas.h>
#include <nexus/peer/Canvas.h>

namespace nexus {
  
    
  Canvas::Canvas () throws (::std::runtime_error)
  : _autoRender(true),_doubleBuffered(true)
  { }

  Canvas::~Canvas() throws()
  {}

  Canvas::PeerComponent* Canvas::createPeerComponent (PeerComponent* p) throws()
  { return Desktop::createCanvas2D(*p); }
	
  void Canvas::initializePeer () throws()
  {
    Component::initializePeer();
    canvas()->setAutoRenderEnabled(_autoRender);
    canvas()->setDoubleBufferEnabled(_doubleBuffered);
    canvas()->setGraphic(_gfx);
  }
      
  void Canvas::setGraphic (const  ::timber::Pointer< ::indigo::Node>& gfx) throws()
  {
    if (_gfx!=gfx) {
      _gfx = gfx;
      if (hasPeer()) {
	canvas()->setGraphic(gfx);
      }
    }
  }

  void Canvas::setSizeHints (const SizeHints& hints) throws()
  {
    if (hints!=_hints) {
      _hints = hints;
      revalidate();
    }
  }

  SizeHints Canvas::getSizeHints () const throws()
  { return _hints; }
      
  void Canvas::setAutoRenderEnabled (bool enabled) throws()
  {
    if (_autoRender!=enabled) {
      _autoRender = enabled;
      if (hasPeer()) {
	canvas()->setAutoRenderEnabled(enabled);
      }
    }
  }

  void Canvas::setDoubleBufferEnabled (bool enabled) throws()
  {
    if (_doubleBuffered != enabled) {
      _doubleBuffered = enabled;
      if (hasPeer()) {
	canvas()->setDoubleBufferEnabled(enabled);
      }
    }
  }

  void Canvas::render () const throws()
  {
    if (hasPeer()) {
      canvas()->render();
    }
  }

  ::std::unique_ptr<Canvas::PickInfo> Canvas::pickNode (const Pixel& px) const throws()
  {
    ::std::unique_ptr<PickInfo> info;
    if (hasPeer()) {
      ::std::unique_ptr<Peer::PickInfo> xinfo = canvas()->pickNode(px);
      if (xinfo.get()!=0) {
	info.reset(new PickInfo());
	info->pixel = px;
	info->node = xinfo->node;
	info->shape = xinfo->shape;
	info->info = xinfo->info;
      }
    }
    return info;
  }

}
