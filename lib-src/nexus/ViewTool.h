#ifndef _NEXUS_VIEWTOOL_H
#define _NEXUS_VIEWTOOL_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

namespace nexus {
   class Viewer2D;

   /**
    * This class provides a tool that allows the view of Viewer2D
    * to be manipulates via elementatry operations. The elementary
    * operations are translation, rotation, and scaling.
    */
   class ViewTool
   {
         /** The tools implementation class */
      private:
         class Impl;

         /** The panning mode */
      public:
         enum class PanMode
         {
            // when the user drags the mouse, the viewport is
            // moved
            DRAG_VIEWPORT,

            // when the user drags the mouse, the pixel
            // under the mouse is moved
            DRAG_OBJECT
         };

         /**
          * Constructor.
          */
      public:
         ViewTool() throws();

         /**
          * Destructor.
          */
      public:
         ~ViewTool() throws();

         /**
          * Set a new viewer or remove the current viewer. If
          * a viewer is already being used by this tool, then the
          * tool will removed from the existing viewer and attached to
          * the new one.
          * @param viewer a viewer or null to detach from the current one
          * @note It is the responsibility of the client to ensure that only tool is use on each viewer.
          */
      public:
         void setViewer(const ::timber::Pointer< Viewer2D>& viewer) throws();

         /**
          * @name Parameters affecting how the tool works.
          * @{
          */

         /**
          * Enable scaling.
          * @param enable true to enable, false to disable
          */
      public:
         void setZoomEnabled(bool enable) throws();

         /**
          * Enable rotations.
          * @param enable true to enable, false to disable
          */
      public:
         void setRotateEnabled(bool enable) throws();

         /**
          * Enable panning.
          * @param enable true to enable, false to disable
          */
      public:
         void setPanEnabled(bool enable) throws();

         /**
          * Set the panning mode.
          * @param panMode the panning mode
          */
      public:
         void setPanMode(PanMode mode) throws();

         /**
          * Set the factor for zooming out. This should be a value
          * less than 1.
          * @param zf the zoom out factor
          */
      public:
         void setZoomOutFactor(double zf) throws();

         /**
          * Set the factor for zooming in. This should be a value
          * larger than 1.
          * @param zf the zoom in factor
          */
      public:
         void setZoomInFactor(double zf) throws();

         /**
          * Set the maximum zoom factor. This value determines the
          * maximum number of pixels that a unit
          * in world coordinates may occupy.
          * @param maxZoom the maximum zoom.
          */
      public:
         void setMaxZoom(double maxZoom) throws();

         /**
          * Set the minimum zoom factor. This value determines the
          * minimum number of pixels that a unit
          * in world coordinates may occupy.
          * @param minZoom the minimum zoom.
          */
      public:
         void setMinZoom(double minZoom) throws();

         /*@}*/

         /**
          * The viewer to which this tool is currently attached.
          */
      private:
         ::timber::Pointer< Viewer2D> _viewer;

         /** The implementation */
      private:
         ::timber::Reference< Impl> _tool;

   }
   ;

}
#endif
