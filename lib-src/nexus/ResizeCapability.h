#ifndef _NEXUS_RESIZABILITY_H
#define _NEXUS_RESIZABILITY_H

#ifndef _NEXUS_H
#include <nexus/nexus.h>
#endif

namespace nexus {

  /**
   * This class defines the capability of 
   * things to be resize.
   */
  class ResizeCapability {
    /** The resize capability values */
  public:
    enum Capability { 
      RESIZE_NONE = 0,       //< not resizable
      RESIZE_VERTICAL = 1,   //< vertically resizable 
      RESIZE_HORIZONTAL = 2, //< horizontally resizable
      RESIZE_BOTH = 3,       //< vertically and horizontally resizable
    };

    /** Not resizable */
  public:
    static const ResizeCapability NONE;

    /** Vertically resizable  */
  public:
    static const ResizeCapability VERTICAL;

    /** Horizontally resizable */
  public:
    static const ResizeCapability HORIZONTAL;

    /** Horizontally and vertically resizable */
  public:
    static const ResizeCapability BOTH;

    /**
     * Create a new resizability.
     * @param a an resize capability
     */
  private:
    inline ResizeCapability (Capability a) throws()
      : _resize(a) {}

    /** Create a NONE resize capability  */
  public:
    inline ResizeCapability () throws()
      : _resize(RESIZE_NONE) {}

    /**
     * Compute a hashvalue.
     * @return a hashvalue
     */
  public:
    inline size_t hashValue () const throws()
    { return _resize; }

    /**
     * Compare two resize capabilities.
     * @param a an resize capability
     * @return true if the two resize capabilities are the same
     */
  public:
    inline bool operator==(const ResizeCapability& a) const throws()
    { return _resize == a._resize; }

    /**
     * Compare two resize capabilities.
     * @param a an resize capability
     * @return true if the two resize capabilities are the same
     */
  public:
    inline bool equals(const ResizeCapability& a) const throws()
    { return *this == a; }

    /**
     * Compare two resize capabilities.
     * @param a an resize capability
     * @return true if the two resize capabilities are different
     */
  public:
    inline bool operator!=(const ResizeCapability& a) const throws()
    { return _resize != a._resize; }

    /**
     * Get the resize capability value. This method is primarily
     * used in switch statements.
     * @return the capability to resize
     */
  public:
    inline Capability value () const throws() { return _resize; }


    /** A resize capability */
  private:
    Capability _resize;
  };
}


#endif
