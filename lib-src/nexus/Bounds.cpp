#include <nexus/Bounds.h>
#include <iostream>

namespace nexus {

  bool Bounds::contains (Int px, Int py) const throws()
  {
    const Int a = x()+width();
    const Int b = y()+height();
      
    return 
      ((x() <= px && px <= a) || (a<= px && px <= x())) &&
      ((y() <= py && py <= b) || (b<=py && py <= y()));
  }
    
  Bounds Bounds::mergeBounds (const Bounds& b) const throws()
  {
    Int left = ::std::min(x(),b.x());
    Int top  = ::std::min(y(),b.y());
    Int right = ::std::max(x()+width(),b.x()+b.width());
    Int bottom = ::std::max(y()+height(),b.y()+b.height());
	
    return Bounds(left,top,right-left,bottom-top);
  }

  Bounds Bounds::intersectBounds (const Bounds& b) const throws()
  {
    Int left = ::std::max(x(),b.x());
    Int top  = ::std::max(y(),b.y());
    Int right = ::std::min(x()+width(),b.x()+b.width());
    Int bottom = ::std::min(y()+height(),b.y()+b.height());
	
    if (left< right && top < bottom) {
      return Bounds(left,top,right-left,bottom-top);
    }
    return Bounds(0,0,0,0);
  }

  Bounds Bounds::centerBounds (const Size& sz) const throws()
  {
    Int px = x() + (width()-sz.width())/2;
    Int py = y() + (height()-sz.height())/2;
    return Bounds(Pixel(px,py),sz);
  }
    
  Bounds Bounds::widenBounds (Int w, Int h) const throws()
  {
    Int px = x()-w/2;
    Int py = y()-h/2;
    w += width();
    h += height();
    return Bounds(px,py,w,h);
  }

}

::std::ostream& operator<< (::std::ostream& out, const ::nexus::Bounds& b) throws()
{
  out << "x="<<b.x() 
      << ", y="<<b.y() 
      << ", width="<<b.width()
      <<", height="<<b.height();
  return out;
}  
