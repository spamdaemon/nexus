#include <nexus/Viewer2D.h>
#include <nexus/event/Viewer2DEvent.h>
#include <nexus/event/ComponentEvent.h>
#include <nexus/event/ButtonEvent.h>
#include <nexus/event/CrossingEvent.h>
#include <nexus/event/MotionEvent.h>
#include <nexus/event/WheelEvent.h>
#include <nexus/Canvas.h>

#include <indigo/ViewTransform2D.h>
#include <indigo/AffineTransform2D.h>

#include <timber/logging.h>

using namespace ::timber::logging;
using namespace ::timber;
using namespace ::indigo;
using namespace ::nexus::event;

namespace nexus {
   namespace {
      static Log logger()
      {
         return Log("nexus.Viewer2D");
      }
   }

   void Viewer2D::VPort::setViewPort(const ::indigo::view::ViewPort::Bounds& newBounds,
         const ::indigo::view::ViewPort::Window& newView)
throws ( ::std::invalid_argument)
{
   ViewPort::setViewPort(newBounds,newView);
   try {
      _viewer.viewChanged();
      _viewer.updateTransform();
   }
   catch (::std::exception& e) {
      logger().severe("Could not update view transform for viewer");
   }
}

Viewer2D::Viewer2D()
throws (::std::exception)
: ProxyComponent(new Canvas()),
_root(new ::indigo::Group(2,false)),
_viewPort(*this)
{
   _root->set(new ::indigo::ViewTransform2D(),0);
   child<Canvas>().setGraphic(_root);
   // initialize the matrices
   updateTransform();
}

Viewer2D::~Viewer2D()
throws()
{}

::std::unique_ptr< Viewer2D::PickInfo> Viewer2D::pickNode(const Pixel& px) const
throws()
{
   ::std::unique_ptr<Viewer2D::PickInfo> res;
   ::std::unique_ptr<Canvas::PickInfo> info = child<Canvas>().pickNode(px);
   if (info.get()) {
      res.reset(new PickInfo());
      res->info = info->info;
      res->node = info->node;
      res->shape = info->shape;
   }
   return res;
}

Pointer< ::indigo::Node> Viewer2D::graphic() const
throws()
{
   if (_root->nodeCount()==1) {
      return Pointer< ::indigo::Node>();
   }
   return _root->node(1);
}

void Viewer2D::setGraphic(const Pointer< ::indigo::Node>& gfx)
throws()
{
   if (gfx!=nullptr) {
      _root->set(gfx,1);
   }
   else if (_root->nodeCount()==2) {
      _root->remove(1);
   }
}

void Viewer2D::setSizeHints(const SizeHints& hints)
throws()
{
   child<Canvas>().setSizeHints(hints);
}

void Viewer2D::doLayout()
throws()
{
   ProxyComponent::doLayout();
   updateTransform();
}
void Viewer2D::viewChanged()
throws()
{
   processViewer2DEvent(new Viewer2DEvent(this,Viewer2DEvent::VIEW_CHANGED));
   updateTransform();
}

ViewPort::Point Viewer2D::transformPixel(const Pixel& p) const
throws()
{  return _viewPort.getPoint(ViewPort::Pixel(p.x(),p.y()));}

Pixel Viewer2D::transformPoint(const ViewPort::Point& p) const
throws()
{
   ViewPort::Pixel px = _viewPort.getPixel(p);
   return Pixel((Int)px.x(),(Int)px.y());
}

void Viewer2D::updateTransform()
throws()
{
   Pointer< ::indigo::ViewTransform2D> t(_root->node(0));
   if(!t) {
      return;
   }

   if (_viewPort.viewWindow().width()!=width() || _viewPort.viewWindow().height()!=height()) {
      double sx = _viewPort.horizontalScale();
      double sy = _viewPort.verticalScale();
      // size has to be at least 1x1 pixel
      double w = ::std::max(1,width());
      double h = ::std::max(1,height());

      // remember the current center point; we want to keep it steady; round-off error may cause it move around a little,
      // because it is a computed quantity
      Point ctr = _viewPort.centerPoint();

      _viewPort.setViewPort(_viewPort.bounds(),ViewPort::Window(w,h));
      _viewPort.resizeBoundsToScale(sx,sy,ctr);

      // just in case, make sure the center point of the bounds does indeed coincide with the center of the screen
      _viewPort.movePointToPixel(ctr,_viewPort.centerPixel());
   }

   try {
      _matrix = _viewPort.getModelViewTransform();
      _invMatrix = _viewPort.getViewModelTransform();

      // update the transform itself
      t->setTransform(_matrix);
   }
   catch (...) {
      Log("nexus.Viewer2D").warn("View transform is not invertible");
   }
}

void Viewer2D::setAutoRenderEnabled(bool enabled)
throws()
{  child<Canvas>().setAutoRenderEnabled(enabled);}

bool Viewer2D::isAutoRenderEnabled() const
throws()
{  return child<Canvas>().isAutoRenderEnabled();}

void Viewer2D::setDoubleBufferEnabled(bool enabled)
throws()
{  child<Canvas>().setDoubleBufferEnabled(enabled);}

bool Viewer2D::isDoubleBufferEnabled() const
throws()
{  return child<Canvas>().isDoubleBufferEnabled();}

void Viewer2D::render() const
throws()
{  child<Canvas>().render();}

void Viewer2D::processViewer2DEvent(const Reference< Viewer2DEvent>& e)
throws ()
{  _viewerListeners.notify(&Viewer2DEventListener::processViewer2DEvent,e);}

}
