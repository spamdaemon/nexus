#include <nexus/Color.h>
#include <iostream>

namespace nexus {
  const Color Color::RED(1.,0.,0.);
  const Color Color::GREEN(0.,1.,0.);
  const Color Color::BLUE(0.,0.,1.);
  const Color Color::BLACK(0.,0.,0.);
  const Color Color::WHITE(1.,1.,1.);
  const Color Color::YELLOW(1.,1.,0.);
  const Color Color::MAGENTA(1.,0.,1.0);
  const Color Color::CYAN(0.,1.,1.);
  const Color Color::LIGHT_GREY(1.,1.,1.,.75);
  const Color Color::GREY(1.,1.,1.,.5);
  const Color Color::DARK_GREY(1.,1.,1,.25);

  Color::Color() throws()
  {}
    
  Color::Color (const ::indigo::Color& c) throws()
  : _rgbi(new SharedImpl(c.red(),c.green(),c.blue(),c.opacity()))
  {}
      
   
  Color::Color(const Color& c) throws()
  : _rgbi(c._rgbi==nullptr ? 0 : new SharedImpl(c.red(),c.green(),c.blue(),c.opacity()))
  {}
      
   
  Color::Color (const Color& c, double a) throws()
  : _rgbi(c._rgbi==nullptr ? 0 : new SharedImpl(c.red(),c.green(),c.blue(),c.opacity()*a))
  {}
      
 
  Color::Color (UByte r, UByte g, UByte b, UByte i) throws()
  : _rgbi(new SharedImpl(static_cast<double>(r)/255.0,
			 static_cast<double>(g)/255.0,
			 static_cast<double>(b)/255.0,
			 static_cast<double>(i)/255.0))
  {}
      
  
  Color::Color (UByte r, UByte g, UByte b) throws()
  : _rgbi(new SharedImpl(static_cast<double>(r)/255.0,
			 static_cast<double>(g)/255.0,
			 static_cast<double>(b)/255.0,
			 1.0))
  {}
      
  
  Color::Color (UShort r, UShort g, UShort b, UShort i) throws()
  : _rgbi(new SharedImpl(static_cast<double>(r)/65535.0,
			 static_cast<double>(g)/65535.0,
			 static_cast<double>(b)/65535.0,
			 static_cast<double>(i)/65535.0))
  {}
      
  
  Color::Color (UShort r, UShort g, UShort b) throws()
  : _rgbi(new SharedImpl(static_cast<double>(r)/65535.0,
			 static_cast<double>(g)/65535.0,
			 static_cast<double>(b)/65535.0,
			 1.0))
  {}
      
   
  Color::Color (double r, double g, double b, double i) throws()
  : _rgbi(new SharedImpl(r,g,b,i))
  {}
       
  
  Color::Color (double r, double g, double b) throws()
  : _rgbi(new SharedImpl(r,g,b,1.0)) {}
    
  Color::~Color() throws()
  {}
      
  Color& Color::operator=(const Color& c) throws()
  { _rgbi = c._rgbi; return *this; }

  bool Color::equals (const Color& c) const throws()
  {
    if (_rgbi==c._rgbi) {
      return true;
    }
    if (_rgbi==nullptr || c._rgbi==nullptr) {
      return false;
    }
    return _rgbi->equals(*c._rgbi);
  }
}

::std::ostream& operator<< (::std::ostream& out, const ::nexus::Color& c) throws()
{
  if (c==0) {
    out << "<null>";
  }
  else {
    out << "r="<<c.red()
	<< ", g="<<c.green() 
	<< ", b="<<c.blue()
	<<", a="<<c.opacity();
  }
  
  return out;
}  
