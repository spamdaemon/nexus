#ifndef _NEXUS_RECTANGLE_H
#define _NEXUS_RECTANGLE_H

#ifndef _INDIGO_RECTANGLE_H
#include <indigo/Rectangle.h>
#endif

namespace nexus {
  
  using ::indigo::Rectangle;
}

#endif
