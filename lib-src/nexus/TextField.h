#ifndef _NEXUS_TEXTFIELD_H
#define _NEXUS_TEXTFIELD_H

#ifndef _NEXUS_TEXTCOMPONENT_H
#include <nexus/TextComponent.h>
#endif

#ifndef _NEXUS_EVENT_ACTIONEVENTLISTENER_H
#include <nexus/event/ActionEventListener.h>
#endif

#ifndef _NEXUS_PEER_TEXTFIELD_H
#include <nexus/peer/TextField.h>
#endif

namespace nexus {
  /**
   * This class is a single-line text entry field.
   * @date 06 Jun 2003
   */
  class TextField : public TextComponent {
    /** No copying allowed */
    TextField(const TextField&);
    TextField&operator=(const TextField&);

    /** The default string from preferred and minimum size */
  private:
    static const char DEFAULT_SIZE_STRING[];

    /** An action event */
  public:
    typedef ::nexus::event::ActionEvent ActionEvent;

    /** The implementation class is a container */
  public:
    typedef ::nexus::peer::TextField Peer;

    /**
     * The default constructor 
     * @throws ConfigurationException if the desktop was not initialized
     */
  public:
    TextField () throws (::std::exception);

    /**
     * Create a new textfield with a specified text model.
     * @param model a text model
     * @pre REQUIRE_NON_ZERO(model)
     */
  public:
    TextField (const TextModel& model) throws (::std::exception);

    /**
     * Create a new textfield with a specified text
     * @param txt a text string or 0
     */
  public:
    TextField (const ::std::string& txt) throws (::std::exception);

    /** Destroy this textfield */
  public:
    ~TextField () throws();

    /**
     * Set the action identifier. The action identifier
     * is a string identifying the action independent of the 
     * source component.
     */
  public:
    inline void setActionID(const ::std::string& s) throws()
    { _actionID = s; }
	
    /**
     * Get the action identifier.
     * @return the action id.
     */
  public:
    inline ::std::string actionID() const throws()
    { return _actionID; }

    /**
     * TextFields can display only a single line.
     * @returns false
     */
  public:
    bool isMultiLineCapable () const throws();

    /**
     * Set the maximum number of characters that are allowed in this field.
     * A count of 0 or less indicates unbounded input. If this
     * field has currently more than cnt characters, then the field
     * is truncated to cnt characters.
     * @param cnt the maximum input size
     */
  public:
    virtual void setInputSize (Int cnt) throws();

    /**
     * Get the input size.
     * @return the maximum number of characters allowed
     */
  public:
    inline Int inputSize () const throws()
    { return _inputSize; }
      
    /**
     * Derive the minimum and maximum size for this field.
     * The derived size will be computed such that the currently
     * preferred size will be contained within the minimum size
     * and the maximum size. 
     * @param m a a string or 0 to use the default minimum size string
     * @param M a string or 0 to use unlimited size.
     */
  public:
    virtual void deriveSizeBounds (const ::std::string& m, const ::std::string& M) throws();

    /**
     * Derive the preferred size from the specified string.
     * The minimum and maximum sizes will be adjusted as necessary
     * to accomodate the preferred size.
     * @param m a string or 0 to use the default preferred size
     */
  public:
    virtual void derivePreferredSize (const ::std::string& m) throws();
	
    /**
     * Add an action listener.
     * @param l an action listener
     * @pre REQUIRE_NON_ZERO(l)
     */
  public:
    inline void addActionEventListener (const ::timber::Reference< ::nexus::event::ActionEventListener>& l) throws()
    { _actionListeners.addListener(l); }
	
    /**
     * Remove an action listener.
     * @param l an action listener
     */
  public:
    inline void removeActionEventListener (const ::timber::Pointer< ::nexus::event::ActionEventListener>& l) throws()
    { _actionListeners.removeListener(l); }

    /**
     * Process an action event.
     * @param e an action event
     */
  protected:
    virtual  void processActionEvent (const ::timber::Reference<ActionEvent>& e) throws();
	
  protected:
    void initializePeer() throws();
    PeerComponent* createPeerComponent (PeerComponent* p) throws();
    SizeHints computeSizeHints() const throws();

    /**
     * Calculate the size for this textfield to completely show
     * the specified string.
     * @param s a string
     * @return the required size for this textfield to show the string
     */
  private:
    Size calculateSize (const ::std::string& s) const throws();

    /**
     * Get the window peer.
     * @return the widow peer
     */
  private:
    inline Peer* textField() const throws()
    { return impl<Peer>(); }

    /** The action listeners */
  private:
    EventListeners< ::nexus::event::ActionEventListener> _actionListeners;

    /** The observer */
  private:
    Peer::TextFieldObserver* _observer;

    /** The action id */
  private:
    ::std::string _actionID;

    /** The maximum input size */
  private:
    Int _inputSize;

    /** The ::std::string determining the minimum size */
  private:
    ::std::string _minSizeString;

    /** The String determining the maximum size */
  private:
    ::std::string _maxSizeString;

    /** The preferred size string */
  private:
    ::std::string _preferredSizeString;
  };
}

#endif
