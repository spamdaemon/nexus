#ifndef _NEXUS_LAYOUT_GRIDLAYOUT_H
#define _NEXUS_LAYOUT_GRIDLAYOUT_H

#ifndef _NEXUS_BOUNDS_H
#include <nexus/Bounds.h>
#endif

#ifndef _NEXUS_SIZEHINTS_H
#include <nexus/SizeHints.h>
#endif

#ifndef _NEXUS_LAYOUT_LAYOUT_H
#include <nexus/layout/Layout.h>
#endif

#include <vector>

namespace nexus {
  namespace layout {

    /**
     * This layout strategy lays components out in a regular
     * grid. All components in a row will have the same height
     * and all components in a column will have the same width.
     * <p>
     * FIXME: spacing between components must be non-negative at this time
     */
    class GridLayout : public Layout {
      GridLayout&operator=(const GridLayout&);

      /** The parameter */
    public:
      typedef ::std::pair<Index,Index> Parameter;

      /** The super class */
    private:
      typedef Layout super;
	
      /** The constraint */
    private:
      typedef ::std::pair<Component*,Parameter> Constraint;
	
      /** The constraints */
    private:
      typedef ::std::vector<Constraint> Constraints;

      /** The grid itself */
    private:
      typedef ::std::pair<Bounds,bool> GridEntry;
	
      /** The default spacing */
    public:
      static const Int DEFAULT_SPACING;
	
      /** 
       * Create a grid layout with the specified number of rows and columns
       * @param w the number of components per row
       * @param h the number of rows
       * @pre REQUIRE_GREATER(w,0)
       * @pre REQUIRE_GREATER(h,0)
       */
    public:
      GridLayout (Int w, Int h) throws();

      /**
       * A copy constructor. The component constraints 
       * are not copied.
       * @param f a grid layout
       */
    public:
      GridLayout (const GridLayout& f) throws();

      /** 
       * Default constructor using a vertical grid.
       * @param s the spacing between components or a negative number
       */
    public:
      GridLayout (Int s) throws();
	  
      /** Destroy this container and release all its resources */
    public:
      ~GridLayout () throws();
	

      /**
       * Use the preferred size for components and don't let them
       * resize arbitrarily.
       * @param usePreferredSize if true use preferred size
       */
    public:
      void setUsePreferredSize (bool usePreferredSize) throws();

      /**
       * Test if components will be laid out at their preferred size.
       * @return true if components will not exceed their preferred size.
       */
    public:
      inline bool isUsePreferredSize () const throws()
      { return _usePreferredSize; }

      /**
       * Get the spacing between components.
       * @return the spacing between components
       */
    public:
      inline Int spacing () const throws()
      { return _componentSpacing; }

      /**
       * Set the spacing for this container.
       * @param sp the new spacing
       */
    public:
      void setSpacing (Int sp) throws();
	  
      /**
       * @name Implementation of the Layout interface 
       * @{
       */
    public:
      Bounds bounds (Index i) const throws();	
      Component* component (Index i) const throws();
      Int size () const throws();
      void doLayout(const Size& sz) throws(LayoutException);
      SizeHints sizeHints() const throws();
	  
      /*@}*/

      /**
       * Add a component with the specified parameter. The subclass
       * must not increment the reference count of the component.
       *
       * @param c a component
       * @param p a parameter
       * @pre REQUIRE_NON_ZERO(c)
       * @return true if the component was added, false otherwise
       */
    protected:
      bool add (Component* c, const Parameter& p) throws();
	
      /**
       * Remove the specified component. It is not an error
       * to remove a component that does not exist!
       * @param c a component
       */
    protected:
      void remove (Component* c) throws();
	
      /** Initialize the grid */
    private:
      void initGrid() throws();

      /** 
       * Compute the column widths 
       * @param width the total available width
       * @throw LayoutException if not enough space is available
       */
    private:
      void computeColumnWidths (Int width) throws (LayoutException);

      /**
       * Compute the row heights 
       * @param height the total available height
       * @throw LayoutException if not enough space is available
       */
    private:
      void computeRowHeights (Int height) throws (LayoutException);

      /** The current width and height */
    private:
      Int _width, _height;

      /** The width of each column */
    private:
      Int *_colWidths;

      /** The height of each row */
    private:
      Int *_rowHeights;

      /** The bounds for each component */
    private:
      GridEntry** _grid;
	
      /** The spacing between components. */
    private:
      Int _componentSpacing;

      /** Use the preferred size */
    private:
      bool _usePreferredSize;

      /** The vector of constraints */
    private:
      Constraints _constraints;

      /** The preferred size */
    private:
      mutable SizeHints _hints;
    };
  }
}

#endif

