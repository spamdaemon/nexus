#ifndef _NEXUS_LAYOUT_UTIL_H
#define _NEXUS_LAYOUT_UTIL_H

#ifndef _NEXUS_SIZE_H
#include <nexus/Size.h>
#endif

#ifndef _NEXUS_BOUNDS_H
#include <nexus/Bounds.h>
#endif

#ifndef _NEXUS_COMPONENT_H
#include <nexus/Component.h>
#endif

namespace nexus {
  namespace layout {
    /** 
     * @name Forward declarations 
     * @{
     */
    class VSize;
    class HSize;
    /*@}*/

    /**
     * A utility class to extra the height
     * from a size, bounds object, or component.
     */
    class VSize {
      /** The complement size */
    public:
      typedef HSize Complement;

      /** The default constructor */
    public:
      inline VSize() throws() {}

      /** 
       * Extract the height from the specified size object.
       * @param sz a size
       * @return sz.height()
       */
    public:
      inline Int operator() (const Size& sz) const throws() { return sz.height(); }

      /** 
       * Extract the height from the specified bounds object.
       * @param b a bounds object
       * @return b.height()
       */
    public:
      inline Int operator() (const Bounds& b) const throws() { return b.height(); }

      /** 
       * Extract the height from the specified component object.
       * @param c a bounds object
       * @return c.height()
       */
    public:
      inline Int operator() (const Component& c) const throws() { return c.height(); }

      /**
       * Create a size object.
       * @param h the height
       * @param w the width
       */
    public:
      inline Size size (Int h, Int w) const throws() { return Size(w,h); }

      /**
       * Create a bounds object.
       * @param y the y offset
       * @param x the x offset
       * @param h the height
       * @param w the width
       */
    public:
      inline Bounds bounds (Int y, Int x, Int h, Int w) const throws() { return Bounds(x,y,w,h); }

      /**
       * Get the hsize object.
       * @return an hsize object
       */
    public:
      Complement complement() const throws();
    };

    /**
     * A utility class to extra the width
     * from a size, bounds object, or component.
     */
    class HSize {
      /** The complement size */
    public:
      typedef VSize Complement;

      /** The default constructor */
    public:
      inline HSize() throws() {}
	
      /** 
       * Extract the width from the specified size object.
       * @param sz a size
       * @return sz.width()
       */
    public:
      inline Int operator() (const Size& sz) const throws() { return sz.width(); }

      /** 
       * Extract the width from the specified bounds object.
       * @param b a bounds object
       * @return b.width()
       */
    public:
      inline Int operator() (const Bounds& b) const throws() { return b.width(); }

      /** 
       * Extract the width from the specified component object.
       * @param c a bounds object
       * @return c.width()
       */
    public:
      inline Int operator() (const Component& c) const throws() { return c.width(); }

      /**
       * Create a size object.
       * @param w the width
       * @param h the height
       */
    public:
      inline Size size (Int w, Int h) const throws() { return Size(w,h); }

      /**
       * Create a bounds object.
       * @param x the x offset
       * @param y the y offset
       * @param w the width
       * @param h the height
       */
    public:
      inline Bounds bounds (Int x, Int y, Int w, Int h) const throws() { return Bounds(x,y,w,h); }
	
      /**
       * Get the vsize object.
       * @return an vsize object
       */
    public:
      inline Complement complement() const throws() { return Complement(); }
    };
    inline VSize::Complement VSize::complement() const throws() { return Complement(); }

  }
}

#endif
