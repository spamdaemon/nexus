#ifndef _NEXUS_LAYOUT_LAYOUT_H
#define _NEXUS_LAYOUT_LAYOUT_H

#ifndef _NEXUS_LAYOUTENGINE_H
#include <nexus/LayoutEngine.h>
#endif

namespace nexus {
  namespace layout {
    /**
     * This class is an intermediate class that enables easy subclassing. 
     * It's purpose is to introduce the Component and LayoutException 
     * classes into the namespace.
     *
     * @date 27 Jun 2003
     */
    class Layout : public LayoutEngine {
      /* No copy operator */
      Layout&operator=(const Layout&);

      /** Default constructor */
    protected:
      inline Layout () throws()
      {}
	
      /** Default copy operator */
    protected:
      inline Layout (const Layout&) throws()
	: ::nexus::LayoutEngine()
	{}
	
      /** Destroy this container and release all its resources */
    public:
      ~Layout () throws()
	{}
    };
  }
}

#endif

