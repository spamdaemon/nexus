#ifndef _NEXUS_LAYOUT_BORDERLAYOUT_H
#define _NEXUS_LAYOUT_BORDERLAYOUT_H

#ifndef _NEXUS_BOUNDS_H
#include <nexus/Bounds.h>
#endif

#ifndef _NEXUS_SIZEHINTS_H
#include <nexus/SizeHints.h>
#endif

#ifndef _NEXUS_LAYOUT_LAYOUT_H
#include <nexus/layout/Layout.h>
#endif

#include <vector>

namespace nexus {
   namespace layout {

      /**
       * This layout is used to layout components in horizontal or vertical
       * way.
       * <p>
       * TODO: spacing cannot be negative at this time.s
       */
      class BorderLayout : public Layout
      {
            BorderLayout&operator=(const BorderLayout&);

            /**
             * The four primary and four secondary direction of the compass
             * There is one additional position, which is the center position.
             */
         public:
            enum Parameter
            {
               NORTHWEST = 0,//< NorthWest
               WEST = 1,//< West
               SOUTHWEST = 2,//< SoutWest
               NORTH = 3, //< North
               CENTER = 4, // the center position
               SOUTH = 5,//< South
               NORTHEAST = 6,
               EAST = 7,//< East
               SOUTHEAST = 8
            //< SouthEast
            };

            /** The super class */
         private:
            typedef Layout super;

            /** The constraints */
         private:
            typedef ::std::vector< Parameter> Constraints;

            /** The default spacing */
         public:
            static const Int DEFAULT_SPACING;

            /**  Create a border layout. */
         public:
            BorderLayout()throws();

            /**
             * A copy constructor. The component constraints
             * are not copied.
             * @param f a border layout
             */
         public:
            BorderLayout(const BorderLayout& f)throws();

            /**
             * Default constructor using a vertical border.
             * @param s the spacing between components or a negative number
             */
         public:
            BorderLayout(Int s)throws();

            /** Destroy this container and release all its resources */
         public:
            ~BorderLayout()throws();

            /**
             * Get the spacing between components.
             * @return the spacing between components
             */
         public:
            inline Int spacing() const throws()
            {  return _componentSpacing;}

            /**
             * Set the spacing for this container.
             * @param sp the new spacing
             */
         public:
            void setSpacing(Int sp)throws();

            /**
             * @name Implementation of the Layout interface
             * @{
             */
         public:
            Bounds bounds(Index i) const throws();
            Component* component(Index i) const throws();
            Int size() const throws();
            void doLayout(const Size& sz)
            throws ( LayoutException);
            SizeHints sizeHints() const throws();

            /*@}*/

            /**
             * Add a component with the specified parameter. The subclass
             * must not increment the reference count of the component.
             *
             * @param c a component
             * @param p a parameter
             * @pre REQUIRE_NON_ZERO(c)
             * @return true if the component was added, false otherwise
             */
         protected:
            bool add(Component* c, const Parameter& p)throws();

            /**
             * Remove the specified component. It is not an error
             * to remove a component that does not exist!
             * @param c a component
             */
         protected:
            void remove(Component* c)throws();

            /** Initialize the border */
         private:
            void initBorder()throws();

            /**
             * Compute the sizes for either columns or rows, for template paramter HSize and VSize respectively.
             * @param sz the total size available
             * @param p1 the cells of the top row or left  column
             * @param p2 the cells fo the center row or column
             * @param p3 the cells of the bottom row or right column
             * @param result where the widths or heights for each row are stored
             * @throw LayoutException if not enough space is available
             */
         private:
            template <class T>
            void computeSizes(const Size& sz, const Parameter* p1, const Parameter* p2, const Parameter* p3,
                  Int* result)
            throws ( LayoutException);

            /**
             * Compute the size for a given set of cells. The
             * template parameter is either VSize or HSize
             * @param parms the parameters for a row or a column
             * @return the preferred size for the specified cells
             */
         private:
            template <class T>
            Int computeSize(const Parameter* parms)throws();

            /**
             * Compute the size hint for the specified set of cells. The
             * template parameter is either VSize or HSize.
             * @param parms the parameters for a row or a column
             * @param m the minimum width for the row
             * @param M the maximum width for the row
             * @param p the preferred width for the row
             */
         private:
            template <class T>
            void computeSizeHint(const Parameter* parms, Int& m, Int& M, Int& p) const throws();

            /** The bounds for each cell */
         private:
            Bounds _bounds[9];

            /** The component which is in each cell (or 0 if none is set) */
         private:
            Component* _components[9];

            /** The widths and height */
         private:
            Int _colWidths[3];

            /** The height */
         private:
            Int _rowHeights[3];

            /** The spacing between components. */
         private:
            Int _componentSpacing;

            /** The vector of constraints */
         private:
            Constraints _constraints;

            /** The preferred size */
         private:
            mutable SizeHints _hints;

            /** The parameters for all rows */
         private:
            static const Parameter ROWS[3][3];

            /** The parameters for all columns */
         private:
            static const Parameter COLUMNS[3][3];
      };
   }
}

#endif

