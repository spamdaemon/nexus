#ifndef _NEXUS_LAYOUT_FLOWLAYOUT_H
#define _NEXUS_LAYOUT_FLOWLAYOUT_H

#ifndef _NEXUS_H
#include <nexus/nexus.h>
#endif

#ifndef _NEXUS_FLOW_H
#include <nexus/Flow.h>
#endif

#ifndef _NEXUS_BOUNDS_H
#include <nexus/Bounds.h>
#endif

#ifndef _NEXUS_SIZEHINTS_H
#include <nexus/SizeHints.h>
#endif

#ifndef _NEXUS_LAYOUT_LAYOUT_H
#include <nexus/layout/Layout.h>
#endif

#include <vector>
#include <map>

namespace nexus {
  namespace layout {
    
    /**
     * This class implements a flow layout strategy.
     *
     * @author Raimund Merkert
     * @date 20 Feb 2003
     */
    class FlowLayout : public Layout {
      /** Disable copying */
    private:
      FlowLayout&operator=(const FlowLayout&);

      /** The parameter */
    public:
      typedef Index Parameter;

      /** The super class */
    private:
      typedef Layout super;
	
      /** A priority queue */
    private:
      typedef ::std::multimap<Int,Index> Queue; 

      /** The constraint */
    private:
      typedef ::std::pair<Component*,Bounds> Constraint;
	
      /** The constraint */
    private:
      typedef ::std::vector<Constraint> Constraints;
	  
      /** The default flow direction */
    public:
      static const Flow DEFAULT_FLOW;

      /** The default spacing */
    public:
      static const Int DEFAULT_SPACING;
	
      /**  Default constructor using DEFAULT_FLOW.  */
    public:
      FlowLayout () throws();

      /**
       * A copy constructor. The component constraints 
       * are not copied.
       * @param f a flow layout
       */
    public:
      FlowLayout (const FlowLayout& f) throws();

      /** 
       * Default constructor using a vertical flow.
       * @param f the flow direction
       */
    public:
      FlowLayout (Flow f) throws();
	  
      /** 
       * Default constructor using a vertical flow.
       * @param f the flow direction
       * @param s the spacing between components or a negative number
       */
    public:
      FlowLayout (Flow f, Int s) throws();
	  
      /** Destroy this container and release all its resources */
    public:
      ~FlowLayout () throws();

      /**
       * Use the preferred size for components and don't let them
       * resize arbitrarily.
       * @param usePreferredSize if true use preferred size
       */
    public:
      void setUsePreferredSize (bool usePreferredSize) throws();

      /**
       * Test if components will be laid out at their preferred size.
       * @return true if components will not exceed their preferred size.
       */
    public:
      inline bool isUsePreferredSize () const throws()
      { return _usePreferredSize; }

      /**
       * Get the flow direction.
       * @return the flow direction
       */
    public:
      inline Flow flow () const throws()
      { return _flow; }

      /**
       * Get the spacing between components.
       * @return the spacing between components
       */
    public:
      inline Int spacing () const throws()
      { return _componentSpacing; }

      /**
       * Set the spacing for this container.
       * @param sp the new spacing
       */
    public:
      void setSpacing (Int sp) throws();
	  
      /**
       * @name Implementation of the Layout interface 
       * @{
       */
    public:
      Bounds bounds (Index i) const throws();	
      Component* component (Index i) const throws();
      Int size () const throws();
      void doLayout(const Size& sz) throws(LayoutException);
      SizeHints sizeHints() const throws();
	  
      /*@}*/

      /**
       * Add a component with the specified parameter. The subclass
       * must not increment the reference count of the component.
       *
       * @param c a component
       * @param p a parameter
       * @pre REQUIRE_NON_ZERO(c)
       * @return true if the component was added, false otherwise
       */
    protected:
      bool add (Component* c, const Parameter& p) throws();
	
      /**
       * Remove the specified component. It is not an error
       * to remove a component that does not exist!
       * @param c a component
       */
    protected:
      void remove (Component* c) throws();
	  
      /**
       * Perform a vertical layout.
       */
    private:
      template <class T> void layout(const Size& sz) throws (LayoutException);

      /**
       * Compute the size hints.
       * @return the size hints
       */
    private:
      template <class T> SizeHints computeSizeHints () const throws();

      /** The layout direction (horizontal or vertical) */
    private:
      Flow _flow;

      /** The spacing between components. */
    private:
      Int _componentSpacing;

      /** Use the preferred size */
    private:
      bool _usePreferredSize;

      /** The constraints */
    private:
      Constraints _constraints;

      /** A priority queue */
    private:
      Queue _queue;

      /** The preferred size */
    private:
      mutable SizeHints _hints;
    };
  }
}

#endif

