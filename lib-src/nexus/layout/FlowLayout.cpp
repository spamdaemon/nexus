#include <nexus/layout/FlowLayout.h>
#include <nexus/Component.h>
#include <nexus/SizeHints.h>
#include <nexus/layout/util.h>
#include <cassert>

namespace nexus {
  namespace layout {

      
    const Int FlowLayout::DEFAULT_SPACING(-1);
    const Flow FlowLayout::DEFAULT_FLOW(Flow::FLOW_TOP_TO_BOTTOM);
      
    FlowLayout::FlowLayout () throws()
    : _flow(DEFAULT_FLOW),
      _componentSpacing(DEFAULT_SPACING),
      _usePreferredSize(false)
    {}
      
    FlowLayout::FlowLayout (Flow f) throws()
    : _flow(f),_componentSpacing(DEFAULT_SPACING),
      _usePreferredSize(false)
    {}
      
    FlowLayout::FlowLayout (Flow f, Int s) throws()
    : _flow(f),_componentSpacing(s<0 ? -1 : s),
      _usePreferredSize(false)
    {}
      
    FlowLayout::FlowLayout (const FlowLayout& f) throws()
    : super(),
      _flow(f.flow()),
      _componentSpacing(f.spacing()), 
      _usePreferredSize(f.isUsePreferredSize())
    {}
      
    FlowLayout::~FlowLayout() throws()
    {}
      

    void FlowLayout::setUsePreferredSize (bool usePreferredSize) throws()
    {
      if (_usePreferredSize!=usePreferredSize) {
	_usePreferredSize=usePreferredSize;
	layoutChanged();
      }
    }

    void FlowLayout::setSpacing (Int sp) throws()
    {
      if (sp<0) {
	sp = -1;
      }
      if (_componentSpacing!=sp) {
	_componentSpacing = sp;
	layoutChanged();
      }
    }

    inline Int FlowLayout::size() const throws()
    { return _constraints.size(); }
	  
    bool FlowLayout::add (Component* c, const Parameter& pos) throws()
    {
      if (pos==size()) {
	_constraints.push_back(Constraint(c,Bounds()));
      }
      else {
	_constraints.insert(_constraints.begin()+pos,Constraint(c,Bounds()));
      }
      return true;
    }
      
    void FlowLayout::remove (Component* c) throws()
    {
      const Int sz = _constraints.size();
      for (Index i=0;i<sz;++i) {
	if (_constraints[i].first==c) {
	  _constraints.erase(_constraints.begin()+i);
	  return;
	}
      }
    }
	  
    Component* FlowLayout::component(Index i) const throws()
    { return _constraints[i].first; }
	  
    Bounds FlowLayout::bounds (Index i) const throws()
    { return _constraints[i].second; }


    void FlowLayout::doLayout(const Size& sz) throws(LayoutException)
    {
      const Int count = _constraints.size();

      // nothing do if there are no children
      if (count==0) {
	return;
      }
      if (flow().isHorizontal()) {
	layout<HSize>(sz);
      }
      else {
	layout<VSize>(sz);
      }
    }
    
    SizeHints FlowLayout::sizeHints() const throws()
    {
      if (_constraints.size()==0) {
	_hints = SizeHints();
      }
      else if (flow().isHorizontal()) {
	_hints = computeSizeHints<HSize>();
	assert(spacing()>=0 || _hints.maximumSize().width()==Size::MAX.width());
      }
      else {
	_hints = computeSizeHints<VSize>();
	assert(spacing()>=0 || _hints.maximumSize().height()==Size::MAX.height());
      }
      return _hints;
    }

      
    template <class T> 
    SizeHints FlowLayout::computeSizeHints () const throws()
    {
      const T length;

      Int lMin,lMax,lPref;
      Int oMin,oMax,oPref;

      lMin  = 0;
      lMax  = 0;
      lPref = 0;
      oMin  = 0;
      oMax  = length.complement()(Size::MAX);
      oPref = length.complement()(Size::MIN);

      for (Constraints::const_iterator i=_constraints.begin();i!=_constraints.end();++i) {
	// what are th size hints for the specified component
	SizeHints hints = i->first->sizeHints();
	  
	lMin += length(hints.minimumSize());
	lMax += length(hints.maximumSize());
	lPref+= length(hints.preferredSize());

	oMin = ::std::max(oMin,length.complement()(hints.minimumSize()));
	oMax = ::std::min(oMax,length.complement()(hints.maximumSize()));
	oPref = ::std::max(oPref,length.complement()(hints.preferredSize()));
      }

      if (spacing() >= 0) {
	Int extraSpace = spacing()*( _constraints.size() - 1);
	lMin  += extraSpace;
	lPref += extraSpace;
	lMax  += extraSpace;
      }
      else {
	lMax = length(Size::MAX);
      }
	
      lMin = ::std::max(lMin,length(Size::MIN));
      lPref = ::std::max(lPref,lMin);
      lMax  = ::std::max(lMax,lPref);
	
      oMin = ::std::max(oMin,length.complement()(Size::MIN));
      oPref = ::std::max(oPref,oMin);
      oMax  = ::std::max(oMax,oPref);

      return SizeHints(length.size(lMin,oMin).min(Size::MAX),
		       length.size(lMax,oMax).min(Size::MAX),
		       length.size(lPref,oPref).min(Size::MAX));
    }
      
    template <class T>
    void FlowLayout::layout(const Size& sz) throws (LayoutException)
    {
      const T length;
      const typename T::Complement complement(length.complement());

      // remove all children from the priority queue
      const Int count = _constraints.size();
      Int extraSpace = length(sz);

      if (spacing()>0) {
	extraSpace -= spacing()*(count-1); 
      }

	
      // see if there is at least 1 pixel per component
      if (extraSpace < count) {
	throw LayoutException("Not enough space");
      }
	
      // put all children into the priority queue and set their size to their
      // preferred size
      for (Index i=0;i<count;++i) {
	SizeHints hints = _constraints[i].first->sizeHints();
	_constraints[i].second = length.bounds(0,0,length(hints.minimumSize()),0);
	extraSpace -= length(_constraints[i].second);
      }
	
      // if we have extra space available, then allocate it 
      if (extraSpace>0) {
	for (Index i=0;i<count;++i) {
	  SizeHints hints = _constraints[i].first->sizeHints();
	  if (isUsePreferredSize()) {
	    _queue.insert(::std::make_pair(i,length(hints.preferredSize())-length(hints.minimumSize())));
	  }
	  else {
	    _queue.insert(::std::make_pair(i,length(hints.maximumSize())-length(hints.minimumSize())));
	  }
	}
	  
	do {
	  Int allocSpace = extraSpace/_queue.size();
	  Int allocRem   = extraSpace%_queue.size();
	  Queue::value_type e(*_queue.begin());
	  _queue.erase(_queue.begin());
	  const Index i = e.first;
	  Int need = e.second;
	  const Int h  = length(_constraints[i].second);
	  // we can increase the length more
	  if (need > 0) {
	    if (need > allocSpace) {
	      need = allocSpace;
	      // then we can add one more
	      if (allocRem > 0) {
		++need;
	      }
	    }
	    _constraints[i].second = length.bounds(0,0,need+h,0);
	    extraSpace -= need;
	  }
	} while (extraSpace > 0 && !_queue.empty());
      }
      else if (extraSpace < 0) {
	for (Index i=0;i<count;++i) {
	  SizeHints hints = _constraints[i].first->sizeHints();
	  _queue.insert(::std::make_pair(i,length(hints.minimumSize())-length(hints.preferredSize())));
	}

	do {
	  Int allocSpace = extraSpace/_queue.size();
	  Int allocRem   = extraSpace%_queue.size();
	  Queue::value_type e(*_queue.begin());
	  _queue.erase(_queue.begin());
	  const Index i = e.first;
	  Int need = e.second;
	  const Int h  = length(_constraints[i].second);
	  // we can increase the length more
	  if (need < 0) {
	    if (need < allocSpace) {
	      need = allocSpace;
	      // then we can add one more
	      if (allocRem > 0) {
		++need;
	      }
	    }
	    _constraints[i].second = length.bounds(0,0,need+h,0);
	    assert(length(_constraints[i].second)>0);
	    extraSpace += need;
	  }
	} while (extraSpace<0 && !_queue.empty());
      }
	
      // now, compute the offsets
      Int offset = 0;

      if (count==1) {
	Int l = length(_constraints[0].second);
	_constraints[0].second = length.bounds(offset,0,l,complement(sz));
	return;
      }
	
      Int spc,spcRem;
      if (isUsePreferredSize()) {
	spc = extraSpace / count;
	spcRem = extraSpace % count;
      }
      else if (spacing()<0 || extraSpace<0) {
	spc    = extraSpace/(count-1);
	spcRem = extraSpace%(count-1);
      }
      else {
	spc = spacing();
	spcRem = 0;
      }

      for (Index i=0;i<count;++i) {
	Int l = length(_constraints[i].second);
	if (isUsePreferredSize()) {
	  const Bounds b = length.bounds(offset,0,spc+spcRem+l,complement(sz));
	  _constraints[i].second = b.centerBounds(length.size(l,complement(sz)));
	}
	else {
	  _constraints[i].second = length.bounds(offset,0,l,complement(sz));
	}
	offset += l +spc+spcRem;
	if (spcRem>0) {
	  --spcRem;
	}
	else if (spcRem<0) {
	  ++spcRem;
	}
      }
    }
  }
}
