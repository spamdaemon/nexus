#include <nexus/layout/FixedLayout.h>
#include <nexus/Component.h>
#include <nexus/SizeHints.h>

namespace nexus {
  namespace layout {

    FixedLayout::~FixedLayout() throws()
    {}
	
    inline Int FixedLayout::size() const throws()
    { return _constraints.size(); }
	  
    bool FixedLayout::add (Component* c, const Parameter& b) throws()
    {
      // must have a go
      if (b.width()> 0 && b.height()>0) {
	_constraints.push_back(Constraint(c,b));
	return true;
      }
      return false;
    }
	  
    void FixedLayout::remove (Component* c) throws()
    {
      const Int sz = _constraints.size();
      for (Index i=0;i<sz;++i) {
	if (_constraints[i].first==c) {
	  _constraints.erase(_constraints.begin()+i);
	  return;
	}
      }
    }
	  
    Component* FixedLayout::component(Index i) const throws()
    { return _constraints[i].first; }
	  
    Bounds FixedLayout::bounds (Index i) const throws()
    { return _constraints[i].second; }

    void FixedLayout::doLayout(const Size&) throws()
    {
      // nothing to do for the layout
    }
	
    SizeHints FixedLayout::sizeHints() const throws()
    {
      Size sz(Size::MIN);
      if (_constraints.size()>0) {
	Bounds b;
	for (Constraints::const_iterator i=_constraints.begin();i!=_constraints.end();++i) {
	  b = b.mergeBounds(i->second);
	}
	sz = b.size();
      }
      return SizeHints(sz);
    }
	
  }
}
