#include <nexus/layout/GridLayout.h>
#include <nexus/Component.h>
#include <nexus/SizeHints.h>
#include <nexus/layout/util.h>

namespace nexus {
   namespace layout {

      const Int GridLayout::DEFAULT_SPACING(0);

      GridLayout::GridLayout(Int w, Int h)
      throws()
      : _width(w),_height(h),_grid(0),
      _componentSpacing(DEFAULT_SPACING),
      _usePreferredSize(false)
      {
         assert(w>0);
         assert(h>0);
         initGrid();}

      GridLayout::GridLayout(const GridLayout& f)
      throws()
      : super(),
      _width(f._width),_height(f._height),_grid(0),
      _componentSpacing(f.spacing()),
      _usePreferredSize(f.isUsePreferredSize())
      {  initGrid();}

      GridLayout::GridLayout(Int s)
      throws()
      :_componentSpacing(0)
      {
         setSpacing(s);
      }

      GridLayout::~GridLayout()
      throws()
      {
         for (Index i=0;i<_width;++i) {
            delete [] _grid[i];
         }
         delete [] _grid;
         delete [] _colWidths;
         delete [] _rowHeights;
      }

      void GridLayout::initGrid()
      throws()
      {
         _grid = new GridEntry*[_width];
         for (Index i=0;i<_width;++i) {
            _grid[i] = new GridEntry[_height];
            for (Index j=0;j<_height;++j) {
               _grid[i][j].second=false;
            }
         }
         _colWidths = new Int[_width];
         _rowHeights= new Int[_height];
      }

      void GridLayout::setUsePreferredSize(bool usePreferredSize)
      throws()
      {
         if (_usePreferredSize!=usePreferredSize) {
            _usePreferredSize=usePreferredSize;
            layoutChanged();
         }
      }

      void GridLayout::setSpacing(Int sp)
      throws()
      {
         if (sp<0) {
            sp = 0;
         }
         if (_componentSpacing!=sp) {
            _componentSpacing = sp;
            layoutChanged();
         }
      }

      inline Int GridLayout::size() const
      throws()
      {  return _constraints.size();}

      bool GridLayout::add(Component* c, const Parameter& pos)
      throws()
      {
         const Index x = pos.first;
         const Index y = pos.second;

         if (_width <= x || _height <= y) {
            return false;
         }
         else if (_grid[x][y].second) {
            // there is already something in the array
            return false;
         }
         assert(!_grid[x][y].second);
         _grid[x][y].second = true;
         _constraints.push_back(Constraint(c,pos));
         return true;
      }

      void GridLayout::remove(Component* c)
      throws()
      {
         const Int sz = _constraints.size();
         for (Index i=0;i<sz;++i) {
            if (_constraints[i].first==c) {
               const Index x = _constraints[i].second.first;
               const Index y = _constraints[i].second.second;
               _constraints.erase(_constraints.begin()+i);
               _grid[x][y].second=false;

               return;
            }
         }
      }

      Component* GridLayout::component(Index i) const
      throws()
      {  return _constraints[i].first;}

      Bounds GridLayout::bounds(Index i) const
      throws()
      {
         const Index x = _constraints[i].second.first;
         const Index y = _constraints[i].second.second;
         return _grid[x][y].first;
      }

      void GridLayout::computeColumnWidths(Int width)
      throws (LayoutException)
      {
         const Int w = width-spacing()*(_width-1);
         if (w<_width) {
            throw LayoutException("Not wide enough");
         }
         const Int hspace = w / _width;
         Int hrem = w % _width;

for      (Index i=0;i<_width;++i) {
         _colWidths[i] = hspace+hrem;
         if (hrem>0) {
            --hrem;
         }
      }
   }

   void GridLayout::computeRowHeights (Int height) throws (LayoutException)
   {
      const Int h = height-spacing()*(_height-1);
      if (h<_height) {
         throw LayoutException("Not tall enough");
      }
      const Int vspace = h / _height;
      Int vrem = h % _height;

      for (Index i=0;i<_height;++i) {
         _rowHeights[i] = vspace+vrem;
         if (vrem>0) {
            --vrem;
         }
      }
   }

void GridLayout::doLayout(const Size& sz) throws(LayoutException)
{
   const Int count = _constraints.size();

   // nothing do if there are no children
   if (count == 0) {
      return;
   }

   computeColumnWidths(sz.width());
   computeRowHeights(sz.height());

   Int xOff = 0;
   for (Index x = 0; x < _width; ++x) {
      Int yOff = 0;
      for (Index y = 0; y < _height; ++y) {
         _grid[x][y].first = Bounds(xOff, yOff, _colWidths[x], _rowHeights[y]);
         yOff += _rowHeights[y] + spacing();
      }
      xOff += _colWidths[x] + spacing();
   }
}

SizeHints GridLayout::sizeHints() const
throws()
{
   const Int count = _constraints.size();
   if (count==0) {
      _hints=SizeHints();
   }
   else {
      _hints = _constraints[0].first->sizeHints();

      if (count>0) {

         Int minW = _hints.minimumSize().width();
         Int minH = _hints.minimumSize().height();
         Int maxW = _hints.maximumSize().width();
         Int maxH = _hints.maximumSize().height();
         Int prfW = _hints.preferredSize().width();
         Int prfH = _hints.preferredSize().height();

         for (Index i=1;i<count;++i) {
            SizeHints h = _constraints[i].first->sizeHints();
            minW = ::std::max(h.minimumSize().width(),minW);
            minH = ::std::max(h.minimumSize().height(),minH);
            maxW = ::std::min(h.maximumSize().width(),maxW);
            maxH = ::std::min(h.maximumSize().height(),maxH);
            prfW = ::std::max(h.preferredSize().width(),prfW);
            prfH = ::std::max(h.preferredSize().height(),prfH);
         }

         minW = minW*_width + spacing()*(_width-1);
         maxW = maxW*_width + spacing()*(_width-1);
         prfW = prfW*_width + spacing()*(_width-1);
         minH = minH*_height + spacing()*(_height-1);
         maxH = maxH*_height + spacing()*(_height-1);
         prfH = prfH*_height + spacing()*(_height-1);

         maxW = ::std::min(Size::MAX.width(),::std::max(minW,maxW));
         maxW = ::std::min(Size::MAX.width(),::std::max(maxW,prfW));
         maxH = ::std::min(Size::MAX.height(),::std::max(minH,maxH));
         maxH = ::std::min(Size::MAX.height(),::std::max(maxH,prfH));

         _hints= SizeHints(Size(minW,minH),Size(maxW,maxH),Size(prfW,prfH));
      }
   }

   return _hints;
}

}
}
