#include <nexus/layout/BorderLayout.h>
#include <nexus/Component.h>
#include <nexus/SizeHints.h>
#include <nexus/layout/util.h>

namespace nexus {
   namespace layout {

      const Int BorderLayout::DEFAULT_SPACING(0);
      const BorderLayout::Parameter BorderLayout::COLUMNS[3][3] = { { NORTHWEST, WEST, SOUTHWEST }, {
            NORTH, CENTER, SOUTH }, { NORTHEAST, EAST, SOUTHEAST } };
      const BorderLayout::Parameter BorderLayout::ROWS[3][3] = {
            { NORTHWEST, NORTH, NORTHEAST }, { WEST, CENTER, EAST }, { SOUTHWEST, SOUTH, SOUTHEAST } };

      BorderLayout::BorderLayout()
      throws()
      : _componentSpacing(DEFAULT_SPACING)
      {  initBorder();}

      BorderLayout::BorderLayout(const BorderLayout& f)
      throws()
      : super(),
      _componentSpacing(f.spacing())
      {  initBorder();}

      BorderLayout::BorderLayout(Int s)
      throws()
      : _componentSpacing(0)
      {  setSpacing(s);}

      BorderLayout::~BorderLayout()
      throws()
      {}

      void BorderLayout::initBorder()
      throws()
      {
         for (Index i=0;i<9;++i) {
            _components[i] = 0;
         }
      }

      void BorderLayout::setSpacing(Int sp)
      throws()
      {
         if (sp<0) {
            // must be non-negative
            sp = 0;
         }
         if (_componentSpacing!=sp) {
            _componentSpacing = sp;
            layoutChanged();
         }
      }

      inline Int BorderLayout::size() const
      throws()
      {  return _constraints.size();}

      bool BorderLayout::add(Component* c, const Parameter& pos)
      throws()
      {
         if (_components[pos]!=0) {
            return false;
         }
         _components[pos] = c;
         _constraints.push_back(pos);
         return true;
      }

      void BorderLayout::remove(Component* c)
      throws()
      {
         const Int sz = _constraints.size();
         for (Index i=0;i<sz;++i) {
            Parameter loc = _constraints[i];
            if (_components[loc]==c) {
               _components[loc]=0;
               _constraints.erase(_constraints.begin()+i);
               return;
            }
         }
      }

      Component* BorderLayout::component(Index i) const
      throws()
      {
         Parameter loc = _constraints[i];
         assert(_components[loc]!=0);
         return _components[loc];
      }

      Bounds BorderLayout::bounds(Index i) const
      throws()
      {
         Parameter loc = _constraints[i];
         assert(_components[loc]!=0);
         return _bounds[loc];
      }

      template <class T>
      Int BorderLayout::computeSize(const Parameter* parms)
      throws()
      {
         const T length;
         Int p = 0;
         for (Index i=0;i<3;++i) {
            if (_components[parms[i]]!=0) {
               SizeHints h = _components[parms[i]]->sizeHints();
               p = ::std::max(length(h.preferredSize()),p);
            }
         }
         return p;
      }

      template <class T>
      void BorderLayout::computeSizes(const Size& sz, const Parameter* p1, const Parameter* p2, const Parameter* p3,
            Int* result)
      throws (LayoutException)
      {
         const T length;
         Int tmp = 0;
result      [0] = computeSize<T>(p1);
      result[1] = computeSize<T>(p2);
      result[2] = computeSize<T>(p3);

      for (Index i=0;i<3;++i) {
         if (result[i]!=0) {
            ++tmp;
         }
      }

      if (length(sz)<tmp) {
         throw LayoutException("Not enough space for BorderLayout");
      }

      result[1] = length(sz) - result[0] - result[2];
      if (result[0]>0) {
         result[1] -= spacing();
      }
      if (result[2]>0) {
         result[1] -= spacing();
      }

      while (result[1] <= 0) {
         Int diff = +1;
         if (result[1]< -1) {
            diff = -result[1]/2;
         }
         Int total = 0;
         if (result[0]>diff) {
            result[0] -= diff;
            result[1] += diff;
            total = diff;
         }
         if (result[2]>diff) {
            result[2] -= diff;
            result[1] += diff;
            total += diff;
         }
         if (total==0) {
            throw LayoutException("Not enough space");
         }
      }
   }

   template <class T>
   void BorderLayout::computeSizeHint (const Parameter* parms, Int& m, Int& M, Int& p) const throws()
   {
      const T length;

      m = p = 0;
      M = length(Size::MAX);
      for (Index i=0;i<3;++i) {
         if (_components[parms[i]]!=0) {
            SizeHints h = _components[parms[i]]->sizeHints();
            m = ::std::max(length(h.minimumSize()),m);
            M = ::std::min(length(h.maximumSize()),M);
            p = ::std::max(length(h.preferredSize()),p);
         }
      }

      if (_components[parms[1]]!=0) {
         if (_components[parms[0]]!=0) {
            m += spacing();
            M += spacing();
            p += spacing();
         }
         if (_components[parms[2]]!=0) {
            m += spacing();
            M += spacing();
            p += spacing();
         }
      }
   }

   void BorderLayout::doLayout(const Size& sz) throws (LayoutException)
   {
      const Int count = _constraints.size();

      // nothing do if there are no children
      if (count==0) {
         return;
      }

      computeSizes<HSize>(sz,COLUMNS[0],COLUMNS[1],COLUMNS[2],_colWidths);
      computeSizes<VSize>(sz,ROWS[0],ROWS[1],ROWS[2],_rowHeights);

      Int xOff=0;
      for (Index x=0;x<3;++x) {
         Int yOff = 0;
         for (Index y=0;y<3;++y) {
            _bounds[x*3+y]=Bounds(xOff,yOff,_colWidths[x],_rowHeights[y]);
            if (_rowHeights[y]>0) {
               yOff += spacing();
            }
            yOff += _rowHeights[y];
         }
         if (_colWidths[x]>0) {
            xOff += spacing();
         }
         xOff += _colWidths[x];
      }
   }

SizeHints BorderLayout::sizeHints() const
throws()
{
   const Int count = _constraints.size();
   if (count==0) {
      _hints=SizeHints();
   }
   else {
      Int minW,maxW,prfW;
      Int m,M,p;
      computeSizeHint<HSize>(COLUMNS[0],minW,maxW,prfW);
      computeSizeHint<HSize>(COLUMNS[1],m,M,p);
      minW += m;
      maxW += M;
      prfW += p;
      computeSizeHint<HSize>(COLUMNS[2],m,M,p);
      minW += m;
      maxW += M;
      prfW += p;

      prfW = ::std::max(minW,prfW);
      maxW = ::std::max(maxW,prfW);
      maxW = ::std::min(Size::MAX.width(),maxW);

      Int minH,maxH,prfH;
      computeSizeHint<VSize>(ROWS[0],minH,maxH,prfH);
      computeSizeHint<VSize>(ROWS[1],m,M,p);
      minH += m;
      maxH += M;
      prfH += p;
      computeSizeHint<VSize>(ROWS[2],m,M,p);
      minH += m;
      maxH += M;
      prfH += p;
      prfH = ::std::max(minH,prfH);
      maxH = ::std::max(maxH,prfH);
      maxH = ::std::min(Size::MAX.height(),maxH);

      _hints= SizeHints(Size(minW,minH),Size(maxW,maxH),Size(prfW,prfH));
   }
   return _hints;
}
}
}
