#ifndef _NEXUS_LAYOUT_FIXEDLAYOUT_H
#define _NEXUS_LAYOUT_FIXEDLAYOUT_H

#ifndef _NEXUS_BOUNDS_H
#include <nexus/Bounds.h>
#endif

#ifndef _NEXUS_LAYOUT_LAYOUT_H
#include <nexus/layout/Layout.h>
#endif

#include <vector>

namespace nexus {
  namespace layout {

    /**
     * This class implements a fixed layout strategy.
     *
     */
    class FixedLayout : public Layout {
      /** Disable copying */
    private:
      FixedLayout&operator=(const FixedLayout&);
	
      /** The parameter */
    public:
      typedef Bounds Parameter;

      /** The super class */
    private:
      typedef Layout super;

      /** The constraint */
    private:
      typedef ::std::pair<Component*,Bounds> Constraint;
	  
      /** The constraints */
    private:
      typedef ::std::vector<Constraint> Constraints;

      /**  Default constructor.  */
    public:
      inline FixedLayout () throws()
      {}
	
      /**
       * The copy constructor.
       * @param l a layout
       */
    public:
      inline FixedLayout (const FixedLayout& l) throws()
	: super(l)
      {}

      /** Destroy this container and release all its resources */
    public:
      ~FixedLayout () throws();
	  
      /**
       * @name Implementation of the Layout interface 
       * @{
       */
    public:
      Bounds bounds (Index i) const throws();	
      Component* component (Index i) const throws();
      Int size () const throws();
      void doLayout(const Size& sz) throws();
      SizeHints sizeHints() const throws();
	  
      /*@}*/

      /**
       * Add a component with the specified parameter. The subclass
       * must not increment the reference count of the component.
       *
       * @param c a component
       * @param p a parameter
       * @pre REQUIRE_NON_ZERO(c)
       * @return true if the component was added, false otherwise
       */
    protected:
      bool add (Component* c, const Parameter& p) throws();
	
      /**
       * Remove the specified component. It is not an error
       * to remove a component that does not exist!
       * @param c a component
       */
    protected:
      void remove (Component* c) throws();

      /** The constraint parameters */
    private:
      Constraints _constraints;
    };
  }
}

#endif

