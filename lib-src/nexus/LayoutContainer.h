#ifndef _NEXUS_LAYOUTCONTAINER_H
#define _NEXUS_LAYOUTCONTAINER_H

#ifndef _NEXUS_CONTAINER_H
#include <nexus/Container.h>
#endif

namespace nexus {
  /**
   * A container templatized in terms of its layout strategy.
   *
   * @todo adjust sizehints for other border, insets, etc.
   */
  template <class LAYOUT> class LayoutContainer : public Container {
    /** No copying allowed */
    LayoutContainer&operator=(const LayoutContainer&);
    LayoutContainer(const LayoutContainer&);

    /** The layout parameter */
  public:
    typedef typename LAYOUT::Parameter LayoutParameter;

    /** The layout class */
  public:
    typedef LAYOUT Layout;

    /** The layout class */
  private:
    class LayoutImpl : public Layout {
      /** The default constructor */
    public:
      inline LayoutImpl () throws() {}

      /** A copying constructor. */
    public:
      inline LayoutImpl (const Layout& l) throws()
	: Layout(l) {}
	  
      /** Destroy this layout */
    public:
      ~LayoutImpl() throws() {}
	  
      /**
       * Add a component.
       * @param c a component
       * @param p the paramter
       */
    public:
      inline bool add (Component* c, const LayoutParameter& p) throws()
      { return LAYOUT::add(c,p); }
	  
      /**
       * Remove the specified component.
       * @param c a component
       */
    public:
      inline void remove (Component* c) throws()
      { LAYOUT::remove(c); }
    };
	
    /**
     * The default constructor. This constructor
     * requires that Layout has a default constructor
     */
  public:
    inline LayoutContainer () throws (::std::runtime_error)
      : Container(makeAutoPtr(new LayoutImpl()))
      {}

    /** 
     * Create a container using the specified layout strategy.
     * @param l a layout strategy
     * @throw ConfigurationException if the desktop has not been configured
     */
  public:
    inline LayoutContainer (const Layout& l) throws (::std::runtime_error)
      : Container(makeAutoPtr(new LayoutImpl(l)))
      { }

    /** Destroy this container and release all its resources */
  public:
    ~LayoutContainer () throws()
      {}
	
    /**
     * Get the layout implementation.
     * @return the layout implementation
     */
  private:
    inline LayoutImpl& layoutImpl() throws()
    { return static_cast<LayoutImpl&>(*layoutEngine()); }

    /**
     * Get the layout. 
     * @return a reference to the layout
     */
  public:
    inline Layout& layout () throws()
    { return static_cast<Layout&>(*layoutEngine()); }
	
    /**
     * Add a component this container.
     * @param c the component to add
     * @param p the layout parameter that is used to determine the
     * position for c in this container
     * @return true if the component was added, false otherwise
     */
  public:
    inline bool addComponent (const ::timber::Reference<Component>& c, const LayoutParameter& p) throws()
    { 
      Index refCount = c->count();
      if (layoutImpl().add(&*c,p)) {
	assert(refCount==c->count());
	Container::addChild(c); 
	return true;
      }
      // could not add the component
      return false;
    }
	
    /**
     * Remove the child at the specified position.
     * @param pos a child index
     * @return the child at the specified position
     */
  protected:
    ::timber::Reference<Component> removeChild(Index pos) throws()
      {
	::timber::Reference<Component> c = Container::removeChild(pos);
	Index refCount = c->count();
	layoutImpl().remove(&*c);
	assert(refCount==c->count());
	return c;
      }

    /**
     * Create a new autoptr.
     * @param l a layout pointer
     */
  private:
    static inline ::std::unique_ptr<LayoutEngine> makeAutoPtr(Layout* l) throws()
    { return ::std::unique_ptr<LayoutEngine>(l); }
  };
}
#endif

