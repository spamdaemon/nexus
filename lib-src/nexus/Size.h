#ifndef _NEXUS_SIZE_H
#define _NEXUS_SIZE_H

#ifndef _NEXUS_H
#include <nexus/nexus.h>
#endif

#ifndef _NEXUS_RECTANGLE_H
#include <nexus/Rectangle.h>
#endif

#ifndef _CANOPY_HASH_H
#include <canopy/hash.h>
#endif

#include <iosfwd>

namespace nexus {
  class Insets;

  /**
   * This class is used to represent the horizontal and
   * vertical size of components.
   *
   */
  class Size {
    /** The maximum size value */
  public:
    static const Size MAX;
    
    /** The minimum size value */
  public:
    static const Size MIN;
    
    /** Default constructor. Creates size 0,0 */
  public:
    inline Size () throws() 
      : _width(0), _height(0) {}
    
    /**
     * Create the size from a rectangle.
     * @param r a graphic rectangle.
     */
  public:
    Size (const Rectangle& r) throws();
    
    /**
     * Create a new size object.
     * @param w the width
     * @param h the height
     */
  public:
    inline Size (Int w, Int h) throws()
      : _width(w), _height(h) {}
	
    /**
     * Create a new size object that is big enough for the specified insets.
     * @param i insets
     */
  public:
    Size (const Insets& i) throws();

    /**
     * Get the width 
     * @return the width
     */
  public:
    inline Int width() const throws() { return _width; }

    /**
     * Get the height.
     * @return the height
     */
  public:
    inline Int height () const throws() { return _height; }

    /**
     * Adjust this size by the specified values. Negative
     * values are avoided.
     * @param dw a delta width
     * @param dh a delta height
     * @return Size(width()+dw,height()+dh)
     */
  public:
    Size adjust (Int dw, Int dh) const throws();

    /**
     * Compute a hashvalue.
     * @return a hashvalue
     */
  public:
    inline size_t hashValue () const throws()
    { return ::canopy::hashValue(width()) ^ ::canopy::hashValue(height()); }
      
    /**
     * Compare two sizes.
     * @param d a size
     * @return true if this and d are equal
     */
  public:
    inline bool operator==(const Size& d) const throws()
    { return width()==d.width() && height()==d.height(); }

    /**
     * Compare two sizes.
     * @param d a size
     * @return true if this and d are equal
     */
  public:
    inline bool equals(const Size& d) const throws()
    { return *this == d; }

    /**
     * Compare two sizes.
     * @param d a size
     * @return true if this and d are different size
     */
  public:
    inline bool operator!=(const Size& d) const throws()
    { return width()!=d.width() || height()!=d.height(); }

    /** 
     * The min operator 
     * @param sz a size
     * @return Size(std::min(width(),sz.width()),std::min(height(),sz.height()))
     */
  public:
    Size min (const Size& sz) const throws();
    
    /** 
     * The max operator 
     * @param sz a size
     * @return Size(std::max(width(),sz.width()),std::max(height(),sz.height()))
     */
  public:
    Size max (const Size& sz) const throws();

    /** The width */
  private:
    Int	_width;

    /** The height */
  private:
    Int	_height;
  };
}

::std::ostream& operator<< (::std::ostream& out, const ::nexus::Size& b) throws();

#endif
