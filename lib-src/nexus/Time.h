#ifndef _NEXUS_TIME_H
#define _NEXUS_TIME_H

#ifndef _CANOPY_TIME_TIME_H
#include <canopy/time/Time.h>
#endif

namespace nexus {
  
  using ::canopy::time::Time;
}

#endif
