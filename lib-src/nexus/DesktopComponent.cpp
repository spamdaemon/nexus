#include <nexus/DesktopComponent.h>

namespace nexus {
  DesktopComponent::DesktopComponent () throws (::std::exception)
    : _showing(false)
  { 
    Component::setVisible(false);
    DesktopComponent::setForegroundColor(Color());
    DesktopComponent::setBackgroundColor(Color());
    DesktopComponent::setFont(::timber::Pointer< ::indigo::Font>());
  }
      
  DesktopComponent::~DesktopComponent() throws()
  {}
      
  void DesktopComponent::initializePeer() throws()
  {
    Desktop::addComponent(this);
    Component::initializePeer();
  }

  void DesktopComponent::destroyPeer () throws()
  {
    Component::destroyPeer();
    Desktop::removeComponent(this);
    setShowing(false);
    setVisible(false);
  }
      
  void DesktopComponent::setShowing(bool showing) throws()
  {
    _showing = showing;
  }
      
  void DesktopComponent::setFont (const ::timber::Pointer< ::indigo::Font>& f) throws()
  {
    if (f==nullptr) {
      Component::setFont(Desktop::defaultFont());
    }
    else {
      Component::setFont(f);
    }
  }

  void DesktopComponent::setForegroundColor (const Color& fg) throws()
  {
    if (fg==0) {
      Component::setForegroundColor(Desktop::defaultForegroundColor());
    }
    else {
      Component::setForegroundColor(fg);
    }
  }

  void DesktopComponent::setBackgroundColor (const Color& bg) throws()
  {
    if (bg==0) {
      Component::setBackgroundColor(Desktop::defaultBackgroundColor());
    }
    else {
      Component::setBackgroundColor(bg);
    }
  }
}
