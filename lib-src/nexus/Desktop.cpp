#include <nexus/Desktop.h>
#include <nexus/peer/Desktop.h>
#include <nexus/peer/Menu.h>
#include <nexus/peer/MenuBar.h>
#include <nexus/DesktopModule.h>
#include <nexus/DesktopComponent.h>
#include <nexus/KeySymbol.h>
#include <nexus/event/KeyEventInterceptor.h>

#include <timber/config/XMLConfiguration.h>
#include <timber/Application.h>
#include <timber/logging.h>

#include <canopy/mt/Condition.h>
#include <canopy/mt/MutexGuard.h>
#include <canopy/User.h>
#include <canopy/fs/FileSystem.h>

#include <cstdlib>
#include <iostream>

using namespace ::timber;
using namespace ::timber::logging;

#define MAKE_STRING2(X) "" #X 
#define MAKE_STRING(X) MAKE_STRING2(X)

namespace nexus {

   namespace {
      static Log log()
      {
         return Log("nexus.Desktop");
      }
#ifndef NEXUS_DESKTOP_MODULE_PATH
#error "Desktop module path has not been defined: NEXUS_DESKTOP_MODULE_PATH";
#endif

      static const char DEFAULT_MODULE_PATH[] = MAKE_STRING(NEXUS_DESKTOP_MODULE_PATH);

      struct KeyInterceptor : public ::nexus::peer::Desktop::KeyInterceptor
      {

            KeyInterceptor()throws()
            {
            }
            ~KeyInterceptor()throws()
            {
            }

            bool keyPressed(const ::timber::Reference< ::nexus::peer::KeySymbol>& ks, const Time& when,
                  const InputModifiers& m)throws()
            {
               ::std::vector< Reference< ::nexus::event::KeyEventInterceptor> > tmp(_interceptors);
               for (size_t i = 0; i < tmp.size(); ++i) {
                  if (tmp[i]->interceptKeyEvent(::nexus::event::KeyEvent::KEY_PRESSED, ks, when, m)) {
                     return true;
                  }
               }
               return false;
            }

            bool keyTyped(const ::timber::Reference< ::nexus::peer::KeySymbol>& ks, const Time& when,
                  const InputModifiers& m)throws()
            {
               ::std::vector< Reference< ::nexus::event::KeyEventInterceptor> > tmp(_interceptors);
               for (size_t i = 0; i < tmp.size(); ++i) {
                  if (tmp[i]->interceptKeyEvent(::nexus::event::KeyEvent::KEY_TYPED, ks, when, m)) {
                     return true;
                  }
               }
               return false;
            }

            bool keyReleased(const ::timber::Reference< ::nexus::peer::KeySymbol>& ks, const Time& when,
                  const InputModifiers& m)throws()
            {
               ::std::vector< Reference< ::nexus::event::KeyEventInterceptor> > tmp(_interceptors);
               for (size_t i = 0; i < tmp.size(); ++i) {
                  if (tmp[i]->interceptKeyEvent(::nexus::event::KeyEvent::KEY_RELEASED, ks, when, m)) {
                     return true;
                  }
               }
               return false;
            }

            /** The interceptors */
         public:
            ::std::vector< Reference< ::nexus::event::KeyEventInterceptor> > _interceptors;
      };

      struct FunctionRunner : public Runnable
      {
            FunctionRunner(const ::std::function< void()>& f)
                  : _function(f)
            {
            }

            ~FunctionRunner()throws()
            {
            }

            void run()
            {
               _function();
            }

         private:
            const ::std::function< void()>& _function;
      };

      struct SynchronizedRunnable : public Runnable
      {
            SynchronizedRunnable(const SharedRef< Runnable>& x)
                  : _runnable(x)
            {
            }

            ~SynchronizedRunnable()throws()
            {
            }

         public:
            void run()
            {
               ::canopy::mt::MutexGuard< ::canopy::mt::Condition> G(_condition);
               _condition.notifyAll();
               _runnable->run();
            }
         public:
            ::canopy::mt::Condition _condition;
         private:
            SharedRef< Runnable> _runnable;
      };

      // the desktop peer, also reference counted
      static KeyInterceptor* global_key_interceptor(0);
      static Pointer< Desktop::DesktopPeer>* global_peer(0);
      // true if the desktop has been initialized
      static volatile bool global_desktopInitialized(false);
      static volatile bool global_desktopShutdown(false);
      static ::std::vector< DesktopComponent*>* global_components(0);
      static Color* global_defaultFGColor(0);
      static Color* global_defaultBGColor(0);

      // a pointer to the default font; this point is raw,
      // but is reference counted
      static Pointer< ::indigo::Font>* global_defaultFont(0);
   }

   static void shutdownDesktopAtExit()
   {
      Desktop::shutdown();

      if (!Desktop::isEventThread()) {
         if (global_peer != 0) {
            (*global_peer)->waitShutdown();
         }
      }
      if (!global_desktopShutdown) {
         global_desktopShutdown = true;
         log().info("Cleaning up static objects");
         (*global_peer)->setKeyInterceptor(0);
         delete global_key_interceptor;
         delete global_components;
         delete global_defaultFGColor;
         delete global_defaultBGColor;
         delete global_defaultFont;
         delete global_peer;
         global_peer = 0;
         global_defaultFont = 0;
         global_defaultBGColor = 0;
         global_defaultFGColor = 0;
         global_components = 0;
         global_key_interceptor = 0;
      }

      log().info("Desktop has been destroyed");
   }
   const Desktop::Period NO_PERIOD = Desktop::Period::zero();
   const Desktop::Delay NO_DELAY = Desktop::Delay::zero();

   const char* Desktop::DESKTOP_PROVIDER = "nexus.desktop.provider";

   struct Desktop::Instantiator : public Runnable
   {
         Instantiator(const Reference< DesktopPeer>& p)
               : _peer(p), _haveException(false)
         {
         }
         ~Instantiator()throws()
         {
         }
         void run()throws()
         {
            ::std::unique_ptr< Pointer< DesktopPeer> > dPeer;
            ::std::unique_ptr< Pointer< ::indigo::Font> > fnt;
            ::std::unique_ptr< Color> fg, bg;
            ::std::unique_ptr < ::std::vector< DesktopComponent*> > components;

            // allocate the fonts and colors
            try {
               dPeer.reset(new Pointer< DesktopPeer>(_peer));
               fnt.reset(new Pointer< ::indigo::Font>(_peer->fonts()->queryDefaultFont()));
               bg.reset(new Color(Color::WHITE));
               fg.reset(new Color(Color::BLACK));
               components.reset(new ::std::vector< DesktopComponent*>());
            }
            catch (const ::std::exception& e) {
               _haveException = true;
               _exceptionWhat = e.what();
               return;
            }

            try {
               global_key_interceptor = new KeyInterceptor();
               global_defaultFGColor = fg.release();
               global_defaultBGColor = bg.release();
               global_components = components.release();
               global_defaultFont = fnt.release();

               global_peer = dPeer.release();

               global_desktopInitialized = true;

               ::std::atexit(shutdownDesktopAtExit);
            }
            catch (...) {
               assert("This section should have never thrown an exception" == 0);
            }

            // register the at exit function
         }

         Reference< DesktopPeer> _peer;
         bool _haveException;
         ::std::string _exceptionWhat;
   };

   static Pointer< Desktop::DesktopPeer> createDefaultDesktop(Int& argc, const char** argv) throws(::std::exception)
   {
      Application& app = Application::instance();
      ::std::string property(Desktop::DESKTOP_PROVIDER);
      ::std::string path;
      if (!app.getProperty(property, path)) {

         // load the .nexus properties file in the user's home directory
         try {
            ::std::string home = ::canopy::User::currentUser()->documentRoot();
            home = ::canopy::fs::FileSystem().joinPaths(home, ".nexus");
            SharedRef< ::timber::config::Configuration> cfg = ::timber::config::XMLConfiguration::loadXML(home);
            ::std::shared_ptr< ::timber::config::Value> cfgPath = cfg->getValue(Desktop::DESKTOP_PROVIDER);
            if (cfgPath) {
               path = cfgPath->value();
            }
         }
         catch (...) {
            // ignore this; configuration was not found
         }
         if (path.empty()) {
            path = DEFAULT_MODULE_PATH;
            log().warn("Cannot create default desktop: property " + property + " not set; using " + path);
         }
      }

      DesktopModule& mod = loadDesktopModule(path, argc, argv);
      Pointer< ::nexus::peer::Desktop> dtPeer = mod.get();
      if (!dtPeer) {
         throw ::std::runtime_error("Could not create desktop peer. Has it already been created");
      }

      return dtPeer;
   }

   void Desktop::configure(Int& argc, const char** argv)
   throws (::std::exception)
   {
      if (argv == nullptr || argc < 0) {
         throw ::std::invalid_argument("Invalid argc and/or argv");
      }
      Pointer< DesktopPeer> peer(createDefaultDesktop(argc, argv));
      if (!peer) {
         log().warn("Desktop::configure: Could not instantiate native desktop");
         throw ::std::runtime_error("Could not instantiate native desktop");
      }
      auto runner = ::std::make_shared<Instantiator>(peer);
      auto sr=::std::make_shared<SynchronizedRunnable>(runner);
      ::canopy::mt::MutexGuard< ::canopy::mt::Condition> G(sr->_condition);
      peer->enqueue(sr, Delay::zero(), Period::zero());
      try {
         sr->_condition.wait();
      }
      catch (...) {
         throw ::std::runtime_error("Could not configure the desktop");
      }
      if (runner->_haveException) {
         throw ::std::runtime_error(runner->_exceptionWhat);
      }
   }

   void Desktop::configure(Int& argc, const char** argv,
         const ::std::function< void(size_t, const char**)>& init) throws(::std::exception)
   {
      configure(argc, argv);
      if (!enqueueAndWait([&] {init(argc,argv);})) {
         throw ::std::runtime_error("Could not execute initialization function");
      }
   }

   void Desktop::configure(Int& argc, const char** argv, const ::std::function< void()>& init) throws(::std::exception)
   {
      configure(argc, argv);
      if (!enqueueAndWait(init)) {
         throw ::std::runtime_error("Could not execute initialization function");
      }
   }

   void Desktop::addComponent(DesktopComponent* c)
   throws()
   {
      assert(c->hasPeer());
      // not sure, how this could happen!!
      for (UInt i = 0; i < (*global_components).size(); ++i) {
         if ((*global_components)[i] == c) {
            LogEntry(log()).severe() << "Error : component " << c << " already added to desktop; aborting" << doLog;
            ::std::abort();
            return;
         }
      }
      c->ref();
      (*global_components).push_back(c);
   }

   void Desktop::removeComponent(DesktopComponent* c)
   throws()
   {
      assert(!c->hasPeer());
      // not sure, how this could happen!!
      for (UInt i = 0; i < (*global_components).size(); ++i) {
         if ((*global_components)[i] == c) {
            (*global_components)[i]->unref();
            ::std::swap((*global_components)[i], (*global_components)[(*global_components).size() - 1]);
            (*global_components).erase((*global_components).end() - 1);
            return;
         }
      }
      LogEntry(log()).severe() << "Error : component " << c << " was not added to desktop; aborting" << doLog;
      ::std::abort();
   }

   bool Desktop::isComponentShowing(::timber::Pointer< Component> c) throws()
   {
      while (c != nullptr && c->parent() != nullptr) {
         if (!c->isVisible()) {
            return false;
         }
         c = c->parent();
      }
      for (UInt i = 0; i < (*global_components).size(); ++i) {
         if ((*global_components)[i] == c) {
            return (*global_components)[i]->isShowing();
         }
      }
      return false;
   }

   Pointer< ::indigo::Font> Desktop::getFont(const ::indigo::FontDescription& d)
   throws()
   {
      return fonts()->queryFont(d);
   }

   void Desktop::setDefaultFont(const Pointer< ::indigo::Font>& f)
   throws()
   {
      Pointer< ::indigo::Font>& tmp(*global_defaultFont);

      if (f) {
         tmp = f;
      }
      else {
         tmp = fonts()->queryDefaultFont();
      }
   }

   void Desktop::setDefaultForegroundColor(const Color& c)
   throws()
   {
      Color& tmp = *global_defaultFGColor;

      if (c == 0) {
         tmp = Color::BLACK;
      }
      else {
         tmp = c;
      }
   }

   void Desktop::setDefaultBackgroundColor(const Color& c)
   throws()
   {
      Color& tmp = *global_defaultBGColor;
      if (c == 0) {
         tmp = Color::WHITE;
      }
      else {
         tmp = c;
      }
   }

   InputModifiers Desktop::getInputModifiers()
   throws()
   {
      return (*global_peer)->queryInputModifiers();
   }

   void Desktop::removeComponents()
   throws()
   {
      if ((*global_components).size() > 0) {
         log().info("Removing unclaimed components from desktop");
      }
      while ((*global_components).size() > 0) {
         DesktopComponent* ptr = (*global_components)[0];
         ptr->ref();
         assert(ptr->hasPeer());
         ptr->destroyPeer();
         assert(!ptr->hasPeer());
         assert((*global_components).size() == 0 || (*global_components)[0] != ptr);
         ptr->unref();
      }
   }

   void Desktop::shutdown()
   throws()
   {
      if ((*global_peer) != nullptr) {
         if (isEventThread()) {
            removeComponents();
            (*global_peer)->shutdown();
         }
         else {
            struct Closer : public Runnable
            {
                  ~Closer() throws()
                  {
                  }
                  void run() throws()
                  {
                     Desktop::shutdown();
                  }
            };
            auto runner= ::std::make_shared<Closer>();
            auto sr = ::std::make_shared<SynchronizedRunnable> (runner);
            if (enqueue(sr)) {
               sr->_condition.wait();
            }
         }
      }
   }

   bool Desktop::isEventThread()
   throws()
   {
      return (*global_peer)->isEventThread();
   }

   Size Desktop::size()
   throws()
   {
      return (*global_peer)->size();
   }

   void Desktop::destroyComponentPeer(ComponentPeer* cp)
   throws()
   {
      (*global_peer)->destroyComponentPeer(cp);
   }

   Desktop::WindowPeer* Desktop::createWindow()
   throws()
   {
      return (*global_peer)->createWindow();
   }

   Desktop::ContainerPeer Desktop::createContainer(ComponentPeer& p) throws()
   {
      return (*global_peer)->createContainer(p);
   }

   Desktop::CanvasPeer Desktop::createCanvas2D(ComponentPeer& p) throws()
   {
      return (*global_peer)->createCanvas2D(p);
   }

   Desktop::LabelPeer Desktop::createLabel(ComponentPeer& p) throws()
   {
      return (*global_peer)->createLabel(p);
   }

   Desktop::PushButtonPeer Desktop::createPushButton(ComponentPeer& p) throws()
   {
      return (*global_peer)->createPushButton(p);
   }

   Desktop::TextFieldPeer Desktop::createTextField(ComponentPeer& p) throws()
   {
      return (*global_peer)->createTextField(p);
   }

   Desktop::MenuPeer Desktop::createMenu(ComponentPeer& p) throws()
   {
      return (*global_peer)->createMenu(p);
   }

   Desktop::MenuItemPeer Desktop::createMenuItem(::nexus::peer::Menu& p) throws()
   {
      return (*global_peer)->createMenuItem(p);
   }

   Desktop::MenuBarPeer Desktop::createMenuBar(WindowPeer& p) throws()
   {
      return (*global_peer)->createMenuBar(p);
   }

   Desktop::TextAreaPeer Desktop::createTextArea(ComponentPeer& p) throws()
   {
      return (*global_peer)->createTextArea(p);
   }

   Pixel Desktop::pointerLocation()
   throws()
   {
      return (*global_peer)->pointerLocation();
   }

   Reference< ::indigo::FontFactory> Desktop::fonts()
   throws()
   {
      return (*global_peer)->fonts();
   }

   void Desktop::computeShadowHighlightColors(const Color& bg, ::indigo::Color& shadow, ::indigo::Color& highlight)
   throws()
   {
      (*global_peer)->computeShadowHighlightColors(bg, shadow, highlight);
   }

   void Desktop::waitShutdown()
   throws()
   {
      (*global_peer)->waitShutdown();
   }

   bool Desktop::run(const SharedRef< Runnable>& r)
   throws()
   {
      if (isEventThread()) {
         try {
            r->run();
         }
         catch (const ::std::exception& e) {
            LogEntry(log()).warn() << "Unexpected exception while executing a runnable of type " << typeid(*r).name()
                  << " : " << e.what() << doLog;
         }
         catch (...) {
            LogEntry(log()).severe() << "Unknown exception while executing a runnable of type " << typeid(*r).name()
                  << doLog;
         }
         return true;
      }
      else {
         return enqueue(r);
      }
   }

   bool Desktop::enqueue(const SharedRef< Runnable>& r, const Delay& delayMS)
   throws()
   {
      return (*global_peer)->enqueue(r, delayMS, Period::zero());
   }

   bool Desktop::enqueue(const SharedRef< Runnable>& r, const Delay& delayMS, const Period& periodMS)
   throws()
   {
      return (*global_peer)->enqueue(r, delayMS, periodMS);
   }

   bool Desktop::enqueue(const SharedRef< Runnable>& r, bool waitForIt)
   throws()
   {
      if (waitForIt) {
         if (isEventThread()) {
            // cannot enqueue if this is already the event thread
            // and we cannot block
            log().warn("Desktop::enqueue called from within event thread");
            return false;
         }
         auto runner = ::std::make_shared<SynchronizedRunnable>(r);
         ::canopy::mt::MutexGuard< ::canopy::mt::Condition> G(runner->_condition);
         if (!(*global_peer)->enqueue(runner, Delay::zero(), Period::zero())) {
            log().warn("Enqueue failed");
            return false;
         }
         if (!runner->_condition.wait()) {
            // interrupted
            log().warn("Enqueue interrupted; runnable not enqueued");
            return false;
         }
      }
      else {
         return (*global_peer)->enqueue(r, Delay::zero(), Period::zero());
      }
      return true;
   }

   bool Desktop::enqueueAndWait(const ::std::function< void()>& r) throws()
   {
      auto runner=::std::make_shared<FunctionRunner>(r);
      return enqueue(runner, true);
   }

   Color Desktop::defaultBackgroundColor()
   throws()
   {
      return *global_defaultBGColor;
   }

   Color Desktop::defaultForegroundColor()
   throws()
   {
      return *global_defaultFGColor;
   }

   ::timber::Pointer< ::indigo::Font> Desktop::defaultFont()
   throws()
   {
      return *global_defaultFont;
   }

   void Desktop::addKeyEventInterceptor(const ::timber::Reference< ::nexus::event::KeyEventInterceptor>& interceptor)
   throws()
   {
      global_key_interceptor->_interceptors.push_back(interceptor);
      if (global_key_interceptor->_interceptors.size() == 1) {
         (*global_peer)->setKeyInterceptor(global_key_interceptor);
      }
   }

   void Desktop::removeKeyEventInterceptor(const ::timber::Pointer< ::nexus::event::KeyEventInterceptor>& interceptor)
   throws()
   {
      for (size_t i = 0, sz = global_key_interceptor->_interceptors.size(); i < sz; ++i) {
         if (global_key_interceptor->_interceptors.at(i) == interceptor) {
            global_key_interceptor->_interceptors.erase(global_key_interceptor->_interceptors.begin() + i);
            break;
         }
      }
      if (global_key_interceptor->_interceptors.size() == 0) {
         (*global_peer)->setKeyInterceptor(0);
      }
   }
}
