#ifndef _NEXUS_VERTICALALIGNMENT_H
#define _NEXUS_VERTICALALIGNMENT_H

#ifndef _NEXUS_H
#include <nexus/nexus.h>
#endif

namespace nexus {
  
  /**
   * This class defines a vertical alignment.
   */
  class VerticalAlignment {
    /** The alignment values */
  public:
    enum Alignment { 
      ALIGN_TOP,     //< align at the top
      ALIGN_CENTER,  //< align in the center
      ALIGN_BOTTOM   //< align at the bottom
    };

    /** Align at the top */
  public:
    static const VerticalAlignment TOP;

    /** Align at the center */
  public:
    static const VerticalAlignment CENTER;

    /** Align at the bottom */
  public:
    static const VerticalAlignment BOTTOM;

    /**
     * Create a new vertical alignment
     * @param a an alignment
     */
  public:
    inline VerticalAlignment (Alignment a) throws()
      : _align(a) {}

    /**
     * Create a center alignment
     */
  public:
    inline VerticalAlignment () throws()
      : _align(ALIGN_CENTER) {}

    /**
     * Compute a hashvalue.
     * @return a hashvalue
     */
  public:
    inline size_t hashValue () const throws()
    { return _align; }
      
    /**
     * Compare two alignments.
     * @param a an alignment
     * @return true if the two alignments are the same
     */
  public:
    inline bool operator==(const VerticalAlignment& a) const throws()
    { return _align == a._align; }

    /**
     * Compare two alignments.
     * @param a an alignment
     * @return true if the two alignments are the same
     */
  public:
    inline bool equals(const VerticalAlignment& a) const throws()
    { return *this == a; }

    /**
     * Compare two alignments.
     * @param a an alignment
     * @return true if the two alignments are different
     */
  public:
    inline bool operator!=(const VerticalAlignment& a) const throws()
    { return _align != a._align; }

    /**
     * Get the alignment value. This method is primarily
     * used in switch statements.
     * @return the alignment value
     */
  public:
    inline Alignment value () const throws() { return _align; }


    /** An alignment */
  private:
    Alignment _align;
  };
}


#endif
