#include <nexus/Label.h>

namespace nexus {
  
    

  Label::Label () throws (::std::runtime_error)
  : _preferredSize(1,1)
  {}
      
  Label::Label (const ::std::string& t) throws (::std::runtime_error)
  : _text(t),_preferredSize(1,1)
  {}
      
  Label::Label (const Icon& i) throws (::std::runtime_error)
  : _icon(i),_preferredSize(i.size())
  {
    assert(i.graphic()!=nullptr);
    assert(i.width()>0);
    assert(i.height()>0);
  }

  Label::~Label () throws()
  {}
      
  bool Label::updatePreferredSize() const throws()
  {
    Size sz(1,1);
    if (_icon.graphic()!=nullptr) {
      sz = _icon.size();
    }
    else if (!_text.empty()) {
      // search for a font
      ::timber::Pointer< ::indigo::Font> f = font();
      if (f) { 
	sz = f->stringBounds(_text);
      }
    }
    if (sz!=_preferredSize) {
      _preferredSize = sz;
      return true;
    }
    return false;
  }
      
      
  void Label::setText (const ::std::string& t) throws()
  { 
    if (t!=_text) {
      _text=t;
      _icon = Icon();
      if (hasPeer()) {
	label()->setText(_text);
      }
      if (updatePreferredSize()) {
	revalidate();
      }
    }
  }
      
  void Label::setIcon (const Icon& i) throws()
  { 
    if (_icon.size()!=i.size() || i.graphic()!=_icon.graphic()) {
      _text.clear();
      _icon = i;
      if (hasPeer()) {
	label()->setIcon(_icon);
      }
      if (updatePreferredSize()) {
	revalidate();
      }
    }
  }
      
  SizeHints Label::getSizeHints () const throws()
  { 
    Int w = Size::MAX.width();
    Int h = Size::MAX.height();

    switch (_resizeCapability.value()) {
    case ResizeCapability::RESIZE_NONE:
      w = _preferredSize.width();
      h = _preferredSize.height();
      break;
    case ResizeCapability::RESIZE_VERTICAL:
      w = _preferredSize.width();
      break;
    case ResizeCapability::RESIZE_HORIZONTAL:
      h = _preferredSize.height();
      break;
    case ResizeCapability::RESIZE_BOTH:
    default:
      break;
    };

    return SizeHints(_preferredSize,Size(w,h),_preferredSize);
  }
      
  void Label::fontChanged () throws()
  { 
    updatePreferredSize(); 
    revalidate();
  }


  void Label::initializePeer () throws()
  {
    Component::initializePeer();
    if (_icon.graphic()!=nullptr) {
      label()->setIcon(_icon);
    }
    else if (!_text.empty()) {
      label()->setText(_text);
    }
    label()->setAlignment(_align);
  }

  Label::PeerComponent* Label::createPeerComponent (PeerComponent* p) throws()
  {
    assert(p!=0);
    return Desktop::createLabel(*p); 
  }

  void Label::setAlignment (const HorizontalAlignment& a) throws()
  {
    if (_align!=a) {
      _align = a;
      if (hasPeer()) {
	label()->setAlignment(a);
      }
    }
  }

  void Label::setResizeCapability (const ResizeCapability& rc) throws()
  {
    if (_resizeCapability!=rc) {
      _resizeCapability = rc;
      if (rc!=ResizeCapability::BOTH) {
	revalidate();
      }
    }
  }

}

