#include <nexus/SizeHints.h>
#include <nexus/Insets.h>
#include <iostream>

namespace nexus {

  SizeHints::SizeHints () throws()
  : _min(Size::MIN),_max(Size::MAX), _preferred(Size::MIN)
  {}
  SizeHints::SizeHints (const Size& m, const Size& M, const Size& p) throws()
  : _min(m),_max(M),_preferred(p) {}

  SizeHints::SizeHints (const Size& sz) throws()
  : _min(sz),_max(sz),_preferred(sz) {}

  bool SizeHints::operator==(const SizeHints& d) const throws()
  { return _min==d._min && _max==d._max && _preferred==d._preferred; }
 
  bool SizeHints::operator!=(const SizeHints& d) const throws()
  { return _min!=d._min || _max!=d._max || _preferred!=d._preferred; }
  size_t SizeHints::hashValue () const throws()
  { return _min.hashValue() ^ _max.hashValue() ^ _preferred.hashValue(); }

 SizeHints SizeHints::removeInsets (const Insets& insets) const throws()
  {
    Int dx = insets.left()+insets.right();
    Int dy = insets.top()+insets.bottom();
    
    // ensure that all sizes are non-negative
    Int wMin  = ::std::max(0,_min.width()-dx);
    Int hMin  = ::std::max(0,_min.height()-dy);
    Int wMax  = ::std::max(0,_max.width()-dx);
    Int hMax  = ::std::max(0,_max.height()-dy);
    Int pw    = ::std::max(0,_preferred.width()-dx);
    Int ph    = ::std::max(0,_preferred.height()-dy);
    
    return SizeHints(Size(wMin,hMin),Size(wMax,hMax),Size(pw,ph));
  }

  SizeHints SizeHints::addInsets (const Insets& insets) const throws()
  {
    Int dx = insets.left()+insets.right();
    Int dy = insets.top()+insets.bottom();
    
    // ensure that all sizes are non-negative
    Int wMin  = ::std::max(0,_min.width()+dx);
    Int hMin  = ::std::max(0,_min.height()+dy);
    Int wMax  = ::std::max(0,_max.width()+dx);
    Int hMax  = ::std::max(0,_max.height()+dy);
    Int pw    = ::std::max(0,_preferred.width()+dx);
    Int ph    = ::std::max(0,_preferred.height()+dy);
    
    return SizeHints(Size(wMin,hMin),Size(wMax,hMax),Size(pw,ph));
  }

  
  bool SizeHints::containsSize (const Size& sz) const throws()
  { 
    return sz.width()>=_min.width() 
      && sz.width()<=_max.width() 
      && sz.height()>=_min.height() 
      && sz.height()<=_max.height(); 
  }
	
  Size SizeHints::fitSize (const Size& sz) const throws()
  {
    Int w = ::std::max(sz.width(),_min.width());
    w = ::std::min(w,_max.width());
    Int h = ::std::max(sz.height(),_min.height());
    h = ::std::min(h,_max.height());
    return Size(w,h);
  }

  SizeHints SizeHints::encloseSize (const Size& sz) const throws()
  {
    Int wMin  = ::std::min(sz.width(),_min.width());
    Int hMin  = ::std::min(sz.height(),_min.height());
    Int wMax  = ::std::max(sz.width(),_max.width());
    Int hMax  = ::std::max(sz.height(),_max.height());

    return SizeHints(Size(wMin,hMin),Size(wMax,hMax),_preferred);
  }

  bool SizeHints::isConsistent () const throws()
  {
    return minimumWidth()<=maximumWidth()
      && minimumHeight()<=maximumHeight() 
      && containsSize(preferredSize());
  }
}

::std::ostream& operator<< (::std::ostream& out, const ::nexus::SizeHints& hints) throws()
{
  return out << "min: " << hints.minimumSize() 
	     << ", preferred: " << hints.preferredSize()
	     << ", max: " << hints.maximumSize();
} 

