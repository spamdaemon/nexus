#include <nexus/ViewTool.h>
#include <nexus/Viewer2D.h>
#include <nexus/event/Viewer2DEvent.h>
#include <nexus/event/ComponentEvent.h>
#include <nexus/event/ButtonEvent.h>
#include <nexus/event/CrossingEvent.h>
#include <nexus/event/MotionEvent.h>
#include <nexus/event/WheelEvent.h>
#include <nexus/Canvas.h>

#include <indigo/Transform2D.h>
#include <indigo/RSTTransform2D.h>

#include <cstdlib>
#include <iostream>

using namespace ::timber;
using namespace ::timber::logging;
using namespace ::indigo;
using namespace ::nexus::event;

namespace nexus {

   namespace {
      static double toRadians(double x)
      {
         return 3.14159265358979323846 * x / 180.0;
      }
   }

   class ViewTool::Impl : public MotionEventListener,
         public ButtonEventListener,
         public CrossingEventListener,
         public ComponentEventListener,
         public WheelEventListener
   {

      public:
         friend class ViewTool;

      public:
         Impl()throws()
               : _acquire(true), _zoomEnabled(true), _rotateEnabled(true), _panEnabled(true), _zoomOutFactor(0.95), _zoomInFactor(
                     1.05), _maxZoom(100), _minZoom(.01), _panMode(PanMode::DRAG_VIEWPORT)
         {
         }

         ~Impl()throws()
         {
         }

         void resetTransform(Viewer2D& viewer)
         {
            // create a viewport of the size of the current viewport,
            // but with a scale of 1
            ViewPort::Window w = viewer.viewPort().viewWindow();
            ViewPort::Bounds b(0, 0, w.width(), w.height());
            viewer.viewPort().setBounds(b);
            LogEntry("nexus.ViewTool").debugging() << "resetTransform " << viewer.viewPort() << doLog;
            _acquire = true;
         }

         ViewPort& viewPort(const Reference< UIEvent>& e)throws()
         {
            Reference< Viewer2D> v = e->source();
            return v->viewPort();
         }

         void processComponentEvent(const Reference< ComponentEvent>& e)throws()
         {
            Pointer< Viewer2D> v = e->source().tryDynamicCast< Viewer2D>();
            if (v && e->id() == ComponentEvent::COMPONENT_RESIZED) {
               _acquire = true;
               LogEntry("nexus.ViewTool").debugging() << "processComponentEvent(resized) " << v->viewPort() << doLog;
            }
         }

         void processCrossingEvent(const Reference< CrossingEvent>& e)throws()
         {
            Pointer< Viewer2D> v = e->source().tryDynamicCast< Viewer2D>();
            if (v && e->id() == CrossingEvent::COMPONENT_EXITED) {
               _acquire = true;
            }
         }

         void processWheelEvent(const Reference< WheelEvent>& e)throws()
         {
            Pointer< Viewer2D> v = e->source().tryDynamicCast< Viewer2D>();
            if (v && _zoomEnabled) {
               ViewPort& port = viewPort(e);
               const ViewPort::Window sz = port.viewWindow();

               // get the viewer's matrix
               const AffineTransform2D vt(port.getModelViewTransform());
               Point pt0 = vt.transformPoint(0, 0);
               Point pt1 = vt.transformPoint(1, 0);
               double dist = pt0.distanceTo(pt1);

               double scaleFactor = e->wheelUp() ? _zoomOutFactor : _zoomInFactor;
               double s = port.horizontalScale() * scaleFactor;

               if (dist >= _maxZoom) {
                  if (scaleFactor >= 1) {
                     // the maximum zoom has already been achieved
                     return;
                  }
               }
               if (dist <= _minZoom) {
                  if (scaleFactor <= 1) {
                     // the minimum zoom factor has already been achieved
                     return;
                  }
               }

               port.resizeBoundsToScale(s, s, port.getPoint(port.centerPixel()));
               LogEntry("nexus.ViewTool").debugging() << "processWheelEvent(scale) " << port << doLog;
               _acquire = true;
            }
         }

         void processMotionEvent(const Reference< MotionEvent>& e)throws()
         {
            Pointer< Viewer2D> v = e->source().tryDynamicCast< Viewer2D>();
            if (!v) {
               return;
            }
            const InputModifiers im = e->modifiers();

            if (im.isButton2Pressed() && !_rotateEnabled) {
               return;
            }
            if (im.isButton1Pressed() && !_panEnabled) {
               return;
            }

            ViewPort& port = viewPort(e);
            const ViewPort::Window sz = port.viewWindow();

            // check if we need to re-acquire the
            // the pointer; this can happen because the
            // viewer was resized or the pointer left the
            // component and then comes back and all the
            // while the button was kept pressed
            if (_acquire) {
               _acquire = false;
               _position = e->pixel();
               return;
            }

            const Pixel pos = e->pixel();

            // button 2 rotates, button 3 translates
            if (im.isButton2Pressed()) {
               // ROTATE
               const double diff = pos.x() - _position.x();
               port.rotateBounds(toRadians(0.5 * diff), port.getPoint(port.centerPixel()));
               LogEntry("nexus.ViewTool").debugging() << "processMotionEvent(rotate) " << port << doLog;
            }
            else if (im.isButton1Pressed()) {
               int32_t dx = pos.x() - _position.x();
               int32_t dy = pos.y() - _position.y();
               if (_panMode == PanMode::DRAG_OBJECT) {
                  dx = -dx;
                  dy = -dy;
               }
               port.translateViewWindow(dx, dy);
               LogEntry("nexus.ViewTool").debugging() << "processMotionEvent(translate) " << port << doLog;
            }

            _position = pos;
         }

         void processButtonEvent(const Reference< ButtonEvent>& e)throws()
         {
            Pointer< Viewer2D> v = e->source().tryDynamicCast< Viewer2D>();
            if (v) {
               InputModifiers im = e->modifiers();
               switch (e->id()) {
                  case ButtonEvent::BUTTON_PRESSED:
                     if (_panEnabled || _rotateEnabled) {
                        if (im.isButton2Pressed() || im.isButton1Pressed()) {
                           _position = e->pixel();
                           _acquire = false;
                        }
                     }
                     break;
                  case ButtonEvent::BUTTON_RELEASED:
                     _acquire = true;
                     break;
                  case ButtonEvent::BUTTON_CLICKED:
                     // ignore
                     break;
               };
            }
         }

         void setZoomEnabled(bool enable)throws()
         {
            _zoomEnabled = enable;
         }
         void setRotateEnabled(bool enable)throws()
         {
            _rotateEnabled = enable;
         }
         void setPanEnabled(bool enable)throws()
         {
            _panEnabled = enable;
         }
         void setPanMode(PanMode mode) throws()
         {
            _panMode = mode;
         }

         /** This will true if an initial pixel must be acqurired */
      private:
         bool _acquire;

         /** The last pixel position */
      private:
         Pixel _position;

         /** True if zoom is enabled */
      private:
         bool _zoomEnabled;

         /** True if rotation is enabled */
      private:
         bool _rotateEnabled;

         /** True if panning is enabled */
      private:
         bool _panEnabled;

         /** The zoom out factor */
      private:
         double _zoomOutFactor;

         /** The zoom in factor */
      private:
         double _zoomInFactor;

         /** The maximum zoom-in */
      private:
         double _maxZoom;

         /** The minimum zoom-value */
      private:
         double _minZoom;

         /** The pan mode */
      private:
         PanMode _panMode;
   };

   ViewTool::ViewTool()
   throws()
         : _tool(new Impl())
   {
   }

   ViewTool::~ViewTool()
   throws()
   {
      setViewer(Pointer< Viewer2D>());
   }

   void ViewTool::setViewer(const ::timber::Pointer< Viewer2D>& viewer)
   throws()
   {
      if (_viewer) {
         // remove the current tool
         _viewer->removeMotionEventListener(_tool);
         _viewer->removeButtonEventListener(_tool);
         _viewer->removeCrossingEventListener(_tool);
         _viewer->removeComponentEventListener(_tool);
         _viewer->removeWheelEventListener(_tool);
      }

      _viewer = viewer;
      if (_viewer) {
         _tool->resetTransform(*viewer);

         _viewer->addWheelEventListener(_tool);
         _viewer->addComponentEventListener(_tool);
         _viewer->addCrossingEventListener(_tool);
         _viewer->addButtonEventListener(_tool);
         _viewer->addMotionEventListener(_tool);
      }
   }

   void ViewTool::setZoomEnabled(bool enable)
   throws()
   {
      _tool->_zoomEnabled = enable;
   }

   void ViewTool::setRotateEnabled(bool enable)
   throws()
   {
      _tool->_rotateEnabled = enable;
   }

   void ViewTool::setPanEnabled(bool enable)
   throws()
   {
      _tool->_panEnabled = enable;
   }

   void ViewTool::setPanMode(PanMode mode)
   throws()
   {
      _tool->_panMode = mode;
   }

   void ViewTool::setZoomOutFactor(double zf)
   throws()
   {
      _tool->_zoomInFactor = zf;
   }

   void ViewTool::setZoomInFactor(double zf)
   throws()
   {
      _tool->_zoomOutFactor = zf;
   }

   void ViewTool::setMaxZoom(double mZoom)
   throws()
   {
      _tool->_maxZoom = mZoom;
   }

   void ViewTool::setMinZoom(double mZoom)
   throws()
   {
      _tool->_minZoom = mZoom;
   }
}
