#include <nexus/MenuItem.h>
#include <nexus/event/ActionEvent.h>
#include <nexus/peer/Menu.h>

namespace nexus {
  
    
  MenuItem::MenuItem () throws (::std::runtime_error)
  : _observer(0)
  { }
  
  MenuItem::MenuItem (const ::std::string& nm) throws (::std::runtime_error)
  : _observer(0),_text(nm)
  { }
  
  MenuItem::MenuItem (const Icon& ic) throws (::std::runtime_error)
  : _observer(0),_icon(ic)
  { }
  
  MenuItem::~MenuItem () throws()
  {
    delete _observer;
  }
   
  void MenuItem::setActionID(const ::std::string& s) throws()
  { _actionID = s; }

   
  void MenuItem::setText (const ::std::string& t) throws()
  { 
    if (t!=_text) {
      _text=t;
      _icon = Icon();
      if (hasPeer()) {
	menuItem()->setText(_text);
      }
#if 0
      if (updatePreferredSize()) {
	revalidate();
      }
#endif
    }
  }
      
  void MenuItem::setIcon (const Icon& i) throws()
  { 
    if (_icon.size()!=i.size() || i.graphic()!=_icon.graphic()) {
      _text.clear();
      _icon = i;
      if (hasPeer()) {
	menuItem()->setIcon(_icon);
      }
#if 0
      if (updatePreferredSize()) {
	revalidate();
      }
#endif
    }
  }

  void MenuItem::initializePeer() throws()
  {
    struct Observer : public Peer::Observer {
      inline Observer (MenuItem& b) throws() : _owner(b) {}
      ~Observer () throws() {}
      void menuActivated () throws()
      { _owner.processActionEvent(new ActionEvent(&_owner,_owner.actionID())); }

    private:
      MenuItem& _owner;
    };

    Component::initializePeer();
    if (_observer==0) {
      _observer = new Observer(*this);
    }

    Component::initializePeer();
    if (_icon.graphic()!=nullptr) {
      menuItem()->setIcon(_icon);
    }
    else if (!_text.empty()) {
      menuItem()->setText(_text);
    }

    menuItem()->setMenuItemObserver(_observer);
  }

  MenuItem::PeerComponent* MenuItem::createPeerComponent (PeerComponent* p) throws()
  {
    ::nexus::peer::Menu* menu = dynamic_cast< ::nexus::peer::Menu*>(p);
    if (menu==0) {
      return 0;
    }
    else {
      return Desktop::createMenuItem(*menu); 
    }
  }

  SizeHints MenuItem::getSizeHints() const throws()
  {
    return SizeHints();
  }

  void MenuItem::addActionEventListener (const ::timber::Reference< ::nexus::event::ActionEventListener>& l) throws()
  { _actionListeners.addListener(l); }
  
  void MenuItem::removeActionEventListener (const ::timber::Pointer< ::nexus::event::ActionEventListener>& l) throws()
  { _actionListeners.removeListener(l); }
      
  void MenuItem::processActionEvent (const ::timber::Reference<ActionEvent>& e) throws()
  { _actionListeners.notify(&::nexus::event::ActionEventListener::processActionEvent,e); }
}

