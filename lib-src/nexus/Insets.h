#ifndef _NEXUS_INSETS_H
#define _NEXUS_INSETS_H

#ifndef _NEXUS_H
#include <nexus/nexus.h>
#endif

#include <cassert>
#include <iosfwd>

namespace nexus {
  /**
   * This class defines the width and height
   * of top/bottom and left/right margins.
   *
   */
  class Insets {
    /**
     * Default constructor. Creates insets 0,0,0,0
     */
  public:
     Insets () throws() ;
      
    /**
     * Create inset with the same widths and heights.
     * This is equivalent to <code>Insets(s,s,s,s)</code>.
     * @param s the size 
     * @pre REQUIRE_GREATER_OR_EQUAL(s,0)
     */
  public:
     Insets (Int s) throws() ;
      
    /**
     * Create a new insets object.
     * @param lw the left width
     * @param rw the right width
     * @param th the top height
     * @param bh the bottom height
     * @pre REQUIRE_GREATER_OR_EQUAL(lw,0)
     * @pre REQUIRE_GREATER_OR_EQUAL(rw,0)
     * @pre REQUIRE_GREATER_OR_EQUAL(th,0)
     * @pre REQUIRE_GREATER_OR_EQUAL(bg,0)
     */
  public:
     Insets (Int lw, Int rw, Int th, Int bh) throws();
      
    /**
     * Create insets by merging two given insets
     * @param i some insets
     * @param j some insets
     */
  public:
    Insets (const Insets& i, const Insets& j) throws();
    /**
     * Get the left inset width.
     * @return the width of the left inset
     */
  public:
    inline Int left() const throws() { return _left; }

    /**
     * Set the left inset width.
     * @param w the left inset width
     * @pre REQUIRE_GREATER_OR_EQUAL(w,0)
     */
  public:
    inline void setLeft (Int w) throws() 
    { 
      assert(w>=0);
      _left = w;
    }

    /**
     * Get the right inset width.
     * @return the width of the right inset
     */
  public:
    inline Int right() const throws() { return _right; }

    /**
     * Set the right inset width.
     * @param w the right inset width
     * @pre REQUIRE_GREATER_OR_EQUAL(w,0)
     */
  public:
    inline void setRight (Int w) throws() 
    {
      assert(w>=0);
      _right=w;
    }

    /**
     * Get the top inset height.
     * @return the height of the top inset
     */
  public:
    inline Int top() const throws() { return _top; }

    /**
     * Set the top inset height.
     * @param h the top inset height
     * @pre REQUIRE_GREATER_OR_EQUAL(h,0)
     */
  public:
    inline void setTop (Int h) throws() 
    {
      assert(h>=0);
      _top=h;
    }

    /**
     * Get the bottom inset height.
     * @return the height of the bottom inset
     */
  public:
    inline Int bottom() const throws() { return _bottom; }

    /**
     * Set the bottom inset height.
     * @param h the bottom inset height
     * @pre REQUIRE_GREATER_OR_EQUAL(h,0)
     */
  public:
    inline void setBottom (Int h) throws() 
    {
      assert(h>=0);
      _bottom=h; 
    }

    /**
     * Compute a hashvalue.
     * @return a hashvalue
     */
  public:
    inline size_t hashValue () const throws()
    { return _left ^ (_right<<1) ^ (_top<<2) ^ (_bottom<<3); }

    /**
     * Compare two insets.
     * @param b a insets object
     * @return true if this and b are equal
     */
  public:
    inline bool operator==(const Insets& b) const throws()
    { return _left==b._left && _right==b._right && _top==b._top && _bottom==b._bottom; }
      
    /**
     * Compare two insets.
     * @param b a insets object
     * @return true if this and b are equal
     */
  public:
    inline bool equals(const Insets& b) const throws()
    { return *this==b; }
      
    /**
     * Compare two insets.
     * @param b a insets object
     * @return true if this and b are equal
     */
  public:
    inline bool operator!=(const Insets& b) const throws()
    { return !equals(b); }

    /** @name The left, right, top, and bottom insets */
  private:
    Int _left,_right,_top,_bottom;
  };
}

::std::ostream& operator<< (::std::ostream& out, const ::nexus::Insets& b) throws();

#endif
