#include <nexus/ResizeCapability.h>

namespace nexus {
  
  const ResizeCapability ResizeCapability::NONE(RESIZE_NONE);
  const ResizeCapability ResizeCapability::VERTICAL(RESIZE_VERTICAL);
  const ResizeCapability ResizeCapability::HORIZONTAL(RESIZE_HORIZONTAL);
  const ResizeCapability ResizeCapability::BOTH(RESIZE_BOTH);
}
