#ifndef _NEXUS_COMPONENT_H
#define _NEXUS_COMPONENT_H

#ifndef _NEXUS_H
#include <nexus/nexus.h>
#endif

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _TIMBER_WEAKCOUNT_H
#include <timber/WeakCount.h>
#endif

#ifndef _TIMBER_WEAKPOINTER_H
#include <timber/WeakPointer.h>
#endif

#ifndef _CANOPY_MT_RECURSIVEMUTEX_H
#include <canopy/mt/RecursiveMutex.h>
#endif

#ifndef _CANOPY_MT_MUTEXGUARD_H
#include <canopy/mt/RecursiveMutex.h>
#endif

#ifndef _NEXUS_DESKTOP_H
#include <nexus/Desktop.h>
#endif

#ifndef _NEXUS_SIZE_H
#include <nexus/Size.h>
#endif

#ifndef _NEXUS_INSETS_H
#include <nexus/Insets.h>
#endif

#ifndef _NEXUS_SIZEHINTS_H
#include <nexus/SizeHints.h>
#endif

#ifndef _NEXUS_BOUNDS_H
#include <nexus/Bounds.h>
#endif

#ifndef _NEXUS_COLOR_H
#include <nexus/Color.h>
#endif

#ifndef _NEXUS_PEER_COMPONENT_H
#include <nexus/peer/Component.h>
#endif

#ifndef _NEXUS_EVENTLISTENERS_H
#include <nexus/EventListeners.h>
#endif

#ifndef _NEXUS_EVENT_BUTTONEVENTLISTENER_H
#include <nexus/event/ButtonEventListener.h>
#endif

#ifndef _NEXUS_EVENT_MOTIONEVENTLISTENER_H
#include <nexus/event/MotionEventListener.h>
#endif

#ifndef _NEXUS_EVENT_WHEELEVENTLISTENER_H
#include <nexus/event/WheelEventListener.h>
#endif

#ifndef _NEXUS_EVENT_KEYEVENTLISTENER_H
#include <nexus/event/KeyEventListener.h>
#endif

#ifndef _NEXUS_EVENT_FOCUSEVENTLISTENER_H
#include <nexus/event/FocusEventListener.h>
#endif

#ifndef _NEXUS_EVENT_CROSSINGEVENTLISTENER_H
#include <nexus/event/CrossingEventListener.h>
#endif

#ifndef _NEXUS_EVENT_COMPONENTEVENTLISTENER_H
#include <nexus/event/ComponentEventListener.h>
#endif

#ifndef _NEXUS_LAYOUTEXCEPTION_H
#include <nexus/LayoutException.h>
#endif

#ifndef _TIMBER_LOGGING_H
#include <timber/logging.h>
#endif

#include <iosfwd>


namespace nexus {
   class Window;

   /**
    * This is the baseclass for all UI components. Components are reference
    * counted and are thus automatically collected. Reference counting, however,
    * does not support circular references and implementers of components must
    * take care not introduce circularity. Event listeners in particular, should not
    * hold references to the component to which they are listening. Once a component's
    * reference count reaches zero and has been detached, it becomes eligible for
    * collection. A collection is scheduled by running the collector as a task in
    * the gui thread; it is possible that a component becomes temporarily eligible
    * for collection, but is claimed by another component or reference before the
    * collection occurs.
    *
    * @author Raimund Merkert
    * @date 18 Feb 2003
    * @todo Should we use an atomic counter for the reference count?
    */
   class Component
   {
         Component&operator=(const Component&);
         Component(const Component&);

         /**
          * A guard for reference counting
          */
      public:
         typedef ::canopy::mt::MutexGuard< ::canopy::mt::RecursiveMutex> RefGuard;


         /**
          * Convert a raw pointer to a component to reference counted pointer.
          * @param refCount a reference count
          * @param c the raw component pointer
          * @return a pointer to c or null if the refCount isn't valid
          */
      private:
         static ::timber::Pointer<Component> convertRawPointer(::timber::ReferenceCount& refCount, Component* c) throws();

         /** The reference count type. This type needs to support the same interface as timber::ReferenceCount. */
      public:
         struct WeakPointerConverter {

            public:
               inline ::timber::Pointer<Component> operator() (::timber::ReferenceCount& cnt, Component* c) throws()
               { return Component::convertRawPointer(cnt,c); }
         };


         /** The weak counter */
      public:
         typedef ::timber::WeakCount<Component,WeakPointerConverter> RefCountType;

         /** An event observer */
      private:
         struct EventObserver;

         /**
          * @name Typedef alias for events
          * @{
          */
      public:
         typedef ::nexus::event::WheelEvent WheelEvent;
         typedef ::nexus::event::FocusEvent FocusEvent;
         typedef ::nexus::event::KeyEvent KeyEvent;
         typedef ::nexus::event::CrossingEvent CrossingEvent;
         typedef ::nexus::event::MotionEvent MotionEvent;
         typedef ::nexus::event::ButtonEvent ButtonEvent;
         typedef ::nexus::event::ComponentEvent ComponentEvent;
         /*@}*/

         /** Define a component reference */
      public:
         typedef ::timber::Pointer< Component> Ref;

         /** The window must be a friend */
      public:
         friend class Window;

         /** The component implementation */
      protected:
         typedef ::nexus::peer::Component PeerComponent;

         /**
          * Default constructor. The constructor must be called inside the
          * desktop's event thread.
          * @pre Desktop::isEventThread()
          * @throw ConfigurationException if the desktop has not been configured
          */
      protected:
         Component()
         throws ( ::std::exception);

         /**
          * Destroy this component
          */
      public:
         virtual ~Component()throws();

         /**
          * @name Instance management through reference counting
          * @{
          */

         /**
          * Get the current reference count for this component.
          * @return the current reference count
          */
      public:
         inline RefCountType::CounterType count() const throws()
         {  return _refCount->count();}

         /**
          * Increase the reference count of this component.
          */
      private:
         void refInternal() const throws();

         /**
          * Decrease the reference count of theis component.
          * @pre getReferenceCount()>0)
          */
      private:
         void unrefInternal() const throws();

         /**
          * Increase the reference count of this component.
          */
      public:
         void ref() const throws();

         /**
          * Decrease the reference count of theis component.
          * @pre getReferenceCount()>0)
          */
      public:
         void unref() const throws();

         /**
          * Get a weakpointer for this component.
          * @return a weak pointer
          */
      public:
         inline ::timber::WeakPointer< Component> weakPointer()throws()
         {  return ::timber::WeakPointer<Component>(_refCount,this);}

         /**
          * Need these two methods to deal support Reference and Pointer.
          */
      public:
         inline void incrementCount() const throws()
         {  ref();}
         inline void decrementCount() const throws()
         {  unref();}

         /**
          * Destroy the specified component.
          * @param c a component
          */
      private:
         static void destroyComponent(const Component* c)throws();

         /**
          * A function to delete all components whose reference
          * count has reached 0 and which have no parents.
          */
      private:
         static void collectComponents()throws();

         /**
          * Remove components.
          */
      private:
         static void sweepComponents()throws();

         /**
          * There are components which can possibly
          * be deleted.
          * @param g a mutex guard
          */
      private:
         static void scheduleCollection(const RefGuard& g)throws();

         /**
          * There are components which can possibly
          * be deleted.
          * @param g a mutex guard
          */
      private:
         static void scheduleCollection()throws();

         /**
          * Move this component from the specified list to another list.
          * @param g a mutex guard
          * @param fromList the from list
          * @param toList the to list
          */
      private:
         void moveComponent(const RefGuard& g, Component** fromList, Component** toList) const throws();

         /**
          * Enable debugging.
          * @param enabled if true, then some information is provided about which objects are collected.
          */
      public:
         static void setDebugEnabled(bool enabled)throws();

         /*@}*/

         /**
          * Get the implementation of the specified component.
          * @param c a component.
          * @return the implementation of the specified component
          */
      protected:
         static inline PeerComponent* implOf(const Component& c)throws()
         {  return c._peer;}

         /**
          * Get the peer implemenation.
          * @return the peer implementation
          */
      protected:
         template <class T>
         inline T* impl() const throws()
         {  return dynamic_cast<T*>(_peer);}

         /**
          * Set the name for this component.
          * @param n the name for this component or 0 to use a default name
          * @throw ArgumentException if is not an alpha numeric name or does not start with a letter
          */
      public:
         void setName(const ::std::string& n)
         throws ( ::std::exception);

         /**
          * Get the name for this component.
          * @return the name for this component or the empty string.
          */
      public:
         inline const ::std::string& name() const throws()
         {  return _name;}

         /**
          * Determine if this component has a peer.
          * @return true if this component has a peer, false otherwise
          */
      public:
         inline bool hasPeer() const throws()
         {  return _peer!=0;}

         /**
          * @name Managing parent-child relations
          * @{
          */

         /**
          * Attach the specified component as a child component.
          * @param c a component
          * @pre REQUIRE_ZERO(c.parent())
          * @pre REQUIRE_ZERO(c.peer())
          * @post c.parent()==this
          * @return the direct pointer to the compont.
          */
      protected:
         Component* attachChild(const ::timber::Pointer< Component>& c)throws();

         /**
          * Detach the specified child.
          * @param c a child
          * @pre REQUIRE_EQUAL(this,c.parent())
          * @post c.parent()==0
          * @post c.peer()==0
          * @return a reference to the child component
          */
      protected:
         ::timber::Pointer< Component> detachChild(Component* c)throws();

         /**
          * Get the parent component.
          * @return the parent component.
          */
      public:
         inline ::timber::Pointer< Component> parent() const throws()
         {  return _parent;}

         /*@}*/

         /**
          * Get the location of the specified pixel on the screen.
          * @param p a pixel relative to this component
          * @return the pixel's position relative to the screen
          */
      public:
         Pixel pixelOnScreen(const Pixel& p) const throws();

         /**
          * @name Managing the native peer component
          * @{
          */

         /**
          * Create a new peer component object for this component only.
          * @param p a peer component or 0 if this is component is the toplevel component
          * @return a peer component
          */
      protected:
         virtual PeerComponent* createPeerComponent(PeerComponent* p)throws() = 0;

         /**
          * Destroy the current peer component object. The
          * parent's peer has not yet been destroyed when this
          * method is called. The default simply calls <code>delete peer</code>
          * @param peer a peer component
          * @pre REQUIRE(parent()==0 || parent()->hasPeer())
          */
      protected:
         virtual void destroyPeerComponent(PeerComponent* peer)throws();

         /**
          * Create the peer for this component and its children.
          * The sequence of steps that are executed:
          * <ol>
          * <li> createPeerComponent() [must override]
          * <li> initializePeer()      [can override]
          * <li> createChildPeers()    [can override]
          * <li> make the peer visible
          * </ol>
          * <br>
          * Most classes will need to override the initializePeer() method,
          * but only few need to do so for the createChildPeers().
          * @pre REQUIRE_FALSE(hasPeer())
          * @pre REQUIRE_TRUE(parent()==0 || parent()->hasPeer())
          * @see initializePeer
          */
      protected:
         virtual void createPeer()throws();

         /**
          * Initialize the peer. This method must be invoked by subclasses.
          * This method is invoked by createPeer() before the peers
          * for each child have been created.
          * @see createPeer
          */
      protected:
         virtual void initializePeer()throws();

         /**
          * Create the peers for each child. This method is invoked
          * after the peer for this component has been initialized with
          * initializePeer(). Subclasses must call this method from
          * if overriding.<br>
          * The peer of this component will not yet be visible.
          */
      protected:
         virtual void createChildPeers()throws();

         /**
          * Destroy the peer of this component and any
          * children. The peers of the children will be
          * destroyed first.
          * @pre REQUIRE_TRUE(hasPeer())
          * @pre REQUIRE_ZERO(parent())
          */
      protected:
         virtual void destroyPeer()throws();

         /*@}*/

         /**
          * Enable or disable this component.
          * @param enabled true if this component is enabled
          */
      public:
         virtual void setEnabled(bool enabled)throws();

         /**
          * Test if this component is currently enabled.
          * @return true if this component is enabled
          */
      public:
         inline bool isEnabled() const throws()
         {  return _enabled;}

         /**
          * @name Geometry Management
          * @{
          */

         /**
          * Get the x position within the parent component.
          * @return the x location
          */
      public:
         inline Int x() const throws()
         {  return _bounds.x();}

         /**
          * Get the y position within the parent component.
          * @return the y location
          */
      public:
         inline Int y() const throws()
         {  return _bounds.y();}

         /**
          * Get the width of this component.
          * @return the width of this component
          */
      public:
         inline Int width() const throws()
         {  return _bounds.width();}

         /**
          * Get the height of this component.
          * @return the height of this component
          */
      public:
         inline Int height() const throws()
         {  return _bounds.height();}

         /**
          * Get the bounds for this component.
          * @return the component bounds
          */
      public:
         inline Bounds bounds() const throws()
         {  return _bounds;}

         /**
          * Get the current location of this component within it's parent. If this
          * is a top-level component, then it returns the absolute screen coordinates.
          * @return the pixel location
          * @see x()
          * @see y()
          */
      public:
         inline Pixel location() const throws()
         {  return _bounds.pixel();}

         /**
          * Get the current absolute screen location of this component.
          * @return the pixel location on the screen
          * @see x()
          * @see y()
          */
      public:
         Pixel locationOnScreen() const throws();

         /**
          * Get the layout size, which is the amount of space available for
          * layout out the content of this component.
          * @return the size of the area in which children of this component can
          * be laid out.
          */
      protected:
         Size layoutSize() const throws();

         /**
          * Get the current size of this component.
          * @return current size
          */
      public:
         inline Size size() const throws()
         {  return _bounds.size();}

         /**
          * Get the preferred size for this component.
          * @return the preferred size for this component
          */
      public:
         inline Size preferredSize() const throws()
         {  return sizeHints().preferredSize();}

         /**
          * Get the minimum component size. Returns Size(1,1).
          * @return the minimum size for this component.
          */
      public:
         inline Size minimumSize() const throws()
         {  return sizeHints().minimumSize();}

         /**
          * Get the minimum component size. Returns Size(1,1).
          * @return the minimum size for this component.
          */
      public:
         inline Size maximumSize() const throws()
         {  return sizeHints().maximumSize();}

         /**
          * Get the size hints for this component.
          * @return the size hints
          */
      protected:
         virtual SizeHints getSizeHints() const throws() = 0;

         /**
          * Get the size hints for this component.
          * @return the size hints
          */
      public:
         SizeHints sizeHints() const throws();

         /** Determine if this component requires a new layout. */
      protected:
         void revalidate()throws();

         /** Layout this component. */
      private:
         void layoutComponent()throws();

         /**
          * Layout this component. This method is invoked in response
          * to either need for a new layout, or because the bounds
          * have changed.
          * The default is to do nothing.
          * @throw LayoutException if the component or its subcomponent could not be laid out.
          */
      protected:
         virtual void doLayout()
         throws ( LayoutException);

         /**
          * Set the bounds of this component and call layoutComponent()
          * if this component needs being laid or its size has changed.
          * @param b the bounds
          */
      private:
         void setBounds(const Bounds& b)throws();

         /**
          * Set the bounds of the specified child component.
          * @param c the component whose bounds should be changed.
          *
          * @pre P has a method <code>void setChildBounds(native::Component*,Bounds)</code>
          * @pre REQUIRE_TRUE(this==c.parent())
          * @param b the new bounds
          * @pre REQUIRE_CONDITION(b,b.width()>0 && b.height()>0)
          */
      protected:
         template <class P>
         inline void setChildBounds(Component& c, const Bounds& b)throws()
         {
            assert(this==c.parent());
            assert(b.width()>0);
            assert(b.height()>0);

            if (hasPeer() && c.hasPeer()) {
               impl<P>()->setChildBounds(*implOf(c),b);
            }
            c.setBounds(b);
         }

         /*@}*/

         /**
          * Set the font.  Calls fontChanged().
          * @param f a font or 0 to inherit the font
          */
      public:
         virtual void setFont(const ::timber::Pointer< ::indigo::Font>& f)throws();

         /**
          * Get the font.
          * @return the font that applies to this component
          */
      public:
         inline ::timber::Pointer< ::indigo::Font> font() const throws()
         {  return _font;}

         /**
          * Set the foreground color. Calls foregroundColorChanged().
          * @param fg the foreground color
          */
      public:
         virtual void setForegroundColor(const Color& fg)throws();

         /**
          * Get the foreground color.
          * @return the foreground color that applies to this component..
          */
      public:
         inline Color foregroundColor() const throws()
         {  return _foreground;}

         /**
          * Set the background color. Calls backgroundColorChanged().
          * @param bg the background color
          */
      public:
         virtual void setBackgroundColor(const Color& bg)throws();

         /**
          * Get the background color.
          * @return the background color that applies to this component.
          */
      public:
         inline Color backgroundColor() const throws()
         {  return _background;}

         /**
          * Update the font. If ext
          * is not set, then the font is only updated if
          * it is inherited.
          * @param f a font
          * @param ext if true, then request to update was external
          * @pre REQUIRE_NON_ZERO(parent())
          */
      private:
         void updateFont(const ::timber::Pointer< ::indigo::Font>& f, bool ext)throws();

         /**
          * Update the foreground. If ext
          * is not set, then the color is only updated if
          * it is inherited.
          * @param c a color
          * @param ext if true, then request to update was external
          * @pre REQUIRE_NON_ZERO(parent())
          */
      private:
         void updateForegroundColor(const Color& c, bool ext)throws();

         /**
          * Update the background. If ext
          * is not set, then the color is only updated if
          * it is inherited.
          * @param c a color
          * @param ext if true, then request to update was external
          * @pre REQUIRE_NON_ZERO(parent())
          */
      private:
         void updateBackgroundColor(const Color& c, bool ext)throws();

         /**
          * Show or hide this component. The component will only
          * be visible if its container is also visible.
          * @param visible true to show this component false otherwise.
          */
      public:
         virtual void setVisible(bool visible)throws();

         /**
          * Test if this component is visible. Note that
          * visibility does not imply isShowing()!
          * @return true if this component is visible.
          */
      public:
         inline bool isVisible() const throws()
         {  return _visible;}

         /**
          * Test if this component is currently showing. This method
          * calls Desktop::isComponentShowing() to determine if this
          * component is actually visible.
          * @return true if this component is currently visible on the screen
          */
      public:
         bool isShowing() const throws();

         /**
          * Print this component to the specified output stream.
          * @param out an output stream
          * @param deep if true, the emit code for children as well
          * @param indent an indentation level
          * @return out
          */
      public:
         virtual ::std::ostream& print(::std::ostream& out, bool deep, Int indent) const throws();

         /**
          * @name Managing event listeners. These methods allow for various event listeners
          * to be added and removed from this component. When adding new listeners,
          * clients must ensure that the listener is not zero.
          * @{
          */

      public:
         virtual void addComponentEventListener(const ::timber::Reference< ::nexus::event::ComponentEventListener>& l)throws();

      public:
         virtual void removeComponentEventListener(const ::timber::Pointer< ::nexus::event::ComponentEventListener>& l)throws();

      public:
         virtual void addMotionEventListener(const ::timber::Reference< ::nexus::event::MotionEventListener>& l)throws();

      public:
         virtual void removeMotionEventListener(const ::timber::Pointer< ::nexus::event::MotionEventListener>& l)throws();

      public:
         virtual void addWheelEventListener(const ::timber::Reference< ::nexus::event::WheelEventListener>& l)throws();

      public:
         virtual void removeWheelEventListener(const ::timber::Pointer< ::nexus::event::WheelEventListener>& l)throws();

      public:
         virtual void addButtonEventListener(const ::timber::Reference< ::nexus::event::ButtonEventListener>& l)throws();

      public:
         virtual void removeButtonEventListener(const ::timber::Pointer< ::nexus::event::ButtonEventListener>& l)throws();

      public:
         virtual void addFocusEventListener(const ::timber::Reference< ::nexus::event::FocusEventListener>& l)throws();

      public:
         virtual void removeFocusEventListener(const ::timber::Pointer< ::nexus::event::FocusEventListener>& l)throws();

      public:
         virtual void addKeyEventListener(const ::timber::Reference< ::nexus::event::KeyEventListener>& l)throws();

      public:
         virtual void removeKeyEventListener(const ::timber::Pointer< ::nexus::event::KeyEventListener>& l)throws();

      public:
         virtual void addCrossingEventListener(const ::timber::Reference< ::nexus::event::CrossingEventListener>& l)throws();

      public:
         virtual void removeCrossingEventListener(const ::timber::Pointer< ::nexus::event::CrossingEventListener>& l)throws();

         /*@}*/

         /**
          * @name Event processing functions
          * @{
          */

         /**
          * Are motion events enabled. The method processMotionEvent()
          * will only be invoked if this method returns true;
          * @return true if this component reacts to motion events.
          */
      public:
         bool isMotionEventEnabled() const throws();

         /**
          * Enable or disable motion event processing. The method
          * processMotionEvent() is only called if motion events
          * are enabled. MotionListeners are still dispatched regardless
          * of this flag.
          */
      protected:
         virtual void setMotionEventEnabled(bool enable)throws();

         /**
          * Are wheel events enabled. The method processWheelEvent()
          * will only be invoked if this method returns true;
          * @return true if this component reacts to wheel events.
          */
      public:
         bool isWheelEventEnabled() const throws();

         /**
          * Enable or disable wheel event processing. The method
          * processWheelEvent() is only called if wheel events
          * are enabled. WheelListeners are still dispatched regardless
          * of this flag.
          */
      protected:
         virtual void setWheelEventEnabled(bool enable)throws();

         /**
          * Are button events enabled. The method processButtonEvent()
          * will only be invoked if this method returns true;
          * @return true if this component reacts to button events.
          */
      public:
         bool isButtonEventEnabled() const throws();

         /**
          * Enable or disable button event processing. The method
          * processButtonEvent() is only called if button events
          * are enabled. ButtonListeners are still dispatched regardless
          * of this flag.
          */
      protected:
         virtual void setButtonEventEnabled(bool enable)throws();

         /**
          * Are focus events enabled. The method processFocusEvent()
          * will only be invoked if this method returns true;
          * @return true if this component reacts to focus events.
          */
      public:
         bool isFocusEventEnabled() const throws();

         /**
          * Enable or disable focus event processing. The method
          * processFocusEvent() is only called if focus events
          * are enabled. FocusListeners are still dispatched regardless
          * of this flag.
          */
      protected:
         virtual void setFocusEventEnabled(bool enable)throws();

         /**
          * Are key events enabled. The method processKeyEvent()
          * will only be invoked if this method returns true;
          * @return true if this component reacts to key events.
          */
      public:
         bool isKeyEventEnabled() const throws();

         /**
          * Enable or disable key event processing. The method
          * processKeyEvent() is only called if key events
          * are enabled. KeyListeners are still dispatched regardless
          * of this flag.
          */
      protected:
         virtual void setKeyEventEnabled(bool enable)throws();

         /**
          * Are crossing events enabled. The method processCrossingEvent()
          * will only be invoked if this method returns true;
          * @return true if this component reacts to crossing events.
          */
      public:
         bool isCrossingEventEnabled() const throws();

         /**
          * Enable or disable crossing event processing. The method
          * processCrossingEvent() is only called if crossing events
          * are enabled. CrossingListeners are still dispatched regardless
          * of this flag.
          */
      protected:
         virtual void setCrossingEventEnabled(bool enable)throws();

         /**
          * Process a motion event. This method is only invoked if the corresponding
          * event type is enabled. This method does nothing by default.
          * @param event a motion event
          */
      protected:
         virtual void processMotionEvent(const ::timber::Reference< MotionEvent>& event)throws();

         /**
          * Process a wheel event.
          * This method is only invoked if the corresponding
          * event type is enabled. This method does nothing by default.
          * @param event a wheel event
          */
      protected:
         virtual void processWheelEvent(const ::timber::Reference< WheelEvent>& event)throws();

         /**
          * Process a button event.
          * This method is only invoked if the corresponding
          * event type is enabled. This method does nothing by default.
          * @param event a button event
          */
      protected:
         virtual void processButtonEvent(const ::timber::Reference< ButtonEvent>& event)throws();

         /**
          * Process a key event.
          * This method is only invoked if the corresponding
          * event type is enabled. This method does nothing by default.
          * @param event a key event
          */
      protected:
         virtual void processKeyEvent(const ::timber::Reference< KeyEvent>& event)throws();

         /**
          * Process a focus event.
          * This method is only invoked if the corresponding
          * event type is enabled. This method does nothing by default.
          * @param event a focus event
          */
      protected:
         virtual void processFocusEvent(const ::timber::Reference< FocusEvent>& event)throws();

         /**
          * Process a crossing event.
          * This method is only invoked if the corresponding
          * event type is enabled. This method does nothing by default.
          * @param event a crossing event
          */
      protected:
         virtual void processCrossingEvent(const ::timber::Reference< CrossingEvent>& event)throws();

         /*@}*/

         /**
          * Process a component event.
          * @param id an event id
          */
      private:
         void generateComponentEvent(Int id)throws();

         /**
          * @name Property changes
          * @{
          */

         /** The font has changed */
      protected:
         virtual void fontChanged()throws();

         /** The background color has changed */
      protected:
         virtual void backgroundColorChanged()throws();

         /** The foreground color has changed */
      protected:
         virtual void foregroundColorChanged()throws();

         /** Get the log */
      protected:
         inline ::timber::logging::Log& log() const throws()
         {  return _log;}

         /*@}*/

         /**
          * Select a new event for processing by this component.
          * @param eventClass the event class
          * @param bool true to enable the event, false to disable
          */
      private:
         void selectInputEvent(Int eventClass, bool enabled)throws();

         /**
          * Set the new event mask. The mask is set to
          * 0 if there is currently no peer.
          * @param mask the new mask
          */
      private:
         void setEventMask(Int mask)throws();

         /**
          * Update the event mask and the call
          * setEventMask with the computed mask.
          */
      private:
         void updateEventMask()throws();

         /**
          * Update the peer insets.
          * @param nInsets the new insets
          */
      private:
         void updatePeerInsets(const Insets& nInsets)throws();

         /**
          * @name Managing component lifetime.
          * For all its lifetime, a component is part of a component list
          * which is sorted by <code>_refCount</code> and <code>_parent</code>.
          * Objects which have a reference count of 0 and no parent are at
          * the front of the list.
          * @{
          */

         /** The reference count */
      private:
         ::timber::Reference< RefCountType> _refCount;

         /** A pointer to the next component in some list */
      private:
         mutable volatile const Component* _nextComponent;

         /** A pointer to the previous component in some list */
      private:
         mutable volatile const Component* _previousComponent;

         /** The parent component */
      private:
         mutable Component* _parent;

         /** The children of this component */
      private:
         mutable Component* _children;

         /*@}*/

         /** The peer component */
      private:
         mutable PeerComponent* _peer;

         /** True if this component must be laid out again */
      private:
         mutable bool _needLayout;

         /** The name for this component */
      private:
         ::std::string _name;

         /** The component bounds */
      private:
         Bounds _bounds;

         /** The peer insets (usually: 0,0,0,0) */
      private:
         Insets _peerInsets;

         /** The foreground and background colors */
      private:
         Color _foreground, _background;

         /** True if the foreground color should be inherited */
      private:
         bool _inheritForegroundColor;

         /** True if the background color should be inherited */
      private:
         bool _inheritBackgroundColor;

         /** The font */
      private:
         ::timber::Pointer< ::indigo::Font> _font;

         /** True if the font should be inherited */
      private:
         bool _inheritFont;

         /** True if this component is enabled */
      private:
         bool _enabled;

         /** The visibility */
      private:
         bool _visible;

         /** The event observer */
      private:
         EventObserver* _observer;

         /** The set of enabled input events */
      private:
         Int _inputEventMask;

         /**
          * The set of input events selected for processing
          * via the set*EventEnabled() methods.
          */
      private:
         Int _selectedInputEventMask;

         /**
          * @name The various event dispatchers
          * @{
          */
      private:
         EventListeners< ::nexus::event::ComponentEventListener> _componentListeners;
         EventListeners< ::nexus::event::WheelEventListener> _wheelListeners;
         EventListeners< ::nexus::event::MotionEventListener> _motionListeners;
         EventListeners< ::nexus::event::ButtonEventListener> _buttonListeners;
         EventListeners< ::nexus::event::KeyEventListener> _keyListeners;
         EventListeners< ::nexus::event::FocusEventListener> _focusListeners;
         EventListeners< ::nexus::event::CrossingEventListener> _crossingListeners;
         /*@}*/

         /** The component log */
      private:
         mutable ::timber::logging::Log _log;
   };
}

inline ::std::ostream& operator<<(::std::ostream& out, const ::nexus::Component& c)
throws()
{  return c.print(out,false,0);}

#endif
