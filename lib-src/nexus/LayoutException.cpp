#include <nexus/LayoutException.h>

namespace nexus {


  LayoutException::LayoutException() throws()
  : ::std::runtime_error("Layout failed")
  {}

  LayoutException::LayoutException(const ::std::string& msg) throws()
  : ::std::runtime_error(msg)
  {}
}
