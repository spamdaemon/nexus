#include <nexus/Insets.h>
#include <iostream>

namespace nexus {
  Insets::Insets () throws() 
  : _left(0),_right(0),_top(0),_bottom(0)
  {}
  
  Insets::Insets (Int s) throws() 
  : _left(s),_right(s),_top(s),_bottom(s)
  {
    assert(s>=0);
  }
  
  Insets::Insets (Int lw, Int rw, Int th, Int bh) throws()
  : _left(lw),_right(rw),
    _top(th),_bottom(bh)
  {
    assert(lw>=0);
    assert(rw>=0);
    assert(th>=0);
    assert(bh>=0);
  }
  
  Insets::Insets (const Insets& i, const Insets& j) throws()
  : _left(i.left()+j.left()),_right(i.right()+j.right()),
    _top(i.top()+j.top()),_bottom(i.bottom()+j.bottom())
  {}
  
}

::std::ostream& operator<< (::std::ostream& out, const ::nexus::Insets& b) throws()
{
  out << b.left() 
      << ", "<<b.right() 
      << ", "<<b.top()
      <<", "<<b.bottom();
  return out;
}  
