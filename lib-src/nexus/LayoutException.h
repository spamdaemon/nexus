#ifndef _GPS_SCIENCE_GEOM_LAYOUTEXCEPTION_H
#define _GPS_SCIENCE_GEOM_LAYOUTEXCEPTION_H

#ifndef _NEXUS_H
#include <nexus/nexus.h>
#endif

#include <string>

namespace nexus {
  /**
   * This exception is thrown by the layout class
   * whenever an error is encountered when computing
   * a layout. Most of the time this will be due to
   * the size being too small to layout the components.
   */
  class LayoutException : public ::std::runtime_error {
    
    /**
     * Create a new layout exception
     */
  public:
    LayoutException() throws();
    
    /**
     * Create a new layout exception
     */
  public:
    LayoutException(const ::std::string& msg) throws();

    /** Destructor */
  public:
    ~LayoutException() throw() {}

};
}

#endif
