#ifndef _NEXUS_H
#define _NEXUS_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

/**
 * The nexus namespace provides an abstract windowing toolkit.
 */
namespace nexus {    

  /** The types from timber that are used */
  using ::timber::UInt;
  using ::timber::Int;
  
  using ::timber::ULong;
  using ::timber::Long;

  using ::timber::UShort;
  using ::timber::Short;

  using ::timber::UByte;
  using ::timber::Byte;

  /** The index type */
  typedef Int Index;

  inline Index invalidIndex() { return -1; }

  inline double sqr(double x) { return x*x; }


  /**
   * Inclusively clamp a value to within a range.
   * @param v the value to be clamped
   * @param lo the low value
   * @param hi the high value
   * @pre !(hi<lo)
   * @return a value v such that lo <= v <= hi
   */
  template <class T>
    inline T clamp (const T& v, const T& lo, const T& hi) throws()
    { 
      assert(!(hi<lo));
      if (v<lo) {
	return lo;
      }
      else if (v>hi) {
	return hi;
      }
      return v;
    }

}
#endif

