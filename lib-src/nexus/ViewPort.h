#ifndef _NEXUS_VIEWPORT_H
#define _NEXUS_VIEWPORT_H

#ifndef _INDIGO_VIEW_VIEWPORT_H
#include <indigo/view/ViewPort.h>
#endif

#ifndef _NEXUS_PIXEL_H
#include <nexus/Pixel.h>
#endif

#ifndef _NEXUS_SIZE_H
#include <nexus/Size.h>
#endif

#include <cstdint>

namespace nexus {
   /**
    * The ViewPort is used to define how a 2D graphics plane (or model) is mapped into window coordinates.
    * The coordinates are all doubles, but this class ensures that all points are integers.
    */
   class ViewPort : public ::indigo::view::ViewPort
   {
         /**
          * Create a default view port. The view port has a size of 1x1 pixel, a scale of 1.
          *
          */
      public:
         ViewPort()throws();

         /** Destructor */
      public:
         ~ViewPort()throws();

         /**
          * @name OverridingMethods
          * These are the methods that need to be overriden to support a mapping of the bounds region to window coordinates.
          * @{
          */
      public:
         ::indigo::AffineTransform2D getModelViewTransform() const throws();
         ::indigo::AffineTransform2D getViewModelTransform() const throws();
         void pixel2point(double px, double py, double& outx, double& outy) const throws();
         void point2pixel(double px, double py, double& outx, double& outy) const throws();
         ::indigo::view::ViewPort::Pixel centerPixel() const throws();

         bool checkViewPort(const ::indigo::view::ViewPort::Bounds& newBounds,
               const ::indigo::view::ViewPort::Window& newView) const throws();

   };
}

#endif
