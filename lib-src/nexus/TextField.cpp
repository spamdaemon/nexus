#include <nexus/TextField.h>
#include <nexus/event/ActionEvent.h>

namespace nexus {
  const char TextField::DEFAULT_SIZE_STRING[] = { "Hello, World!" };
  
  TextField::TextField () throws (::std::exception)
  : _observer(0),
    _inputSize(0),
    _minSizeString(DEFAULT_SIZE_STRING),
    _preferredSizeString(DEFAULT_SIZE_STRING)
  {
  }
      
  TextField::TextField (const TextModel& m) throws (::std::exception)
  : TextComponent(m), 
    _observer(0),
    _inputSize(0),
    _minSizeString(DEFAULT_SIZE_STRING),
    _preferredSizeString(DEFAULT_SIZE_STRING)
  {
  }
      
  TextField::TextField (const ::std::string& t) throws (::std::exception)
  : _observer(0),
    _inputSize(0)
  {
    if (t.empty()) {
      _minSizeString=DEFAULT_SIZE_STRING;
      _preferredSizeString=DEFAULT_SIZE_STRING;
    }
    else {
      _minSizeString=t;
      _preferredSizeString=t;
    }
    setText(t);
  }
      
  TextField::~TextField () throws()
  {	delete _observer; }
      
  bool TextField::isMultiLineCapable () const throws()
  { return false; }

  void TextField::setInputSize (Int w) throws()
  {
    _inputSize = w > 0 ? w : 0;
    Int len = textModel()->textLength();
    if (w>0 && len > w) {
      // truncate the text
      textModel()->deleteText(w,len-w);
    }
  }
      
  Size TextField::calculateSize (const ::std::string& s) const throws()
  {
    Size sz;
    // search for a font
    ::timber::Pointer< ::indigo::Font> f = font();
    if (f) { 
      sz = f->stringBounds(s);
    }
	
    if (sz.width()==0 || sz.height()==0) {
      sz = Size(::std::max(sz.width(),1),::std::max(sz.height(),1));
    }
    return sz;
  }

  SizeHints TextField::computeSizeHints () const throws()
  {
    Size prefSize = calculateSize(_preferredSizeString);
    Size minSize = calculateSize(_minSizeString);
    Size maxSize = Size(Size::MAX.width(),minSize.height());
    if (_maxSizeString.length()>0) {
      maxSize = calculateSize(_maxSizeString);
    }
	
    Int preferredWidth = prefSize.width();
    Int minWidth = ::std::min(minSize.width(),::std::min(maxSize.width(), preferredWidth));
    Int maxWidth = ::std::max(minSize.width(),::std::max(maxSize.width(), preferredWidth));
    Int h = ::std::max(minSize.height(),::std::max(maxSize.height(), prefSize.height()));
	
    return SizeHints(Size(minWidth,h),
		     Size(maxWidth,h),
		     Size(preferredWidth,h));
  }
      
    
  void TextField::deriveSizeBounds(const ::std::string& m, const ::std::string& M) throws()
  {
    _maxSizeString = M;
    if (m.empty()==0) {
      _minSizeString = DEFAULT_SIZE_STRING;
    }
    else {
      _minSizeString = m;
    }
    updateSizeHints();
  }

  void TextField::derivePreferredSize (const ::std::string& m) throws()
  {
    if (m.empty()) {
      _preferredSizeString = DEFAULT_SIZE_STRING;
    }
    else {
      _preferredSizeString = m;
    }
	  
    updateSizeHints();
  }

  void TextField::initializePeer () throws()
  {
    struct Observer : public Peer::TextFieldObserver {
      inline Observer (TextField& b) throws() : _owner(b) {}
      ~Observer () throws() {}
      void textFieldActivated () throws()
      { _owner.processActionEvent(new ActionEvent(&_owner,_owner.actionID())); }
	  
    private:
      TextField& _owner;
    };

    TextComponent::initializePeer();
    if (_observer==0) {
      _observer = new Observer(*this);
    }

    textField()->setTextFieldObserver(_observer);
  }

  TextField::PeerComponent* TextField::createPeerComponent (PeerComponent* p) throws()
  {
    assert(p!=0);
    return Desktop::createTextField(*p); 
  }

  void TextField::processActionEvent (const ::timber::Reference<ActionEvent>& e) throws()
  { _actionListeners.notify(&::nexus::event::ActionEventListener::processActionEvent,e); }
      
}

