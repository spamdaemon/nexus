#ifndef _NEXUS_INPUTMODIFIERS_H
#define _NEXUS_INPUTMODIFIERS_H

#ifndef _NEXUS_H
#include <nexus/nexus.h>
#endif

#include <iosfwd>
#include <cassert>

namespace nexus {
  /**
   * This class can represent the state of buttons and special 
   * keys on the keyboard.
   * @see PointerEvent
   * @see CrossingEvent
   */
  class InputModifiers {
	  
    /**
     * @name The various input modifiers 
     * @{
     */
  public:
    static const Int BUTTON1   = 1;
    static const Int BUTTON2   = 2;
    static const Int BUTTON3   = 4;
    static const Int BUTTON4   = 8;
    static const Int BUTTON5   = 16;
    static const Int CONTROL   = 32;
    static const Int CAPS_LOCK = 64;
    static const Int SHIFT     = 128;
    static const Int META1     = 256;
    /*@}*/

    /** The mask of all 1 */
  public:
    static const Int MASK = 
      BUTTON1|BUTTON2|BUTTON3|BUTTON4|BUTTON5|
      CONTROL|CAPS_LOCK|SHIFT|META1;
	  
    /** 
     * Create a default input modifier mask.
     */
  public:
    inline InputModifiers () throws()  : _mask(0) {}

    /** 
     * Create a input modifier mask.
     * @param m a mask
     * @pre REQUIRE_TRUE((m&(~MASK)) == 0)
     */
  public:
    inline InputModifiers (Int m) throws()  
      : _mask(::canopy::checked_cast<UShort>(m)) { assert((m&(~MASK)) == 0); }
    
    /**
     * Combine two input modifier masks.
     * @param m another input modifier mask
     * @return the a new mask
     */
  public:
    inline InputModifiers orWith (const InputModifiers& m) const throws()
    { return InputModifiers(_mask|m._mask); }

    /**
     * Combine two input modifier masks.
     * @param m another input modifier mask
     * @return the a new mask
     */
  public:
    inline InputModifiers andWith (const InputModifiers& m) const throws()
    { return InputModifiers(_mask&m._mask); }

    /**
     * Test if the specified bits are set.
     * @param m the mask to be tested
     * @return true if all bits of the mask are set
     */
  public:
    inline bool isMaskSet (Int m) const throws() { return (_mask & m) == m; }
	  
    /**
     * Test if button 1 is pressed.
     * @return true if button 1 is pressed
     */
  public:
    inline bool isButton1Pressed () const throws() { return isMaskSet(BUTTON1); }

    /**
     * Test if button 2 is pressed.
     * @return true if button 2 is pressed
     */
  public:
    inline bool isButton2Pressed () const throws() { return isMaskSet(BUTTON2); }

    /**
     * Test if button 3 is pressed.
     * @return true if button 3 is pressed
     */
  public:
    inline bool isButton3Pressed () const throws() { return isMaskSet(BUTTON3); }

    /**
     * Test if button 4 is pressed.
     * @return true if button 4 is pressed
     */
  public:
    inline bool isButton4Pressed () const throws() { return isMaskSet(BUTTON4); }

    /**
     * Test if button 5 is pressed.
     * @return true if button 5 is pressed
     */
  public:
    inline bool isButton5Pressed () const throws() { return isMaskSet(BUTTON5); }
	  
    /**
     * Test if the control key is pressed.
     * @return true if the control key is pressed
     */
  public:
    inline bool isControlPressed () const throws() { return isMaskSet(CONTROL); }

    /**
     * Test if the caps-lock key is pressed.
     * @return true if the caps-lock key is pressed
     */
  public:
    inline bool isCapsLockPressed () const throws() { return isMaskSet(CAPS_LOCK); }

    /**
     * Test if the shift key is pressed.
     * @return true if the shift key is pressed
     */
  public:
    inline bool isShiftPressed () const throws() { return isMaskSet(SHIFT); }

    /**
     * Test if the meta key is pressed.
     * @return true if the meta key is pressed
     */
  public:
    inline bool isMeta1Pressed () const throws() { return isMaskSet(META1); }
	  
    /** Input modifier mask */
  private:
    UShort _mask;
  };
}

/**
 * Print a string representation of the modifiers to the specified output stream.
 * @param out an output stream
 * @param im input modifiers
 * @return out
 */
::std::ostream& operator<< (::std::ostream& out, const ::nexus::InputModifiers& im);

#endif
