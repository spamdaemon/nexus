#include <nexus/nexus.h>
#include <cctype>
#include <cstdarg>
#include <cstdio>

namespace nexus {


  static Int hashStringIgnoreCase (const char* str, size_t length) throws()
  {
    // this hashfunction was found on the web
    // and seems to be based on g_str_hash()
    Int h = 0;
    const char* a = str;
    const char* e = a + length;
    while (a<e) {
      char c = ::std::tolower(*a++);
      if (c >= 0x60) {
	c -= 40;
      }
      h = (h<<3) + (h>>28) + c;
    }
    return h;
  }

  static Int hashStringIgnoreCase (const char* str) throws()
  {
    // this hashfunction was found on the web
    // and seems to be based on g_str_hash()
    Int h = 0;
    const char* a = str;
    while (*a!='\0') {
      char c = ::std::tolower(*a++);
      if (c >= 0x60) {
	c -= 40;
      }
      h = (h<<3) + (h>>28) + c;
    }
    return h;
  }

  static bool createFormattedString (const char* fmt, va_list argv, ::std::string& res) throws()
  {
    if (fmt==0) {
      res.clear();
      return true;
    }

    // make a guess, which should be large enough for most strings
    char tmp[256];
    Int sz = 256;
    char* str = tmp;
    char* xstr = 0;

    do {
      Int n;


      // try a print of the arguments
      // we do this in a separate block, because of the use
      // of va_list, and va_end, which may introduce braces.
      {
	va_list ap;
	// for portability reasons, we need to call va_copy
	// with a matching va_end() call
	va_copy(ap,argv);
	n = ::std::vsnprintf(str,sz,fmt,ap);
	va_end(ap);
      }

      if (n>=0) {
	if (n<sz) {
	  res = ::std::string(str,0,n);
	  ::std::free(xstr);
	  return true;
	}
	sz = n+1;
      }
      else {
	::std::free(xstr);
	res.clear();
	return false;
      }
      xstr = (char*)::std::realloc(xstr,sz);
      str  = xstr;
    } while (str!=0);
    
    return false;
  }

  static ::std::string formatString (const char* fmt, ...) throws (::std::exception)
  {
    bool ok = true;
    {
      ::std::string tmp;
      if (fmt!=0) {
	va_list ap;
	va_start(ap,fmt);
	ok = createFormattedString(fmt,ap,tmp);
	va_end(ap);
      }
      if (ok) {
	return tmp;
      }
    }
    throw ::std::runtime_error("Could not format string");
  }

  static Int matchPattern (const char* s, const char* t) throws()
  {
    if (*t == '\0') {
      return -1;
    }
    
    const char* first = s;
    const char* a = s;
    const char* b = t;
    while (*a != '\0' && *b!= '\0') {
      if (*a == *b) {
	++a;
	++b;
      }
      else {
	a = ++s;
	b = t;
      }
    }
    return (*b=='\0') ? static_cast<Int>(s-first) : -1;
  }
}
