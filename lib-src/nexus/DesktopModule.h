#ifndef _NEXUS_DESKTOPMODULE_H
#define _NEXUS_DESKTOPMODULE_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

namespace nexus {
  namespace peer {
    class Desktop;
  }

  /**
   * A DesktopModule contains pointers to function to instantiate 
   * the peer desktop.
   */
  struct DesktopModule {
    
    /** 
     * Create a new peer desktop. If this function returns -1 on the first
     * invokation, then other invocations may be attempted until this function
     * returns 0. Once this function returns 0, all subsequenent invocations must
     * return 1.
     * @param argc the number of arguments
     * @param argv the argument vector
     * @return 0 if there was no error, 1 if this function was already called, -1 if getPeerDesktop() returns 0
     */
    typedef int (*InitializePeerDesktop)(int argc, const char** argv);
    
    /** 
     * Create a new peer desktop. If this function is invoked more
     * than once, then 0 is returned.
     * @return a desktop or 0 if none was created.
     */
    typedef ::timber::Pointer< ::nexus::peer::Desktop> (*GetPeerDesktop)();
    
    /** 
     * Shutdown this peer desktop. This function may or may not be invoked.
     */
    typedef void (*ShutdownPeerDesktop)();

    /**
     * The create desktop function
     */
    InitializePeerDesktop initialize;

    /**
     * Get the peer desktop.
     */
    GetPeerDesktop get;
    
    /**
     * The shutdown function 
     */
    ShutdownPeerDesktop shutdown;
  };

  /** 
   * Load and initialize the desktop module at the given path. The module must export
   * a pointer to a desktop module structure under the c-name <em>peerModule</em>.
   * A module should follow the following template:
   * @code
   * #include <nexus/peer/Desktop.h>
   *
   * namespace {
   *   static ::nexus::peer::Desktop* _desktop = 0;
   *  
   *   static int initPeer (int argc, const char** argv)
   *   {
   *     ::timber::Pointer< ::nexus::peer::Desktop>  dt = ...; // instantiate the desktop
   *     dt->incrementCount();  // make sure to reference the desktop
   *     _desktop = &*dt;
   *     return 0;
   *   }
   *  
   *   static ::timber::Pointer< ::nexus::peer::Desktop> getPeer()
   *   { return _desktop; }
   * 
   *   static void shutdownPeer()
   *   {
   *      // do whatever; not currently used
   *   }
   * }
   *
   * // since we need to export the peerModule as a C-name, we need to extern "C" it
   * extern "C" {
   *  // initialize the structure that we need to export. 
   *   ::nexus::DesktopModule peerModule = {
   *       initPeer,
   *       getPeer,
   *       shutdownPeer
   *   };
   * }
   * @endcode
   * @param path the desktop module path
   * @param argc the number of arguments in argv
   * @param argv an argument vector (same format as the one passed to main())
   * @return a reference to a desktop module file
   * @throws std::exception if the module could not be loaded properly
   */
  DesktopModule& loadDesktopModule (const ::std::string& path, int& argc, const char** argv) throws (::std::exception);
}
#endif
