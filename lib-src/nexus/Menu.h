#ifndef _NEXUS_MENU_H
#define _NEXUS_MENU_H

#ifndef _NEXUS_COMPONENT_H
#include <nexus/Component.h>
#endif

#ifndef _NEXUS_PEER_MENU_H
#include <nexus/peer/Menu.h>
#endif

#ifndef _NEXUS_ICON_H
#include <nexus/Icon.h>
#endif

namespace nexus {
  class MenuItem;

  /**
   * The Menu is attached to a window and serves as the hook for adding menu items
   * to it. Typically, a menu is instantiated by a Window.
   */
  class Menu : public Component {
    Menu(const Menu&);
    Menu&operator=(const Menu&);
	
    /** The implementation class */
  public:
    typedef ::nexus::peer::Menu Peer;
    
    /** 
     * Default constructor.
     * @throw ConfigurationException if the desktop has not been configured
     */
  public:
    Menu () throws (::std::runtime_error);
    
    /** 
     * Constructor with a name.
     * @param nm the name of the menu
     * @throw ConfigurationException if the desktop has not been configured
     */
  public:
    Menu (const ::std::string& nm) throws (::std::runtime_error);
    
    /** 
     * Constructor with an icon.
     * @param i an icon
     * @throw ConfigurationException if the desktop has not been configured
     */
  public:
    Menu (const Icon& i) throws (::std::runtime_error);
    
    /** Destroy this menu and release all its resources */
  public:
    ~Menu () throws();
    
    /**
     * Get the text for this button.
     * @return the text for this button or 0 if no text is used.
     */
  public:
    inline const ::std::string& text() const throws() 
    { return _text; }

    /**
     * Set the text for this button. Sets the icon to 0.
     * @param t the text for this button or 0
     */
  public:
    virtual void setText (const ::std::string& t) throws();
	
    /**
     * Get the icon for this button.
     * @return the icon for this button or an icon with no graphic.
     */
  public:
    inline Icon icon() const throws() 
    { return _icon; }
	
    /**
     * Set the icon for this button. Sets the text
     * to 0.
     * @param i the icon for this button
     */
  public:
    virtual void setIcon (const Icon& i) throws();

    /**
     * Remove the specified item on this menu.
     * @param menu the menu to be removed.
     */
  public:
    void remove (::timber::Reference<Component> menu) throws();
    
    /**
     * @name Submenus
     * @{
     */
    /**
     * Add a new menu to this bar.
     * @param menu the new menu
     */
  public:
    void addMenu(::timber::Reference<Menu> menu) throws();
    
    /**
     * Add a new menu to this bar.
     * @param name the name of the new menu
     * @return the menu
     */
  public:
    ::timber::Reference<Menu> addMenu(const ::std::string& name) throws();
    
    /**
     * Add a new menu to this bar.
     * @param name the name of the new menu
     * @return the menu
     */
  public:
    ::timber::Reference<Menu> addMenu(const char* name) throws();
    
    /*@}*/

    /**
     * @name Menu items
     * @{
     */

    /**
     * Add a new menu to this bar.
     * @param item a menu item
     */
  public:
    void addMenuItem(::timber::Reference<MenuItem> item) throws();
    
    /**
     * Add a new menu to this bar.
     * @param name the name of the new menu
     * @return the menu item
     */
  public:
    ::timber::Reference<MenuItem> addMenuItem(const ::std::string& name) throws();
    
    /**
     * Add a new menu to this bar.
     * @param name the name of the new menu
     * @return the menu item
     */
  public:
    ::timber::Reference<MenuItem> addMenuItem(const char* name) throws();
    /*@}*/
   
    /**
     * Get the menu peer.
     * @return the widow peer
     */
  private:
    inline Peer* menu() const throws()
    { return impl<Peer>(); }
    
  protected:
    void initializePeer() throws();
    PeerComponent* createPeerComponent (PeerComponent* p) throws();
    SizeHints getSizeHints() const throws();

    /** The observer */
  private:
    Peer::Observer* _observer;

    /** The text for this button */
  private:
    ::std::string _text;

    /** The icon for this button */
  private:
    Icon _icon;
	
    /** The menus */
  private:
    ::std::vector<Component*> _menus;
  };
}

#endif

