#include <nexus/ProxyComponent.h>
#include <nexus/peer/Container.h>
#include <nexus/event/ComponentEvent.h>
#include <nexus/event/MotionEvent.h>
#include <nexus/event/ButtonEvent.h>
#include <nexus/event/WheelEvent.h>
#include <nexus/event/KeyEvent.h>
#include <nexus/event/FocusEvent.h>
#include <nexus/event/CrossingEvent.h>
#include <iostream>

using namespace ::timber;

namespace nexus {
   class ProxyComponent::EventListener : public ::nexus::event::MotionEventListener,
         public ::nexus::event::ButtonEventListener,
         public ::nexus::event::WheelEventListener,
         public ::nexus::event::KeyEventListener,
         public ::nexus::event::FocusEventListener,
         public ::nexus::event::CrossingEventListener,
         public ::nexus::event::ComponentEventListener
   {

      public:
         inline EventListener(ProxyComponent& proxy)throws()
         : _proxy(proxy) {}

         ~EventListener()throws() {}
         void processMotionEvent(const Reference< ::nexus::event::MotionEvent>& e)throws()
         {
            const Int px = _proxy._child.x()+e->x();
            const Int py = _proxy._child.y()+e->y();
            const Reference< ::nexus::event::MotionEvent> xe(new ::nexus::event::MotionEvent(&_proxy,px,py,e->when(),e->modifiers()));
            _proxy._motionListeners.notify(&::nexus::event::MotionEventListener::processMotionEvent,xe);
         }
         void processWheelEvent(const Reference< ::nexus::event::WheelEvent>& e)throws()
         {
            const Reference< ::nexus::event::WheelEvent> xe(new ::nexus::event::WheelEvent(&_proxy,e->id(),e->when(),e->modifiers()));
            _proxy._wheelListeners.notify(&::nexus::event::WheelEventListener::processWheelEvent,xe);
         }
         void processButtonEvent(const Reference< ::nexus::event::ButtonEvent>& e)throws()
         {
            const Int px = _proxy._child.x()+e->x();
            const Int py = _proxy._child.y()+e->y();
            const Reference< ::nexus::event::ButtonEvent> xe(new ::nexus::event::ButtonEvent(&_proxy,e->id(),px,py,e->button(),e->when(),e->modifiers()));
            _proxy._buttonListeners.notify(&::nexus::event::ButtonEventListener::processButtonEvent,xe);
         }
         void processKeyEvent(const Reference< ::nexus::event::KeyEvent>& e)throws()
         {
            const Reference< ::nexus::event::KeyEvent> xe(new ::nexus::event::KeyEvent(&_proxy,e->id(),e->when(),e->modifiers(),e->key()));
            _proxy._keyListeners.notify(&::nexus::event::KeyEventListener::processKeyEvent,xe);
         }
         void processFocusEvent(const Reference< ::nexus::event::FocusEvent>& e)throws()
         {
            const Reference< ::nexus::event::FocusEvent> xe(new ::nexus::event::FocusEvent(&_proxy,e->id()));
            _proxy._focusListeners.notify(&::nexus::event::FocusEventListener::processFocusEvent,xe);
         }
         void processCrossingEvent(const Reference< ::nexus::event::CrossingEvent>& e)throws()
         {
            const Reference< ::nexus::event::CrossingEvent> xe(new ::nexus::event::CrossingEvent(&_proxy,e->id(),e->when(),e->modifiers()));
            _proxy._crossingListeners.notify(&::nexus::event::CrossingEventListener::processCrossingEvent,xe);
         }
         void processComponentEvent(const Reference< ::nexus::event::ComponentEvent>& e)throws()
         {
            const Reference< ::nexus::event::ComponentEvent> xe(new ::nexus::event::ComponentEvent(&_proxy,e->id()));
            _proxy._componentListeners.notify(&::nexus::event::ComponentEventListener::processComponentEvent,xe);
         }

      private:
         ProxyComponent& _proxy;
   };

   ProxyComponent::ProxyComponent(const Reference< Component>& c)
   throws (::std::runtime_error)
   : _child(*attachChild(c)),
   _proxyListener(new EventListener(*this))

   {}

   ProxyComponent::~ProxyComponent()
   throws()
   {
      _child.removeMotionEventListener(_proxyListener);
      _child.removeButtonEventListener(_proxyListener);
      _child.removeKeyEventListener(_proxyListener);
      _child.removeFocusEventListener(_proxyListener);
      _child.removeCrossingEventListener(_proxyListener);
      detachChild(&_child);
   }

   void ProxyComponent::createChildPeers()
   throws()
   {
      Component::createChildPeers();
      if (_child.hasPeer()) {
         impl<Peer>()->setChildBounds(*implOf(_child),_child.bounds());
      }
   }

   ProxyComponent::PeerComponent* ProxyComponent::createPeerComponent (PeerComponent* p) throws()
   {
      assert(p!=0);
      return Desktop::createContainer(*p);
   }

   void ProxyComponent::doLayout()
throws(LayoutException)
{
   const Size sz(layoutSize());
   setChildBounds<Peer>(_child,Bounds(0,0,sz.width(),sz.height()));
}

SizeHints ProxyComponent::getSizeHints() const
throws()
{  return _child.sizeHints();}

::std::ostream& ProxyComponent::print(::std::ostream& out, bool deep, Int indent) const
throws()
{
   Component::print(out,deep,indent);
   ::std::string indentString;
   for (Index i=0;i<indent;++i) {
      indentString += "    ";
   }

   if (deep) {
      out << indentString << "{\n";
      _child.print(out,true,indent+1);
      out << indentString << "}\n";
   }
   return out << ::std::flush;
}

void ProxyComponent::addComponentEventListener(const Reference< ::nexus::event::ComponentEventListener>& l)
throws()
{
   size_t n = _componentListeners.listenerCount();
   _componentListeners.addListener(l);
   if (n==0 && _componentListeners.listenerCount()==1) {
      _child.addComponentEventListener(_proxyListener);
   }

}

void ProxyComponent::removeComponentEventListener(const Pointer< ::nexus::event::ComponentEventListener>& l)
throws()
{
   size_t n = _componentListeners.listenerCount();
   _componentListeners.removeListener(l);
   if (n==1 && _componentListeners.listenerCount()==0) {
      _child.removeComponentEventListener(_proxyListener);
   }
}

void ProxyComponent::addMotionEventListener(const Reference< ::nexus::event::MotionEventListener>& l)
throws()
{
   size_t n = _motionListeners.listenerCount();
   _motionListeners.addListener(l);
   if (n==0 && _motionListeners.listenerCount()==1) {
      _child.addMotionEventListener(_proxyListener);
   }
}

void ProxyComponent::removeMotionEventListener(const Pointer< ::nexus::event::MotionEventListener>& l)
throws()
{
   size_t n = _motionListeners.listenerCount();
   _motionListeners.removeListener(l);
   if (n==1 && _motionListeners.listenerCount()==0) {
      _child.removeMotionEventListener(_proxyListener);
   }
}

void ProxyComponent::addWheelEventListener(const Reference< ::nexus::event::WheelEventListener>& l)
throws()
{
   size_t n = _wheelListeners.listenerCount();
   _wheelListeners.addListener(l);
   if (n==0 && _wheelListeners.listenerCount()==1) {
      _child.addWheelEventListener(_proxyListener);
   }
}

void ProxyComponent::removeWheelEventListener(const Pointer< ::nexus::event::WheelEventListener>& l)
throws()
{
   size_t n = _wheelListeners.listenerCount();
   _wheelListeners.removeListener(l);
   if (n==1 && _wheelListeners.listenerCount()==0) {
      _child.removeWheelEventListener(_proxyListener);
   }
}

void ProxyComponent::addButtonEventListener(const Reference< ::nexus::event::ButtonEventListener>& l)
throws()
{
   size_t n = _buttonListeners.listenerCount();
   _buttonListeners.addListener(l);
   if (n==0 && _buttonListeners.listenerCount()==1) {
      _child.addButtonEventListener(_proxyListener);
   }
}

void ProxyComponent::removeButtonEventListener(const Pointer< ::nexus::event::ButtonEventListener>& l)
throws()
{
   size_t n = _buttonListeners.listenerCount();
   _buttonListeners.removeListener(l);
   if (n==1 && _buttonListeners.listenerCount()==0) {
      _child.removeButtonEventListener(_proxyListener);
   }
}

void ProxyComponent::addFocusEventListener(const Reference< ::nexus::event::FocusEventListener>& l)
throws()
{
   size_t n = _focusListeners.listenerCount();
   _focusListeners.addListener(l);
   if (n==0 && _focusListeners.listenerCount()==1) {
      _child.addFocusEventListener(_proxyListener);
   }
}

void ProxyComponent::removeFocusEventListener(const Pointer< ::nexus::event::FocusEventListener>& l)
throws()
{
   size_t n = _focusListeners.listenerCount();
   _focusListeners.removeListener(l);
   if (n==1 && _focusListeners.listenerCount()==0) {
      _child.removeFocusEventListener(_proxyListener);
   }
}

void ProxyComponent::addKeyEventListener(const Reference< ::nexus::event::KeyEventListener>& l)
throws()
{
   size_t n = _keyListeners.listenerCount();
   _keyListeners.addListener(l);
   if (n==0 && _keyListeners.listenerCount()==1) {
      _child.addKeyEventListener(_proxyListener);
   }
}

void ProxyComponent::removeKeyEventListener(const Pointer< ::nexus::event::KeyEventListener>& l)
throws()
{
   size_t n = _keyListeners.listenerCount();
   _keyListeners.removeListener(l);
   if (n==1 && _keyListeners.listenerCount()==0) {
      _child.removeKeyEventListener(_proxyListener);
   }
}

void ProxyComponent::addCrossingEventListener(const Reference< ::nexus::event::CrossingEventListener>& l)
throws()
{
   size_t n = _crossingListeners.listenerCount();
   _crossingListeners.addListener(l);
   if (n==0 && _crossingListeners.listenerCount()==1) {
      _child.addCrossingEventListener(_proxyListener);
   }
}

void ProxyComponent::removeCrossingEventListener(const Pointer< ::nexus::event::CrossingEventListener>& l)
throws()
{
   size_t n = _crossingListeners.listenerCount();
   _crossingListeners.removeListener(l);
   if (n==1 && _crossingListeners.listenerCount()==0) {
      _child.removeCrossingEventListener(_proxyListener);
   }
}
}

