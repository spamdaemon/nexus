#ifndef _NEXUS_RUNNABLE_H
#define _NEXUS_RUNNABLE_H

#ifndef _TIMBER_SCHEDULER_RUNNABLE_H
#include <timber/scheduler/Runnable.h>
#endif

namespace nexus {
  
  /**
   * A runnable is used when a function must be executed in the Event thread
   */
   typedef ::timber::scheduler::Runnable Runnable;
}

#endif
