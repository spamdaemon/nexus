#ifndef _NEXUS_CANVAS_H
#define _NEXUS_CANVAS_H

#ifndef _NEXUS_COMPONENT_H
#include <nexus/Component.h>
#endif

#ifndef _NEXUS_PEER_CANVAS_H
#include <nexus/peer/Canvas.h>
#endif

#ifndef _INDIGO_NODE_H
#include <indigo/Node.h>
#endif

#ifndef _INDIGO_PICKINFO_H
#include <indigo/PickInfo.h>
#endif

namespace nexus {
  
    

  /**
   * This abstract components defines a desktop component.
   * Desktop components cannot have parents. 
   * @note Most likely it is the Viewer2D class that is wanted as 
   * it provides more functionality than the blank canvas.
   * 
   */
  class Canvas : public Component {
    
    /** The structure returns to whoever is picking at a pixel */
  public:
    struct PickInfo {
      /** The pixel (x,y) that was picked */
      Pixel pixel;
      
      /** The node that was picked (null if none was picked) */
      ::timber::Pointer< ::indigo::PickInfo> info;
      
      /** The node that was picked (null if none was picked) */
      ::timber::Pointer< ::indigo::Node> node;
      
      /** The index of the shape that was picked (undefined if node is null) */
      size_t shape;
    };

    /** The implementation class */
  public:
    typedef ::nexus::peer::Canvas Peer;

    /**
     * Create a double-buffered canvas which is automatically redrawn
     * whenever the graphic changes.
     * @throw ConfigurationException if the desktop has not been configured
     */
  public:
    Canvas () throws (::std::runtime_error);
	    
    /** Destroy this desktop component. */
  public:
    ~Canvas () throws();

    /**
     * Get the window peer.
     * @return the widow peer
     */
  private:
    inline Peer* canvas() const throws()
    { return impl<Peer>(); }

    /**
     * Set the size hints for this canvas.
     * @param hints the size hints
     */
  public:
    void setSizeHints (const SizeHints& hints) throws();

  protected:
    SizeHints getSizeHints () const throws();

    /**
     * Set the graphic to be displayed in this canvas.
     * @param gfx a graphic.
     */
  public:
    virtual void setGraphic (const  ::timber::Pointer< ::indigo::Node>& gfx) throws();

    /**
     * Get the current graphic.
     * @return the current graphic
     */
  public:
    ::timber::Pointer< ::indigo::Node> graphic () const throws()
      { return _gfx; }

    /**
     * Enable automatic rendering.
     * @param enabled if true, then enable automatic rendering
     */
  public:
    virtual void setAutoRenderEnabled (bool enabled) throws();

    /**
     * Test if automatic rendering is enabled.
     * @return true if automatic rendering is enabled.
     */
  public:
    inline bool isAutoRenderEnabled () const throws()
    { return _autoRender; }

    /**
     * Enable or disable double buffering. This is merely a hint
     * and need not be respected; if resource allocation should 
     * fail for double buffering, then rendering will be done
     * with singe-buffering.<br>
     * @param enabled true if double buffering should be enabled.
     */
  public:
    virtual void setDoubleBufferEnabled (bool enabled) throws();
	
    /**
     * Test if double buffering is enabled. If this method 
     * returns true, then this does not necessarily mean that
     * double buffering is enabled in the display hardware/software.
     * @return true if double buffering should be used by the display software
     */
  public:
    inline bool isDoubleBufferEnabled () const throws()
    { return _doubleBuffered; }
    
    /**
     * Pick the graphic at the specified compoenent coordinates
     * @param px a window pixel
     * @return a pick info object
     */
  public:
    ::std::unique_ptr<PickInfo> pickNode (const Pixel& px) const throws();

    
    /** Render the graphic now. */
  public:
    void render() const throws();

  protected:
    void initializePeer () throws();
    PeerComponent* createPeerComponent (PeerComponent* p) throws();

    /** The size hints */
  private:
    SizeHints _hints;

    /** The graphic */
  private:
    ::timber::Pointer< ::indigo::Node> _gfx;
	
    /** True if graphics are automatically rendered */
  private:
    bool _autoRender;

    /** True if double buffering should be used by the display software */
  private:
    bool _doubleBuffered;
  };
}

#endif
