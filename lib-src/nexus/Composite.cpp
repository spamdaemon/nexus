#include <nexus/Composite.h>
#include <nexus/peer/Container.h>
#include <timber/logging.h>

#include <iostream>

using namespace ::timber::logging;

namespace nexus {
   namespace {
      static Log getLog()
      {
         return Log("nexus.Composite");
      }
   }

   Composite::Composite(::std::unique_ptr< LayoutEngine> engine)
   throws (::std::exception) :
   _layout(::std::move(engine)), _interceptEvents(false), _border(Border::createNullBorder()),
   _doubleBuffered(false)
   {
      class LayoutObserver : public LayoutEngine::Observer
      {
         public:
         ~LayoutObserver() throws()
         {
         }
         inline LayoutObserver(Composite& c) throws() :
         _component(c)
         {
         }
         void layoutChanged(LayoutEngine&) throws()
         {
            _component.revalidate();
         }
         private:
         Composite& _component;
      };

      if (_layout.get() == 0) {
         throw ::std::invalid_argument("No layout");
      }
      LayoutEngine::Observer* obs = new LayoutObserver(*this);
      _layout->addObserver(obs);
   }

   Composite::~Composite()
   throws()
   {
      _borderGraphic = nullptr;
      _bgGraphic = nullptr;
      _layout.reset(0);

      for (Index i = 0; i < childCount(); ++i) {
         detachChild(child(i));
      }
   }

   void Composite::initializePeer()
   throws()
   {
      Component::initializePeer();
      impl< Peer> ()->setInterceptEventsEnabled(_interceptEvents);
      impl< Peer> ()->setBackgroundGraphic(_bgGraphic);
      impl< Peer> ()->setDoubleBufferEnabled(_doubleBuffered);
      revalidateBorder(false);
   }

   void Composite::createChildPeers()
   throws()
   {
      Component::createChildPeers();
      for (Index i = 0; i < childCount(); ++i) {
         Component& c = *_children[i];
         if (c.hasPeer()) {
            impl< Peer> ()->setChildBounds(*implOf(c), c.bounds());
         }
      }
   }

   Composite::PeerComponent* Composite::createPeerComponent(PeerComponent* p) throws()
   {
      assert(p != 0);
      return Desktop::createContainer(*p);
   }

   Bounds Composite::contentBounds() const
   throws()
   {
      Int offx = _whitespace.left();
      Int offy = _whitespace.top();

      const Size cSize(layoutSize());

      Int w = cSize.width();
      Int h = cSize.height();
      w -= _whitespace.left() + _whitespace.right();
      h -= _whitespace.top() + _whitespace.bottom();
      w -= _borderInsets.left() + _borderInsets.right();
      h -= _borderInsets.top() + _borderInsets.bottom();
      offx += _borderInsets.left();
      offy += _borderInsets.top();

      return Bounds(offx, offy, w, h);
   }

   void Composite::setInterceptEventsEnabled(bool enable)
   throws()
   {
      if (_interceptEvents != enable) {
         _interceptEvents = enable;
         if (hasPeer()) {
            impl< Peer> ()->setInterceptEventsEnabled(_interceptEvents);
         }
      }
   }

   Index Composite::indexOfChild(const ::timber::Reference< Component>& c) const
   throws()
   {
      for (Index i = 0; i < childCount(); ++i) {
         if (_children[i] == &*c) {
            return i;
         }
      }
      return invalidIndex();
   }

   void Composite::insertChild(const ::timber::Reference< Component>& c, Index pos)
   throws()
   {
      assert(pos >= 0 && pos <= childCount());
      assert(c->parent() == nullptr);

      _children.reserve(_children.size() + 1);
      Component* pc = attachChild(c);
      if (pos == childCount()) {
         _children.push_back(pc);
      }
      else {
         _children.insert(_children.begin() + pos, pc);
      }
      revalidate();

   }

   ::timber::Reference< Component> Composite::removeChild(Index pos)
   throws()
   {
      ::timber::Reference< Component> c(detachChild(_children[pos]));
      // ref this, because setting the parent could destroy this component
      _children.erase(_children.begin() + pos);
      revalidate();
      return c;
   }

   void Composite::removeChildren()
   throws()
   {
      // guard this component; this is not strictly necessary,
      // but it avoid the case where we repeatedly increase
      // references all the way to the top, and then decrease
      // them.
      while (childCount() > 0) {
         // remove the child and immediately destroy it
         removeChild(childCount() - 1);
      }
   }

   bool Composite::revalidateBorder(bool updateInsets)
   throws()
   {
      if (hasPeer()) {
         _borderGraphic = _border->createGraphic(*this);
         impl< Peer> ()->setBorderGraphic(_border->createGraphic(*this));
      }
      else {
         _borderGraphic = nullptr;
      }
      if (updateInsets) {
         const Insets oldInsets(_borderInsets);
         _borderInsets = _border->insets(*this);
         return _borderInsets != oldInsets;
      }
      return false;
   }

   void Composite::setBorder(::timber::Pointer< Border> b)
   throws()
   {
      // ensure that there is always some border
      if (b == nullptr) {
         b = Border::createNullBorder();
      }

      if (_border != b) {
         _border = b;
         if (revalidateBorder(true)) {
            revalidate();
         }
      }
   }

   void Composite::setWhitespace(const Insets& ws)
   throws()
   {
      if (_whitespace != ws) {
         _whitespace = ws;
         revalidate();
      }
   }

   void Composite::fontChanged()
   throws()
   {
      Component::fontChanged();
      if (revalidateBorder(true)) {
         revalidate();
      }
   }

   void Composite::backgroundColorChanged()
   throws()
   {
      Component::backgroundColorChanged();
      revalidateBorder(false);
   }

   void Composite::foregroundColorChanged()
   throws()
   {
      Component::foregroundColorChanged();
      revalidateBorder(false);
   }

   void Composite::doLayout() throws(LayoutException)
   {
      revalidateBorder(false);
      const Size cSize(layoutSize());
      const Insets offset(_whitespace, _borderInsets);
      const Size space(offset);
      const Size sz(cSize.width() - space.width(), cSize.height() - space.height());

      if (_layout.get() == 0) {
         // cannot layout more than 1 child without a layout
         if (childCount() == 1) {
            setChildBounds< Peer> (*child(0), Bounds(offset.left(), offset.top(), sz.width(), sz.height()));
         }
      }
      else {
         _layout->doLayout(sz);
         const Int cnt = _layout->size();

         for (Index i = 0; i < cnt; ++i) {
            Component* c = _layout->component(i);
            Bounds b(_layout->bounds(i));
            // actually, we need to offset the bounds slightly
            setChildBounds< Peer> (*c, b.moveBounds(offset.left(), offset.top()));
         }
      }
   }

   SizeHints Composite::getSizeHints() const
   throws()
   {
      const Insets offset(_whitespace, _borderInsets); // actually need to merge with border insets
      const Size space(offset);
      SizeHints h;
      if (_layout.get() == 0) {
         if (childCount() == 1) {
            h = child(0)->sizeHints();
         }
      }
      else {
         h = _layout->sizeHints();
      }
      SizeHints res(Size(h.minimumWidth() + space.width(), h.minimumHeight() + space.height()), Size(h.maximumWidth()
                  + space.width(), h.maximumHeight() + space.height()), Size(h.preferredWidth() + space.width(),
                  h.preferredHeight() + space.height()));

      Log lg(getLog());
      if (lg.isLoggable(Level::DEBUGGING)) {
         LogEntry(lg).debugging() << "Hints : " << res << doLog;
      }
      return res;
   }

   void Composite::setBackgroundGraphic(const ::timber::Pointer< ::indigo::Node>& gfx)
   throws()
   {
      if (gfx != _bgGraphic) {
         _bgGraphic = gfx;
         if (hasPeer()) {
            impl< Peer> ()->setBackgroundGraphic(gfx);
         }
      }
   }

   void Composite::setDoubleBufferEnabled(bool enabled)
   throws()
   {
      if (_doubleBuffered != enabled) {
         _doubleBuffered = enabled;
         if (hasPeer()) {
            impl< Peer> ()->setDoubleBufferEnabled(enabled);
         }
      }
   }

   ::std::ostream& Composite::print(::std::ostream& out, bool deep, Int indent) const
throws()
{
   Component::print(out, deep, indent);
   ::std::string indentString;
   for (Index i = 0; i < indent; ++i) {
      indentString += "    ";
   }
   if (deep) {
      out << indentString << " children : " << childCount() << "{\n";
      for (Index i = 0; i < childCount(); ++i) {
         child(i)->print(out, true, indent + 1);
      }
      out << indentString << "}\n";
   }
   return out << ::std::flush;
}

}

