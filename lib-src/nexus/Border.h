#ifndef _NEXUS_BORDER_H
#define _NEXUS_BORDER_H

#ifndef _NEXUS_H
#include <nexus/nexus.h>
#endif

#ifndef _NEXUS_INSETS_H
#include <nexus/Insets.h>
#endif

#ifndef _NEXUS_COLOR_H
#include <nexus/Color.h>
#endif

#ifndef _INDIGO_NODE_H
#include <indigo/Node.h>
#endif


namespace nexus {

  /** 
   * @name Forward declarations 
   * @{*/
  class Bounds;
  /*@}*/
  
  /** Forward declare a component */
  class Component;

  /**
   * This abstract class is the base for all
   * borders that can be drawn around some components.
   * Border are created with specified insets which 
   * are unchanged during the lifetime of the border object.
   * @author Raimund Merkert
   * @date 08 Aug 2003
   */
  class Border : public ::timber::Counted {
    Border(const Border&);
    Border&operator=(const Border&);

    /** Default constructor. */
  protected:
    inline Border () throws()
    {}

    /** Destroy this border. */
  public:
    ~Border () throws() {}

    /**
     * @name Creation methods for various types of borders.
     * @{
     */

    /**
     * A null border. This is a border that generates no graphic
     * and has an inset of 0
     * @return a null border
     */
  public:
    static ::timber::Reference<Border> createNullBorder () throws();

    /**
     * Create a simple border.
     * @param w the width
     * @pre REQUIRE_GREATER_OR_EQUAL(w,0)
     * @param c a color or 0 to use a component's color
     * @return a raised border
     */
  public:
    static ::timber::Reference<Border> createSimpleBorder(Int w, const Color& c) throws();

    /**
     * Create a simple border.
     * @param w the width
     * @pre REQUIRE_GREATER_OR_EQUAL(w,0)
     * @return a raised border
     */
  public:
    inline static ::timber::Reference<Border> createSimpleBorder(Int w) throws()
    { return createSimpleBorder(w,Color()); }
      
    /**
     * Create a raised border.
     * @param w the width
     * @pre REQUIRE_GREATER_OR_EQUAL(w,0)
     * @return a raised border
     */
  public:
    static ::timber::Reference<Border> createRaisedBorder(Int w) throws();

    /**
     * Create a lowered border
     * @param w the border width
     * @pre REQUIRE_GREATER_OR_EQUAL(w,0)
     * @return a lowered border
     */
  public:
    static ::timber::Reference<Border> createLoweredBorder(Int w) throws();

    /**
     * Create a bevel border with the specied shadow and highlights. 
     * @param w the width of the border
     * @param shadow the shadow color
     * @param hilight the highlight color
     * @pre REQUIRE_NON_ZERO(shadow)
     * @pre REQUIRE_NON_ZERO(hilight)
     * @return a bevelled border
     */
  public:
    static ::timber::Reference<Border> createBevelBorder (Int w, const Color& shadow, const Color& hilight) throws();

    /**
     * Create an etched in border. The width of the border is automatically
     * increased by 1 if the width is an odd number
     * @param w the width of the etched border
     * @pre REQUIRE_GREATER_OR_EQUAL(w,0)
     * @return an etched in border
     */
  public:
    static ::timber::Reference<Border> createEtchedInBorder(Int w) throws();

    /**
     * Create an etched out border. The width of the border is automatically
     * increased by 1 if the width is an odd number
     * @param w the width of the etched border
     * @pre REQUIRE_GREATER_OR_EQUAL(w,0)
     * @return an etched out border
     */
  public:
    static ::timber::Reference<Border> createEtchedOutBorder(Int w) throws();
 
    /*@}*/

    /**
     * Get the border insets for the specified component.
     * @param c a component
     * @return the insets of this border
     */
  public:
    virtual Insets insets(const Component& c) const throws() = 0;
      
    /**
     * Create the graphic that draws this border around the specified componenet.
     * @param c a component
     * @param b the actual bounds to be used for the border
     * @return a readonly graphic for this border
     */
  public:
    ::timber::Pointer< ::indigo::Node> createGraphic (const Component& c, const Bounds& b) const throws();

    /**
     * Create the graphic that draws this border around the specified componenet.
     * @param c a component
     * @return a readonly graphic for this border
     */
  public:
    ::timber::Pointer< ::indigo::Node> createGraphic (const Component& c) const throws();

    /**
     * Create the graphic that draws this border inside the
     * bounds implied by the specified size. 
     * @param c a component
     * @param b the actual bounds to be used for the border
     * @return a graphic for this border
     */
  protected:
    virtual ::timber::Pointer< ::indigo::Node> buildBorderGraphic (const Component& c, const Bounds& b) const throws() = 0;
  };
    
    
}

#endif
