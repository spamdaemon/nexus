#include <nexus/Border.h>
#include <nexus/Component.h>
#include <indigo/Group.h>
#include <indigo/PolyLines.h>
#include <indigo/Polygons.h>
#include <indigo/StrokeNode.h>
#include <indigo/SolidFillNode.h>


namespace nexus {

  static void createRectangleHalf (const ::timber::Reference< ::indigo::PolyLines>& lines, Int x, Int y, Int w, Int h) throws()
  {
    const ::indigo::Point pt1(x+w,y);
    const ::indigo::Point pt2(x,y);
    const ::indigo::Point pt3(x,y+h);
    lines->add(::indigo::PolyLine(pt1,pt2,pt3));
  }


  static ::timber::Pointer< ::indigo::Node> createBorderGraphic (Int w, const Bounds& b,
								 const ::indigo::Color& outerShadow,const ::indigo::Color& outerHighlight,
								 const ::indigo::Color& innerShadow,const ::indigo::Color& innerHighlight) throws()
  {
    if (w==0) {
      return ::timber::Pointer< ::indigo::Node>();
    }
    const Int thickness = w/2;
    const Int maxX = b.width()-1;
    const Int maxY = b.height()-1;
    ::timber::Pointer< ::indigo::PolyLines> outerS,innerS,outerH,innerH;

    ::timber::Pointer< ::indigo::Group> g(new ::indigo::Group(4,true));
    g->add(new ::indigo::StrokeNode(outerShadow));
    outerS = new ::indigo::PolyLines();
    g->add(outerS);
    for (Index i=0;i<thickness;++i) {
      Int ww = b.width()-1-2*i;
      Int hh = b.height()-1-2*i;
      createRectangleHalf(outerS,i,i,ww,hh);
    }
    if (innerShadow==outerShadow) {
      innerS = outerS;
    }
    else {
      g->add(new ::indigo::StrokeNode(innerShadow));
      innerS = new ::indigo::PolyLines();
      g->add(innerS);
    }
    for (Index i=thickness;i<w;++i) {
      Int ww = b.width()-1-2*i;
      Int hh = b.height()-1-2*i;
      createRectangleHalf(innerS,maxX-i,maxY-i,-ww,-hh);
    }
    if (outerHighlight==outerShadow) {
      outerH = outerS;
    }
    else if (outerHighlight==innerShadow) {
      outerH = innerS;
    }
    else {
      g->add(new ::indigo::StrokeNode(outerHighlight));
      outerH = new ::indigo::PolyLines();
      g->add(outerH);
    }
    for (Index i=0;i<thickness;++i) {
      Int ww = b.width()-1-2*i;
      Int hh = b.height()-1-2*i;
      createRectangleHalf(outerH,maxX-i,maxY-i,-ww,-hh);
    }
    if (outerShadow==innerHighlight) {
      innerH = outerS;
    }
    else if (innerShadow==innerHighlight) {
      innerH = innerS;
    }
    else if (outerHighlight==innerHighlight) {
      innerH = outerH;
    }
    else {
      g->add(new ::indigo::StrokeNode(innerHighlight));
      innerH = new ::indigo::PolyLines();
      g->add(innerH);
    }
    for (Index i=thickness;i<w;++i) {
      Int ww = b.width()-1-2*i;
      Int hh = b.height()-1-2*i;
      createRectangleHalf(innerH,i,i,ww,hh);
    }
      
    return g;
  }

  static ::timber::Reference<Border> createBorder3D (Int w, bool raised, bool etched) throws()
  {
    struct TheBorder : public Border {
      TheBorder (Int ww, bool raiseIt, bool etchIt) throws()
      : Border(),_insets(ww),_raised(raiseIt),_etched(etchIt) {}
      ~TheBorder() throws() {}
      Insets insets (const ::nexus::Component&) const throws()
      { return _insets; }
	
    protected:
      ::timber::Pointer< ::indigo::Node> buildBorderGraphic (const ::nexus::Component& c, const Bounds& b) const throws()
      {
	::indigo::Color shadow,highlight;
	if (_raised) {
	  Desktop::computeShadowHighlightColors(c.backgroundColor(),shadow,highlight);
	}
	else {
	  Desktop::computeShadowHighlightColors(c.backgroundColor(),highlight,shadow);
	}
	if (_etched) {
	  return createBorderGraphic(_insets.left(),b,highlight,shadow,highlight,shadow);
	}
	else {
	  return createBorderGraphic(_insets.left(),b,shadow,highlight,highlight,shadow);
	}
      }
    private:
      Insets _insets;
      bool _raised;
      bool _etched;
    };

    Border* border= new TheBorder(w,raised,etched);
    return border;
  }

  ::timber::Pointer< ::indigo::Node> Border::createGraphic (const ::nexus::Component& c, const Bounds& b) const throws()
  {
    return buildBorderGraphic(c,b);
  }
    
  ::timber::Pointer< ::indigo::Node> Border::createGraphic (const ::nexus::Component& c) const throws()
  { return createGraphic(c,c.bounds()); }
    
  ::timber::Reference<Border> Border::createNullBorder () throws()
  {
    struct TheBorder : public Border {
      inline TheBorder () throws() {}
      ~TheBorder() throws() {}
      Insets insets (const ::nexus::Component&) const throws()
      { return Insets(0,0,0,0); }
	
    protected:
      ::timber::Pointer< ::indigo::Node> buildBorderGraphic (const ::nexus::Component&, const Bounds&) const throws()
      { return ::timber::Pointer< ::indigo::Node>(); }
    };
    
    static ::timber::Reference<Border> BORDER(dynamic_cast<Border*>(new TheBorder()));
    return BORDER;
  }
    

  ::timber::Reference<Border> Border::createSimpleBorder(Int w, const Color& c) throws()
  {
    struct TheBorder : public Border {
      TheBorder (Int ww, const Color& col) throws()
      : Border(),_insets(ww),_color(col) {}
      ~TheBorder() throws() {}
      Insets insets (const ::nexus::Component&) const throws()
      { return _insets; }

    protected:
      ::timber::Pointer< ::indigo::Node> buildBorderGraphic (const ::nexus::Component&, const Bounds& b) const throws()
      {
	const Int maxX = b.width()-1;
	const Int maxY = b.height()-1;
	  
	const Int thickness = _insets.left();
	if (thickness==0) {
	  return ::timber::Pointer< ::indigo::Node>();
	}
	::timber::Pointer< ::indigo::Polygons> p(new ::indigo::Polygons());
	p->add(::indigo::Polygon::createRectangle(::indigo::Point(0,0),thickness,b.height()-1));
	p->add(::indigo::Polygon::createRectangle(::indigo::Point(maxX-thickness,0),thickness,b.height()-1));
	p->add(::indigo::Polygon::createRectangle(::indigo::Point(0,0),b.width()-1,thickness));
	p->add(::indigo::Polygon::createRectangle(::indigo::Point(0,maxY-thickness),b.width()-1,thickness));

	if (_color==0) {
	  return p;
	}
	::timber::Pointer< ::indigo::Group> g(new ::indigo::Group(4,true));
	g->add(new ::indigo::SolidFillNode(_color.color()));
	g->add(p);
	return g;
      }
    private:
      const Insets _insets;
      const Color _color;
    };
    
    Border* xb(new TheBorder(w,c));
    return xb;
  }

  ::timber::Reference<Border> Border::createRaisedBorder(Int w) throws()
  { return createBorder3D(w,true,false); }
    
  ::timber::Reference<Border> Border::createLoweredBorder(Int w) throws()
  { return createBorder3D(w,false,false); }

  ::timber::Reference<Border> Border::createEtchedInBorder(Int w) throws()
  { return createBorder3D(w,false,true); }

  ::timber::Reference<Border> Border::createEtchedOutBorder(Int w) throws()
  { return createBorder3D(w,true,true); }

  ::timber::Reference<Border> Border::createBevelBorder (Int w, const Color& shadow, const Color& hilight) throws()
  {
    struct TheBorder : public Border {
      TheBorder (Int ww, const Color& s, const Color& h) throws()
      : Border(),_insets(ww),_shadow(s.color()),_hilight(h.color()) {}

      ~TheBorder() throws() {}

      Insets insets (const ::nexus::Component&) const throws()
      { return _insets; }
	
    protected:
      ::timber::Pointer< ::indigo::Node> buildBorderGraphic (const ::nexus::Component&, const Bounds& b) const throws()
      { return createBorderGraphic(_insets.left(),b,_shadow,_hilight,_hilight,_shadow); }
	
    private:
      const Insets _insets;
      const ::indigo::Color _shadow;
      const ::indigo::Color _hilight;
    };
    
    assert(shadow!=0);
    assert(hilight!=0);
    Border* xb(new TheBorder(w,shadow,hilight));
    return xb;
  }
}
