#include <nexus/InputModifiers.h>
#include <iostream>

::std::ostream& operator<< (::std::ostream& out, const ::nexus::InputModifiers& im)
{
  out << "{";
  if (im.isButton1Pressed()) {
    out << " Button1";
  }
  if (im.isButton2Pressed()) {
    out << " Button2";
  }
  if (im.isButton3Pressed()) {
    out << " Button3";
  }
  if (im.isButton4Pressed()) {
    out << " Button4";
  }
  if (im.isButton5Pressed()) {
    out << " Button5";
  }
  if (im.isControlPressed()) {
    out << " Control";
  }
  if (im.isShiftPressed()) {
    out << " Shift";
  }
  if (im.isCapsLockPressed()) {
    out << " CapsLock";
  }
  if (im.isMeta1Pressed()) {
    out << " Meta1";
  }
  out << " }";
  return out;
}
