#ifndef _NEXUS_TEXTAREA_H
#define _NEXUS_TEXTAREA_H

#ifndef _NEXUS_TEXTCOMPONENT_H
#include <nexus/TextComponent.h>
#endif

#ifndef _NEXUS_PEER_TEXTAREA_H
#include <nexus/peer/TextArea.h>
#endif

namespace nexus {
  /**
   * This class is a multi-line text entry field.
   * @date 06 Jun 2003
   */
  class TextArea : public TextComponent {
    TextArea(const TextArea&);
    TextArea&operator=(const TextArea&);

    /** Default minimum row count */
  public:
    static const Int DEFAULT_MIN_ROW_COUNT = 3;

    /** The default string from preferred and minimum size */
  private:
    static const char DEFAULT_SIZE_STRING[];
	
    /** The default border width */
  public:
    static const Int DEFAULT_BORDER_WIDTH = 2;

    /** The implementation class is a container */
  public:
    typedef ::nexus::peer::TextArea Peer;

    /**
     * The default constructor 
     * @throws ConfigurationException if the desktop was not initialized
     */
  public:
    TextArea () throws (::std::exception);

    /**
     * Create a new textarea with a specified text model
     * @param model the text model to be used
     * @pre REQUIRE_NON_ZERO(model)
     */
  public:
    TextArea (const TextModel& model) throws (::std::exception);

    /**
     * Create a new textarea with a specified text
     * @param txt a text string or 0
     */
  public:
    TextArea (const ::std::string& txt) throws (::std::exception);

    /** Destroy this textarea */
  public:
    ~TextArea () throws();

    /**
     * TextAreas can display multiple lines of text.
     * @returns true
     */
  public:
    bool isMultiLineCapable () const throws();

    /**
     * Derive the size bounds by specified the minimum
     * and maximum number of rows that should be displayed
     * @param ms the string that should at least be displayable
     * @param MS the longest string that should be displayed in any given row
     * @param m the minimum number of rows
     * @param M the maximum number of rows or -1 for unlimited maximum size
     */
  public:
    virtual void deriveSizeBounds (const ::std::string& ms, const ::std::string& MS, Int m, Int M) throws();
	
    /**
     * Derive the preferred size from the specified string and row count.
     * The minimum and maximum sizes will be adjusted as necessary
     * to accomodate the preferred size.
     * @param s a string or 0 to use the default preferred size
     * @param nr the number of rows (if less than 1 a default value is used)
     */
  public:
    virtual void derivePreferredSize (const ::std::string& s, Int nr) throws();


  protected:
    void initializePeer() throws();
    PeerComponent* createPeerComponent (PeerComponent* p) throws();
    SizeHints computeSizeHints() const throws();

    /**
     * Calculate the size for this textarea to completely show
     * the specified string in nrows rows.
     * @param s a string
     * @param nrows the number of rows we need
     * @return the required size for this textfield to show the string
     */
  private:
    Size calculateSize (const ::std::string& s, Int nrows) const throws();
	
    /**
     * Get the window peer.
     * @return the widow peer
     */
  private:
    inline Peer* textArea() const throws()
    { return impl<Peer>(); }

    /** The minimum and maximum number of rows */
  private:
    Int _minRowCount, _maxRowCount, _preferredRowCount;

    /** The String determining the minimum size */
  private:
    ::std::string _minSizeString;

    /** The String determining the maximum size */
  private:
    ::std::string _maxSizeString;

    /** The preferred size string */
  private:
    ::std::string _preferredSizeString;
	
  };
}
#endif
