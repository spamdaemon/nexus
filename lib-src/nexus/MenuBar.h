#ifndef _NEXUS_MENUBAR_H
#define _NEXUS_MENUBAR_H

#ifndef _NEXUS_COMPONENT_H
#include <nexus/Component.h>
#endif

#ifndef _NEXUS_PEER_MENUBAR_H
#include <nexus/peer/MenuBar.h>
#endif

namespace nexus {
  class Menu;

  /**
   * The MenuBar is attached to a window and serves as the hook for adding menu items
   * to it. Typically, a menubar is instantiated by a Window.
   */
  class MenuBar : public Component {
    MenuBar(const MenuBar&);
    MenuBar&operator=(const MenuBar&);
	
    /** The implementation class */
  public:
    typedef ::nexus::peer::MenuBar Peer;
    
    /** 
     * Default constructor.
     * @throw ConfigurationException if the desktop has not been configured
     */
  public:
    MenuBar () throws (::std::runtime_error);
    
    /** Destroy this menuBar and release all its resources */
  public:
    ~MenuBar () throws();
    
    /**
     * Add a new menu to this bar.
     * @param menu the new menu
     */
  public:
    void addMenu(::timber::Reference<Menu> menu) throws();
    
    /**
     * Add a new menu to this bar.
     * @param name the name of the new menu
     * @return the menu
     */
  public:
    ::timber::Reference<Menu> addMenu(const ::std::string& name) throws();
    
    /**
     * Add a new menu to this bar.
     * @param name the name of the new menu
     * @return the menu
     */
  public:
    ::timber::Reference<Menu> addMenu(const char* name) throws();
    
    /**
     * Remove the specified menu.
     * @param menu the menu component to be removed.
     */
  public:
    void removeMenu (::timber::Reference<Component> menu) throws();
    
    /**
     * Get the menuBar peer.
     * @return the widow peer
     */
  private:
    inline Peer* menuBar() const throws()
    { return impl<Peer>(); }
    
  protected:
    void initializePeer() throws();
    PeerComponent* createPeerComponent (PeerComponent* p) throws();
    SizeHints getSizeHints() const throws();

    /** The observer */
  private:
    Peer::Observer* _observer;

    /** The menus */
  private:
    ::std::vector<Component*> _menus;
  };
}

#endif

