#include <nexus/Size.h>
#include <nexus/Rectangle.h>
#include <nexus/Insets.h>

#include <cmath>
#include <iostream>

namespace nexus {
  const Size Size::MIN(1,1);
  const Size Size::MAX(0x7fff,0x7fff);   
  
  Size::Size (const Rectangle& r) throws()
  : _width(static_cast<Int>(::std::ceil(r.width()))),
    _height(static_cast<Int>(::std::ceil(r.height())))
  {}

  Size::Size (const Insets& i) throws()
  : _width(i.left()+i.right()),_height(i.top()+i.bottom()) 
  {}

  Size Size::adjust (Int dw, Int dh) const throws()
  { return Size(::std::max(0,dw+_width),::std::max(0,dh+_height)); }
  
  Size Size::min (const Size& sz) const throws()
  { return Size(::std::min(width(),sz.width()),::std::min(height(),sz.height())); }
  
  Size Size::max (const Size& sz) const throws()
  { return Size(::std::max(width(),sz.width()),::std::max(height(),sz.height())); }
  
}

::std::ostream& operator<< (::std::ostream& out, const ::nexus::Size& b) throws()
{
  return out << "width="<<b.width() <<", height="<<b.height();
} 
