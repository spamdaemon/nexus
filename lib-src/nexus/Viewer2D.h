#ifndef _NEXUS_VIEWER2D_H
#define _NEXUS_VIEWER2D_H

#ifndef _NEXUS_PROXYCOMPONENT_H
#include <nexus/ProxyComponent.h>
#endif

#ifndef _NEXUS_PIXEL_H
#include <nexus/Pixel.h>
#endif

#ifndef _INDIGO_NODE_H
#include <indigo/Node.h>
#endif

#ifndef _INDIGO_AFFINETRANSFORM2D_H
#include <indigo/AffineTransform2D.h>
#endif

#ifndef _INDIGO_GROUP_H
#include <indigo/Group.h>
#endif

#ifndef _INDIGO_POINT_H
#include <indigo/Point.h>
#endif

#ifndef _INDIGO_PICKINFO_H
#include <indigo/PickInfo.h>
#endif

#ifndef _NEXUS_EVENT_VIEWER2DEVENTLISTENER_H
#include <nexus/event/Viewer2DEventListener.h>
#endif

#ifndef _NEXUS_VIEWPORT_H
#include <nexus/ViewPort.h>
#endif

namespace nexus {
  
    
  /**
   * This class provides the capability to render 
   * a graphic using a coordinate system where positive y points upwards
   * and positive x points to the right.
   */
  class Viewer2D : public ProxyComponent {

    /** The structure returns to whoever is picking at a pixel */
  public:
    struct PickInfo {
      /** The pixel (x,y) that was picked */
      Pixel pixel;
      
      /** The node that was picked (null if none was picked) */
      ::timber::Pointer< ::indigo::PickInfo> info;
      
      /** The node that was picked (null if none was picked) */
      ::timber::Pointer< ::indigo::Node> node;
      
      /** The index of the shape that was picked (undefined if node is null) */
      size_t shape;
    };

    /** The viewport. */
  private:
    struct VPort : public ViewPort {
       VPort(Viewer2D& v) throws() : _viewer(v) {}
       ~VPort() throws() {}

       protected:
       void viewPortChanged() throws();

       /**
        * Override this method
        * @param newBounds
        * @param newView
        */
    public:
       void setViewPort(const ::indigo::view::ViewPort::Bounds& newBounds,
             const ::indigo::view::ViewPort::Window& newView)
       throws ( ::std::invalid_argument);

       /*@}*/

    public:
       Viewer2D& _viewer;
    };

    /**
     * Default constructor 
     * @throw ConfigurationException if the desktop has not been configured
     */
  public:
    Viewer2D () throws (::std::exception);
	    
    /** Destroy this desktop component. */
  public:
    ~Viewer2D () throws();

    /**
     * Get the viewport. The viewport may be used to affect the transformation of the graphic to be rendered.
     * @return the viewport.
     */
  public:
    inline ViewPort& viewPort() throws() { return _viewPort; }

    /**
     * Get the viewport. The viewport may be used to affect the transformation of the graphic to be rendered.
     * @return the viewport.
     */
  public:
    inline const ViewPort& viewPort() const throws() { return _viewPort; }

    /**
     * Set the size hints for this Viewer2D.
     * @param hints the size hints
     */
  public:
    void setSizeHints (const SizeHints& hints) throws();
	
    /**
     * Set the graphic to be displayed in this Viewer2D.
     * @param gfx a graphic.
     */
  public:
    void setGraphic (const  ::timber::Pointer< ::indigo::Node>& gfx) throws();

    /**
     * Get the current graphic.
     * @return the current graphic
     */
  public:
    ::timber::Pointer< ::indigo::Node> graphic () const throws();
	
    /**
     * Enable automatic rendering.
     * @param enabled if true, then enable automatic rendering
     */
  public:
    void setAutoRenderEnabled (bool enabled) throws();

    /**
     * Test if automatic rendering is enabled.
     * @return true if automatic rendering is enabled.
     */
  public:
    bool isAutoRenderEnabled () const throws();

    /**
     * Enable or disable double buffering. This is merely a hint
     * and need not be respected; if resource allocation should 
     * fail for double buffering, then rendering will be done
     * with singe-buffering.<br>
     * @param enabled true if double buffering should be enabled.
     */
  public:
    void setDoubleBufferEnabled (bool enabled) throws();
	
    /**
     * Test if double buffering is enabled. If this method 
     * returns true, then this does not necessarily mean that
     * double buffering is enabled in the display hardware/software.
     * @return true if double buffering should be used by the display software
     */
  public:
    bool isDoubleBufferEnabled () const throws();

    /** Render the graphic now. */
  public:
    void render() const throws();

    /**
     * Pick the graphic at the specified compoenent coordinates
     * @param px a window pixel
     * @return a pick info object
     */
  public:
    ::std::unique_ptr<PickInfo> pickNode (const Pixel& px) const throws();

   /**
     * Add a Viewer2D listener.
     * @param l a Viewer2D event listener
     * @pre REQUIRE_NON_ZERO(l)
     */
  public:
    inline void addViewer2DEventListener (const ::timber::Reference< ::nexus::event::Viewer2DEventListener>& l) throws()
    { _viewerListeners.addListener(l); }

    /**
     * Remove a Viewer2D listener.
     * @param l a Viewer2D event listener
     */
  public:
    inline void removeViewer2DEventListener (const ::timber::Pointer< ::nexus::event::Viewer2DEventListener>& l) throws()
    { _viewerListeners.removeListener(l); }
	
    /**
     * @name Manipulating the viewing matrix.
     * @{
     */
 
    /**
     * Transform the specified pixel into a point. This is used
     * to transform from screen coordinates to model coordinates.
     * @param p a pixel  (not necessarily within the bounds of this viewer)
     * @return a point 
     */
  public:
    ViewPort::Point transformPixel (const Pixel& p) const throws();
	
    /**
     * Transform from the specified point to a pixel. This is used
     * to transform model coordinates to screen coordinates.
     * @param p a point 
     * @return a pixel (not necessarily within the bounds of this viewer)
     */
  public:
    Pixel transformPoint (const ViewPort::Point& p) const throws();

    /*@}*/

    /**
     * Process a Viewer2D event.
     * @param e a Viewer2D event
     */
  protected:
    virtual void processViewer2DEvent (const ::timber::Reference< ::nexus::event::Viewer2DEvent>& e) throws();

    /**
     * Layout this viewer. If isKeepOriginOnScreen() returns true, then the
     * origin will be kept on the screen.
     */
  protected:
    void doLayout () throws();

    /**
     * Update the model view transform.
     */
  private:
    void updateTransform () throws();

    /**
     * Notify subclasses that the view has changed.
     */
  private:
    void viewChanged() throws();

    /** The size hints */
  private:
    SizeHints _hints;

    /** The root node */
  private:
    ::timber::Pointer< ::indigo::Group> _root;
	
    /**  The viewer's listeners */
  private:
    EventListeners< ::nexus::event::Viewer2DEventListener> _viewerListeners;

    /** The transform as a matrix */
  private:
    ::indigo::AffineTransform2D _matrix;

    /** The inverse of the matrix */
  private:
    ::indigo::AffineTransform2D _invMatrix;

    /** The viewport */
  private:
    VPort _viewPort;
    /*@}*/
  };
}
#endif
