#include <nexus/Window.h>
#include <nexus/MenuBar.h>
#include <nexus/peer/Window.h>
#include <nexus/event/WindowEvent.h>
#include <timber/logging.h>

#include <cstdlib>

using namespace ::timber;
using namespace ::timber::logging;

namespace nexus {
   namespace {

      struct ExitApplication : public Runnable
      {
            ~ExitApplication() throws()
            {
            }

            void run() throws()
            {
               ::std::exit(0);
            }

      };

      static Log getLog()
      {
         return Log("nexus.Window");
      }
   }

   Window::Window() throws (::std::runtime_error)
         : _closeAction(CLOSE), _content(0), _menuBar(0)
   {
      struct WindowListener : public Peer::WindowObserver
      {
            WindowListener(Window& win) throws()
                  : _window(win)
            {
            }
            ~WindowListener() throws()
            {
            }
            void windowBoundsChanged(const Bounds& b) throws()
            {
               _window.processBoundsChange(b);
            }
            void windowVisibilityChanged(bool v) throws()
            {
               _window.processVisibilityChange(v);
            }
            void windowClosedByUser() throws()
            {
               _window.processWindowCloseRequest();
            }
            void windowDestroyed() throws()
            {
               if (_window.hasPeer()) {
                  _window.destroyPeer();
               }
               // at this point there is no peer anymore
               _window.setVisible(false);
               _window.setShowing(false);
               assert(!_window.hasPeer());

               _window.processWindowEvent(
                     new ::nexus::event::WindowEvent(&_window, ::nexus::event::WindowEvent::WINDOW_CLOSED));
               _window.windowDestroyed();

               // abort the application
               if (_window.closeAction() == EXIT) {
                  Desktop::enqueue(::std::make_shared<ExitApplication>());
               }

            }

         private:
            Window& _window;
      };
      _observer = new WindowListener(*this);
   }

   ::timber::Reference< Window> Window::create(const ::timber::Pointer< Component>& c) throws (::std::runtime_error)
   {
      Reference< Window> w(new Window());
      w->setContent(c);
      return w;
   }

   Window::~Window() throws()
   {
      assert(window() == 0);
      if (_content != 0) {
         detachChild(_content);
      }
      if (_menuBar != 0) {
         detachChild(_menuBar);
      }
      delete _observer;
   }

   void Window::setCloseAction(CloseAction act) throws()
   {
      _closeAction = act;
   }

   void Window::requestSize(Int w, Int h) throws()
   {
      requestBounds(Bounds(x(), y(), w, h));
   }

   void Window::requestSize(const Size& sz) throws()
   {
      requestBounds(Bounds(x(), y(), sz.width(), sz.height()));
   }

   void Window::setContent(const ::timber::Pointer< Component>& c) throws()
   {
      ::timber::Pointer< Component> tmp(_content);
      if (c != tmp) {
         Component* pc = attachChild(c);
         detachChild(_content);
         _content = pc;
         revalidate();
      }
   }

   void Window::setTitle(const ::std::string& t) throws()
   {
      if (_title != t) {
         _title = t;
         if (hasPeer()) {
            window()->setTitle(_title);
         }
      }
   }

   void Window::setVisible(bool visible) throws()
   {
      DesktopComponent::setVisible(visible);
      // create the peer
      if (isVisible() && !hasPeer()) {
         createPeer();
      }
   }

   void Window::requestBounds(const Bounds& b) throws()
   {
      struct ResizeFunction : public Runnable
      {
            inline ResizeFunction(Window* c, const Bounds& bn) throws()
                  : _win(c), _b(bn)
            {
               _win->ref();
            }
            ~ResizeFunction() throws()
            {
               _win->unref();
            }
            void run()
            {
               if (_win->hasPeer()) {
                  _win->window()->setBounds(_b);
               }
            }
         private:
            Window* _win;
            const Bounds _b;
      };

      if (b != bounds()) {
         if (hasPeer()) {
            Desktop::enqueue(::std::make_shared<ResizeFunction>(this,b));
         }
         else {
            setBounds(b);
         }
      }
   }

   void Window::initializePeer() throws()
   {
      DesktopComponent::initializePeer();

      // we need not clean this one up!
      window()->setWindowObserver(_observer);

      Bounds b(bounds());
      SizeHints hints = sizeHints();
      Size sz = hints.fitSize(b.size());
      window()->setMaximumSize(hints.maximumSize());
      window()->setMinimumSize(hints.minimumSize());
      b = Bounds(b.x(), b.y(), sz.width(), sz.height());
      window()->setBounds(b);

      // set the title
      window()->setTitle(_title);
   }

   Window::PeerComponent* Window::createPeerComponent(PeerComponent* p) throws()
   {
      assert(p == 0);
      return Desktop::createWindow();
   }

   void Window::processWindowCloseRequest() throws()
   {
      setVisible(false);
   }

   void Window::windowDestroyed() throws()
   {
      getLog().debugging("Window has been destroyed");
   }

   void Window::processVisibilityChange(bool visible) throws()
   {
      setShowing(visible);
      if (visible) {
         processWindowEvent(new ::nexus::event::WindowEvent(this, ::nexus::event::WindowEvent::WINDOW_OPENED));
      }
      else {
         processWindowEvent(new ::nexus::event::WindowEvent(this, ::nexus::event::WindowEvent::WINDOW_OPENED));
      }
      // always revalidate
      revalidate();
   }

   void Window::processBoundsChange(const Bounds& b) throws()
   {
      SizeHints hints = sizeHints();
      window()->setMaximumSize(hints.maximumSize());
      window()->setMinimumSize(hints.minimumSize());
      setBounds(b);
      Int dx = hints.minimumSize().width() - b.width();
      Int dy = hints.minimumSize().height() - b.height();
      if (dx > 0 || dy > 0) {
         requestBounds(Bounds(b.x(), b.y(), hints.minimumSize().width(), hints.minimumSize().height()));
      }
   }

   void Window::doLayout() throws(LayoutException)
   {
      if (_content == 0) {
         return;
      }

      Bounds b(Pixel(), layoutSize());

      Int w = b.width();
      Int h = b.height();
      SizeHints hints = getSizeHints();

      if (!hints.containsSize(b.size())) {
         if (w > hints.maximumWidth()) {
            w = hints.maximumWidth();
         }
         if (h > hints.maximumHeight()) {
            h = hints.maximumHeight();
         }
         b = b.centerBounds(Size(w, h));
      }
      if (b.width() > 0 && b.height() > 0) {
         setChildBounds< Peer>(*_content, b);
      }
   }

   SizeHints Window::getSizeHints() const throws()
   {
      SizeHints res;
      if (_content != 0) {
         res = _content->sizeHints();
      }
      LogEntry(getLog()).debugging() << "Window Hints : " << res << doLog;
      return res;
   }

   void Window::processWindowEvent(const ::timber::Reference< ::nexus::event::WindowEvent>& e) throws()
   {
      _windowListeners.notify(&::nexus::event::WindowEventListener::processWindowEvent, e);
   }

   void Window::setMenuBar(::timber::Pointer< MenuBar> bar) throws()
   {
      ::timber::Pointer< MenuBar> tmp(_menuBar);
      if (bar != tmp) {
         detachChild(_menuBar);
         _menuBar = dynamic_cast< MenuBar*>(attachChild(bar));
      }
   }

   ::timber::Pointer< MenuBar> Window::menuBar() throws()
   {
      return ::timber::Pointer< MenuBar>(_menuBar);
   }
}
