#include <nexus/VerticalAlignment.h>

namespace nexus {
  const VerticalAlignment VerticalAlignment::TOP(VerticalAlignment::ALIGN_TOP);
  const VerticalAlignment VerticalAlignment::CENTER(VerticalAlignment::ALIGN_CENTER);
  const VerticalAlignment VerticalAlignment::BOTTOM(VerticalAlignment::ALIGN_BOTTOM);
}
