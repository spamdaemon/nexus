#ifndef _NEXUS_CONTAINER_H
#define _NEXUS_CONTAINER_H

#ifndef _NEXUS_COMPOSITE_H
#include <nexus/Composite.h>
#endif

namespace nexus {

   /**
    * This class provides a public to add, remove, and manipulate
    * composite components.
    *
    */
   class Container : public Composite
   {
         Container(const Container&);
         Container&operator=(const Container&);

         /**
          * Default constructor. Clients using this constructor
          * should override the doLayout() and sizeHints() methods.
          * @throw ConfigurationException if the desktop has not been configured
          */
      protected:
         Container() throws (::std::exception);

         /**
          * Create a container with the specified layout strategy.
          * @param engine the layout engine
          * @throw ConfigurationException if the desktop has not been configured
          */
      protected:
         Container(::std::unique_ptr< LayoutEngine> engine) throws (::std::exception);

         /** Destroy this container and release all its resources */
      public:
         ~Container() throws();

         /**
          * Get the child at the specified index.
          * @param pos a position
          * @return a child
          */
      public:
         inline ::timber::Reference< Component> component(Index pos) const throws()
         {
            return child(pos);
         }

         /**
          * Get the index of the specified child.
          * @return the index of the specified child or invalidIndex() if not found
          */
      public:
         inline Index indexOf(const ::timber::Reference< Component>& c) const throws()
         {
            return indexOfChild(c);
         }

         /**
          * Remove the component at the specified index.
          * @param pos a component index
          * @return the component
          */
      public:
         inline ::timber::Reference< Component> removeComponent(Index pos) throws()
         {
            return removeChild(pos);
         }

         /**
          * Insert a component into this container. Since this is invoked
          * by the derived class, it is not necessary to make this method
          * virtual. A subclass may not even know what to do with this
          * if it were called without its knowledge, for example, by a
          * derived class.
          *
          * @param c the component to add
          * @param pos the position at which to add c
          * @pre REQUIRE_ZERO(c->parent())
          * @pre REQUIRE_RANGE(pos,0,childCount());
          */
      public:
         void insertChild(const ::timber::Reference< Component>& c, Index pos) throws()
         {
            Composite::insertChild(c, pos);
         }

         /**
          * Add a component this container. Same as
          * <code>insertChild(c,childCount())</code>.
          * @param c the component to add
          * @pre REQUIRE_ZERO(c->parent())
          */
      public:
         inline void addChild(const ::timber::Reference< Component>& c) throws()
         {
            Composite::addChild(c);
         }

         /**
          * Get the number of components in this container.
          * @return the number of contained components
          */
      public:
         inline Int componentCount() const throws()
         {
            return childCount();
         }

         /**
          * Set the background graphic for this container.
          * @param gfx the background graphic for this container
          */
      public:
         using Composite::setBackgroundGraphic;

         /**
          * Get the background graphic if there is one.
          * @return the background graphic.
          */
      public:
         using Composite::backgroundGraphic;
   };
}

#endif

