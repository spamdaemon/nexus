#include <nexus/DesktopModule.h>
#include <canopy/dso/DSO.h>
#include <timber/logging.h>

#include <memory>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace {
   static Log log()
   {
      return Log("nexus.DesktopModule");
   }
}

namespace nexus {
   DesktopModule& loadDesktopModule(const ::std::string& path, int& argc, const char** argv) throws (::std::exception)
   {
      unique_ptr< ::canopy::dso::DSO> dso;
      try {
         dso = ::canopy::dso::DSO::load(path.c_str(), false);
         log().info("Loaded module " + path);
      }
      catch (...) {
         log().severe("Could not load module " + path);
         throw;
      }

      try {
         DesktopModule* mod = reinterpret_cast< DesktopModule*>(dso->symbol("peerModule"));
         int status = mod->initialize(argc, argv);
         if (status < 0) {
            throw ::std::runtime_error("Could not initialize the desktop");
         }
         return *mod;
      }
      catch (const ::exception& e) {
         log().severe("Unloading module " + path + " due to error: " + ::std::string(e.what()));
         dso->close(); // we do need to close it here
         throw;
      }
      catch (...) {
         log().severe("Unloading module " + path + " due to error");
         dso->close(); // we do need to close it here
         throw;
      }
   }
}
