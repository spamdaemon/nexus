#include <nexus/Menu.h>
#include <nexus/MenuItem.h>
#include <nexus/peer/Window.h>

namespace nexus {
  
    
  Menu::Menu () throws (::std::runtime_error)
  : _observer(0)
  { }
  
  Menu::Menu (const ::std::string& nm) throws (::std::runtime_error)
  : _observer(0),_text(nm)
  { }
  
  Menu::Menu (const Icon& ic) throws (::std::runtime_error)
  : _observer(0),_icon(ic)
  { }
  
  Menu::~Menu () throws()
  {
    for (size_t i=0;i<_menus.size();++i) {
      detachChild(_menus[i]);
    }
    delete _observer;
  }
      
  void Menu::setText (const ::std::string& t) throws()
  { 
    if (t!=_text) {
      _text=t;
      _icon = Icon();
      if (hasPeer()) {
	menu()->setText(_text);
      }
#if 0
      if (updatePreferredSize()) {
	revalidate();
      }
#endif
    }
  }
      
  void Menu::setIcon (const Icon& i) throws()
  { 
    if (_icon.size()!=i.size() || i.graphic()!=_icon.graphic()) {
      _text.clear();
      _icon = i;
      if (hasPeer()) {
	menu()->setIcon(_icon);
      }
#if 0
      if (updatePreferredSize()) {
	revalidate();
      }
#endif
    }
  }

  void Menu::initializePeer() throws()
  {
    struct Observer : public Peer::Observer {
      inline Observer (Menu& b) throws() : _owner(b) {}
      ~Observer () throws() {}

    private:
      Menu& _owner;
    };

    Component::initializePeer();
    if (_observer==0) {
      _observer = new Observer(*this);
    }

    Component::initializePeer();
    if (_icon.graphic()!=nullptr) {
      menu()->setIcon(_icon);
    }
    else if (!_text.empty()) {
      menu()->setText(_text);
    }

    menu()->setMenuObserver(_observer);
  }

  Menu::PeerComponent* Menu::createPeerComponent (PeerComponent* p) throws()
  {
    return Desktop::createMenu(*p); 
  }

  SizeHints Menu::getSizeHints() const throws()
  {
    return SizeHints();
  }

  void Menu::addMenu(::timber::Reference<Menu> mn) throws()
  {
    Component* m = attachChild(mn);
    if (m) {
      _menus.push_back(m);
    }
  }
    
  ::timber::Reference<Menu> Menu::addMenu(const ::std::string& nm) throws()
  {
    ::timber::Reference<Menu> m = new Menu(nm);
    addMenu(m);
    return m;
  }
  
  ::timber::Reference<Menu> Menu::addMenu(const char* nm) throws()
  { return addMenu( ::std::string(nm)); }
  
  void Menu::addMenuItem(::timber::Reference<MenuItem> mi) throws()
  {
    Component* m = attachChild(mi);
    if (m) {
      _menus.push_back(m);
    }
  }
    
  ::timber::Reference<MenuItem> Menu::addMenuItem(const ::std::string& nm) throws()
  {
    ::timber::Reference<MenuItem> m = new MenuItem(nm);
    addMenuItem(m);
    return m;
  }
  
  ::timber::Reference<MenuItem> Menu::addMenuItem(const char* nm) throws()
  { return addMenuItem( ::std::string(nm)); }

  void Menu::remove (::timber::Reference<Component> mn) throws()
  {
    for (size_t i=0;i<_menus.size();++i) {
      ::timber::Reference<Component> m = _menus[i];
      if (m==mn) {
	detachChild(_menus[i]);
	_menus.erase(_menus.begin()+i);
	break;
      }
    }
  }

}

