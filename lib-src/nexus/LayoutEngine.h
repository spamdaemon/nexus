#ifndef _NEXUS_LAYOUTENGINE_H
#define _NEXUS_LAYOUTENGINE_H

#ifndef _NEXUS_H
#include <nexus/nexus.h>
#endif

#ifndef _NEXUS_LAYOUTEXCEPTION_H
#include <nexus/LayoutException.h>
#endif

#ifndef _NEXUS_EVENTLISTENERS_H
#include <nexus/EventListeners.h>
#endif

namespace nexus {
  
  /** 
   * @name Forward declarations 
   * @{*/
  class Bounds;
  class Size;
  class SizeHints;
  /*@}*/

    
  /** Forward declare a component */
  class Component;

  /**
   * This class implements the interface required by 
   * components for a layout. Components added to this
   * engine must not have their reference counts increased,
   * or decreased if they are removed.
   */
  class LayoutEngine {
    LayoutEngine(const LayoutEngine&);
    LayoutEngine&operator=(const LayoutEngine&);
	
    /**
     * An observer object
     */
  public:
    class Observer : public virtual ::timber::Counted {
    Observer(const Observer&);
    Observer&operator=(const Observer&);

      /** The default constructor */
    protected:
      inline Observer () throws() {}

      /** Destroy this observer */
    public:
      ~Observer() throws();

      /**
       * The layout has changed 
       * @param engine the layout engine
       */
    public:
      virtual void layoutChanged (LayoutEngine& engine) throws() = 0;
    };

    /** Default constructor */
  protected:
    inline LayoutEngine () throws()
    {}
	
    /** Destroy this container and release all its resources */
  public:
    virtual ~LayoutEngine () throws() = 0;
	
    /**
     * Compute the layout given the specified parameters.
     * @param sz the size available to the layout
     * @throws LayoutException if the layout could not be computed
     */
  public:
    virtual void doLayout(const Size& sz) throws (LayoutException) = 0;
	
    /**
     * Compute the size hints.
     * @return the size hints
     */
  public:
    virtual SizeHints sizeHints() const throws() = 0;

    /**
     * Get the number of components that these parameters
     * describe.
     * @return the number of components
     */
  public:
    virtual Int size () const throws() = 0;
	
    /**
     * Get the component at the specified index.
     * @param i a component index
     * @return the component at position i
     */
  public:
    virtual Component* component (Index i) const throws() = 0;
	  
    /**
     * Get the bounds for the component at the given index.
     * @param i a component index
     * @return the bounds for the component at position i
     */
  public:
    virtual Bounds bounds (Index i) const throws() = 0;	

    /**
     * Add a layout observer.
     * @param obs an observer 
     * @pre REQUIRE_NON_ZERO(obs)
     */
  public:
    void addObserver (const ::timber::Reference<Observer>& obs) throws();
	
    /**
     * Remove the specified observer.
     * @param obs an observer
     */
  public:
    void removeObserver (const ::timber::Pointer<Observer>& obs) throws();

    /**
     * The layout has changed. Notifies all observers. Subclasses must
     * call this if the layout changes.
     */
  protected:
    void layoutChanged () throws();

    /** The observers */
  private:
    EventListeners<Observer> _observers;
  };

}
#endif

