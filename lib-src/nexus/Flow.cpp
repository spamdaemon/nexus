#include <nexus/Flow.h>

namespace nexus {
  
  const Flow Flow::HORIZONTAL(FLOW_LEFT_TO_RIGHT);
  const Flow Flow::LEFT_TO_RIGHT(FLOW_LEFT_TO_RIGHT);
  const Flow Flow::VERTICAL(FLOW_TOP_TO_BOTTOM);
  const Flow Flow::TOP_TO_BOTTOM(FLOW_TOP_TO_BOTTOM);
}
