#include <nexus/Component.h>
#include <nexus/event/MotionEvent.h>
#include <nexus/event/WheelEvent.h>
#include <nexus/event/ButtonEvent.h>
#include <nexus/event/KeyEvent.h>
#include <nexus/event/FocusEvent.h>
#include <nexus/event/CrossingEvent.h>
#include <nexus/event/ComponentEvent.h>

#include <timber/logging.h>

#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <iostream>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace nexus {

   namespace {

      // we've got an invariant:
      // + if a child is newly created (i.e. its reference cout has NEVER been increased)
      // then the child is in the componentList
      // + if a child is added to a component then it will be in the list of children
      //   of the parent component
      // + if a child has no parent and its reference count is greater 0, then it is not in
      //   the global component list
      // + otherwise, the component is in the _componentList

      // should there be a peer list: if a component has no parent but a peer then
      // it should be in the peer list and not be available for destruction


      ::canopy::mt::RecursiveMutex _mutex;
      volatile bool _destroyWhenCollecting(false);
      volatile const Component* _componentList(0);
      volatile bool _collectionScheduled(false);
      volatile bool _debug(false);
      volatile Int _liveCount(0);

      struct ComponentGC : public Runnable
      {
         public:
            ComponentGC(void(*f)())throws() : _f(f) {}
            ~ComponentGC()throws() {}

         public:
            void run()
            {
               (*_f)();
            }

         private:
            void (*_f)();
      };

   }

   Pointer< Component> Component::convertRawPointer(ReferenceCount& cnt, Component* component)
   throws()
   {
      const RefGuard G(_mutex);

      // since we don't know if the pointer is valid, we need to use the reference count object directly
      if (cnt.count()== ReferenceCount::INVALID) {
         return Pointer<Component>();
      }

      // now we now that component is a valid pointer
      // and we start traversing the hierarchy to check for ancestors whose counts are invalid
      // if we have no parent, then we know we've hit the root of a component hierarchy and it's all valid
      Component* c = component;
      while(c->_parent) {
         c = c->_parent;
         if (c->_refCount->count() == ReferenceCount::INVALID) {
            return Pointer<Component>();
         }
      }

      return Pointer<Component>(component);
   }

   struct Component::EventObserver : public PeerComponent::KeyObserver,
         public PeerComponent::FocusObserver,
         public PeerComponent::CrossingObserver,
         public PeerComponent::MotionObserver,
         public PeerComponent::ButtonObserver,
         public PeerComponent::WheelObserver,
         public PeerComponent::ComponentObserver
   {
         EventObserver(Component& c) :
            _owner(c)
         {
         }
         ~EventObserver()throws() {}

         void notifyInsets(const Insets& pInsets)throws()
         {  _owner.updatePeerInsets(pInsets);}

         void notifyBounds(const Bounds& bounds)throws()
         {  _owner.setBounds(bounds);}

         void moved(Int x, Int y, const Time& when, const InputModifiers& m)throws()
         {
            const Reference<MotionEvent> e(new MotionEvent(&_owner,x,y,when,m));
            if (_owner.isMotionEventEnabled()) {
               _owner.processMotionEvent(e);
            }
            _owner._motionListeners.notify(&::nexus::event::MotionEventListener::processMotionEvent,e);

         }

         void wheelMoved(bool up, const Time& when, const InputModifiers& m)throws()
         {
            const Reference<WheelEvent> e(new WheelEvent(&_owner,
                        (up ? WheelEvent::WHEEL_UP: WheelEvent::WHEEL_DOWN)
                        ,when,m));
            if (_owner.isWheelEventEnabled()) {
               _owner.processWheelEvent(e);
            }
            _owner._wheelListeners.notify(&::nexus::event::WheelEventListener::processWheelEvent,e);
         }

         void buttonPressed(Int x, Int y, Int b, const Time& when, const InputModifiers& m)throws()
         {
            const Reference<ButtonEvent> e(new ButtonEvent(&_owner,ButtonEvent::BUTTON_PRESSED,x,y,b,when,m));

            if (_owner.isButtonEventEnabled()) {
               _owner.processButtonEvent(e);
            }
            _owner._buttonListeners.notify(&::nexus::event::ButtonEventListener::processButtonEvent,e);
         }

         void buttonReleased(Int x, Int y, Int b, const Time& when, const InputModifiers& m)throws()
         {
            const Reference<ButtonEvent> e(new ButtonEvent(&_owner,ButtonEvent::BUTTON_RELEASED,x,y,b,when,m));
            if (_owner.isButtonEventEnabled()) {
               _owner.processButtonEvent(e);
            }
            _owner._buttonListeners.notify(&::nexus::event::ButtonEventListener::processButtonEvent,e);
         }

         void buttonClicked(Int x, Int y, Int b, const Time& when, const InputModifiers& m)throws()
         {
            const Reference<ButtonEvent> e(new ButtonEvent(&_owner,ButtonEvent::BUTTON_CLICKED,x,y,b,when,m));
            if (_owner.isButtonEventEnabled()) {
               _owner.processButtonEvent(e);
            }
            _owner._buttonListeners.notify(&::nexus::event::ButtonEventListener::processButtonEvent,e);
         }

         void keyPressed(const Reference< ::nexus::peer::KeySymbol>& key, const Time& when, const InputModifiers& m)throws()
         {
            const Reference<KeyEvent> e(new KeyEvent(&_owner,KeyEvent::KEY_PRESSED,when,m,key));
            if (_owner.isKeyEventEnabled()) {
               _owner.processKeyEvent(e);
            }
            _owner._keyListeners.notify(&::nexus::event::KeyEventListener::processKeyEvent,e);
         }

         void keyTyped(const Reference< ::nexus::peer::KeySymbol>& key, const Time& when, const InputModifiers& m)throws()
         {
            const Reference<KeyEvent> e(new KeyEvent(&_owner,KeyEvent::KEY_TYPED,when,m,key));
            if (_owner.isKeyEventEnabled()) {
               _owner.processKeyEvent(e);
            }
            _owner._keyListeners.notify(&::nexus::event::KeyEventListener::processKeyEvent,e);
         }

         void keyReleased(const Reference< ::nexus::peer::KeySymbol>& key, const Time& when, const InputModifiers& m)throws()
         {
            const Reference<KeyEvent> e(new KeyEvent(&_owner,KeyEvent::KEY_RELEASED,when,m,key));
            if (_owner.isKeyEventEnabled()) {
               _owner.processKeyEvent(e);
            }
            _owner._keyListeners.notify(&::nexus::event::KeyEventListener::processKeyEvent,e);
         }

         void focusGained()throws()
         {
            const Reference<FocusEvent> e(new FocusEvent(&_owner,FocusEvent::FOCUS_GAINED));
            if (_owner.isFocusEventEnabled()) {
               _owner.processFocusEvent(e);
            }
            _owner._focusListeners.notify(&::nexus::event::FocusEventListener::processFocusEvent,e);
         }

         void focusLost()throws()
         {
            const Reference<FocusEvent> e(new FocusEvent(&_owner,FocusEvent::FOCUS_LOST));
            if (_owner.isFocusEventEnabled()) {
               _owner.processFocusEvent(e);
            }
            _owner._focusListeners.notify(&::nexus::event::FocusEventListener::processFocusEvent,e);
         }

         void componentEntered(const Time& when, const InputModifiers& m)throws()
         {
            const Reference<CrossingEvent> e(new CrossingEvent(&_owner,CrossingEvent::COMPONENT_ENTERED,when,m));
            if (_owner.isCrossingEventEnabled()) {
               _owner.processCrossingEvent(e);
            }
            _owner._crossingListeners.notify(&::nexus::event::CrossingEventListener::processCrossingEvent,e);
         }

         void componentExited(const Time& when, const InputModifiers& m)throws()
         {
            const Reference<CrossingEvent> e(new CrossingEvent(&_owner,CrossingEvent::COMPONENT_EXITED,when,m));
            if (_owner.isCrossingEventEnabled()) {
               _owner.processCrossingEvent(e);
            }
            _owner._crossingListeners.notify(&::nexus::event::CrossingEventListener::processCrossingEvent,e);
         }

      private:
         Component& _owner;
   };

   Component::Component()
   throws (::std::exception)
   : _refCount(RefCountType::create()),
   _parent(0),_children(0),
   _peer(0),
   _needLayout(false),
   _bounds(0,0,1,1),
   _inheritForegroundColor(true),
   _inheritBackgroundColor(true),
   _inheritFont(true),
   _enabled(true),
   _visible(true),
   _observer(new EventObserver(*this)),
   _inputEventMask(0),
   _selectedInputEventMask(0),
   _log("nexus.Component")
   {
      assert(Desktop::isEventThread());

      const RefGuard G(_mutex);
      ++_liveCount;
      if (log().isLoggable(Level::DEBUGGING)) {
         LogEntry(log()).debugging() << "Creating component " << this << doLog;
         if (_liveCount==1) {
            log().debugging("One live component");
         }
      }

      static bool scheduleAtExit = true;

      if (scheduleAtExit) {
         scheduleAtExit = false;
         ::std::atexit(sweepComponents);
      }

      // new components need to go onto the component list for eventual
      // garbage collection, if there isn't going to be a reference to it
      if (_componentList!=0) {
         _componentList->_previousComponent = this;
      }
      _previousComponent = 0;
      _nextComponent = _componentList;
      _componentList = this;
      scheduleCollection(G);
   }

   Component::~Component()
   throws()
   {
      assert(count()<=0);
      assert(_parent==0);
      assert(_children==0);
      assert(_peer==0);
      delete _observer;

      const RefGuard G(_mutex);
      --_liveCount;
      if (log().isLoggable(Level::DEBUGGING)) {
         LogEntry(log()).debugging() << "Deleting component " << this << doLog;
         if (_liveCount==0) {
            log().debugging("No more live components");
         }
      }
   }

   void Component::refInternal() const
   throws()
   {
      const Component* c = this;
      while(c->_refCount->ref()==1) {
         if (c->_parent) {
            c = c->_parent;
         }
         else {
            const RefGuard G(_mutex);
            c->moveComponent(G,const_cast<Component**>(&_componentList),0);
            break;
         }
      }
   }

   void Component::unrefInternal() const
   throws()
   {
      const Component* c = this;
      Int n;
      while((n = c->_refCount->unref()) == 0) {
         if (c->_parent) {
            c = c->_parent;
         }
         else {
            // the reference count of the top-level component
            // has reached 0; so mark it for deletion
            const RefGuard G(_mutex);
            c->moveComponent(G,0,const_cast<Component**>(&_componentList));
            break;
         }
      }
   }

   void Component::ref() const
   throws()
   {
      const RefGuard G(_mutex);
      refInternal();
   }

   void Component::unref() const
   throws()
   {
      const RefGuard G(_mutex);
      unrefInternal();
   }

   void Component::setDebugEnabled(bool enabled)
   throws()
   {  _debug=enabled;}

   void Component::moveComponent(const RefGuard&, Component** fromList, Component** toList) const
   throws()
   {
      const RefGuard G(_mutex);

      if (_nextComponent!=0) {
         _nextComponent->_previousComponent = _previousComponent;
      }
      if (_previousComponent==0) {
         if (fromList!=0) {
            assert(this==*fromList);
            *fromList = const_cast<Component*>(_nextComponent);
         }
      }
      else {
         _previousComponent->_nextComponent = _nextComponent;
         _previousComponent = 0;
      }
      if (fromList==&_componentList) {
         if (log().isLoggable(Level::DEBUGGING)) {
            LogEntry(log()).debugging() << "moveComponent: unclaimed component : " << this << doLog;
         }
      }

      _nextComponent = 0;
      if (toList!=0) {

         // we're going to add the component in the reverse order; the last
         // component that was added will be at the head of the list and the
         // first component add the tail of the list!!!
         _nextComponent = *toList;
         if (_nextComponent!=0) {
            _nextComponent->_previousComponent = this;
         }
         *toList = const_cast<Component*>(this);
         if (toList==&_componentList) {
            if (log().isLoggable(Level::DEBUGGING)) {
               LogEntry(log()).debugging() << "moveComponent: claimed component : " << this << doLog;
            }
            if (_destroyWhenCollecting) {
               destroyComponent(this);
            }
            else {
               scheduleCollection(G);
            }
         }
      }
   }

   void Component::scheduleCollection(const RefGuard&)
   throws()
   {
      if (!_collectionScheduled) {
         _collectionScheduled = true;
         Desktop::enqueue(::std::make_shared<ComponentGC>(collectComponents));
      }
   }

   void Component::scheduleCollection()
   throws()
   {
      RefGuard G(_mutex);
      scheduleCollection(G);
   }

   void Component::destroyComponent(const Component* c)
   throws()
   {
      if (c->hasPeer()) {
         const_cast<Component*>(c)->destroyPeer();
      }
      delete c;
   }

   void Component::sweepComponents()
   throws()
   {
      const RefGuard G(_mutex);
      collectComponents();
      _destroyWhenCollecting = true;

      while (_componentList!=0) {
         Component* c = const_cast<Component*>(_componentList);
         c->moveComponent(G,const_cast<Component**>(&_componentList),0);
         Level level(Level::DEBUGGING);
         if (c->log().isLoggable(level)) {
            LogEntry(c->log(),level).stream() << "Sweeping component : " << c << "( "<<typeid(*c).name() << " )" << doLog;
         }
      }
   }

   void Component::collectComponents()
   throws()
   {
      const RefGuard G(_mutex);
      Log("nexus.Component").debugging("Collecting components");
      while (_componentList!=0 && _componentList->_parent==0) {

         //FIXME: should we invalidate the pointer here?
         Component* c = const_cast<Component*>(_componentList);

         // the component may or may not be valid but it's reference count must be 0
         bool ok = c->_refCount->invalidate();
         if (!ok) {
            LogEntry("nexus.Component").warn() << "collectComponents: could not invalidate component " << c << doLog;
            continue;
         }

         const Level level(Level::DEBUGGING);
         if (c->log().isLoggable(level)) {
            LogEntry(c->log(),level).stream() << "Collecting component : " << c << doLog;
         }
         c->moveComponent(G,const_cast<Component**>(&_componentList),0);
         assert(c!=_componentList);
         destroyComponent(c);
      }
      _collectionScheduled = false;
   }

   void Component::destroyPeerComponent(PeerComponent* peer)
   throws()
   {
      Desktop::destroyComponentPeer(peer);
   }

   void Component::createPeer()
   throws()
   {
      assert(_parent==0 || _parent->hasPeer());
      if (_parent==0) {
         _peer = createPeerComponent(0);
      }
      else {
         assert(_parent->_peer!=0);
         _peer = createPeerComponent(_parent->_peer);
      }

      if (!_peer) {
         _log.warn(string("Could not create peer for component ")+typeid(*this).name());
         return;
      }

      // set the component observer
      _peer->setComponentObserver(_observer);

      // update the event mask as needed
      updateEventMask();

      // initialize the peer
      initializePeer();

      // create the peers for any attached components
      createChildPeers();

      // set the visibility last so that everything has been set up
      _peer->setVisible(isVisible());
   }

   void Component::createChildPeers()
   throws()
   {
      Component* c = _children;
      if (c==0) {
         return;
      }

      // since the children of c are organized in the reverse order
      // we need to traverse the list from back to front
      // so, first, let's find the last child
      while(c->_nextComponent) {
         c=const_cast<Component*>(c->_nextComponent);
      }
      // we'll have a valid c at this point
      assert(c);

      // now, traverse back to front
      do {
         c->createPeer();
         c=const_cast<Component*>(c->_previousComponent);
      }while(c!=0);

      // now, child peers have been created in the order in which they have been attached
   }

   void Component::initializePeer()
   throws()
   {
      // complete peer initialization
      // set the foreground, background, and font
      _peer->setForegroundColor(_foreground,false);
      _peer->setBackgroundColor(_background,false);
      _peer->setFont(_font,false);
   }

   void Component::destroyPeer()
   throws()
   {
      // enable the current event mask, but since there is no
      // peer, the observers will be removed
      setEventMask(0);

      PeerComponent* tmp = _peer;
      _peer = 0;

      // destroy the child peers as well
      Component* c = _children;
      while (c!=0) {
         c->destroyPeer();
         c=const_cast<Component*>(c->_nextComponent);
      }

      destroyPeerComponent(tmp);
   }

   Component* Component::attachChild(const ::timber::Pointer< Component>& c)
   throws()
   {
      if (c) {
         assert(c->_parent==static_cast<Component*>(0));
         assert(!c->hasPeer());

         // since c has become a child we also need to increase the reference
         // count of this component
         {
            const RefGuard G(_mutex);
            c->_parent = this;
            refInternal();
            // since we have a good pointer to the child, we know its not in the
            // componentList
            c->moveComponent(G,0,const_cast<Component**>(&_children));
         }

         c->updateFont(_font,false);
         c->updateForegroundColor(_foreground,false);
         c->updateBackgroundColor(_background,false);

         // if the parent has a peer, then create it as well
         if (hasPeer()) {
            c->createPeer();
         }
      }
      return c.unsafePointer();
   }

   ::timber::Pointer<Component> Component::detachChild (Component* c) throws()
   {
      Pointer<Component> res;
      if (c) {
         assert(this==c->_parent);

         // destroy the current peer
         if (c->hasPeer()) {
            c->destroyPeer();
         }

         // first we need to detach the child before incrementing its reference count
         {
            const RefGuard G(_mutex);
            c->_parent=0;
            // because we're going to return a pointer to the child we don't need
            // to move it back to the global component list
            c->moveComponent(G,const_cast<Component**>(&_children),0);
            // only if there is some reference to the child do we need
            // to reduce the reference count;  we don't want to call
            // c->refInternal here, because THIS component may be in its destructor
            // already (as a matter of fact, the refcount value may be -1)
            if (c->count() > 0) {
               unrefInternal();
            }

            // temporarily increment the reference count of the child to
            // prevent the execution of the loop in refInternal; this way
            // the moveComponent function will not assert
            c->_refCount->ref();
            res = c;
            c->_refCount->unref();
         }

      }
      return res;
   }

   void Component::setName(const string& n)
throws (::std::exception)
{
   if (n==_name) {
      return;
   }

   if (n.empty()) {
      ::std::ostringstream tmp;
      tmp << this;
      _name = tmp.str();
      return;
   }

   // check the name
   for (string::const_iterator i=n.begin();i!=n.end();++i) {
      if (!isalnum(*i)) {
         throw ::std::invalid_argument("Not a valid component name "+n);
      }
   }
   if (!isalpha(n[0])) {
      throw ::std::invalid_argument("Component must begin with a letter");
   }
   _name = n;
}

::std::ostream& Component::print(::std::ostream& out, bool, Int indent) const
throws()
{
   ::std::string indentString;
   for (Index i=0;i<indent;++i) {
      indentString += "    ";
   }
   SizeHints sz(sizeHints());

   out << indentString << "Component " << name() << " [ " << typeid(*this).name() << " ] :" << '\n'
   << indentString << " bounds    : " << bounds() << '\n'
   << indentString << " best size : " << sz.preferredSize() << '\n'
   << indentString << " min size  : " << sz.minimumSize() << '\n'
   << indentString << " max size  : " << sz.maximumSize() << '\n'
   << indentString << " bg color  : " << backgroundColor() << '\n'
   << indentString << " fg color  : " << foregroundColor() << '\n'
   << indentString << " visible   : " << (isVisible()?"YES":"NO") << '\n'
   ;
   return out << ::std::flush;
}

Pixel Component::locationOnScreen() const
throws()
{
   const Component* c = this;
   Int px = c->x();
   Int py = c->y();
   c = c->_parent;
   while(c) {
      px += c->_peerInsets.left();
      py += c->_peerInsets.top();
      px += c->x();
      py += c->y();
      c = c->_parent;
   }
   return Pixel(px,py);
}

Pixel Component::pixelOnScreen(const Pixel& p) const
throws()
{
   // get the location of the top-left corner of this component on the screen
   const Pixel q = locationOnScreen();
   // add in the pixel's local coordinates and we have the actual location
   // of the pixel on the screen; we must not however forget the insets

   return Pixel(q.x()+p.x()+_peerInsets.left(),q.y()+p.y()+_peerInsets.top());
}

void Component::setBounds(const Bounds& b)
throws()
{
   assert (b.width() >= 0 && b.height()>=0);

   const bool moved = b.x()!=x() || b.y()!=y();
   const bool newSize = b.width()!=width() || b.height()!=height();
   if (newSize || moved) {
      _bounds = b;
   }

   // notify the subclass that the size has changed
   if (newSize || _needLayout) {
      layoutComponent();
   }

   if (newSize) {
      generateComponentEvent(ComponentEvent::COMPONENT_RESIZED);
   }
   if (moved) {
      generateComponentEvent(ComponentEvent::COMPONENT_MOVED);
   }
}

void Component::setEnabled(bool enabled)
throws()
{
   if (enabled!=_enabled) {
      _enabled = enabled;
      if (hasPeer()) {
         _peer->setEnabled(enabled);
      }
   }
}

void Component::setVisible(bool visible)
throws()
{
   if (visible!=_visible) {
      _visible = visible;
      if (hasPeer()) {
         _peer->setVisible(visible);
      }
   }
}

void Component::setFont(const ::timber::Pointer< ::indigo::Font>& f)
throws()
{
   updateFont(f,true);
}

void Component::setForegroundColor(const Color& c)
throws()
{  updateForegroundColor(c,true);}

void Component::setBackgroundColor(const Color& c)
throws()
{  updateBackgroundColor(c,true);}

void Component::updateFont(const ::timber::Pointer< ::indigo::Font>& f, bool ext)
throws()
{
   if (ext || _inheritFont) {

      _font = f;
      if (f==nullptr) {
         _inheritFont = true;
         if (_parent!=0) {
            _font = _parent->_font;
         }
      }
      else {
         _inheritFont = !ext;
      }

      // notify about the font change
      generateComponentEvent(ComponentEvent::FONT_CHANGED);
      fontChanged();

      if (hasPeer() && _font!=nullptr) {
         _peer->setFont(_font,_inheritFont);
      }
      // each child should now inherit the font
      Component* cc = _children;
      while (cc!=0) {
         cc->updateFont(_font,false);
         cc=const_cast<Component*>(cc->_nextComponent);
      }

   }
}

void Component::updateForegroundColor(const Color& c, bool ext)
throws()
{
   if (ext || _inheritForegroundColor) {

      _foreground = c;
      if (c==0) {
         _inheritForegroundColor = true;
         if (_parent!=0) {
            _foreground = _parent->_foreground;
         }
      }
      else {
         _inheritForegroundColor = !ext;
      }
      foregroundColorChanged();
      if (hasPeer() && _foreground!=0) {
         _peer->setForegroundColor(_foreground,_inheritForegroundColor);
      }
      // each child should now inherit the color
      Component* cc = _children;
      while (cc!=0) {
         cc->updateForegroundColor(_foreground,false);
         cc=const_cast<Component*>(cc->_nextComponent);
      }
   }
}

void Component::updateBackgroundColor(const Color& c, bool ext)
throws()
{
   if (ext || _inheritBackgroundColor) {

      _background = c;
      if (c==0) {
         _inheritBackgroundColor = true;
         if (_parent!=0) {
            _background = _parent->_background;
         }
      }
      else {
         _inheritBackgroundColor = !ext;
      }

      backgroundColorChanged();
      if (hasPeer() && _background!=0) {
         _peer->setBackgroundColor(_background,_inheritBackgroundColor);
      }
      // each child should now inherit the color
      Component* cc = _children;
      while (cc!=0) {
         cc->updateBackgroundColor(_background,false);
         cc=const_cast<Component*>(cc->_nextComponent);
      }
   }
}

void Component::revalidate()
throws()
{
   struct LayoutFunction : public Runnable {
      inline LayoutFunction(Component* c) throws()
      : _c(c)
      {
         assert(c!=0);
         _c->ref();
      }

      ~LayoutFunction () throws()
      {  _c->unref();}

      void run ()
      {
         if (_c->_needLayout) {
            Component* c = _c->_parent;
            while (c!=0) {
               if (c->_needLayout) {
                  return;
               }
               c = c->_parent;
            }
            _c->layoutComponent();
         }
      }

      private:
      Component* _c;
   };

   if (_needLayout) {
      // already need to layout this component
      return;
   }

   _needLayout = true;

   if (hasPeer()) {
      auto xLayout=::std::make_shared<LayoutFunction>(this);
      Desktop::enqueue(xLayout);
   }

   // if this component needs to be laid out again,
   // then bubble up to the parent
   if (_needLayout && _parent!=0) {
      _parent->revalidate();
   }
}

bool Component::isMotionEventEnabled() const
throws()
{  return (_selectedInputEventMask & ::nexus::event::InputEvent::MOTION_EVENT) != 0;}

bool Component::isWheelEventEnabled() const
throws()
{  return (_selectedInputEventMask & ::nexus::event::InputEvent::WHEEL_EVENT) != 0;}

bool Component::isButtonEventEnabled() const
throws()
{  return (_selectedInputEventMask & ::nexus::event::InputEvent::BUTTON_EVENT) != 0;}

bool Component::isFocusEventEnabled() const
throws()
{  return (_selectedInputEventMask & ::nexus::event::InputEvent::FOCUS_EVENT) != 0;}

bool Component::isKeyEventEnabled() const
throws()
{  return (_selectedInputEventMask & ::nexus::event::InputEvent::KEY_EVENT) != 0;}

bool Component::isCrossingEventEnabled() const
throws()
{  return (_selectedInputEventMask & ::nexus::event::InputEvent::CROSSING_EVENT) != 0;}

void Component::setMotionEventEnabled(bool enable)
throws()
{  selectInputEvent(::nexus::event::InputEvent::MOTION_EVENT,enable);}

void Component::setWheelEventEnabled(bool enable)
throws()
{  selectInputEvent(::nexus::event::InputEvent::WHEEL_EVENT,enable);}

void Component::setButtonEventEnabled(bool enable)
throws()
{  selectInputEvent(::nexus::event::InputEvent::BUTTON_EVENT,enable);}

void Component::setKeyEventEnabled(bool enable)
throws()
{  selectInputEvent(::nexus::event::InputEvent::KEY_EVENT,enable);}

void Component::setFocusEventEnabled(bool enable)
throws()
{  selectInputEvent(::nexus::event::InputEvent::FOCUS_EVENT,enable);}

void Component::setCrossingEventEnabled(bool enable)
throws()
{  selectInputEvent(::nexus::event::InputEvent::CROSSING_EVENT,enable);}

void Component::generateComponentEvent(Int id)
throws()
{
   if (_componentListeners.listenerCount()>0) {
      ComponentEvent::EventID eid = static_cast<ComponentEvent::EventID>(id);
      const Reference<ComponentEvent> e(new ComponentEvent(this,eid));
      _componentListeners.notify(&::nexus::event::ComponentEventListener::processComponentEvent,e);
   }
}

void Component::updateEventMask()
throws()
{
   Int mask=0;
   if (hasPeer()) {
      mask = _selectedInputEventMask;
      if (_wheelListeners.listenerCount()>0) {
         mask |= ::nexus::event::InputEvent::WHEEL_EVENT;
      }
      if (_motionListeners.listenerCount()>0) {
         mask |= ::nexus::event::InputEvent::MOTION_EVENT;
      }
      if (_buttonListeners.listenerCount()>0) {
         mask |= ::nexus::event::InputEvent::BUTTON_EVENT;
      }
      if (_keyListeners.listenerCount()>0) {
         mask |= ::nexus::event::InputEvent::KEY_EVENT;
      }
      if (_focusListeners.listenerCount()>0) {
         mask |= ::nexus::event::InputEvent::FOCUS_EVENT;
      }
      if (_crossingListeners.listenerCount()>0) {
         mask |= ::nexus::event::InputEvent::CROSSING_EVENT;
      }
   }
   setEventMask(mask);
}

void Component::setEventMask(Int newMask)
throws()
{
   if (!hasPeer()) {
      _inputEventMask = 0;
      return;
   }

   const Int oldMask = _inputEventMask;
   _inputEventMask = newMask;

   const Int addObserverMask = _inputEventMask & (~oldMask);
   const Int remObserverMask = oldMask & (~_inputEventMask);

   // first, create an event observer if necessary
   if (addObserverMask!=0) {
      if ((addObserverMask & ::nexus::event::InputEvent::MOTION_EVENT)!=0) {
         _peer->setMotionObserver(_observer);
      }
      if ((addObserverMask & ::nexus::event::InputEvent::WHEEL_EVENT)!=0) {
         _peer->setWheelObserver(_observer);
      }
      if ((addObserverMask & ::nexus::event::InputEvent::BUTTON_EVENT)!=0) {
         _peer->setButtonObserver(_observer);
      }
      if ((addObserverMask & ::nexus::event::InputEvent::KEY_EVENT)!=0) {
         _peer->setKeyObserver(_observer);
      }
      if ((addObserverMask & ::nexus::event::InputEvent::FOCUS_EVENT)!=0) {
         _peer->setFocusObserver(_observer);
      }
      if ((addObserverMask & ::nexus::event::InputEvent::CROSSING_EVENT)!=0) {
         _peer->setCrossingObserver(_observer);
      }
   }

   // now, clean up all old observers that we no longer want
   if (remObserverMask!=0) {
      if ((remObserverMask & ::nexus::event::InputEvent::MOTION_EVENT)==0) {
         _peer->setMotionObserver(0);
      }
      if ((remObserverMask & ::nexus::event::InputEvent::BUTTON_EVENT)==0) {
         _peer->setButtonObserver(0);
      }
      if ((remObserverMask & ::nexus::event::InputEvent::WHEEL_EVENT)==0) {
         _peer->setWheelObserver(0);
      }
      if ((remObserverMask & ::nexus::event::InputEvent::KEY_EVENT)==0) {
         _peer->setKeyObserver(0);
      }
      if ((remObserverMask & ::nexus::event::InputEvent::FOCUS_EVENT)==0) {
         _peer->setFocusObserver(0);
      }
      if ((remObserverMask & ::nexus::event::InputEvent::CROSSING_EVENT)==0) {
         _peer->setCrossingObserver(0);
      }
   }

}

void Component::selectInputEvent(Int ieClass, bool enable)
throws()
{
   if (enable) {
      _selectedInputEventMask |= ieClass;
   }
   else {
      _selectedInputEventMask &= ~ieClass;
   }
   updateEventMask();
}

void Component::processWheelEvent(const Reference< WheelEvent>&)
throws()
{
}

void Component::processMotionEvent(const Reference< MotionEvent>&)
throws()
{
}

void Component::processButtonEvent(const Reference< ButtonEvent>&)
throws()
{
}

void Component::processKeyEvent(const Reference< KeyEvent>&)
throws()
{
}

void Component::processFocusEvent(const Reference< FocusEvent>&)
throws()
{
}

void Component::processCrossingEvent(const Reference< CrossingEvent>&)
throws()
{
}

void Component::fontChanged()
throws()
{
}

void Component::backgroundColorChanged()
throws()
{
}

void Component::foregroundColorChanged()
throws()
{}

void Component::doLayout()
throws (LayoutException)
{}

void Component::layoutComponent () throws()
{
   _needLayout = false;
   try {
      doLayout();
   }
   catch (const LayoutException& e) {
      log().caught("Problem layout a component",e);
   }
}

void Component::addComponentEventListener(const Reference< ::nexus::event::ComponentEventListener>& l)
throws()
{
   _componentListeners.addListener(l);
   updateEventMask();
}

void Component::removeComponentEventListener(const Pointer< ::nexus::event::ComponentEventListener>& l)
throws()
{
   _componentListeners.removeListener(l);
   updateEventMask();
}

void Component::addMotionEventListener(const Reference< ::nexus::event::MotionEventListener>& l)
throws()
{
   _motionListeners.addListener(l);
   updateEventMask();
}

void Component::removeMotionEventListener(const Pointer< ::nexus::event::MotionEventListener>& l)
throws()
{
   _motionListeners.removeListener(l);
   updateEventMask();
}

void Component::addWheelEventListener(const Reference< ::nexus::event::WheelEventListener>& l)
throws()
{
   _wheelListeners.addListener(l);
   updateEventMask();
}

void Component::removeWheelEventListener(const Pointer< ::nexus::event::WheelEventListener>& l)
throws()
{
   _wheelListeners.removeListener(l);
   updateEventMask();
}

void Component::addButtonEventListener(const Reference< ::nexus::event::ButtonEventListener>& l)
throws()
{
   _buttonListeners.addListener(l);
   updateEventMask();
}

void Component::removeButtonEventListener(const Pointer< ::nexus::event::ButtonEventListener>& l)
throws()
{
   _buttonListeners.removeListener(l);
   updateEventMask();
}

void Component::addFocusEventListener(const Reference< ::nexus::event::FocusEventListener>& l)
throws()
{
   _focusListeners.addListener(l);
   updateEventMask();
}

void Component::removeFocusEventListener(const Pointer< ::nexus::event::FocusEventListener>& l)
throws()
{
   _focusListeners.removeListener(l);
   updateEventMask();
}

void Component::addKeyEventListener(const Reference< ::nexus::event::KeyEventListener>& l)
throws()
{
   _keyListeners.addListener(l);
   updateEventMask();
}

void Component::removeKeyEventListener(const Pointer< ::nexus::event::KeyEventListener>& l)
throws()
{
   _keyListeners.removeListener(l);
   updateEventMask();
}

void Component::addCrossingEventListener(const Reference< ::nexus::event::CrossingEventListener>& l)
throws()
{
   _crossingListeners.addListener(l);
   updateEventMask();
}

void Component::removeCrossingEventListener(const Pointer< ::nexus::event::CrossingEventListener>& l)
throws()
{
   _crossingListeners.removeListener(l);
   updateEventMask();
}

bool Component::isShowing() const
throws()
{  return Desktop::isComponentShowing(const_cast<Component*>(this));}

void Component::updatePeerInsets(const Insets& nInsets)
throws()
{
   if (_peerInsets!=nInsets) {
      _peerInsets = nInsets;
      // should do some sort of notification here and relayout
      if (_log.isLoggable(Level::DEBUGGING)) {
         LogEntry(_log).debugging() << "New insets : " << nInsets << doLog;
      }
      revalidate();
   }

}

Size Component::layoutSize() const
throws()
{
   Size sz = size();
   return Size(sz.width()-_peerInsets.left() - _peerInsets.right(),
         sz.height()-_peerInsets.top() - _peerInsets.bottom());
}

SizeHints Component::sizeHints() const
throws()
{
   return getSizeHints().addInsets(_peerInsets);
}

}

