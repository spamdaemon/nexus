#ifndef _NEXUS_SIZEHINTS_H
#define _NEXUS_SIZEHINTS_H

#ifndef _NEXUS_SIZE_H
#include <nexus/Size.h>
#endif

#include <iosfwd>

namespace nexus {
  class Insets;

  /**
   * This class defines the hints that a component
   * can provide to its parent about its size requirements.
   */
  class SizeHints {
    /** 
     * Create default size hints.
     * The minimum and preferred sizes are set to Size::MIN,
     * and the maximum size is set to Size::MAX
     */
  public:
    SizeHints () throws();
	    
    /**
     * Create new size hints. Consistency of the hints is not required,
     * but may cause some non-sensical results.
     * @param m the minimum size
     * @param M the maximum size
     * @param p the preferred size
     */
  public:
    SizeHints (const Size& m, const Size& M, const Size& p) throws();

    /**
     * Create a size hints that sets the preferred, minimum, and
     * maximum sizes to the same value
     */
  public:
    SizeHints (const Size& sz) throws();

    /**
     * Create a new insets by accounting for the specified insets.
     * @param insets an insets object
     * @return size hints
     */
  public:
    SizeHints addInsets (const Insets& insets) const throws();

    /**
     * Create a new insets by removing the space for the specified insets.
     * @param insets an insets object
     * @return size hints
     */
  public:
    SizeHints removeInsets (const Insets& insets) const throws();

    /**
     * Test if the specified size is contained enclosed.
     * @param sz a size
     * @return true if the specified size is between the minimum and maximum sizes.
     */
  public:
    bool containsSize (const Size& sz) const throws();

    /**
     * Modify the size hints so that the specified size is enclosed.
     * @param sz a size
     * @return size hints based on these hints
     */
  public:
    SizeHints encloseSize (const Size& sz) const throws();

    /**
     * Modify a size to fit into the bounds specified by these hints.
     * @param sz a size
     * @return a size such that these hints contain it inside.
     */
  public:
    Size fitSize (const Size& sz) const throws();

    /**
     * Get the preferred size.
     * @return the preferred size.
     */
  public:
    inline const Size& preferredSize () const throws() { return _preferred; }

    /**
     * Get the maximum size.
     * @return the maximum size
     */
  public:
    inline const Size& maximumSize () const throws() { return _max; }

    /**
     * Get the maximum size.
     * @return the maximum size
     */
  public:
    inline const Size& minimumSize () const throws() { return _min; }

    /**
     * Get the minimum width.
     * @return the minimum width
     */
  public:
    inline Int minimumWidth() const throws() { return _min.width(); }

    /**
     * Get the minimum height.
     * @return the minimum height
     */
  public:
    inline Int minimumHeight() const throws() { return _min.height(); }

    /**
     * Get the maximum width.
     * @return the maximum width
     */
  public:
    inline Int maximumWidth() const throws() { return _max.width(); }

    /**
     * Get the maximum height.
     * @return the maximum height
     */
  public:
    inline Int maximumHeight() const throws() { return _max.height(); }

    /**
     * Get the preferred width.
     * @return the preferred width
     */
  public:
    inline Int preferredWidth() const throws() { return _preferred.width(); }

    /**
     * Get the preferred height.
     * @return the preferred height
     */
  public:
    inline Int preferredHeight() const throws() { return _preferred.height(); }

    /**
     * Test if these hints make sense. SizeHints are self
     * consistent if <br>
     * <code>
     *    minimumWidth()<=maximumWidth() <br>
     * &amp;&amp; minimumHeight()<=maximumHeight()<br>
     * &amp;&amp; containsSize(preferredSize())
     * </code>
     * @return true if the hints are not self-consistent.
     */
  public:
    bool isConsistent () const throws();

    /**
     * Compute a hashvalue.
     * @return a hashvalue
     */
  public:
    size_t hashValue () const throws();

    /**
     * Compare two size hints.
     * @param d size hints
     * @return true if this and d are equal
     */
  public:
    bool operator==(const SizeHints& d) const throws();

    /**
     * Compare two size hints.
     * @param d size hints
     * @return true if this and d are equal
     */
  public:
    inline bool equals(const SizeHints& d) const throws()
    { return *this == d; }

    /**
     * Compare two size hints.
     * @param d size hints
     * @return true if this and d are different size
     */
  public:
    bool operator!=(const SizeHints& d) const throws();

    /** The minimium component size */
  private:
    Size _min;

    /** The maximum component size */
  private:
    Size _max;

    /** The preferred component size */
  private:
    Size _preferred;
  };
}

::std::ostream& operator<< (::std::ostream& out, const ::nexus::SizeHints& hints) throws();

#endif
