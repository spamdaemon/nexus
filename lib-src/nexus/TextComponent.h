#ifndef _NEXUS_TEXTCOMPONENT_H
#define _NEXUS_TEXTCOMPONENT_H

#ifndef _NEXUS_COMPONENT_H
#include <nexus/Component.h>
#endif

#ifndef _NEXUS_PEER_TEXTCOMPONENT_H
#include <nexus/peer/TextComponent.h>
#endif

#ifndef _NEXUS_TEXT_TEXTMODEL_H
#include <nexus/text/TextModel.h>
#endif

namespace nexus {
  /**
   * This is the base class for components that allow for text input.
   * @date 01 Aug 2003
   */
  class TextComponent : public Component {
    TextComponent(const TextComponent&);
    TextComponent&operator=(const TextComponent&);

    /** The default border width */
  public:
    static const Int DEFAULT_BORDER_WIDTH = 2;

    /** The implementation class is a container */
  public:
    typedef ::nexus::peer::TextComponent Peer;

    /** A reference to a text model */
  public:
    typedef ::timber::Reference< ::nexus::text::TextModel> TextModel;

    /**
     * Create a new text model with the default text model.
     * @throws ConfigurationException if the desktop was not initialized
     */
  public:
    TextComponent () throws (::std::exception);

    /**
     * Create a new text component with the specified text model.
     * @param m a text model
     * @pre REQUIRE_NON_ZERO(m)
     * @throws ConfigurationException if the desktop was not initialized
     */
  public:
    TextComponent (const TextModel& m) throws (::std::exception);

    /** Destroy this textcomponent */
  public:
    ~TextComponent () throws();

    /**
     * Get the text for this textcomponent. 
     * @return the text for this TextComponent
     */
  public:
    inline const ::std::string& text() const throws()
    { return _model->text(); }

    /**
     * Get the length of the text.
     * @return the length of the text.
     */
  public:
    inline Int textLength() const throws()
    { return _model->textLength(); }

    /**
     * Get the text model.
     * @return the text model
     */
  public:
    inline TextModel textModel () const throws()
    { return _model; }

    /**
     * Test if this is a multi-line text component.
     * @return true if this type of component is capable of displaying multiple lines of text.
     */
  public:
    virtual bool isMultiLineCapable () const throws() = 0;
	
    /**
     * Make this textcomponent either editable or non-editable.
     * @param editingEnabled if true, then user can edit contents
     */
  public:
    virtual void setEditable (bool editingEnabled) throws();
	
    /**
     * Test if this textcomponent can be edited.
     * @return true if this textcomponent can be edited
     */
  public:
    inline bool isEditable () const throws()
    { return _editable; }
      
    /**
     * Set the border width.
     * @param w the border width
     * @pre REQUIRE_GREATER_OR_EQUAL(w,0)
     */
  public:
    virtual void setBorderWidth (Int w) throws();

    /**
     * Get the border width.
     * @return the border width 
     */
  public:
    inline Int borderWidth () const throws()
    { return _borderWidth; }

  protected:
    void initializePeer () throws();
    void destroyPeer() throws();
    void fontChanged () throws();
	
  protected:
    SizeHints getSizeHints () const throws();

    /**
     * Update the size hints. Calls computeSizeHints().
     * The preferred size is enlarged by the current border width.
     */
  protected:
    void updateSizeHints () throws();

    /**
     * Compute new size hints. The computed size hints are 
     * cached and returned by the call sizeHints(). The computed
     * size hints should <em>not</em> account for the border width.
     * @return the size hints
     */
  protected:
    virtual SizeHints computeSizeHints () const throws() = 0;

    /**
     * Set the text for this textfield.
     * @param t the text for this textfield or 0
     * @throws Exception if the text could not be set
     */
  public:
    inline void setText (const ::std::string& t) throws(::std::exception)
    { _model->setText(t); }
	
    /**
     * Set the model.
     * @param m a new model
     * @pre REQUIRE_NON_ZERO(m)
     */
  protected:
    void setTextModel (const TextModel& m) throws();

    /**
     * Get the window peer.
     * @return the widow peer
     */
  private:
    inline Peer* textComponent() const throws()
    { return impl<Peer>(); }

    /** True if the text can be edited */
  private:
    bool _editable;

    /** The border width */
  private:
    Int _borderWidth;

    /** The minimum, maximum, and preferred sizes */
  private:
    SizeHints _sizeHints;

    /** An observer */
  private:
    Peer::TextModel* _observer;

    /** A text model listener */
  private:
    ::timber::Pointer< ::nexus::text::TextModelEventListener> _modelListener;

    /** A text model */
  private:
    TextModel _model;
  };
}

#endif
