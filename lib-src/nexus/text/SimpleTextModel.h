#ifndef _NEXUS_TEXT_SIMPLETEXTMODEL_H
#define _NEXUS_TEXT_SIMPLETEXTMODEL_H

#ifndef _NEXUS_TEXT_ABSTRACTTEXTMODEL_H
#include <nexus/text/AbstractTextModel.h>
#endif 

namespace nexus {
  namespace text {
    /**
     * This simple text model partially implements TextModel.
     */
    class SimpleTextModel : public AbstractTextModel {
      SimpleTextModel&operator=(const SimpleTextModel&);

      /** Default constructor */
    public:
      SimpleTextModel () throws();

      /** 
       * Copy constructor.
       * @param c a text model
       */
    public:
      SimpleTextModel (const SimpleTextModel& c) throws();

      /** Destroy this model */
    public:
      ~SimpleTextModel () throws();

      /** 
       * Get the text stored in this model.
       * @return the text stored in this model.
       */
    public:
      const ::std::string& text() const throws();

      /**
       * Get the character at the specified index.
       * @param pos the position
       * @pre REQUIRE_RANGE(pos,0,textLength()-1)
       * @return the character at the specified position
       */
    public:
      inline char charAt (size_type pos) const throws()
      { return _text.at(pos); }

    protected:
      void replace (size_type pos, size_type count, const ::std::string& s) throws(::std::exception);
	
      /** The text */
    private:
      ::std::string _text;
    };
  }
}

#endif
