#include <nexus/text/SimpleTextModel.h>

namespace nexus {
  namespace text {
    

    SimpleTextModel::SimpleTextModel () throws() 
    {}
      
    SimpleTextModel::~SimpleTextModel () throws() 
    {}
      
    const ::std::string& SimpleTextModel::text() const throws()
    { return _text; }
    
    void SimpleTextModel::replace (size_type pos, size_type cnt, const ::std::string& s) throws(::std::exception)
    {
      if (isNotifyingListeners()) {
	throw ::std::runtime_error("Cannot modify text model from listener");
      }
      if (s.length()>0 || cnt>0) {
	_text.replace(pos,cnt,s); 
	fireTextModelEvent(TextModelEvent(this,pos,cnt,s));
      }
    }
  }
}

