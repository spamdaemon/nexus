#include <nexus/text/AbstractTextModel.h>
 
namespace nexus {
  namespace text {

    AbstractTextModel::AbstractTextModel () throws() 
    {}
     
    AbstractTextModel::AbstractTextModel (const AbstractTextModel& i) throws() 
    : TextModel(i) 
    {}
     
    AbstractTextModel::~AbstractTextModel () throws() {}

    void AbstractTextModel::addTextModelEventListener (const ::timber::Reference<TextModelEventListener>& l) throws()
    {	_listeners.addListener(l); }

    void AbstractTextModel::removeTextModelEventListener (const ::timber::Pointer<TextModelEventListener>& l) throws()
    {	_listeners.removeListener(l); }
      
    void AbstractTextModel::fireTextModelEvent (const TextModelEvent& e) throws()
    {
      assert(!isNotifyingListeners());
      _listeners.notify(&TextModelEventListener::textModelChanged,e); 
    }
     
    bool AbstractTextModel::isNotifyingListeners () const throws()
    { return _listeners.isDispatching(); }
      
  }
}
