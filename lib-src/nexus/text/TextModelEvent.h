#ifndef _NEXUS_TEXT_TEXTMODELEVENT_H
#define _NEXUS_TEXT_TEXTMODELEVENT_H

#ifndef _NEXUS_TEXT_TEXTMODEL_H
#include <nexus/text/TextModel.h>
#endif

namespace nexus {
  namespace text {

    /**
     * This class represents action events that are
     * caused by the user pressing some key or button.
     */
    class TextModelEvent {
      /* No copy method */
      TextModelEvent&operator=(TextModelEvent);
	
      /** The size type */
    public:
      typedef TextModel::size_type size_type;

      /**
       * Create a new event.
       * @param m the text model that caused this event
       * @pre REQUIRE_NON_ZERO(m)
       * @param pos the position of the first character that was replaced
       * @param cnt the number of characters replaced
       * @param s the replacing string
       */
    public:
      inline TextModelEvent (const ::timber::Reference<TextModel>& m, size_type pos, size_type cnt, const ::std::string& s) throws()
	: _model(m),_pos(pos),_count(cnt),_text(s) {}

      /** Destroy this event */
    public:
      ~TextModelEvent () throws() {}
	  
      /**
       * Get the text model that caused this event.
       * @return the text model that generated this event.
       */
    public:
      inline ::timber::Reference<TextModel> model() const throws()
      { return _model; }

      /**
       * Get the index of the first character to be replaced.
       * @return the index of the first character to be replaced
       */
    public:
      inline size_type position () const throws() { return _pos; }

      /**
       * Get the number of characters to be replaced.
       * @return the number of characters to be replaced
       */
    public:
      inline size_type count () const throws() { return _count; }

      /**
       * Get the string that replaced a substring of the original text.
       * @return the string that replaced a substring of the original text
       */
    public:
      inline const ::std::string& replacement() const throws() { return _text; }

      /** The model that caused this event */
    private:
      ::timber::Reference<TextModel> _model;

      /** The character position at which to insert, delete, or replace */
    private:
      size_type _pos;

      /** The number of characters to replace or delete */
    private:
      size_type _count;

      /** The string that is inserted */
    private:
      ::std::string _text;
    };
  }
}

#endif
