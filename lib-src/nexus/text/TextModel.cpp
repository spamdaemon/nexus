#include <nexus/text/TextModel.h>

namespace nexus {
  namespace text {

    TextModel::TextModel () throws() 
    {}
     
    TextModel::TextModel (const TextModel&) throws() 
    : ::timber::Counted()
    {}
     
    TextModel::~TextModel () throws() {}

    TextModel::size_type TextModel::textLength() const throws()
    { return text().length(); }

    void TextModel::clearText () throws(::std::exception)
    { replace(0,textLength(),::std::string()); }
      
    void TextModel::setText (const ::std::string& s) throws(::std::exception)
    { replace(0,textLength(),s); }

    void TextModel::insertText (size_type pos, const ::std::string& s) throws(::std::exception)
    { replace(pos,0,s); }

    void TextModel::deleteText (size_type pos, size_type cnt) throws(::std::exception)
    { 
      assert(pos+cnt<=textLength());
      replace(pos,cnt,::std::string()); 
    }

    void TextModel::replaceText (size_type pos, size_type cnt, const ::std::string& s) throws(::std::exception)
    {
      assert(pos+cnt<=textLength());
      replace(pos,cnt,s);
    }

  }
}

