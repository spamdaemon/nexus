#ifndef _NEXUS_TEXT_ABSTRACTTEXTMODEL_H
#define _NEXUS_TEXT_ABSTRACTTEXTMODEL_H

#ifndef _NEXUS_TEXT_TEXTMODEL_H
#include <nexus/text/TextModel.h>
#endif 

#ifndef _NEXUS_TEXT_TEXTMODELEVENT_H
#include <nexus/text/TextModelEvent.h>
#endif 

#ifndef _NEXUS_EVENTLISTENERS_H
#include <nexus/EventListeners.h>
#endif

namespace nexus {
  namespace text {
    /**
     * This abstract text model partially implements TextModel.
     */
    class AbstractTextModel : public TextModel {
      AbstractTextModel&operator=(const AbstractTextModel&);

      /** Default constructor */
    protected:
      AbstractTextModel () throws();

      /** 
       * Copy constructor.
       * @param c a text model
       */
    protected:
      AbstractTextModel (const AbstractTextModel& c) throws();

      /** Destroy this model */
    protected:
      ~AbstractTextModel () throws();

      /**
       * Add a text model event listener
       * @param l a text model event listener.
       * @pre REQUIRE_NON_ZERO(l)
       */
    public:
      void addTextModelEventListener (const ::timber::Reference<TextModelEventListener>& l) throws();

      /**
       * Remove a text model event listener
       * @param l a text model event listener.
       */
    public:
      void removeTextModelEventListener (const ::timber::Pointer<TextModelEventListener>& l) throws();

      /**
       * Test if this model is currently notifying TextModelEventListeners. If this
       * method returns true, then no modifications may be made to this model.
       * @return true if the listeners are current being notified.
       */
    public:
      bool isNotifyingListeners () const throws();

      /**
       * Fire a text model event. This method will also notify the
       * the dispatch a model changed event.
       * @param e a text model event
       * @pre REQUIRE_FALSE(isNotifyingListeners())
       */
    protected:
      void fireTextModelEvent (const TextModelEvent& e) throws();

      /** The event dispatcher */
    private:
      EventListeners<TextModelEventListener> _listeners;
    };
  }
}

#endif
