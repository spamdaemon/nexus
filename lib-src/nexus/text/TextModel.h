#ifndef _NEXUS_TEXT_TEXTMODEL_H
#define _NEXUS_TEXT_TEXTMODEL_H

#ifndef _NEXUS_H
#include <nexus/nexus.h>
#endif

#ifndef _NEXUS_TEXT_TEXTMODELEVENTLISTENER_H
#include <nexus/text/TextModelEventListener.h>
#endif 

namespace nexus {
  namespace text {
    /**
     * This abstract class can be subclassed to provide
     * different kinds of models for text. 
     */
    class TextModel : public ::timber::Counted {
      /** No copy operator allowed */
      TextModel&operator=(const TextModel&);

      /** The index */
    public:
      typedef ::std::string::size_type size_type;

      /** Default constructor */
    protected:
      TextModel () throws();

      /** 
       * Copy constructor.
       * @param c a text model
       */
    protected:
      TextModel (const TextModel& c) throws();

      /** Destroy this model */
    protected:
      ~TextModel () throws();

      /**
       * Add a text model event listener
       * @param l a text model event listener.
       * @pre REQUIRE_NON_ZERO(l)
       */
    public:
      virtual void addTextModelEventListener (const ::timber::Reference<TextModelEventListener>& l) throws() = 0;

      /**
       * Remove a text model event listener
       * @param l a text model event listener.
       */
    public:
      virtual void removeTextModelEventListener (const ::timber::Pointer<TextModelEventListener>& l) throws() = 0;

      /**
       * Get the text content. This method must not return 0.
       * @return the text.
       */
    public:
      virtual const ::std::string& text() const throws() = 0;
	
      /**
       * Get the length of the text content. By default
       * this method returns <code>text().length()</code>.
       * @return the length of the text
       */
    public:
      virtual size_type textLength() const throws();

      /**
       * Replace a text.<br>
       * This one method is used to insert, delete, and replace text.
       * If <code>count==0</code> then text is inserted at the position.
       * If <code>count>0 && s.length()==0</code> then text is deleted.
       * If <code>count>0 && s.length()>0</code> then text is replaced.
       * @param pos the position of the first character to be replaced
       * @param count the number of characters to replace
       * @param s the string that replaces the substring.
       * @pre REQUIRE_GREATER_OR_EQUAL(pos,0)
       * @pre REQUIRE_RANGE((pos+count),pos,textLength())
       * @throw Exception if the string cannot be replaced
       */
    public:
      void replaceText (size_type pos, size_type count, const ::std::string& s) throws(::std::exception);

      /**
       * Insert a string at the specified offset.
       * @param pos an offset
       * @param s a string  (can be 0)
       * @throws Exception if the string could not be inserted
       * @pre REQUIRE_RANGE(pos,0,textLength())
       */
    public:
      void insertText (size_type pos, const ::std::string& s) throws(::std::exception);

      /**
       * Delete a substring.
       * @param pos the position of the first character to be deleted
       * @param count the number of characters to be deleted
       * @throws Exception if the string could not be deleted
       * @pre REQUIRE_GREATER_OR_EQUAL(pos,0)
       * @pre REQUIRE_RANGE((pos+count),pos,textLength())
       */
    public:
      void deleteText (size_type pos, size_type count) throws(::std::exception);

      /**
       * Clear this text.
       * @throws Exception if the string could not be cleared
       */
    public:
      void clearText () throws(::std::exception);

      /**
       * Set the text to the specified string.
       * @param s a string (can be 0)
       * @throws Exception if the text could not be set to s
       */
    public:
      void setText (const ::std::string& s) throws(::std::exception);

      /**
       * Replace a text.<br>
       * This one method is used to insert, delete, and replace text.
       * If <code>count==0</code> then text is inserted at the position.
       * If <code>count>0 && s.length()==0</code> then text is deleted.
       * If <code>count>0 && s.length()>0</code> then text is replaced.
       * @param pos the position of the first character to be replaced
       * @param count the number of characters to replace
       * @param s the string that replaces the substring.
       * @pre REQUIRE_GREATER_OR_EQUAL(pos,0)
       * @pre REQUIRE_RANGE((pos+count),pos,textLength())
       * @pre REQUIRE_NON_ZERO(s)
       * @throw Exception if the string cannot be replaced
       */
    protected:
      virtual void replace (size_type pos, size_type count, const ::std::string& s) throws(::std::exception) = 0;
    };
  }
}

#endif
