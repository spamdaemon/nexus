#ifndef _NEXUS_COMPOSITE_H
#define _NEXUS_COMPOSITE_H

#ifndef _NEXUS_INSETS_H
#include <nexus/Insets.h>
#endif

#ifndef _NEXUS_BORDER_H
#include <nexus/Border.h>
#endif

#ifndef _NEXUS_COMPONENT_H
#include <nexus/Component.h>
#endif

#ifndef _NEXUS_LAYOUTENGINE_H
#include <nexus/LayoutEngine.h>
#endif

#ifndef _INDIGO_NODE_H
#include <indigo/Node.h>
#endif

#include <vector>

namespace nexus {
   namespace peer {
      class Container;
   }

   /**
    * This is the baseclass for all components
    * that are composed for multiple children. A composite
    * can have an optional layout engine that is used
    * to layout the composite. It is the responsibility
    * of a subclass to manage the adding and removing
    * components to the layout. Changes to the layout are
    * tracked and revalidate() is called whenever a change is
    * made. For simplification, the composite is capable of
    * laying out a single child according to its sizehints.
    *
    */
   class Composite : public Component
   {
         Composite(const Composite&);
         Composite&operator=(const Composite&);

         /** The implementation class is a container */
      public:
         typedef ::nexus::peer::Container Peer;

         /**
          * Default constructor. Clients using this constructor
          * should override the doLayout() and sizeHints() methods if they
          * have more than 1 child.<br>
          * Note that double-buffering is disable by default.
          * @throw ConfigurationException if the desktop has not been configured
          */
      protected:
         inline Composite() throws (::std::exception)
         {
         }

         /**
          * Create a composite with the specified layout strategy.
          * @param engine the layout engine
          * @pre REQUIRE_NON_ZERO(engine.get())
          * @throw ConfigurationException if the desktop has not been configured
          */
      protected:
         Composite(::std::unique_ptr< LayoutEngine> engine) throws (::std::exception);

         /**
          * Destroy this container.
          */
      public:
         ~Composite() throws();

         /**
          * Get the child at the specified index.
          * @param pos a position
          * @return a child
          */
      protected:
         inline Component* child(Index pos) const throws()
         {
            return _children[pos];
         }

         /**
          * Get the index of the specified child.
          * @param c a component
          * @return the index of the specified child or invalidIndex() if not found
          */
      protected:
         Index indexOfChild(const ::timber::Reference< Component>& c) const throws();

         /**
          * Insert a component into this container. Since this is invoked
          * by the derived class, it is not necessary to make this method
          * virtual. A subclass may not even know what to do with this
          * if it were called without its knowledge, for example, by a
          * derived class.
          *
          * @param c the component to add
          * @param pos the position at which to add c
          * @pre REQUIRE_ZERO(c->parent())
          * @pre REQUIRE_RANGE(pos,0,childCount());
          */
      protected:
         void insertChild(const ::timber::Reference< Component>& c, Index pos) throws();

         /**
          * Add a component this container. Same as
          * <code>insertChild(c,childCount())</code>.
          * @param c the component to add
          * @pre REQUIRE_ZERO(c->parent())
          */
      protected:
         inline void addChild(const ::timber::Reference< Component>& c) throws()
         {
            insertChild(c, childCount());
         }

         /**
          * Remove the component at the specified index.
          * @param pos a component index
          * @pre REQUIRE_RANGE(pos,0,childCount()-1);
          * @return the component
          */
      protected:
         virtual ::timber::Reference< Component> removeChild(Index pos) throws();

         /** Remove all children. */
      protected:
         void removeChildren() throws();

         /**
          * Get the number of components in this container.
          * @return the number of contained components
          */
      protected:
         inline Int childCount() const throws()
         {
            return static_cast< Int> (_children.size());
         }

         void initializePeer() throws();
         void createChildPeers() throws();
         PeerComponent* createPeerComponent(PeerComponent* p) throws();
         void fontChanged() throws();
         void backgroundColorChanged() throws();
         void foregroundColorChanged() throws();

      public:
         ::std::ostream& print(::std::ostream& out, bool deep, Int indent) const throws();

         /**
          * @name Geometry and border management
          * @{
          */

         /**
          * Get the bounds for the content.
          * This method takes the border and whitespace into account.
          * @return the bounds in which the content should be drawn.
          */
      public:
         Bounds contentBounds() const throws();

         /**
          * Get the size that is available for the actual children of this composite.
          * This method takes the border and whitespace into account.
          * @return the size actually available for the children or content.
          */
      public:
         inline Size contentSize() const throws()
         {
            return contentBounds().size();
         }

         /**
          * Set the whitespace margin insets for this composite.
          * @param ws the whitespace margins
          */
      public:
         virtual void setWhitespace(const Insets& ws) throws();

         /**
          * Get the whitespace margins.
          * @return the whitespace margins
          */
      public:
         inline const Insets& whitespace() const throws()
         {
            return _whitespace;
         }

         /**
          * Set the border for this component. If <code>b==0</code>,
          * then <code>Border::createNullBorder()</code> is called
          * to obtain a border.
          * @param b the new border or 0 to remove the current one
          */
      public:
         virtual void setBorder(::timber::Pointer< Border> b) throws();

         /**
          * Get the currently used border. This method always returns
          * a non-zero value.
          * @return the current border
          */
      public:
         inline ::timber::Pointer< Border> border() const throws()
         {
            return _border;
         }

         /**
          * Compute the layout for the children. If no layout was specified
          * but there is only 1 child, then that child is made as big as this
          * component (accounting for insets). If there are at least 2 children,
          * but there is no layout, then this method does nothing, otherwise it
          * uses the layout to place each child.
          * @throw LayoutException if the container could not be laid out
          */
      protected:
         void doLayout() throws(LayoutException);

         /**
          * Compute the size hints. This call is simply forwarded
          * to the layout if there is one. Override this method
          * if no layout is specified or there are at least 2 children.
          * @return the size hints computed by the layout or default hints if there is no layout.
          */
      protected:
         SizeHints getSizeHints() const throws();

         /**
          * Access the layout engine. The returned pointer is
          * owned by this component.
          * @return a pointer to the layout engine or 0 if none was specified.
          */
      protected:
         inline LayoutEngine* layoutEngine() throws()
         {
            return _layout.get();
         }

         /*@}*/

         /**
          * Enable interception of events of children. All events that would
          * normally be sent to children of this component are sent to this
          * component instead.
          * @param enable if true, then enable event interception
          */
      public:
         virtual void setInterceptEventsEnabled(bool enable) throws();

         /**
          * Test if events are intercepted and always sent to this container.
          * @return true if event interception is enabled
          */
      public:
         inline bool isInterceptEventsEnabled() const throws()
         {
            return _interceptEvents;
         }

         /**
          * Set the background graphic for this composite.
          * @param gfx the background graphic for this composite
          */
      protected:
         virtual void setBackgroundGraphic(const ::timber::Pointer< ::indigo::Node>& gfx) throws();

         /**
          * Get the background graphic if there is one.
          * @return the background graphic.
          */
      protected:
         inline ::timber::Pointer< ::indigo::Node> backgroundGraphic() const throws()
         {
            return _bgGraphic;
         }

         /**
          * Enable or disable double buffering. Double-buffering may affect
          * performance when resizing.
          * @param enable true if double buffering should be enabled
          */
      public:
         virtual void setDoubleBufferEnabled(bool enable) throws();

         /**
          * Test if double buffering is enabled. If this method
          * returns true, then this does not necessarily mean that
          * double buffering is enabled in the display hardware/software.
          * @return true if double buffering should be used by the display software
          */
      public:
         inline bool isDoubleBufferEnabled() const throws()
         {
            return _doubleBuffered;
         }

         /**
          * Refresh the border graphic.
          * @param updateInsets true if border insets should be updated
          * @return true if the border insets have changed
          */
      private:
         bool revalidateBorder(bool updateInsets) throws();

         /** The (optional) layout engine */
      private:
         ::std::unique_ptr< LayoutEngine> _layout;

         /** The children of this component */
      private:
         mutable ::std::vector< Component*> _children;

         /** True if events over children are intercepted by this composite */
      private:
         bool _interceptEvents;

         /** The background graphic (if there is one) */
      private:
         mutable ::timber::Pointer< ::indigo::Node> _bgGraphic;

         /**
          * @name Border and whitespace attributes
          * @{
          */

         /** The whitespace insets for this composite */
      private:
         Insets _whitespace;

         /** The current border or a null border */
      private:
         ::timber::Pointer< Border> _border;

         /** The border insets */
      private:
         Insets _borderInsets;

         /** The border graphic */
      private:
         ::timber::Pointer< ::indigo::Node> _borderGraphic;

         /** True if double buffering */
      private:
         bool _doubleBuffered;
         /*@}*/
   };
}

#endif
